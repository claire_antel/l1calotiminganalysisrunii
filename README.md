# README #

### What is this repository for? ###

Perform timing analysis on ATLAS Run II EODs to determine timing shifts for trigger towers.


### How do I get set up? ###

* tested up to athena AtlasProduction release 20.7.9.9 (cmt based): 
```
asetup 20.7.9.9,here
```

* CMakeLists.txt file present but gives issues.

### What can I configure in the job options? ###

* run number
* is data 80MHz read-out? _m_is80MHz_=true (for normal running conditions it is not, see list of 80 MHz physics runs at https://twiki.cern.ch/twiki/bin/view/Atlas/L1CaloCommissioningChanges)
* if is 80MHz read-out, keep fitting parameters fixed? _m_fixedpars_=true (if false, sigma_left and sigma_right of fitting functions are unfixed in fits; used to derive correction factors or if wish to be independent of parameter choice).
* the input xml file of fixed function parameters for each Trigger Tower (currently points to my public directory - make sure it is valid).
* the input xml file of fit width correction factors for each Trigger Tower (currently points to my public directory - make sure it is valid). 

### What happens when I run the job file? ###

The job will either run the TimeFitting or TimeFitting80MHz algorithm (depending on what m_80MHz is set to).

The following event-level cuts are applied:

* Background flag HaloMuonTwoSided veto
* LAr LArECTimeDiffHalo veto (nicely gets rid of noise bursts)
* L1Calo-triggered
* primary vertex with >= 5 tracks found.

The following trigger tower-level cuts are applied:

* ADC height > 90 a.d.c (~ 14.5 GeV).
* ADC height < 1020 a.d.c. (~250 GeV - saturation level).
* calo quality < 4000.


A fit is performed to each trigger tower with a significant energy deposit (~ >14.5GeV). Depending on the region, the fit is either a Gauss-Landau or Landau-Landau function combo.

Root outputs are

* _OverviewHistos_ root file: overview plots, as eta-phi maps or by region.
* _TowerTimingDistr_ root file: timing distributions for each trigger tower.
* _TowerInfo_ root file: timing correlation distributions (to calo energy, caloquality) and normalised pulses, for each trigger tower.

if m_80MHz true...

* _ParameterFitvalues_ root file: unfixed parameter distributions for each trigger tower.

### Post-Athena analysis: What do I do now? ###

Refer to the files in Root to derive the final timing shifts for each trigger tower.

* getTimingOffsets.cxx: 
    * uses TowerTimingDistr as input.
    * derives timing shift from mean of distribution.
    * outputs helpful overview plots.
    * outputs xml file of time offset for each tower.

* ErrorEncoding.cxx:
    * outputs root file with error overview.
    * outputs xml file with error status of each trigger tower.

* FinalTimingStep.cxx:
    * derives final timing offsets to be input into database.
    * outputs a coolinit file.