#include "TimingRun2/DeriveCorrectionFactors.h"
#include <stdio.h>
// #define SPIT(a) std::cout << #a << ": " << (a) << std::endl

std::pair<double, double> computeMandRMS(std::vector<double> vec) {                                                                                                                                                                         
          // compute rms                                                                                                                                                                                                                   
          double sum = std::accumulate(std::begin(vec), std::end(vec), 0.0);                                                                                                                                                                
          double m =  sum / vec.size();                                                                                                                                                                                                     
          double accum = 0.0;                                                                                                                                                                                                               
          std::for_each (std::begin(vec), std::end(vec), [&](const double d) {                                                                                                                                                              
              accum += (d - m) * (d - m);                                                                                                                                                                                                 
          });                                                                                                                                                                                                                               
          double stdev = sqrt(accum / (vec.size()-1));                                                                                                                                                                                      
          /*if (isinf(stdev)) {                                                                                                                                                                                                             
                std::pair<double, double> mRMS_pair = std::make_pair(-10, -10);                                                                                                                                                             
                return mRMS_pair;                                                                                                                                                                                                           
                                                                                                                                                                                                                                            
          } */                                                                                                                                                                                                                              
          std::pair<double, double> mRMS_pair = std::make_pair(m, stdev);                                                                                                                                                                   
          return mRMS_pair;                                                                                                                                                                                                                 
}       

void runSigValues(std::string xmlName, std::string rootName){
  
  //define maps

  // by id
  std::map<unsigned int,double> Part;
  std::map<unsigned int,double> Eta, Phi;
  std::map<unsigned int,double> SigL, SigR, SigLrms, SigRrms;
  std::map<unsigned int,double> NFits;
  std::map<unsigned int,double> Chi2NDF_sigL;
  std::map<unsigned int,double> Chi2NDF_sigR;  
  
  // by eta bin
  std::map<unsigned int,std::vector<double>> sigL_EM_vec;
  std::map<unsigned int,std::vector<double>> sigR_EM_vec;
  std::map<unsigned int,std::vector<double>> sigL_HAD_vec;
  std::map<unsigned int,std::vector<double>> sigR_HAD_vec;
 
  std::map<unsigned int,double> sigL_avg_EM;
  std::map<unsigned int,double> sigR_avg_EM;
  std::map<unsigned int,double> sigL_avgerr_EM;
  std::map<unsigned int,double> sigR_avgerr_EM;
  std::map<unsigned int,double> sigL_avg_HAD;
  std::map<unsigned int,double> sigR_avg_HAD;
  std::map<unsigned int,double> sigL_avgerr_HAD;
  std::map<unsigned int,double> sigR_avgerr_HAD;
  
  TH2TT* map = new TH2TT("template", "template");
  
  TTxml Inputxml;
  std::string xmlFullName = "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/"+xmlName;
  Inputxml.Read(xmlFullName.c_str());

  Part = Inputxml.Get("Part");
  Eta = Inputxml.Get("Eta");
  SigL = Inputxml.Get("SigL");
  SigR = Inputxml.Get("SigR");
  NFits = Inputxml.Get("NFits");
  
  std::string rootFullName = "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/"+rootName;
  TFile rootout = TFile(rootFullName.c_str(), "RECREATE");
  
  for (std::map<unsigned int,double>::iterator it = Eta.begin(); it != Eta.end(); it++){

    unsigned int id = it->first;
    double abseta = fabs(it->second);
    
    unsigned int etabin = map->GetXaxis()->FindBin(-abseta);
    
    unsigned int nfits = (unsigned int) NFits[id];
    if (nfits<80) continue;
    
    unsigned int part = (unsigned int) Part[id];
    double sigl = SigL[id];
    double sigr = SigR[id];
    
    if (part<=3) {
      sigL_EM_vec[etabin].push_back(sigl);
      sigR_EM_vec[etabin].push_back(sigr);
    }
    else {    
      sigL_HAD_vec[etabin].push_back(sigl);
      sigR_HAD_vec[etabin].push_back(sigr);
    }
    
  }
  
  for (std::map<unsigned int,std::vector<double>>::iterator it2 = sigL_EM_vec.begin(); it2 != sigL_EM_vec.end(); it2++){
      
    unsigned int etabin = it2->first;
        
    std::vector<double> sigl_em_vec = sigL_EM_vec[etabin];
    std::vector<double> sigr_em_vec = sigR_EM_vec[etabin];    
    std::vector<double> sigl_had_vec = sigL_HAD_vec[etabin];
    std::vector<double> sigr_had_vec = sigR_HAD_vec[etabin];
    
    std::cout<<"Filling sigL for EM eta bin "<<etabin<<std::endl;
    FillMandRMS(sigl_em_vec, sigL_avg_EM, sigL_avgerr_EM, etabin);
    std::cout<<"Filling sigR for EM eta bin "<<etabin<<std::endl;        
    FillMandRMS(sigr_em_vec, sigR_avg_EM, sigR_avgerr_EM, etabin);
    std::cout<<"Filling sigR for HAD eta bin "<<etabin<<std::endl;    
    FillMandRMS(sigl_had_vec, sigL_avg_HAD, sigL_avgerr_HAD, etabin);
    std::cout<<"Filling sigL for HAD eta bin "<<etabin<<std::endl;    
    FillMandRMS(sigr_had_vec, sigR_avg_HAD, sigR_avgerr_HAD, etabin);
  }
  
  std::cout<<"Creating histograms by eta bin ... "<<std::endl;

  std::cout<<"\noooOOOooo sigL values EM oooOOOooo"<<std::endl;
  std::cout<<"etabin \t\t mean \t\t error \n";
  TH1D* h_sigl_em = makeHist(sigL_avg_EM, sigL_avgerr_EM, "sigL_averages_EM");
  std::cout<<"\noooOOOooo sigR values EM oooOOOooo"<<std::endl;
  std::cout<<"etabin \t\t mean \t\t error \n";
  TH1D* h_sigr_em = makeHist(sigR_avg_EM, sigR_avgerr_EM, "sigR_averages_EM");
  std::cout<<"\noooOOOooo sigL values HAD oooOOOooo"<<std::endl;
  std::cout<<"etabin \t\t mean \t\t error \n";
  TH1D* h_sigl_had = makeHist(sigL_avg_HAD, sigL_avgerr_HAD, "sigL_averages_HAD");
  std::cout<<"\noooOOOooo sigR values HAD oooOOOooo"<<std::endl;
  std::cout<<"etabin \t\t mean \t\t error \n";
  TH1D* h_sigr_had = makeHist(sigR_avg_HAD, sigR_avgerr_HAD, "sigR_averages_HAD");
  
  rootout.Write();
}

void runSigCorrections(std::string xmlName, std::string rootName){
  
  //define maps

  // by id
  std::map<unsigned int,double> Part;
  std::map<unsigned int,double> Eta, Phi;
  std::map<unsigned int,double> SigL, SigR, SigLrms, SigRrms;
  std::map<unsigned int,double> NFits;
  std::map<unsigned int,double> Chi2NDF_sigL;
  std::map<unsigned int,double> Chi2NDF_sigR;  
  
  std::map<unsigned int,double> SigL_P4, SigR_P4;
  
  // by eta bin
  std::map<unsigned int,std::vector<double>> sigL_EM_vec;
  std::map<unsigned int,std::vector<double>> sigR_EM_vec;
  std::map<unsigned int,std::vector<double>> sigL_HAD_vec;
  std::map<unsigned int,std::vector<double>> sigR_HAD_vec;
 
  std::map<unsigned int,double> sigL_avg_EM;
  std::map<unsigned int,double> sigR_avg_EM;
  std::map<unsigned int,double> sigL_avgerr_EM;
  std::map<unsigned int,double> sigR_avgerr_EM;
  std::map<unsigned int,double> sigL_avg_HAD;
  std::map<unsigned int,double> sigR_avg_HAD;
  std::map<unsigned int,double> sigL_avgerr_HAD;
  std::map<unsigned int,double> sigR_avgerr_HAD;
  
  TH2TT* map = new TH2TT("template", "template");
  
  TTxml CFxml;
  CFxml.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");
  SigL_P4=CFxml.Get("sigmal");
  SigR_P4=CFxml.Get("sigmar");

  
  TTxml Inputxml;
  std::string xmlFullName = "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/"+xmlName;
  Inputxml.Read(xmlFullName.c_str());

  Part = Inputxml.Get("Part");
  Eta = Inputxml.Get("Eta");
  SigL = Inputxml.Get("SigL");
  SigR = Inputxml.Get("SigR");
  NFits = Inputxml.Get("NFits");
  
  std::string rootFullName = "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/"+rootName;
  TFile rootout = TFile(rootFullName.c_str(), "RECREATE");
  
  for (std::map<unsigned int,double>::iterator it = Eta.begin(); it != Eta.end(); it++){

    unsigned int id = it->first;
    unsigned int part = (unsigned int) Part[id];
    
    unsigned int etabin(0);
    
    //dealing with special asymmetry case in HAD FCal
    if (part == 4 ) etabin = 1;
    else if (part == 5 ) etabin = 2;
    else {    
      double abseta = fabs(it->second);   
      etabin = map->GetXaxis()->FindBin(-abseta);
    }
    
    unsigned int nfits = (unsigned int) NFits[id];
    if (nfits<80) continue;
    
    double sigl = SigL[id];
    double sigr = SigR[id];
    double sigl_p4 = SigL_P4[id];
    double sigr_p4 = SigR_P4[id];
    
    if (part<=3 && part >=0) {
      sigL_EM_vec[etabin].push_back(sigl/sigl_p4);
      sigR_EM_vec[etabin].push_back(sigr/sigr_p4);
    }
    else if ( part >=4 && part<=8 ) {    
      sigL_HAD_vec[etabin].push_back(sigl/sigl_p4);
      sigR_HAD_vec[etabin].push_back(sigr/sigr_p4);
      if (etabin==1) { sigL_HAD_vec[3].push_back(sigl/sigl_p4); sigR_HAD_vec[3].push_back(sigr/sigr_p4);} 
      if (etabin==2) { sigL_HAD_vec[4].push_back(sigl/sigl_p4); sigR_HAD_vec[4].push_back(sigr/sigr_p4);}    
    }
    else { std::cout<<"WARNING! Invalid part given! Breaking off.."<<std::endl; return;}
    
  }
  
  for (std::map<unsigned int,std::vector<double>>::iterator it2 = sigL_EM_vec.begin(); it2 != sigL_EM_vec.end(); it2++){
      
    unsigned int etabin = it2->first;
        
    std::vector<double> sigl_em_vec = sigL_EM_vec[etabin];
    std::vector<double> sigr_em_vec = sigR_EM_vec[etabin];    
    std::vector<double> sigl_had_vec = sigL_HAD_vec[etabin];
    std::vector<double> sigr_had_vec = sigR_HAD_vec[etabin];
    
    std::cout<<"*** for eta bin "<<etabin<<" ***"<<std::endl;
    std::cout<<"size sigL EM: "<<sigl_em_vec.size()<<std::endl;
    std::cout<<"size sigR EM: "<<sigr_em_vec.size()<<std::endl;
    std::cout<<"size sigL HAD: "<<sigl_had_vec.size()<<std::endl;
    std::cout<<"size sigR HAD: "<<sigr_had_vec.size()<<std::endl;
    
//     std::cout<<"Filling sigL for EM eta bin "<<etabin<<std::endl;
    FillMandRMS(sigl_em_vec, sigL_avg_EM, sigL_avgerr_EM, etabin);
//     std::cout<<"Filling sigR for EM eta bin "<<etabin<<std::endl;        
    FillMandRMS(sigr_em_vec, sigR_avg_EM, sigR_avgerr_EM, etabin);
//     std::cout<<"Filling sigR for HAD eta bin "<<etabin<<std::endl;    
    FillMandRMS(sigl_had_vec, sigL_avg_HAD, sigL_avgerr_HAD, etabin);
//     std::cout<<"Filling sigL for HAD eta bin "<<etabin<<std::endl;    
    FillMandRMS(sigr_had_vec, sigR_avg_HAD, sigR_avgerr_HAD, etabin);
  }
  
  std::cout<<"Creating histograms by eta bin ... "<<std::endl;

  std::cout<<"\noooOOOooo sigL values EM oooOOOooo"<<std::endl;
  std::cout<<"etabin \t\t mean \t\t error \n";
  TH1D* h_sigl_em = makeHist(sigL_avg_EM, sigL_avgerr_EM, "sigL_CFaverages_EM");
  std::cout<<"\noooOOOooo sigR values EM oooOOOooo"<<std::endl;
  std::cout<<"etabin \t\t mean \t\t error \n";
  TH1D* h_sigr_em = makeHist(sigR_avg_EM, sigR_avgerr_EM, "sigR_CFaverages_EM");
  std::cout<<"\noooOOOooo sigL values HAD oooOOOooo"<<std::endl;
  std::cout<<"etabin \t\t mean \t\t error \n";
  TH1D* h_sigl_had = makeHist(sigL_avg_HAD, sigL_avgerr_HAD, "sigL_CFaverages_HAD");
  std::cout<<"\noooOOOooo sigR values HAD oooOOOooo"<<std::endl;
  std::cout<<"etabin \t\t mean \t\t error \n";
  TH1D* h_sigr_had = makeHist(sigR_avg_HAD, sigR_avgerr_HAD, "sigR_CFaverages_HAD");
  
  rootout.Write();
  
  std::cout<<"Completed without errors."<<std::endl;
  return;
}

void writeCorrFactors(std::string rootInName, std::string xmlOutName = "sigmacorrectionfactors_new.xml") {
  
  // id maps
  std::map<unsigned int,double> cfSigmal, cfSigmar;
  std::map<unsigned int,double> Eta, Phi, Part;
 
  // bin maps
  std::map<unsigned int, double> siglCF_EM_binned;
  std::map<unsigned int, double> sigrCF_EM_binned;
  std::map<unsigned int, double> siglCF_HAD_binned;
  std::map<unsigned int, double> sigrCF_HAD_binned;
  
  std::string rootFullName = "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/"+rootInName;
  TFile rootIn = TFile(rootFullName.c_str(), "UPDATE"); 
    
  TH1D* h_sigLCF_EM;
  TH1D* h_sigRCF_EM;
  TH1D* h_sigLCF_HAD;
  TH1D* h_sigRCF_HAD;
  
  TH2TT* h_sigLCF_EMmap = new TH2TT("h_sigLCF_EMmap", "sigLeft CFs (EM);#eta;#phi");
  TH2TT* h_sigRCF_EMmap = new TH2TT("h_sigRCF_EMmap", "sigRight CFs (EM);#eta;#phi");
  TH2TT* h_sigLCF_HADmap = new TH2TT("h_sigLCF_HADmap", "sigLeft CFs (HAD);#eta;#phi");
  TH2TT* h_sigRCF_HADmap = new TH2TT("h_sigRCF_HADmap", "sigRight CFs (HAD);#eta;#phi");  
  
  rootIn.GetObject("sigL_CFaverages_EM", h_sigLCF_EM );
  rootIn.GetObject("sigR_CFaverages_EM", h_sigRCF_EM );
  rootIn.GetObject("sigL_CFaverages_HAD", h_sigLCF_HAD );
  rootIn.GetObject("sigR_CFaverages_HAD", h_sigRCF_HAD );
   
  TTxml TTprops;
                                       
  TTprops.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");      
                                       
  Eta = TTprops.Get("eta");            
  Phi = TTprops.Get("phi");            
  Part = TTprops.Get("part");          

  TH2TT* map = new TH2TT("template", "template");
    
  unsigned int no_etabins =  h_sigLCF_EM->GetXaxis()->GetNbins();
  
  std::cout<<"no. of bins found: "<<no_etabins<<std::endl;
  
  for ( unsigned int bin = 1; bin!=no_etabins+1; bin++) {
      
      unsigned int binC = bin;
      unsigned int binA_EM(0);
      unsigned int binA_HAD(0);
      

      //dealing with special asymmetric Had FCal case
      if (bin <= 4) {
	if ( bin == 1) binA_HAD = 65;
	if ( bin == 2) binA_HAD = 66;
	if ( bin == 3) binA_HAD = 63;
	if ( bin == 4) binA_HAD = 64;
      }
      else binA_HAD = 67-bin;
      
      binA_EM = 67-bin;
	
      siglCF_EM_binned[binC]=h_sigLCF_EM->GetBinContent(bin);
      siglCF_EM_binned[binA_EM]=h_sigLCF_EM->GetBinContent(bin);      
      sigrCF_EM_binned[binC]=h_sigRCF_EM->GetBinContent(bin);
      sigrCF_EM_binned[binA_EM]=h_sigRCF_EM->GetBinContent(bin);      
      siglCF_HAD_binned[binC]=h_sigLCF_HAD->GetBinContent(bin);
      siglCF_HAD_binned[binA_HAD]=h_sigLCF_HAD->GetBinContent(bin);      
      sigrCF_HAD_binned[binC]=h_sigRCF_HAD->GetBinContent(bin);
      sigrCF_HAD_binned[binA_HAD]=h_sigRCF_HAD->GetBinContent(bin);      
  }

  for ( std::map<unsigned int, double>::iterator it = Eta.begin(); it != Eta.end(); it++) {
   
      unsigned int id = it->first;
      double eta = it->second;
      double phi = Phi[id]; 
      unsigned int part = (unsigned int) Part[id];
      
      unsigned int etabin = map->GetXaxis()->FindBin(eta);
      unsigned int phibin = map->GetYaxis()->FindBin(phi);
           
      if (part<=3) {
	if (!siglCF_EM_binned.count(etabin) || siglCF_EM_binned[etabin]==0 ) std::cerr<<"WARNING Average for EM sigL eta bin "<<etabin<<" does not exist. Needs to be manually set."<<std::endl;
	else { cfSigmal[id] = siglCF_EM_binned[etabin]; h_sigLCF_EMmap->SetBinContent((int) etabin, (int) phibin, cfSigmal[id]);}
	if (!sigrCF_EM_binned.count(etabin) || sigrCF_EM_binned[etabin]==0 ) std::cerr<<"WARNING Average for EM sigR eta bin "<<etabin<<" does not exist. Needs to be manually set."<<std::endl;
	else { cfSigmar[id] = sigrCF_EM_binned[etabin]; h_sigRCF_EMmap->SetBinContent((int) etabin, (int) phibin, cfSigmar[id]);}	
      }
      else {
	if (!siglCF_HAD_binned.count(etabin) || siglCF_HAD_binned[etabin]==0 ) std::cerr<<"WARNING Average for HAD sigL eta bin "<<etabin<<" does not exist. Needs to be manually set."<<std::endl;
	else { cfSigmal[id] = siglCF_HAD_binned[etabin]; h_sigLCF_HADmap->SetBinContent((int) etabin, (int) phibin, cfSigmal[id]);}
	if (!sigrCF_HAD_binned.count(etabin) || sigrCF_HAD_binned[etabin]==0 ) std::cerr<<"WARNING Average for HAD sigR eta bin "<<etabin<<" does not exist. Needs to be manually set."<<std::endl;
	else { cfSigmar[id] = sigrCF_HAD_binned[etabin]; h_sigRCF_HADmap->SetBinContent((int) etabin, (int) phibin, cfSigmar[id]);}	
	
	if (etabin==5 || etabin == (67-5)) { 
	    cfSigmal[id] = 0.99; cfSigmar[id] = 0.95; std::cout<<"Setting sigL and sigR values manually for HAD eta bin "<<etabin<<"."<<std::endl;
	    h_sigLCF_HADmap->SetBinContent((int) etabin, (int) phibin, cfSigmal[id]);
	    h_sigRCF_HADmap->SetBinContent((int) etabin, (int) phibin, cfSigmar[id]);
	}
      }

  }  
  
  // writing to xml
  
  
  std::string xmlOutFullName = "/afs/cern.ch/work/c/cantel/private/output/"+xmlOutName;
  TTxml xmlOut;

  xmlOut.Add("eta", Eta);
  xmlOut.Add("phi", Phi);
  xmlOut.Add("part", Part);
  xmlOut.Add("cf_sigmal", cfSigmal);
  xmlOut.Add("cf_sigmar", cfSigmar);

  xmlOut.Write(xmlOutFullName.c_str());
  
  h_sigLCF_EMmap->SetOption("colz");
  h_sigRCF_EMmap->SetOption("colz");
  h_sigLCF_HADmap->SetOption("colz");
  h_sigRCF_HADmap->SetOption("colz");  
  h_sigLCF_EMmap->SetStats(0);
  h_sigRCF_EMmap->SetStats(0);
  h_sigLCF_HADmap->SetStats(0);
  h_sigRCF_HADmap->SetStats(0);
  
  h_sigLCF_EMmap->SetMinimum(0.7);
  h_sigRCF_EMmap->SetMinimum(0.7);
  h_sigLCF_HADmap->SetMinimum(0.7);
  h_sigRCF_HADmap->SetMinimum(0.7);
  h_sigLCF_EMmap->SetMaximum(1.6);
  h_sigRCF_EMmap->SetMaximum(1.6);
  h_sigLCF_HADmap->SetMaximum(1.6);
  h_sigRCF_HADmap->SetMaximum(1.6);
  
  rootIn.cd();
  h_sigLCF_EMmap->Write();
  h_sigRCF_EMmap->Write();
  h_sigLCF_HADmap->Write();
  h_sigRCF_HADmap->Write();
  rootIn.Close();
  
}

void FillMandRMS(std::vector<double> vec , std::map<unsigned int,double> &Mmap, std::map<unsigned int,double> &RMSmap, unsigned int bin ){
    std::pair<double, double> MandRMS(-1, -1);
    MandRMS = computeMandRMS(vec);
    
//     std::cout<<"bin 19 vec: ";
//     if (bin == 19) {std::for_each (std::begin(vec), std::end(vec), [&](const double d) {                                                                                                                                                              
//               std::cout<<d<<"\t";                                                                                                                                                                                                 
//           }); std::cout<<std::endl;}
          
	 
        
    if (std::isnormal(MandRMS.first)) Mmap[bin] = MandRMS.first;
    else {std::cout<<"Invalid mean value for eta bin "<<bin<<" in vector."<<std::endl; //SPIT(vec); 
    }
    if (std::isnormal(MandRMS.second)) RMSmap[bin] = MandRMS.second; 
    else {std::cout<<"Invalid rms value for eta bin "<<bin<<" in vector. "<<std::endl; //SPIT(vec); 
    }
    
}

TH1D* makeHist(std::map<unsigned int,double> MEANmap, std::map<unsigned int,double> RMSmap, std::string name) {
  
  std::string labels = name+"; etabin; ns";
  
  TH1D* hist = new TH1D(name.c_str(), labels.c_str(), 33, 0.5, 33.5);
  
  for (std::map<unsigned int,double>::iterator it = MEANmap.begin(); it != MEANmap.end(); it++ ){
      unsigned int etabin = it->first;
      double val = it->second;
      double valerr = RMSmap[etabin];
      hist->SetBinContent(etabin, val);
      hist->SetBinError(etabin, valerr);
      std::cout<<etabin<<"\t\t"<<val<<"\t\t"<<valerr<<"\n";
  }

  return hist;
  
}

/*void runSigValuesFromCombHists(std::string rootInName, std::string rootOutName){
 
  std::map<unsigned int,double> Part;
  std::map<unsigned int,double> Eta;
  
  TTxml Inputxml;
  std::string xmlFullName = "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/"+xmlName;
  Inputxml.Read(xmlFullName.c_str());

  Part = Inputxml.Get("Part");
  Eta = Inputxml.Get("Eta");

  
  
}*/





