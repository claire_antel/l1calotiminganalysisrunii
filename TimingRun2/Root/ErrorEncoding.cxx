// these functions are used for defining the ErrorCode of the final timing of a Trigger Tower
// 0: Timing from average of Fit Method values, > 10 hits exist for this TT
// 1: calculated timing from average over phi strip; only done if more than 62.5% of channels in phi strip have valid timing values.
// 2: bad channel, RMS>3.2 & timing deviation from neighbours.
// 3: manually set.
// 4: can't be set.

#include "TH2TT.h"
#include "TTxml.h"
#include "isBadChannel.h"

#include "TFile.h"
#include "TDirectory.h"
#include "TKey.h"
#include "TObject.h"

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <algorithm>

double getEtaMean(double eta, double phi, TH2TT * goodTimingHist){

	double etamean = 0.;
	int N = 0;

	int nbinsy = goodTimingHist->GetNbinsY();

	int binx = 0;
	int biny = 0;
	int binz = 0;
	int globalbin = goodTimingHist->FindBin(eta,phi);
	goodTimingHist->GetBinXYZ(globalbin,binx,biny,binz);
	
	for (int j = 1; j <= nbinsy; j++ ){
		
		double content = goodTimingHist->GetBinContent(binx,j);
		
		if ( !(content < -100.) ){
			etamean += content;
			N++;
		}
	}
	if ( N >= 40 ){
		etamean = etamean/N;
		return etamean;
	}
	else return -2000;
		
}

void encode_error_preManualSet(const char* outputfile = "/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/timing_errorcoded_preManualSet.xml"){

	std::map<unsigned int,double> GoodTiming;
	std::map<unsigned int,double> GoodTimingRMS;
	std::map<unsigned int,double> BadTiming;
	std::map<unsigned int,double> BadTimingRMS;
	std::map<unsigned int,double> CalculatedTiming;
	std::map<unsigned int,double> TimingSum;
	std::map<unsigned int,double> ErrorCode;

	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;
	std::map<unsigned int,double> TDiff, Trms;
	std::map<unsigned int,double> NFits;


	TTxml m_values;
	m_values.Read("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/TimeValues80MHz2016_run80MHz2016.xml");

	Eta = m_values.Get("Eta");
	Phi = m_values.Get("Phi");
	Part = m_values.Get("Part");
	TDiff = m_values.Get("TDiff");
	Trms = m_values.Get("Trms");
	NFits = m_values.Get("NFits");

	TKey* key;

	TFile* timemaps_hists = new TFile("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/tdiff_maps_run80MHz2016.root");
	timemaps_hists->cd();
	key = gDirectory->GetKey("hist_tdiffsEM");
	TObject *o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT* hist_emtdiffs = (TH2TT*)o;
	key = gDirectory->GetKey("hist_tdiffsHAD");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT* hist_hadtdiffs = (TH2TT*)o;
	TH2TT* hist_goodemtdiffs=(TH2TT*)hist_emtdiffs->Clone();
	TH2TT* hist_goodhadtdiffs=(TH2TT*)hist_hadtdiffs->Clone();



	std::cout<<"TTs that do not have enough hits:"<<std::endl;

	std::map<unsigned int,double>::iterator it_vals;

	for ( it_vals = NFits.begin(); it_vals != NFits.end(); it_vals++ ){	
			unsigned int id = it_vals->first;
			double eta = Eta[id];
			double phi = Phi[id];
			int etabin = hist_emtdiffs->GetXaxis()->FindBin(eta);
			int phibin = hist_emtdiffs->GetYaxis()->FindBin(phi);
			double time = TDiff[id];
			double rms = Trms[id];
			int hits = (int) it_vals->second;
			int part = (int) Part[id];
			if (part<=3){
				//int hitsEM = hist_emgoodfits->GetBinContent(etabin, phibin);
				if (hits>10) {
					int isbad;
					isbad = isBadChannel(eta, phi, "em", rms, hist_emtdiffs);  
					if (isbad==0){ // TT has enough hits and good RMS
						ErrorCode[id]=0;
						GoodTiming[id] = time;
						GoodTimingRMS[id] = rms;
						TimingSum[id] = time;
					}
					else if (isbad==1) { //TT has enough hits BUT RMS bad
						ErrorCode[id]=2;
						BadTiming[id] = time;
						BadTimingRMS[id] = rms;
						TimingSum[id] = time;
						hist_goodemtdiffs->SetBinContent(etabin, phibin, -1000);
					}
					else if (isbad==2) { //TT has enough hits, RMS good BUT timing deviates from neighbours too much
						ErrorCode[id]=3;
						BadTiming[id] = time;
						BadTimingRMS[id] = rms;
						TimingSum[id] = time;
						hist_goodemtdiffs->SetBinContent(etabin, phibin, -1000);
					}
					else {
						ErrorCode[id]=4; //TT has enough hits and good RMS BUT timing deviation could not be determined.
						TimingSum[id] = time;
						hist_goodemtdiffs->SetBinContent(etabin, phibin, -1000);
					}					
				}
				else {
					ErrorCode[id]=6; // TT does not have enough hits.
					hist_goodemtdiffs->SetBinContent(etabin, phibin, -1000);
					std::cout<<"(Part"<<part<<", eta"<<eta<<", phi"<<phi<<") 0x"<<std::hex<<id<<std::endl;
				}
			}
			if (part>3 && part<=8){
				//int hitsHAD = hist_hadgoodfits->GetBinContent(etabin, phibin);
				if (hits>10) {
					int isbad;
					isbad = isBadChannel(eta, phi, "had", rms, hist_hadtdiffs);  
					if (isbad==0){
						ErrorCode[id]=0;
						GoodTiming[id] = time;
						GoodTimingRMS[id] = rms;
						TimingSum[id] = time;
					}
					else if (isbad==1) { //TT has enough hits BUT RMS bad
						ErrorCode[id]=2;
						BadTiming[id] = time;
						BadTimingRMS[id] = rms;
						TimingSum[id] = time;
						hist_goodhadtdiffs->SetBinContent(etabin, phibin, -1000);
					}
					else if (isbad==2) { //TT has enough hits, RMS good BUT timing deviates from neighbours too much
						ErrorCode[id]=3;
						BadTiming[id] = time;
						BadTimingRMS[id] = rms;
						TimingSum[id] = time;
						hist_goodhadtdiffs->SetBinContent(etabin, phibin, -1000);
					}
					else {
						ErrorCode[id]=4; //TT has enough hits and good RMS BUT timing deviation could not be determined.
						TimingSum[id] = time;
						hist_goodhadtdiffs->SetBinContent(etabin, phibin, -1000);
					}					
				}
				else {
					ErrorCode[id]=6; // TT does not have enough hits.
					hist_goodhadtdiffs->SetBinContent(etabin, phibin, -1000);
					std::cout<<"(Part"<<part<<", eta"<<eta<<", phi"<<phi<<") 0x"<<std::hex<<id<<std::endl;
				}
			}
	}


	// look at low stats TTs

	std::cout<<"TTs that need to be inspected:"<<std::endl;

	std::map<unsigned int,double>::iterator it;
	for ( it = ErrorCode.begin(); it != ErrorCode.end(); it++ ){	
		unsigned int id = it->first;
		int errorcode = (int) it->second;
		if (!(errorcode==6)) continue;
		double eta = Eta[id];
		double phi = Phi[id];
		int part = (int) Part[id];
		if (part<=3){
			double tcalculated = getEtaMean(eta, phi, hist_goodemtdiffs);
			if (!(tcalculated==-2000)) {
				CalculatedTiming[id] = tcalculated;
				TimingSum[id] = tcalculated;
				ErrorCode[id]=1;
				std::cout<<"Calculation done for Part"<<part<<", eta"<<eta<<", phi"<<phi<<", 0x"<<std::hex<<id<<": "<<std::dec<<tcalculated<<std::endl;
			}
			else std::cout<<"(Part"<<part<<", eta"<<eta<<", phi"<<phi<<") 0x"<<std::hex<<id<<std::endl;
		}
		else {
			double tcalculated = getEtaMean(eta, phi, hist_goodhadtdiffs);
			if (!(tcalculated==-2000)) {
				CalculatedTiming[id] = tcalculated;
				TimingSum[id] = tcalculated;
				ErrorCode[id]=1;
				std::cout<<"Calculation done for Part"<<part<<", eta"<<eta<<", phi"<<phi<<", 0x"<<std::hex<<id<<": "<<std::dec<<tcalculated<<std::endl;
			}
			else std::cout<<"(Part"<<part<<", eta"<<eta<<", phi"<<phi<<") 0x"<<std::hex<<id<<std::endl;
		}
	}

	TTxml preManual ;

	preManual.Add("GoodTiming", GoodTiming);
	preManual.Add("GoodTimingRMS", GoodTimingRMS);
	preManual.Add("BadTiming", BadTiming);
	preManual.Add("BadTimingRMS", BadTimingRMS);
	preManual.Add("CalculatedTiming", CalculatedTiming);
	preManual.Add("TimingSum", TimingSum);
	preManual.Add("ErrorCode", ErrorCode);

	ifstream ifile(outputfile);

	if (ifile) {
		if( remove( outputfile ) != 0 )
		perror( "Error deleting file" );
  		else puts( "Old output xml file successfully deleted" );
	}
	else puts("Creating new xml file.");

	preManual.Write(outputfile);

}

void encode_error(const char* outputfile = "/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/timing_errorcoded_postManualSet.xml"){

	std::map<unsigned int,double> GoodTiming;
	std::map<unsigned int,double> GoodTimingRMS;
	std::map<unsigned int,double> BadTiming;
	std::map<unsigned int,double> BadTimingRMS;
	std::map<unsigned int,double> CalculatedTiming;
	std::map<unsigned int,double> TimingSum;
	std::map<unsigned int,double> ErrorCode;
	std::map<unsigned int,double> ManualTiming;
	
	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta;

	TTxml m_values;
	m_values.Read("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/TimeValues80MHz2016_run80MHz2016.xml");	
	Eta = m_values.Get("Eta");
	Part = m_values.Get("Part");

	TTxml preManual ;
	preManual.Read("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/timing_errorcoded_preManualSet.xml");

	GoodTiming=preManual.Get("GoodTiming");
	GoodTimingRMS=preManual.Get("GoodTimingRMS");
	BadTiming=preManual.Get("BadTiming");
	BadTimingRMS=preManual.Get("BadTimingRMS");
	CalculatedTiming=preManual.Get("CalculatedTiming");
	TimingSum=preManual.Get("TimingSum");
	ErrorCode=preManual.Get("ErrorCode");

	const char* arrnoCorr[]={"04110002", "041e0e01", "04180601", "05180800"};//"05100800", "05100700", "05100100", "05100000", "04100901", "04100801", "04100701", "041b0d00", "061e0703", "07140c02", "04110002", "01100303", "01180403", "051e0e01", "051e0f02"};
	std::vector<const char*> noCorr_hex (arrnoCorr, arrnoCorr + sizeof(arrnoCorr) / sizeof(arrnoCorr[0]) );
	
	double arrTimeCorr[]={1., 0., 8., 5.};
	std::vector<double> TimeCorr (arrTimeCorr, arrTimeCorr + sizeof(arrTimeCorr) / sizeof(arrTimeCorr[0]) );
	
	std::vector<unsigned int> noCorr;
	for (unsigned int i=0; i<noCorr_hex.size(); i++) {
		noCorr.push_back(strtoul(noCorr_hex[i], NULL, 16));
	}

	std::map<unsigned int,double>::iterator it;
	for ( it = ErrorCode.begin(); it != ErrorCode.end(); it++ ){
		unsigned int id = it->first;
		int errorcode = (int) it->second;
		unsigned int part = (unsigned int) Part[id];
		double eta = Eta[id];
// 		if (!(errorcode==2 || errorcode==3 )) continue;
		if (std::find(noCorr.begin(), noCorr.end(), id)!=noCorr.end()) {
			int pos = std::find(noCorr.begin(), noCorr.end(), id) - noCorr.begin();
			double set_time = TimeCorr[pos] ;
			std::cout<<"Manually inserting timing value for TT0x"<<std::hex<<id<<" to "<<set_time<<" and reseting ErrorCode to 5"<<std::endl;
			ManualTiming[id]=set_time;
			TimingSum[id]=set_time;
			ErrorCode[id]=5;
		}
		if (part == 6 && fabs(eta)==3.15) {
		    double set_time = 1.;
		    ManualTiming[id]=set_time;
		    TimingSum[id]=set_time;
		    ErrorCode[id]=5;
		    std::cout<<"Manually inserting timing value for tower in HAD eta strip 3.15 to "<<set_time<<" and reseting ErrorCode to 5"<<std::endl;
		}
	}	

	TTxml postManual;

	postManual.Add("GoodTiming", GoodTiming);
	postManual.Add("GoodTimingRMS", GoodTimingRMS);
	postManual.Add("BadTiming", BadTiming);
	postManual.Add("BadTimingRMS", BadTimingRMS);
	postManual.Add("CalculatedTiming", CalculatedTiming);
	postManual.Add("TimingSum", TimingSum);
	postManual.Add("ErrorCode", ErrorCode);
	postManual.Add("ManualTiming", ManualTiming);


	ifstream ifile(outputfile);

	if (ifile) {
		if( remove( outputfile ) != 0 )
		perror( "Error deleting file" );
  		else puts( "Old output xml file successfully deleted" );
	}
	else puts("Creating new xml file.");

	postManual.Write(outputfile);

}






