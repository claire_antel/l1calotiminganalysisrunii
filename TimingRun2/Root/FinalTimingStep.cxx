//produce final timing files of timing update based on July 2015 data

#include "TTxml.h"
#include "IntTTxml.h"
#include <map>
#include "TH2TT.h"

#include "TFile.h"
#include <TH1.h>
#include <TF1.h>
#include "TMath.h"
#include <iostream>
#include "TCanvas.h"

#include <stdio.h>
#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>

double truncate_to_P4step(double offset){

	double ns_per_step = 25./24.;
	double steps = trunc(offset/ns_per_step); 
	double ns = steps*ns_per_step;

	//std::cout<<"time before truncating... "<<offset<<std::endl;
	//std::cout<<"final offset... "<<ns<<std::endl;
	return ns;
} 

int FinalTiming(){

	double step_to_ns = 25./24.;

	std::map<unsigned int,double> Timing;
	std::map<unsigned int,double> TimingRMS;
	std::map<unsigned int,double> BadTiming;
	std::map<unsigned int,double> BadTimingRMS;
	std::map<unsigned int,double> ManualTiming;
	std::map<unsigned int,double> TimingSum;
	std::map<unsigned int,double> ErrorCode;
	std::map<unsigned int,double> Timing_truncated;
	std::map<unsigned int,double> CalculatedTiming;

	std::map<unsigned int,int> FullDelayData_old;
	std::map<unsigned int,double> FullDelayDataOld;
	std::map<unsigned int,double> FullDelayDataNew_truncated;
	std::map<unsigned int,double> FullDelayDataDiff_truncated;

	std::map<unsigned int,double> Eta,Phi,Part;

	TTxml TTproperties;
	if (!(TTproperties.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml"))){
		puts("ERROR: TT properties xml file not found.");
		return 0;
	};
	Eta = TTproperties.Get("eta");
	Phi = TTproperties.Get("phi");
	Part = TTproperties.Get("part");

	
	//opening input files	
	TTxml xml_in;
	if (!(xml_in.Read("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/timing_errorcoded_postManualSet.xml"))){
		puts("ERROR: errorcoded xml file not found.");
		return 0;
	};
	

	Timing = xml_in.Get("GoodTiming");
	TimingRMS = xml_in.Get("GoodTimingRMS");
	BadTiming = xml_in.Get("BadTiming");
	BadTimingRMS = xml_in.Get("BadTimingRMS");
	ManualTiming = xml_in.Get("ManualTiming");
	ErrorCode = xml_in.Get("ErrorCode");
	CalculatedTiming = xml_in.Get("CalculatedTiming");
	TimingSum = xml_in.Get("TimingSum");

	//file with timing settings active during data taking of analyzed data
	IntTTxml xml_old;

	if (!(xml_old.Read("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/PprChanCalib_Physics.coolinit.map.xml"))){
		puts("ERROR: old full delay xml file not found.");
		return 0;
	};
	FullDelayData_old = xml_old.Get("FullDelayData");

	std::cout<<"map size is "<<FullDelayData_old.size()<<std::endl;
	
	std::cout << "-----------------------------------------------------------------" << std::endl;
	std::cout << "Determining the final timing corrections and full delay values" << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;

	// list of test TTs
	static const int arr[]={34603008, 34603009, 34603010, 34603011, 34603264, 34603265, 34603266, 34603267, 34603520, 34603521, 34603522, 34603523, 34603776, 34603777, 34603778, 34603779, 34604032, 34604033, 34604034, 34604035, 34604288, 34604289, 34604290, 34604291, 34604544, 34604545, 34604546, 34604547, 34604800, 34604801, 34604802, 34604803, 34605056, 34605057, 34605058, 34605059, 34605312, 34605313, 34605314, 34605315, 34605568, 34605569, 34605570, 34605571, 34605824, 34605825, 34605826, 34605827, 34606080, 34606081, 34606082, 34606083, 34606336, 34606337, 34606338, 34606339, 34606592, 34606593, 34606594, 34606595, 34606848, 34606849, 34606850, 34606851, 34799619, 34799873, 34799874, 34799875, 34800129, 34800130, 34800131, 34800387, 34800643, 34800897, 34800898, 34800899, 34801153, 34801154, 34801155, 34801411, 34801667, 34801921, 34801922, 34801923, 34802177, 34802178, 34802179, 34802435, 34802691, 34802945, 34802946, 34802947, 34803201, 34803202, 34803203, 34803459, 34865153, 34865155, 34865409, 34865411, 34865665, 34865667, 34865921, 34865923, 34866177, 34866179, 34866433, 34866435, 34866689, 34866691, 34866945, 34866947, 34867201, 34867203, 34867457, 34867459, 34867713, 34867715, 34867969, 34867971, 34868225, 34868227, 34868481, 34868483, 34868737, 34868739, 34868993, 34868995, 35061763, 35062017, 35062018, 35062019, 35062273, 35062274, 35062275, 35062531, 35062787, 35063041, 35063042, 35063043, 35063297, 35063298, 35063299, 35063555, 35063811, 35064065, 35064066, 35064067, 35064321, 35064322, 35064323, 35064579, 35064835, 35065089, 35065090, 35065091, 35065345, 35065346, 35065347, 35065603, 35127296, 35127297, 35127298, 35127299, 35127552, 35127553, 35127554, 35127555, 35127808, 35127809, 35127810, 35127811, 35128064, 35128065, 35128066, 35128067, 35128320, 35128321, 35128322, 35128323, 35128576, 35128577, 35128578, 35128579, 35128832, 35128833, 35128834, 35128835, 35129088, 35129089, 35129090, 35129091, 35129344, 35129345, 35129346, 35129347, 35129600, 35129601, 35129602, 35129603, 35129856, 35129857, 35129858, 35129859, 35130112, 35130113, 35130114, 35130115, 35130368, 35130369, 35130370, 35130371, 35130624, 35130625, 35130626, 35130627, 35130880, 35130881, 35130882, 35130883, 35131136, 35131137, 35131138, 35131139, 35323907, 35324161, 35324162, 35324163, 35324417, 35324418, 35324419, 35324675, 35324931, 35325185, 35325186, 35325187, 35325441, 35325442, 35325443, 35325699, 35325955, 35326209, 35326210, 35326211, 35326465, 35326466, 35326467, 35326723, 35326979, 35327233, 35327234, 35327235, 35327489, 35327490, 35327491, 35327747, 35389441, 35389443, 35389697, 35389699, 35389953, 35389955, 35390209, 35390211, 35390465, 35390467, 35390721, 35390723, 35390977, 35390979, 35391233, 35391235, 35391489, 35391491, 35391745, 35391747, 35392001, 35392003, 35392257, 35392259, 35392513, 35392515, 35392769, 35392771, 35393025, 35393027, 35393281, 35393283, 35586051, 35586305, 35586306, 35586307, 35586561, 35586562, 35586563, 35586819, 35587075, 35587329, 35587330, 35587331, 35587585, 35587586, 35587587, 35587843, 35588099, 35588353, 35588354, 35588355, 35588609, 35588610, 35588611, 35588867, 35589123, 35589377, 35589378, 35589379, 35589633, 35589634, 35589635, 35589891, 51380224, 51380225, 51380226, 51380227, 51380480, 51380481, 51380482, 51380483, 51380736, 51380737, 51380738, 51380739, 51380992, 51380993, 51380994, 51380995, 51381248, 51381249, 51381250, 51381251, 51381504, 51381505, 51381506, 51381507, 51381760, 51381761, 51381762, 51381763, 51382016, 51382017, 51382018, 51382019, 51382272, 51382273, 51382274, 51382275, 51382528, 51382529, 51382530, 51382531, 51382784, 51382785, 51382786, 51382787, 51383040, 51383041, 51383042, 51383043, 51383296, 51383297, 51383298, 51383299, 51383552, 51383553, 51383554, 51383555, 51383808, 51383809, 51383810, 51383811, 51384064, 51384065, 51384066, 51384067, 51445761, 51445762, 51445763, 51446017, 51446273, 51446529, 51446530, 51446531, 51446785, 51446786, 51446787, 51447041, 51447297, 51447553, 51447554, 51447555, 51447809, 51447810, 51447811, 51448065, 51448321, 51448577, 51448578, 51448579, 51448833, 51448834, 51448835, 51449089, 51449345, 51449601, 51449602, 51449603, 51642369, 51642371, 51642625, 51642627, 51642881, 51642883, 51643137, 51643139, 51643393, 51643395, 51643649, 51643651, 51643905, 51643907, 51644161, 51644163, 51644417, 51644419, 51644673, 51644675, 51644929, 51644931, 51645185, 51645187, 51645441, 51645443, 51645697, 51645699, 51645953, 51645955, 51646209, 51646211, 51707905, 51707906, 51707907, 51708161, 51708417, 51708673, 51708674, 51708675, 51708929, 51708930, 51708931, 51709185, 51709441, 51709697, 51709698, 51709699, 51709953, 51709954, 51709955, 51710209, 51710465, 51710721, 51710722, 51710723, 51710977, 51710978, 51710979, 51711233, 51711489, 51711745, 51711746, 51711747, 51904512, 51904513, 51904514, 51904515, 51904768, 51904769, 51904770, 51904771, 51905024, 51905025, 51905026, 51905027, 51905280, 51905281, 51905282, 51905283, 51905536, 51905537, 51905538, 51905539, 51905792, 51905793, 51905794, 51905795, 51906048, 51906049, 51906050, 51906051, 51906304, 51906305, 51906306, 51906307, 51906560, 51906561, 51906562, 51906563, 51906816, 51906817, 51906818, 51906819, 51907072, 51907073, 51907074, 51907075, 51907328, 51907329, 51907330, 51907331, 51907584, 51907585, 51907586, 51907587, 51907840, 51907841, 51907842, 51907843, 51908096, 51908097, 51908098, 51908099, 51908352, 51908353, 51908354, 51908355, 51970049, 51970050, 51970051, 51970305, 51970561, 51970817, 51970818, 51970819, 51971073, 51971074, 51971075, 51971329, 51971585, 51971841, 51971842, 51971843, 51972097, 51972098, 51972099, 51972353, 51972609, 51972865, 51972866, 51972867, 51973121, 51973122, 51973123, 51973377, 51973633, 51973889, 51973890, 51973891, 52166657, 52166659, 52166913, 52166915, 52167169, 52167171, 52167425, 52167427, 52167681, 52167683, 52167937, 52167939, 52168193, 52168195, 52168449, 52168451, 52168705, 52168707, 52168961, 52168963, 52169217, 52169219, 52169473, 52169475, 52169729, 52169731, 52169985, 52169987, 52170241, 52170243, 52170497, 52170499, 52232193, 52232194, 52232195, 52232449, 52232705, 52232961, 52232962, 52232963, 52233217, 52233218, 52233219, 52233473, 52233729, 52233985, 52233986, 52233987, 52234241, 52234242, 52234243, 52234497, 52234753, 52235009, 52235010, 52235011, 52235265, 52235266, 52235267, 52235521, 52235777, 52236033, 52236034, 52236035, 68354051, 68354305, 68354306, 68354307, 68354561, 68354562, 68354563, 68354819, 68355075, 68355329, 68355330, 68355331, 68355585, 68355586, 68355587, 68355843, 68356099, 68356353, 68356354, 68356355, 68356609, 68356610, 68356611, 68356867, 68357123, 68357377, 68357378, 68357379, 68357633, 68357634, 68357635, 68357891, 68419585, 68419587, 68419841, 68419843, 68420097, 68420099, 68420353, 68420355, 68420609, 68420611, 68420865, 68420867, 68421121, 68421123, 68421377, 68421379, 68421633, 68421635, 68421889, 68421891, 68422145, 68422147, 68422401, 68422403, 68422657, 68422659, 68422913, 68422915, 68423169, 68423171, 68423425, 68423427, 68616195, 68616449, 68616450, 68616451, 68616705, 68616706, 68616707, 68616963, 68617219, 68617473, 68617474, 68617475, 68617729, 68617730, 68617731, 68617987, 68618243, 68618497, 68618498, 68618499, 68618753, 68618754, 68618755, 68619011, 68619267, 68619521, 68619522, 68619523, 68619777, 68619778, 68619779, 68620035, 68878339, 68878593, 68878594, 68878595, 68878849, 68878850, 68878851, 68879107, 68879363, 68879617, 68879618, 68879619, 68879873, 68879874, 68879875, 68880131, 68880387, 68880641, 68880642, 68880643, 68880897, 68880898, 68880899, 68881155, 68881411, 68881665, 68881666, 68881667, 68881921, 68881922, 68881923, 68882179, 68943873, 68943875, 68944129, 68944131, 68944385, 68944387, 68944641, 68944643, 68944897, 68944899, 68945153, 68945155, 68945409, 68945411, 68945665, 68945667, 68945921, 68945923, 68946177, 68946179, 68946433, 68946435, 68946689, 68946691, 68946945, 68946947, 68947201, 68947203, 68947457, 68947459, 68947713, 68947715, 69140483, 69140737, 69140738, 69140739, 69140993, 69140994, 69140995, 69141251, 69141507, 69141761, 69141762, 69141763, 69142017, 69142018, 69142019, 69142275, 69142531, 69142785, 69142786, 69142787, 69143041, 69143042, 69143043, 69143299, 69143555, 69143809, 69143810, 69143811, 69144065, 69144066, 69144067, 69144323, 85000193, 85000194, 85000195, 85000449, 85000705, 85000961, 85000962, 85000963, 85001217, 85001218, 85001219, 85001473, 85001729, 85001985, 85001986, 85001987, 85002241, 85002242, 85002243, 85002497, 85002753, 85003009, 85003010, 85003011, 85003265, 85003266, 85003267, 85003521, 85003777, 85004033, 85004034, 85004035, 85196801, 85196803, 85197057, 85197059, 85197313, 85197315, 85197569, 85197571, 85197825, 85197827, 85198081, 85198083, 85198337, 85198339, 85198593, 85198595, 85198849, 85198851, 85199105, 85199107, 85199361, 85199363, 85199617, 85199619, 85199873, 85199875, 85200129, 85200131, 85200385, 85200387, 85200641, 85200643, 85262337, 85262338, 85262339, 85262593, 85262849, 85263105, 85263106, 85263107, 85263361, 85263362, 85263363, 85263617, 85263873, 85264129, 85264130, 85264131, 85264385, 85264386, 85264387, 85264641, 85264897, 85265153, 85265154, 85265155, 85265409, 85265410, 85265411, 85265665, 85265921, 85266177, 85266178, 85266179, 85524481, 85524482, 85524483, 85524737, 85524993, 85525249, 85525250, 85525251, 85525505, 85525506, 85525507, 85525761, 85526017, 85526273, 85526274, 85526275, 85526529, 85526530, 85526531, 85526785, 85527041, 85527297, 85527298, 85527299, 85527553, 85527554, 85527555, 85527809, 85528065, 85528321, 85528322, 85528323, 85721089, 85721091, 85721345, 85721347, 85721601, 85721603, 85721857, 85721859, 85722113, 85722115, 85722369, 85722371, 85722625, 85722627, 85722881, 85722883, 85723137, 85723139, 85723393, 85723395, 85723649, 85723651, 85723905, 85723907, 85724161, 85724163, 85724417, 85724419, 85724673, 85724675, 85724929, 85724931, 85786625, 85786626, 85786627, 85786881, 85787137, 85787393, 85787394, 85787395, 85787649, 85787650, 85787651, 85787905, 85788161, 85788417, 85788418, 85788419, 85788673, 85788674, 85788675, 85788929, 85789185, 85789441, 85789442, 85789443, 85789697, 85789698, 85789699, 85789953, 85790209, 85790465, 85790466, 85790467};

	std::vector<unsigned int> spare (arr, arr + sizeof(arr) / sizeof(arr[0]) );

	//combining timing correction values and determining new full delay values
	//setting error code and counting the occurrences of the different error codes
	int nTTs(0), nTTerr1(0), nTTerr0(0), nTTerr2(0), nTTerr3(0), nTTerr4(0), nTTerr5(0), nTTerr6(0);
	std::map<unsigned int,int>::iterator it_timing;
	//std::cout<<"ids with ec 2 in HAD: "<<std::endl;
	for ( it_timing = FullDelayData_old.begin(); it_timing != FullDelayData_old.end(); it_timing++ ){
		unsigned int id = it_timing->first;
		if (std::find(spare.begin(), spare.end(), id)!=spare.end()) {
			Eta[id] = -10;
			Phi[id] = -10;
			ErrorCode[id] = 10;
		}
		double eta = Eta[id];
		double phi = Phi[id];
		int part = (int) Part[id];
		
		if (!(ErrorCode.count(id))) std::cout<<id<<", ";
		nTTs++;
		int errorcode = (int) ErrorCode[id];
		if (errorcode == 0) nTTerr0++;
		if (errorcode == 1) nTTerr1++;
		if (errorcode == 2) nTTerr2++;
		if (errorcode == 3) nTTerr3++;
		if (errorcode == 4) nTTerr4++;
		if (errorcode == 5) nTTerr5++;
		if (errorcode == 6) nTTerr6++;

		//if (errorcode == 2 && part<=3) std::cout<<id<<", ";
		//std::cout<<"high RMS in HAD: "<<std::endl;		
		//if (errorcode == 2 && part>3) std::cout<<id<<", ";

	
		//old timing
		FullDelayDataOld[id] = (double) FullDelayData_old[id];
		
		if ( ErrorCode[id] < 6 ){
			//transform timing correction values to values in step size
			Timing_truncated[id] = trunc(TimingSum[id]); //truncate_to_P4step( TimingSum[id] );
			if (Timing_truncated[id] == -0.00) Timing_truncated[id] = 0.00;
			//set new full delay values
			FullDelayDataNew_truncated[id] = FullDelayDataOld[id] - Timing_truncated[id]; 
		}
		else {
			//set new full delay values
			FullDelayDataNew_truncated[id] = FullDelayDataOld[id]; 
			TimingSum[id] = 0;
		}
		//determine difference to former timing settings
		FullDelayDataDiff_truncated[id] = FullDelayDataOld[id] - FullDelayDataNew_truncated[id];
		if (TimingSum[id] == -0.00) TimingSum[id] = 0.00;
	}
	//Error Code counting output
	int nTTsum = nTTerr0 + nTTerr1 + nTTerr2 + nTTerr3 + nTTerr4 + nTTerr5 + nTTerr6; // for sanity check
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "Error Code statistics" << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "Error code\t\tnumber of TTs\t\tpercentage" << std::endl;
	std::cout << "0\t\t\t" << nTTerr0 <<"\t\t\t"<<(double(nTTerr0)/double(nTTsum))*100 <<std::endl;
	std::cout << "1\t\t\t" << nTTerr1 <<"\t\t\t"<<(double(nTTerr1)/double(nTTsum))*100  << std::endl;
	std::cout << "2\t\t\t" << nTTerr2 <<"\t\t\t"<<(double(nTTerr2)/double(nTTsum))*100  << std::endl;
	std::cout << "3\t\t\t" << nTTerr3 <<"\t\t\t"<<(double(nTTerr3)/double(nTTsum))*100  << std::endl;
	std::cout << "4\t\t\t" << nTTerr4 <<"\t\t\t"<<(double(nTTerr4)/double(nTTsum))*100  << std::endl;
	std::cout << "5\t\t\t" << nTTerr5 <<"\t\t\t"<<(double(nTTerr5)/double(nTTsum))*100  << std::endl;
	std::cout << "6\t\t\t" << nTTerr6 <<"\t\t\t"<<(double(nTTerr6)/double(nTTsum))*100  << std::endl;
	std::cout << "total number of TTs\t (should be 8192) " << nTTs << std::endl;
	std::cout << "which should not match total sum\t (which should be 7168) " << nTTsum << std::endl;
	
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "Writing new  full signal delays to files" << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	
	TTxml xmlout_trunc;


	xmlout_trunc.Add("eta",Eta); 
	xmlout_trunc.Add("phi",Phi);
   	xmlout_trunc.Add("Part",Part);
	xmlout_trunc.Add("FullDelayDataOld",FullDelayDataOld);
	xmlout_trunc.Add("FullDelayDataNew",FullDelayDataNew_truncated);
	xmlout_trunc.Add("FullDelayDataDiff",FullDelayDataDiff_truncated);
	xmlout_trunc.Add("Timing_trunc",Timing_truncated);
	xmlout_trunc.Add("Timing",TimingSum);
	xmlout_trunc.Add("TimingRMS",TimingRMS);
	xmlout_trunc.Add("Timing_badTT",BadTiming);
	xmlout_trunc.Add("BadTimingRMS",BadTimingRMS);
	xmlout_trunc.Add("ManualTiming",ManualTiming);
	xmlout_trunc.Add("ErrorCode",ErrorCode);

	ifstream ifilexml("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/CollisionTiming_80MHz2016_final_trunc.xml");

	if (ifilexml) {
		if( remove( "/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/CollisionTiming_80MHz2016_final_trunc.xml" ) != 0 )
		perror( "Error deleting file" );
  		else puts( "Old xml file successfully deleted" );
	}
	else puts("Creating new xml file.");

	xmlout_trunc.Write("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/CollisionTiming_80MHz2016_final_trunc.xml");
	
	//write coolinit file
	std::cout << "Opened coolinit file for writing" << std::endl;

	ifstream ifile("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/PprTimingResults.Collisions80MHz2016.coolinit");

	if (ifile) {
		if( remove( "/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/PprTimingResults.Collisions80MHz2016.coolinit" ) != 0 )
		perror( "Error deleting file" );
  		else puts( "Old coolinit file successfully deleted" );
	}
	else puts("Creating new coolinit file.");

	FILE * timingFile;
	timingFile = fopen("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/PprTimingResults.Collisions80MHz2016.coolinit","w");
	std::map<unsigned int,double>::iterator it_file;

	for ( it_file = FullDelayDataNew_truncated.begin(); it_file != FullDelayDataNew_truncated.end(); it_file++ ){
		unsigned int id = it_file->first;
		//Convert double to int
		int fullDelay = (int) it_file->second;
		int error = (int) ErrorCode[id];
		fprintf(timingFile,"PprTimingResults,0x%08x,FullDelayData:unsigned:%i,ErrorCode:unsigned:%i\n",id,fullDelay,error);
	}
	fclose (timingFile);


	//plot interesting variables
	//for general timing values
	std::map<unsigned int,double> Timing_plot;
	std::map<unsigned int,double> TimingRMS_plot;
	std::map<unsigned int,double> ErrorCode_plot;
	std::map<unsigned int,double> Timing_trunc_plot;
	std::map<unsigned int,double> FullDelayDataNew_trunc_plot;
	std::map<unsigned int,double> FullDelayDataDiff_trunc_plot;

	//read in the maps from the two different xml files
	TTxml xmlfinal_trunc_plot;
	xmlfinal_trunc_plot.Read("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/CollisionTiming_80MHz2016_final_trunc.xml");
	Timing_plot = xmlfinal_trunc_plot.Get("Timing");
	TimingRMS_plot = xmlfinal_trunc_plot.Get("TimingRMS");
	ErrorCode_plot = xmlfinal_trunc_plot.Get("ErrorCode");
	Timing_trunc_plot = xmlfinal_trunc_plot.Get("Timing_trunc");
	FullDelayDataNew_trunc_plot = xmlfinal_trunc_plot.Get("FullDelayDataNew");
	FullDelayDataDiff_trunc_plot = xmlfinal_trunc_plot.Get("FullDelayDataDiff");
	
	//define rootfile and histograms
	TFile *rootfile = new TFile("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/finaltiming_80MHz2016_truncated.root","RECREATE");
	//for general timing files	
	TH2TT *emTiming = new TH2TT("emTiming","emTiming;#eta;#phi");
	TH2TT *hadTiming = new TH2TT("hadTiming","hadTiming;#eta;#phi");
	emTiming->Fillall(-1000.);
	hadTiming->Fillall(-1000.);
	TH1 * emTiming_spread = new TH1D("emTiming_spread","emTiming_spread;Timing correction [ns];Number of TTs",82,-20.5,20.5);
	TH1 * hadTiming_spread = new TH1D("hadTiming_spread","hadTiming_spread;Timing correction [ns];Number of TTs",82,-20.5,20.5);
	TH2TT *emTimingRMS = new TH2TT("emTimingRMS","emTimingRMS;#eta;#phi");
	TH2TT *hadTimingRMS = new TH2TT("hadTimingRMS","hadTimingRMS;#eta;#phi");
	TH2TT *emErrorCode = new TH2TT("emErrorCode","emErrorCode;#eta;#phi");
	TH2TT *hadErrorCode = new TH2TT("hadErrorCode","hadErrorCode;#eta;#phi");
	//for truncated timing corrections
	TH2TT *emTiming_trunc = new TH2TT("emTiming_trunc","emTiming_trunc;#eta;#phi");
	TH2TT *hadTiming_trunc = new TH2TT("hadTiming_trunc","hadTiming_trunc;#eta;#phi");
	emTiming_trunc->Fillall(-1000.);
	hadTiming_trunc->Fillall(-1000.);
	//TH1 * emTiming_trunc_spread = new TH1D("emTiming_trunc_spread","emTiming_trunc_spread;Timing correction [ns];Number of TTs",41,-20.5*25./24,20.5*25./24);
	TH1 * emTiming_trunc_spread = new TH1D("emTiming_trunc_spread","emTiming_trunc_spread;Timing correction [ns];Number of TTs",41,-20.5,20.5);
	//TH1 * hadTiming_trunc_spread = new TH1D("hadTiming_trunc_spread","hadTiming_trunc_spread;Timing correction [ns];Number of TTs",41,-20.5*25./24,20.5*25./24);
	TH1 * hadTiming_trunc_spread = new TH1D("hadTiming_trunc_spread","hadTiming_trunc_spread;Timing correction [ns];Number of TTs",41,-20.5,20.5);
	TH1 * emTiming_trunc_test_spread = new TH1D("emTiming_P4correction_spread","emTiming_P4correction_spread;Timing correction [ns];Number of TTs",41,-20.5,20.5);
	TH1 * hadTiming_trunc_test_spread = new TH1D("hadTiming_P4correction_spread","hadTiming_P4correction_spread;Timing correction [ns];Number of TTs",41,-20.5,20.5);
	TH2TT *emFullDelay_trunc = new TH2TT("emFullDelayNew_trunc","emFullDelayNew_trunc;#eta;#phi");
	TH2TT *hadFullDelay_trunc = new TH2TT("hadFullDelayNew_trunc","hadFullDelayNew_trunc;#eta;#phi");
	TH2TT *emFullDelayOld_trunc = new TH2TT("emFullDelayOld_trunc","emFullDelayOld_trunc;#eta;#phi");
	TH2TT *hadFullDelayOld_trunc = new TH2TT("hadFullDelayOld_trunc","hadFullDelayOld_trunc;#eta;#phi");
	TH2TT *emFullDelayDiff_trunc = new TH2TT("emFullDelayDiff_trunc","emFullDelayDiff_trunc;#eta;#phi");
	TH2TT *hadFullDelayDiff_trunc = new TH2TT("hadFullDelayDiff_trunc","hadFullDelayDiff_trunc;#eta;#phi");

	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "Counting TTs with certain timing corrections" << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	//count TTs with certain corrections
	std::vector<int> nTTsXnsCorr;
	for (int XnsCorr = 0; XnsCorr <= 6; XnsCorr++){
		nTTsXnsCorr.push_back(0);
	}
	//nTTsXnsCorr counting, i.e. how many TTs have Xns correction
	std::map<unsigned int,double>::iterator it_timingCorr;

	for (int XnsCorr = 0; XnsCorr <= 6; XnsCorr++){
		nTTsXnsCorr.at(XnsCorr) = 0; // resetting vector to zero
	}
	for ( it_timingCorr = FullDelayDataDiff_trunc_plot.begin(); it_timingCorr != FullDelayDataDiff_trunc_plot.end(); it_timingCorr++ ){
		unsigned int id = it_timingCorr->first;
		if (std::find(spare.begin(), spare.end(), id)!=spare.end()) continue;
		double timing = it_timingCorr->second;
		if ( fabs(timing) < 0.01 ) nTTsXnsCorr.at(0)++;
		else if ( (fabs(timing)-1.*step_to_ns) < 0.01 ) nTTsXnsCorr.at(1)++;
		else if ( (fabs(timing)-2.*step_to_ns) < 0.01 ) nTTsXnsCorr.at(2)++;
		else if ( (fabs(timing)-3.*step_to_ns) < 0.01 ) nTTsXnsCorr.at(3)++;
		else if ( (fabs(timing)-4.*step_to_ns) < 0.01 ) nTTsXnsCorr.at(4)++;
		else if ( (fabs(timing)-5.*step_to_ns) < 0.01 ) nTTsXnsCorr.at(5)++;
		else if ( (fabs(timing)-5.*step_to_ns) > 0.01 ) {
			nTTsXnsCorr.at(6)++;
			if ( fabs(timing) >= 5.01*step_to_ns && fabs(timing) < 10.*step_to_ns )
				std::cout << "WARNING Large timing correction for TT " << std::hex << id << std::dec << "(eta=" << Eta[id] << ", phi=" << Phi[id] << ")! --> correction = " << timing << std::endl;
			else if ( fabs(timing) >= 10. )
				std::cout << "WARNING Very large timing correction for TT " << std::hex << id << std::dec << "(eta=" << Eta[id] << ", phi=" << Phi[id] << ")! --> correction = " << timing << std::endl;
		}
		else std::cout << "ERROR in counting the truncated timing corrections at TT " << std::hex << id << std::dec << "!" << std::endl;	
	}
	//output the results of the nTTsXnsCorr counting to screen, i.e. how many TTs have Xns correction
	std::cout << "Absolute value\t\t" << "Number of TTs\t" << "Percentage of TTs" << std::endl;
	std::cout << "of timing correction" << std::endl;
	for ( unsigned int XnsCorr = 0; XnsCorr < nTTsXnsCorr.size(); XnsCorr++ ){
		std::cout << XnsCorr << "\t\t\t" << nTTsXnsCorr.at(XnsCorr) << "\t\t" << double(nTTsXnsCorr.at(XnsCorr))/7168.*100. << std::endl;
	}

	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "Plotting final distributions to root file" << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	
	std::map<unsigned int,double>::iterator it_plot;
	//timing variables
	for ( it_plot = Timing_plot.begin(); it_plot != Timing_plot.end(); it_plot++ ){
		unsigned int id = it_plot->first;
		double eta = Eta[id];
		double phi = Phi[id];
		int part = (int) Part[id];
		if (std::find(spare.begin(), spare.end(), id)!=spare.end()) continue;
		if (part<=3){ //em layer
			//if (ErrorCode_plot[id] == 6) emErrorCode->SetBinContent(eta,phi,5);
			emErrorCode->SetBinContent(eta,phi,ErrorCode_plot[id]);
			if (std::find(spare.begin(), spare.end(), id)!=spare.end()) continue;
			emTiming->SetBinContent(eta,phi,it_plot->second);
			emTiming_spread->Fill(it_plot->second);
			emTimingRMS->SetBinContent(eta,phi,TimingRMS_plot[id]);
			if (ErrorCode_plot[id] == 6) continue;			
			emTiming_trunc->SetBinContent(eta,phi,Timing_trunc_plot[id]);
			emTiming_trunc_spread->Fill(Timing_trunc_plot[id]);
		}
		else if (part>3 && part<=8){ //had layer
			hadTiming->SetBinContent(eta,phi,it_plot->second);
			hadTiming_spread->Fill(it_plot->second);
			hadTimingRMS->SetBinContent(eta,phi,TimingRMS_plot[id]);
			//if (ErrorCode_plot[id] == 6) hadErrorCode->SetBinContent(eta,phi,5);
			hadErrorCode->SetBinContent(eta,phi,ErrorCode_plot[id]);
			if (ErrorCode_plot[id] == 6) continue;
			hadTiming_trunc->SetBinContent(eta,phi,Timing_trunc_plot[id]);
			hadTiming_trunc_spread->Fill(Timing_trunc_plot[id]);
		}
		else std::cout << "Part in Timing invalid for plotting" << std::endl;
	}
	unsigned int cntP4=0;
	static float stepsToNs = 25./24.;
	static const unsigned int stepsPerTick = 24;
	static const unsigned int fudelOffset = 400;
	//truncated timing corrections
	for ( it_plot = FullDelayDataNew_trunc_plot.begin(); it_plot != FullDelayDataNew_trunc_plot.end(); it_plot++ ){
		unsigned int id = it_plot->first;
		double eta = Eta[id];
		double phi = Phi[id];
		int part = (int) Part[id];
		if (std::find(spare.begin(), spare.end(), id)!=spare.end()) continue;
		if (part<=3){ //em layer
			emFullDelay_trunc->SetBinContent(eta,phi,it_plot->second);
			emFullDelayOld_trunc->SetBinContent(eta,phi,FullDelayData_old[id]);
			emFullDelayDiff_trunc->SetBinContent(eta,phi,FullDelayDataDiff_trunc_plot[id]);
			float val = (fudelOffset - static_cast<unsigned int>(it_plot->second)) / stepsToNs ;
			int return_Val=static_cast<unsigned int>(0.5 + val) % stepsPerTick ;
			float valold = (fudelOffset - static_cast<unsigned int>(FullDelayData_old[id])) / stepsToNs ;
			int return_Valold=static_cast<unsigned int>(0.5 + valold) % stepsPerTick ;
			emTiming_trunc_test_spread->Fill(return_Val-return_Valold);

			if ((return_Val-return_Valold ==0) && (it_plot->second!=FullDelayData_old[id])){
					std::cout << "new full delay" <<it_plot->second<<std::endl;
					std::cout << "old full delay" <<FullDelayData_old[id]<<std::endl;
					std::cout << "return val" <<return_Val<<std::endl;
					cntP4++;
					std::cout << "no. of times " <<cntP4<<std::endl;
			}
		}
		else if (part>3 && part<=8){ //had layer
			hadFullDelay_trunc->SetBinContent(eta,phi,it_plot->second);
			hadFullDelayOld_trunc->SetBinContent(eta,phi,FullDelayData_old[id]);
			hadFullDelayDiff_trunc->SetBinContent(eta,phi,FullDelayDataDiff_trunc_plot[id]);
			float val = (fudelOffset - static_cast<unsigned int>(it_plot->second)) / stepsToNs ;
			int return_Val=static_cast<unsigned int>(0.5 + val) % stepsPerTick ;
			float valold = (fudelOffset - static_cast<unsigned int>(FullDelayData_old[id])) / stepsToNs ;
			int return_Valold=static_cast<unsigned int>(0.5 + valold) % stepsPerTick ;
			hadTiming_trunc_test_spread->Fill(return_Val-return_Valold);
		}
		else std::cout << "WARNING Part in Timing invalid for plotting" << std::endl;
	}

	//Fit Gaussian to timing_spread histograms
	emTiming_spread->Fit("gaus");
	hadTiming_spread->Fit("gaus");

	//set drawing options
	emTiming->SetOption("colz");
	hadTiming->SetOption("colz");
	emTimingRMS->SetOption("colz");
	hadTimingRMS->SetOption("colz");
	emErrorCode->SetOption("colz");
	hadErrorCode->SetOption("colz");
	emTiming_trunc->SetOption("colz");
	hadTiming_trunc->SetOption("colz");
	emFullDelay_trunc->SetOption("colz");
	hadFullDelay_trunc->SetOption("colz");
	emFullDelayOld_trunc->SetOption("colz");
	hadFullDelayOld_trunc->SetOption("colz");
	emFullDelayDiff_trunc->SetOption("colz");
	hadFullDelayDiff_trunc->SetOption("colz");

	emTiming->SetStats(0);
	hadTiming->SetStats(0);
	emTimingRMS->SetStats(0);
	hadTimingRMS->SetStats(0);
	emErrorCode->SetStats(0);
	hadErrorCode->SetStats(0);
	emTiming_trunc->SetStats(0);
	hadTiming_trunc->SetStats(0);
	emFullDelay_trunc->SetStats(0);
	hadFullDelay_trunc->SetStats(0);
	emFullDelayOld_trunc->SetStats(0);
	hadFullDelayOld_trunc->SetStats(0);
	emFullDelayDiff_trunc->SetStats(0);
	hadFullDelayDiff_trunc->SetStats(0);


	emTiming->SetMinimum(-20.);
	hadTiming->SetMinimum(-20.);
	emTiming->Write();
	hadTiming->Write();
	emTiming_spread->Write();
	hadTiming_spread->Write();
	emTimingRMS->Write();
	hadTimingRMS->Write();
	emErrorCode->SetMinimum(-0.01);
	hadErrorCode->SetMinimum(-0.01);
	emErrorCode->Write();
	hadErrorCode->Write();
	emTiming_trunc->SetMinimum(-20.);
	hadTiming_trunc->SetMinimum(-20.);
	emTiming_trunc->Write();
	hadTiming_trunc->Write();
	emTiming_trunc_spread->Write();
	emTiming_trunc_test_spread->Write();
	hadTiming_trunc_test_spread->Write();
	hadTiming_trunc_spread->Write();
	emFullDelay_trunc->Write();
	hadFullDelay_trunc->Write();
	emFullDelayOld_trunc->Write();
	hadFullDelayOld_trunc->Write();
	emFullDelayDiff_trunc->Write();
	hadFullDelayDiff_trunc->Write();
	


	rootfile->Close();

	return 0;
}






