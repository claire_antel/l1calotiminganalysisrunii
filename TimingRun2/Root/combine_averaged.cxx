
#include "TH2TT.h"
#include <TH1.h>
#include <TH2.h>
#include "TFile.h"
#include "TKey.h"
#include "TObject.h"

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <stdio.h>

void combine_averaged(const char * outputfile_em="/afs/cern.ch/user/c/cantel/20.1.4.9/averaged_em.root", const char * outputfile_had="/afs/cern.ch/user/c/cantel/20.1.4.9/averaged_had.root"){


	TFile o_em(outputfile_em,"RECREATE");
	TFile o_had(outputfile_had,"RECREATE");

	TFile f1("/afs/cern.ch/user/c/cantel/20.1.4.9/runfitting/TimingFitHistos_265545_lb46.root");
	f1.Print();
	f1.cd("HistsFit");
	//gDirectory->GetListOfKeys()->Print();
	TKey* key;
	key = gDirectory->GetKey("emNGoodFits");
	TObject *o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsEM1 = (TH2TT*)o;//new TH2TT("NFitsEM1", "emN1");
	NFitsEM1->Print();
	//gDirectory->GetObject("emNGoodFits",NFitsEM1);
	//NFitsEM1->Print();
	key = gDirectory->GetKey("hadNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsHAD1 = (TH2TT*)o;//new TH2TT("NFitsHAD1", "hadN1");
	NFitsHAD1->Print();
	key = gDirectory->GetKey("emTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	//gDirectory->GetObject("hadNGoodFits",NFitsHAD1);
	TH2TT *emTPeak1 = (TH2TT*)o;
	//gDirectory->GetObject("emTPeak",emTPeak1);
	key = gDirectory->GetKey("hadTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *hadTPeak1=(TH2TT*)o;
	//gDirectory->GetObject("hadTPeak",hadTPeak1);

	TFile f2("/afs/cern.ch/user/c/cantel/20.1.4.9/runfitting/TimingFitHistos_265545_lb65.root");
	f2.cd("HistsFit");
	key = gDirectory->GetKey("emNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsEM2= (TH2TT*)o;
	//gDirectory->GetObject("emNGoodFits",NFitsEM2);
	key = gDirectory->GetKey("hadNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsHAD2= (TH2TT*)o;
	//gDirectory->GetObject("hadNGoodFits",NFitsHAD2);
	key = gDirectory->GetKey("emTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *emTPeak2= (TH2TT*)o;
	//gDirectory->GetObject("emTPeak",emTPeak2);
	key = gDirectory->GetKey("hadTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *hadTPeak2= (TH2TT*)o;
	//gDirectory->GetObject("hadTPeak",hadTPeak2);

	TFile f3("/afs/cern.ch/user/c/cantel/20.1.4.9/runfitting/TimingFitHistos_265545_lb83.root");
	f3.cd("HistsFit");
	key = gDirectory->GetKey("emNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsEM3= (TH2TT*)o;
	//gDirectory->GetObject("emNGoodFits",NFitsEM2);
	key = gDirectory->GetKey("hadNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsHAD3= (TH2TT*)o;
	//gDirectory->GetObject("hadNGoodFits",NFitsHAD2);
	key = gDirectory->GetKey("emTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *emTPeak3= (TH2TT*)o;
	//gDirectory->GetObject("emTPeak",emTPeak2);
	key = gDirectory->GetKey("hadTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *hadTPeak3= (TH2TT*)o;

	TFile f4("/afs/cern.ch/user/c/cantel/20.1.4.9/runfitting/TimingFitHistos_265545_lb121-131.root");
	f4.cd("HistsFit");
	key = gDirectory->GetKey("emNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsEM4= (TH2TT*)o;
	//gDirectory->GetObject("emNGoodFits",NFitsEM2);
	key = gDirectory->GetKey("hadNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsHAD4= (TH2TT*)o;
	//gDirectory->GetObject("hadNGoodFits",NFitsHAD2);
	key = gDirectory->GetKey("emTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *emTPeak4= (TH2TT*)o;
	//gDirectory->GetObject("emTPeak",emTPeak2);
	key = gDirectory->GetKey("hadTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *hadTPeak4= (TH2TT*)o;

	TFile f5("/afs/cern.ch/user/c/cantel/20.1.4.9/runfitting/TimingFitHistos_265573_lb003-015.root");
	f5.cd("HistsFit");
	key = gDirectory->GetKey("emNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsEM5= (TH2TT*)o;
	//gDirectory->GetObject("emNGoodFits",NFitsEM2);
	key = gDirectory->GetKey("hadNGoodFits");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *NFitsHAD5= (TH2TT*)o;
	//gDirectory->GetObject("hadNGoodFits",NFitsHAD2);
	key = gDirectory->GetKey("emTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *emTPeak5= (TH2TT*)o;
	//gDirectory->GetObject("emTPeak",emTPeak2);
	key = gDirectory->GetKey("hadTPeak");
	o = (TH2TT*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2TT *hadTPeak5= (TH2TT*)o;


	emTPeak1->Multiply(NFitsEM1);
	hadTPeak1->Multiply(NFitsHAD1);
	emTPeak2->Multiply(NFitsEM2);
	hadTPeak2->Multiply(NFitsHAD2);
	emTPeak3->Multiply(NFitsEM3);
	hadTPeak3->Multiply(NFitsHAD3);
	emTPeak4->Multiply(NFitsEM4);
	hadTPeak4->Multiply(NFitsHAD4);
	emTPeak5->Multiply(NFitsEM5);
	hadTPeak5->Multiply(NFitsHAD5);

	emTPeak1->Add(emTPeak2);
	hadTPeak1->Add(hadTPeak2);
	NFitsEM1->Add(NFitsEM2);
	NFitsHAD1->Add(NFitsHAD2);
	emTPeak1->Add(emTPeak3);
	hadTPeak1->Add(hadTPeak3);
	NFitsEM1->Add(NFitsEM3);
	NFitsHAD1->Add(NFitsHAD3);
	emTPeak1->Add(emTPeak4);
	hadTPeak1->Add(hadTPeak4);
	NFitsEM1->Add(NFitsEM4);
	NFitsHAD1->Add(NFitsHAD4);
	emTPeak1->Add(emTPeak5);
	hadTPeak1->Add(hadTPeak5);
	NFitsEM1->Add(NFitsEM5);
	NFitsHAD1->Add(NFitsHAD5);

	
	emTPeak1->Divide(NFitsEM1);
	hadTPeak1->Divide(NFitsHAD1);

	emTPeak1->SetStats(0);
	hadTPeak1->SetStats(0);

	o_em.cd();
	emTPeak1->Write();
	o_had.cd();
	hadTPeak1->Write();
}
	
