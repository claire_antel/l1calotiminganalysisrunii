//ooooOOOOOoooOOOooOOOOoooOOOoooOOOOoooOOOooOOOOoooOOOOOoooOOOoooOOOOoooOOOoooOOOOoooOOoooOOOOooOOOOooOOOOoooOOoo
//this script is to obtain timing offsets for each trigger tower from the timing distribution of fits. the mean
//is simply retrieved from the mean of the (narrowed) histogram. 
//the main function to use is
//
//			draw_tdiffmaps(run, file)
//
//where 'run' is a label that is used in the output files, 
//and file is the name of the input root file.
//you can set the base directory and output directory within the script. it is set to my private directory 
//at the moment, so it needs to be customised first.
//ooooOOOOOoooOOOooOOOOoooOOOoooOOOOoooOOOooOOOOoooOOOOOoooOOOoooOOOOoooOOOoooOOOOoooOOoooOOOOooOOOOooOOOOoooOOoo

#include "../TimingRun2/TH2TT.h"
#include "../TimingRun2/TTxml.h"
#include <TH1.h>
#include <TH2.h>
#include "TFile.h"
#include "TKey.h"
#include "TObject.h"
#include "TString.h"
#include "TColor.h"
#include "TStyle.h"
#include "TLine.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "THStack.h"
#include "TObjectTable.h"
#include "TF1.h"

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>     //for using the function sleep
#include <sstream> // for stringstream


void set_plot_style()
{
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;

    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    //gStyle->SetNumberContours(NCont);
}

void draw_line(double x, double y)
{
  TLine *line = new TLine(x,0,x,1.2*y);
  line->SetLineColor(kRed);
  line->SetLineWidth(2);
  line->Draw();
}

void draw_box(TH1D* hist, const char * region)
{
  const char * text1 = region;
  char * text2;
  sprintf (text2, "mean = %.2f", hist->GetMean());
  char * text3;
  sprintf (text3, "RMS = %.2f", hist->GetRMS());

  TLatex l; l.SetTextAlign(12);
  l.SetNDC();
  l.DrawLatex(0.2, 0.5, text1);
  l.DrawLatex(0.2, 0.3, text2);
  l.DrawLatex(0.2, 0.1, text3);
}

void draw_tdiffmaps(std::string run, std::string file) {

	/*ooOOOooooOOOooooOOOoooOOOOoooOOOOoooOOOOoooOOOoo
	  set output directories - double check that valid.
	  ooOOOooooOOOooooOOOoooOOOOoooOOOOoooOOOOoooOOOoo*/

        std::string basedir = "/afs/cern.ch/work/c/cantel/private/collisions/"; 
	std::string outputdirname = basedir+"TimingJune2016";
	
	// property maps 
	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;
	std::map<unsigned int,double> TDiff, Trms;
	std::map<unsigned int,double> NFits;

	TTxml m_props;
	m_props.Read("/afs/cern.ch/work/c/cantel/public/l1caloTimingAnalysiSxmls/TTparameters_filledblanks.xml");

	Eta = m_props.Get("eta");
	Phi = m_props.Get("phi");
	Part = m_props.Get("part");

	if (!(Eta.size()==7168)){
		std::cout<<"size of TT parameter maps of wrong size. Should be 7168 but is "<<Eta.size()<<"."<<std::endl;
	}

	set_plot_style();
	
	std::stringstream sstm_outputfile;
	sstm_outputfile <<outputdirname << "/tdiff_maps_run"<<run<<".root";

        std::string outputfile = sstm_outputfile.str();
	sstm_outputfile.clear();
	sstm_outputfile.str("");	


	//char * input_name = (char*) malloc(snprintf(NULL, 0,"/afs/cern.ch/work/c/cantel/private/collisions/%s", file)+1);
	//sprintf(input_name,"/afs/cern.ch/work/c/cantel/private/collisions/%s", file);
	std::string input_name_str = basedir+file;
	std::cout<<"input file is "<<input_name_str<<std::endl; 
	std::cout<<"output file is "<<outputfile<<std::endl;

	const char * input_name = input_name_str.c_str();

	TFile* timing_hists = new TFile(input_name);
	if (timing_hists->IsZombie()) {std::cout<<"File does not exist. Check your path and file name."<<std::endl;  return;}
	std::cout<<"opened timing_hists file"<<std::endl;

	/*ooOOOooooOOOooooOOOoooOOOOoooOOOOoooOOOOoooOOOoo
	  		setting up histograms
         ooOOOooooOOOooooOOOoooOOOOoooOOOOoooOOOOoooOOOoo*/


	// first set range of eta-phi timing map z-axis
	int minval = -7;
	int maxval = 7;

	TH2TT* hist_tdiffsEM = new TH2TT("hist_tdiffsEM", "avg timing offsets (EM);#eta;#phi");
	TH2TT* hist_trmsEM = new TH2TT("hist_trmsEM", "rms of avg timing offsets (EM);#eta;#phi");
	TH2TT* hist_tdiffsEM_EM = new TH2TT("hist_tdiffsEM_EM", "avg timing offsets EM triggers (EM);#eta;#phi");
	TH2TT* hist_trmsEM_EM = new TH2TT("hist_trmsEM_EM", "rms of avg timing offsets EM triggers (EM);#eta;#phi");
	TH2TT* hist_tdiffsEM_JET = new TH2TT("hist_tdiffsEM_JET", "avg timing offsets JET triggers (EM);#eta;#phi");
	TH2TT* hist_trmsEM_JET = new TH2TT("hist_trmsEM_JET", "rms of avg timing offsets JET triggers (EM);#eta;#phi");
	TH2TT* hist_tdiffsHAD = new TH2TT("hist_tdiffsHAD", "avg timing offsets (HAD);#eta;#phi");
	TH2TT* hist_trmsHAD = new TH2TT("hist_trmsHAD", "rms of avg timing offsets (HAD);#eta;#phi");
	TH2TT* hist_tdiffsHAD_EM = new TH2TT("hist_tdiffsHAD_EM", "avg timing offsets EM triggers (HAD);#eta;#phi");
	TH2TT* hist_trmsHAD_EM = new TH2TT("hist_trmsHAD_EM", "rms of avg timing offsets EM triggers (HAD);#eta;#phi");
	TH2TT* hist_tdiffsHAD_JET = new TH2TT("hist_tdiffsHAD_JET", "avg timing offsets JET triggers (HAD);#eta;#phi");
	TH2TT* hist_trmsHAD_JET = new TH2TT("hist_trmsHAD_JET", "rms of avg timing offsets JET triggers (HAD);#eta;#phi");
	TH2TT* hist_goodfitsHAD = new TH2TT("hist_goodfitsfromEntriesHAD", "no. of good fits (HAD);#eta;#phi");
	TH2TT* hist_goodfitsEM = new TH2TT("hist_goodfitsfromEntriesEM", "no. of good fits (EM);#eta;#phi");

        hist_tdiffsEM->Fillall(-1000);
        hist_trmsEM->Fillall(-1000);
        hist_tdiffsEM_EM->Fillall(-1000);
        hist_trmsEM_EM->Fillall(-1000);
        hist_tdiffsEM_JET->Fillall(-1000);
        hist_trmsEM_JET->Fillall(-1000);
  	hist_goodfitsHAD->Fillall(-1000);
  	hist_goodfitsEM->Fillall(-1000);
        hist_tdiffsEM_EM->SetMinimum(minval);
        hist_trmsEM_EM->SetMinimum(0);
        hist_tdiffsEM_JET->SetMinimum(minval);
        hist_trmsEM_JET->SetMinimum(0);
        hist_tdiffsEM->SetMinimum(minval);
        hist_trmsEM->SetMinimum(0);
	hist_trmsEM->SetMaximum(3);
        hist_tdiffsEM->SetStats(0);
        hist_trmsEM->SetStats(0);
        hist_tdiffsEM_EM->SetMinimum(minval);
        hist_trmsEM_EM->SetMinimum(0);
        hist_tdiffsEM_EM->SetStats(0);
        hist_trmsEM_EM->SetStats(0);
	hist_tdiffsEM_EM->SetOption("colz");
  	hist_trmsEM_EM->SetOption("colz");

        hist_tdiffsEM_JET->SetMinimum(minval);
        hist_trmsEM_JET->SetMinimum(0);
        hist_tdiffsEM_JET->SetStats(0);
        hist_trmsEM_JET->SetStats(0);
	hist_tdiffsEM_JET->SetOption("colz");
  	hist_trmsEM_JET->SetOption("colz");

	hist_tdiffsEM->SetMaximum(maxval);
	hist_tdiffsEM_EM->SetMaximum(maxval);
	hist_tdiffsEM_JET->SetMaximum(maxval);

        hist_tdiffsHAD->Fillall(-1000);
        hist_trmsHAD->Fillall(-1000);
        hist_tdiffsHAD->SetMinimum(minval);
        hist_trmsHAD->SetMinimum(0);
	hist_tdiffsHAD->SetMaximum(maxval);
        hist_trmsHAD->SetMaximum(3);
        hist_tdiffsHAD->SetStats(0);
        hist_trmsHAD->SetStats(0);

        hist_tdiffsHAD_EM->Fillall(-1000);
        hist_trmsHAD_EM->Fillall(-1000);
        hist_tdiffsHAD_EM->SetMinimum(minval);
        hist_trmsHAD_EM->SetMinimum(0);
	hist_tdiffsHAD_EM->SetMaximum(maxval);
        hist_trmsHAD_EM->SetMaximum(3);
        hist_tdiffsHAD_EM->SetStats(0);
        hist_trmsHAD_EM->SetStats(0);
 	hist_tdiffsHAD_EM->SetOption("colz");
  	hist_trmsHAD_EM->SetOption("colz");

        hist_tdiffsHAD_JET->Fillall(-1000);
        hist_trmsHAD_JET->Fillall(-1000);
        hist_tdiffsHAD_JET->SetMinimum(minval);
        hist_trmsHAD_JET->SetMinimum(0);
	hist_tdiffsHAD_JET->SetMaximum(maxval);
        hist_trmsHAD_JET->SetMaximum(3);
        hist_tdiffsHAD_JET->SetStats(0);
        hist_trmsHAD_JET->SetStats(0);
 	hist_tdiffsHAD_JET->SetOption("colz");
  	hist_trmsHAD_JET->SetOption("colz");

  	hist_goodfitsHAD->SetStats(0);
  	hist_goodfitsEM->SetStats(0);
  	hist_tdiffsEM->SetOption("colz");
  	hist_trmsEM->SetOption("colz");
  	hist_tdiffsHAD->SetOption("colz");
  	hist_trmsHAD->SetOption("colz");
  	hist_goodfitsHAD->SetOption("colz");
  	hist_goodfitsEM->SetOption("colz");

	//setting up 1D profiles

	TH1D* hist_EMB = new TH1D("hist_EMB", "timing distribution in EMB; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_OVERLAP = new TH1D("hist_OVERLAP", "timing distribution in OVERLAP; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_EMEC = new TH1D("hist_EMEC", "timing distribution in EMEC; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_FCAL1 = new TH1D("hist_FCAL1", "timing distribution in FCAL1; time offset [ns]; events", 122,-30.5,30.5);

	TH1D* hist_TileLB = new TH1D("hist_TileLB", "timing distribution in TileLB; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_TileEB = new TH1D("hist_TileEB", "timing distribution in TileEB; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_HEC = new TH1D("hist_HEC", "timing distribution in HEC; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_FCAL2 = new TH1D("hist_FCAL2", "timing distribution in FCAL2; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_FCAL3 = new TH1D("hist_FCAL3", "timing distribution in FCAL3; time offset [ns]; events", 122,-30.5,30.5);

	timing_hists->cd("EM/");
	TIter next( gDirectory->GetListOfKeys() );

	std::string histName;
	TKey* key;
	while( (key=(TKey*) next() )){
		//std::cout << "key is... " << key->GetName() << std::endl;
		histName = key->GetName();
		TObject *o = (TH1D*) key->ReadObj()->Clone(TString::Format("%s", histName.c_str()));
		if(!o->InheritsFrom("TH1")) continue; //only look at histos
		Long64_t foundtime = histName.find("hist_tdistr_id");
		//std::cout<<"found time is "<<foundtime<<std::endl;
		if ( foundtime<histName.length()){
			//THStack *hist_stack = (THStack*)o;
			TH1 *hist_t = (TH1*)o;
			//std::cout<<"Line colour of histogram is "<<hist_t->GetLineColor()<<std::endl;
			//printf("key: %s points to an object of class: %s at %dn",key->GetName(),key->GetClassName(),key->GetSeekKey());
			unsigned int id=  atoi(histName.substr(14, histName.length()-14).c_str()); 
			//std::cout << "histname is... " << histName << std::endl;
			//std::cout << "corresponding id is... " << id << std::endl;
			double eta = Eta[id];
			double phi = Phi[id];
			int etabin = hist_tdiffsEM->GetXaxis()->FindBin(eta);
			int phibin = hist_tdiffsEM->GetYaxis()->FindBin(phi);
			//TIter nexthist (hist_stack->GetHists());
			//while ( (hist_t=(TH1*) nexthist() )){
// 			unsigned int maxbin = hist_t->GetMaximumBin();
// 			double startrange = hist_t->GetMean()-5.;
// 			double endrange = hist_t->GetMean()-5.;			
// 			hist_t->Fit("gaus", "", "", startrange, endrange);
// 			double time = hist_t->GetFunction("gaus")->GetParameter(1);
// 			double rms = hist_t->GetFunction("gaus")->GetParameter(2);
			hist_t->GetXaxis()->SetRangeUser(-10, 10); //limit range to cut out background. above also method to obtain mean from gauss fit...I found too many weird fits from doing this, but should double chceck for yourself.
				double time = hist_t->GetMean();
				double rms = hist_t->GetRMS();
				unsigned int fits = hist_t->GetEntries();
				int part = (int) Part[id];
				if (part<=3){
					if (hist_t->GetLineColor() == 602){
						if (fabs(time)>10) std::cout << "WARNING, large time offset for ID: "<< id<<" part: "<<part<<" eta: "<<eta<<" phi: "<<phi<<" time: "<<time<<std::endl;
						TDiff[id]=time;
						Trms[id]=rms;
						hist_tdiffsEM->SetBinContent(etabin, phibin, time);
						hist_trmsEM->SetBinContent(etabin, phibin, rms);
						hist_goodfitsEM->SetBinContent(etabin, phibin, fits);
						if (part==0) hist_FCAL1->Fill(time);
						if (part==1) hist_EMEC->Fill(time);
						//if (part == 1) std::cout << "time offset for ID: "<< id<<" part: "<<part<<" eta: "<<eta<<" phi: "<<phi<<" time: "<<time<<" rms: "<<rms<<std::endl;
						if (part==2) hist_OVERLAP->Fill(time);
						if (part==3) hist_EMB->Fill(time);
					}
					if (hist_t->GetLineColor() == 46){
						hist_tdiffsEM_EM->SetBinContent(etabin, phibin, time);
						hist_trmsEM_EM->SetBinContent(etabin, phibin, rms);
					}
					if (hist_t->GetLineColor() == 85){
						hist_tdiffsEM_JET->SetBinContent(etabin, phibin, time);
						hist_trmsEM_JET->SetBinContent(etabin, phibin, rms);
					}
				}
				else {
					std::cout<<"WARNING: part for ID 0x"<< id<<" returned not in EM layer. Histos in wrong file."<<std::endl;
				}
			//}
		}
	}

	timing_hists->cd("HAD/");
	TIter nextHAD( gDirectory->GetListOfKeys() );

	//std::string histName;
	//TKey* key;
	while( (key=(TKey*) nextHAD() )){
		//std::cout << "key is... " << key->GetName() << std::endl;
		histName = key->GetName();
		TObject *o = (TH1D*) key->ReadObj()->Clone(TString::Format("%s", histName.c_str()));
		if(!o->InheritsFrom("TH1")) continue; //only look at histos
		Long64_t foundtime = histName.find("hist_tdistr_id");
		//std::cout<<"found time is "<<foundtime<<std::endl;
		if ( foundtime<histName.length()){
			//THStack *hist_stack = (THStack*)o;
			//TList *hist_list = hist_stack->GetHists(); 
			TH1 *hist_t = (TH1*)o;
		//printf("key: %s points to an object of class: %s at %dn",key->GetName(),key->GetClassName(),key->GetSeekKey());
			unsigned int id=  atoi(histName.substr(14, histName.length()-14).c_str()); 
			//std::cout << "histname is... " << histName << std::endl;
		//std::cout << "corresponding id is... " << id << std::endl;
			double eta = Eta[id];
			double phi = Phi[id];
			int etabin = hist_tdiffsHAD->GetXaxis()->FindBin(eta);
			int phibin = hist_tdiffsHAD->GetYaxis()->FindBin(phi);
			//TIter nexthist (hist_stack->GetHists());
			//while ( (hist_t=(TH1*) nexthist() )){
				hist_t->GetXaxis()->SetRangeUser(-10, 10);
				double time = hist_t->GetMean();
				double rms = hist_t->GetRMS();
// 				unsigned int maxbin = hist_t->GetMaximumBin();
/*				double startrange = hist_t->GetMean()-5.;
				double endrange = hist_t->GetMean()-5.;			
				hist_t->Fit("gaus", "", "", startrange, endrange);
				double time = hist_t->GetFunction("gaus")->GetParameter(1);
				double rms = hist_t->GetFunction("gaus")->GetParameter(2);*/				
				unsigned int fits = hist_t->GetEntries();
				int part = (int) Part[id];
				if (part>3 && part<=8){
						if (hist_t->GetLineColor() == 602){
							if (time>10 || time<-10) std::cout << "WARNING, large time offset for ID: "<< id<<" part: "<<part<<" eta: "<<eta<<" phi: "<<phi<<" time: "<<time<<std::endl;
							TDiff[id]=time;
							Trms[id]=rms;
							hist_tdiffsHAD->SetBinContent(etabin, phibin, time);
							hist_trmsHAD->SetBinContent(etabin, phibin, rms);
							hist_goodfitsHAD->SetBinContent(etabin, phibin, fits);
							if (part==4) hist_FCAL2->Fill(time);
							if (part==5) hist_FCAL3->Fill(time);
							if (part==6) hist_HEC->Fill(time);
							if (part==7) hist_TileEB->Fill(time);
							if (part==8) hist_TileLB->Fill(time);
						}
						if (hist_t->GetLineColor() == 46){
							hist_tdiffsHAD_EM->SetBinContent(etabin, phibin, time);
							hist_trmsHAD_EM->SetBinContent(etabin, phibin, rms);
						}
						if (hist_t->GetLineColor() == 85){
							hist_tdiffsHAD_JET->SetBinContent(etabin, phibin, time);
							hist_trmsHAD_JET->SetBinContent(etabin, phibin, rms);
						}
				}
				else {
					std::cout<<"WARNING: part for ID 0x"<< id<<" returned not in HAD layer. Histos in wrong file."<<std::endl;
				}
			//}
		}
	}

	std::map<unsigned int,double>::iterator it;
	for ( it = Eta.begin(); it != Eta.end(); it++ ){
			unsigned int id = it->first;
			double eta = Eta[id];
			double phi = Phi[id];
			int etabin = hist_goodfitsEM->GetXaxis()->FindBin(eta);
			int phibin = hist_goodfitsEM->GetYaxis()->FindBin(phi);
			int part = (int) Part[id];
			if (part<=3){
				NFits[id]=hist_goodfitsEM->GetBinContent(etabin, phibin);
			}
			if (part>3 && part<=8){
				NFits[id]=hist_goodfitsHAD->GetBinContent(etabin, phibin);
			}
	}		

	TTxml ftime_values;
	ftime_values.Add("Eta", Eta);
	ftime_values.Add("Phi", Phi);
	ftime_values.Add("Part", Part);
	ftime_values.Add("TDiff", TDiff);
	ftime_values.Add("Trms", Trms);
	ftime_values.Add("NFits", NFits);

	std::stringstream sstm_outputXML;
	sstm_outputXML<<outputdirname<<"/TimeValues_run"<<run<<".xml"; 

	std::string outputXML = sstm_outputXML.str();
	sstm_outputXML.clear();
	sstm_outputXML.str("");

	ftime_values.Write(outputXML.c_str());

	std::cout<<"size of eta-phi maps are..."<<hist_tdiffsEM->GetEntries()<<"(EM layer) and "<<hist_tdiffsEM->GetEntries()<<"(HAD layer)."<<std::endl;

	double ymin = 0;
	double ymax = 6.3;
	TLine l;
	l.SetLineWidth(2);
	unsigned int bins[6]={5, 63, 19, 48, 20, 49};

	/*
	double ls=0.06;
	double os=0.5;

	hist_tdiffsEM->GetXaxis()->SetLabelSize(ls);
	hist_tdiffsEM->GetYaxis()->SetLabelSize(ls);
	hist_tdiffsEM->GetXaxis()->SetLabelOffset(os);
	hist_tdiffsEM->GetYaxis()->SetLabelOffset(os);

	hist_trmsEM->GetXaxis()->SetLabelSize(ls);
	hist_trmsEM->GetYaxis()->SetLabelSize(ls);
	hist_trmsEM->GetXaxis()->SetLabelOffset(os);
	hist_trmsEM->GetYaxis()->SetLabelOffset(os);

	hist_tdiffsHAD->GetXaxis()->SetLabelSize(ls);
	hist_tdiffsHAD->GetYaxis()->SetLabelSize(ls);
	hist_tdiffsHAD->GetXaxis()->SetLabelOffset(os);
	hist_tdiffsHAD->GetYaxis()->SetLabelOffset(os);

	hist_trmsHAD->GetXaxis()->SetLabelSize(ls);
	hist_trmsHAD->GetYaxis()->SetLabelSize(ls);
	hist_trmsHAD->GetXaxis()->SetLabelOffset(os);
	hist_trmsHAD->GetYaxis()->SetLabelOffset(os);*/

	//output png files.
        std::string PNG1 = "tdiff_mapEM_run"+run+".png"; 
	std::string PNG2 = "tdiff_mapEM_trigEM_run"+run+".png"; 
	std::string PNG3 = "tdiff_mapEM_trigJET_run"+run+".png"; 
	std::string PNG4 = "tdiff_mapHAD_trigEM_run"+run+".png"; 
	std::string PNG5 = "tdiff_mapHAD_trigJET_run"+run+".png"; 
	std::string PNG6 = "tdiff_mapHAD_run"+run+".png"; 
	std::string PNG7 = "tdiff_profiles_EM_run"+run+".png"; 
	std::string PNG8 = "tdiff_profiles_HAD_run"+run+".png"; 

	TCanvas* c = new TCanvas("canvas1", "c1", 1024, 980);
	c->Divide(1,2);
	//c->cd();
	c->cd(1);
	hist_tdiffsEM->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_tdiffsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymin,hist_tdiffsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymax);
	c->cd(2);
	hist_trmsEM->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymax);
	c->Update();
	//sleep(2000);
	c->Print(PNG1.c_str());
	//c->WaitPrimitive();

	TCanvas* cEM_EM = new TCanvas("canvasEM_EM", "cEM_EM", 1024, 980);
	cEM_EM->Divide(1,2);
	//c->cd();
	cEM_EM->cd(1);
	hist_tdiffsEM_EM->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_tdiffsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymin,hist_tdiffsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymax);
	cEM_EM->cd(2);
	hist_trmsEM_EM->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymax);
	cEM_EM->Update();
	//sleep(2000);
	//ch->WaitPrimitive();
	cEM_EM->Print(PNG2.c_str());

	TCanvas* cEM_JET = new TCanvas("canvasEM_JET", "cEM_JET", 1024, 980);
	cEM_JET->Divide(1,2);
	//c->cd();
	cEM_JET->cd(1);
	hist_tdiffsEM_JET->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_tdiffsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymin,hist_tdiffsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymax);
	cEM_JET->cd(2);
	hist_trmsEM_JET->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymax);
	cEM_JET->Update();
	//sleep(2000);
	cEM_JET->Print(PNG3.c_str());

	unsigned int binsHAD[4]={5, 63, 19, 49};
	TCanvas* ch = new TCanvas("canvash", "ch", 1024, 980);
	ch->Divide(1,2);
	ch->cd(1);
	hist_tdiffsHAD->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymax);
	ch->cd(2);
	hist_trmsHAD->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymax);
	ch->Update();
	//sleep(2000);
	ch->Print(PNG4.c_str());
	//ch->WaitPrimitive();

	TCanvas* cHAD_EM = new TCanvas("canvasHAD_EM", "cHAD_EM", 1024, 980);
	cHAD_EM->Divide(1,2);
	cHAD_EM->cd(1);
	hist_tdiffsHAD_EM->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymax);
	cHAD_EM->cd(2);
	hist_trmsHAD_EM->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymax);
	cHAD_EM->Update();
	//sleep(2000);
	cHAD_EM->Print(PNG5.c_str());

	TCanvas* cHAD_JET = new TCanvas("canvasHAD_JET", "cHAD_JET", 1024, 980);
	cHAD_JET->Divide(1,2);
	cHAD_JET->cd(1);
	hist_tdiffsHAD_JET->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymax);
	cHAD_JET->cd(2);
	hist_trmsHAD_JET->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymax);
	cHAD_JET->Update();
	//sleep(2000);
	cHAD_JET->Print(PNG6.c_str());

	TCanvas* c3 = new TCanvas("canvas3EM", "c3EM", 1280, 1024);
	c3->Divide(2,2);
	c3->cd(1);
	hist_EMB->Draw();
	//draw_box(hist_EMB, "EMB");
	c3->cd(2);
	hist_OVERLAP->Draw();
	c3->cd(3);
	hist_EMEC->Draw();
	c3->cd(4);
	hist_FCAL1->Draw();
	c3->Update();
	//sleep(2000);
	c3->Print(PNG7.c_str());
	//c3->WaitPrimitive();

	TCanvas* c4 = new TCanvas("canvas4HAD", "c4HAD", 1280, 1024);
	c4->Divide(2,2);
	c4->cd(1);
	hist_TileLB->Draw();
	c4->cd(2);
	hist_TileEB->Draw();
	c4->cd(3);
	hist_HEC->Draw();
	c4->cd(4);
	hist_FCAL2->Draw();
	c4->Update();
	//sleep(2000);
	c4->Print(PNG8.c_str());
	//c4->WaitPrimitive();

	//TFile* hitmap_out = new TFile("/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/hit_maps_80MHz.root", "RECREATE");

	//theApp.Run();
	//hist_trmsEM->Draw();
	//hist_tdiffsHAD->Draw();
	//hist_trmsHAD->Draw();	

	TFile* out = new TFile(outputfile.c_str(), "RECREATE");
	hist_goodfitsEM->SetMinimum(0);
	hist_goodfitsHAD->SetMinimum(0);
	hist_goodfitsEM->Write();
	hist_goodfitsHAD->Write();
	hist_tdiffsEM->Write();
	hist_trmsEM->Write();
	hist_tdiffsHAD->Write();
	hist_trmsHAD->Write();	
	hist_EMB->Write();	
	hist_OVERLAP->Write();		
	hist_EMEC->Write();		
	hist_FCAL1->Write();		
	hist_TileLB->Write();		
	hist_TileEB->Write();
	hist_HEC->Write();	
	hist_FCAL2->Write();
	hist_FCAL3->Write();		
	out->Close();
	timing_hists->Close();
	
	//free(input_name);	

/*	TH1* hist_timeLB = new TH1("hist_timeLB", "timing distribution, time offset [ns], events", 50, -25, 25);

        const char *dirname="run266904/";
	const char *ext=".root"

	TSystemDirectory dir(dirname, dirname);
	TList *files = dir.GetListOfFiles();

	if (files) {
	      TSystemFile *file;
	      TString fname;
	      TIter next(files);
	      while ((file=(TSystemFile*)next())) {
		 fname = file->GetName();
		 if (!file->IsDirectory() && fname.EndsWith(ext) && fname.StartsWith("TimingPropagation_run")) {
		    cout << fname << endl;
		    TFile f(fname);
		 }
	      }
	}	*/

/*
	//std::map<unsigned int,std::vector<double>> Map_Tdiffs = *Map_Tdiffs_point;
	std::cout<<"map from map pointer retrieved"<<std::endl;


	std::cout<<"hists created"<<std::endl;

	std::map<unsigned int,std::vector<double>>::iterator it;
	std::cout<<"map iterator created."<<std::endl;
	for ( it = Map_Tdiffs->begin(); it != Map_Tdiffs->end(); it++ ){
		unsigned int id = it->first;
		std::cout<<"id is... "<<id<<std::endl;
		std::vector<double> tdiff_vec = it->second;
		double tdiff_sum =0;
		TH1* hist_tdistr = new TH1("hist_tdistr", "timing distribution, time offset [ns], events", 50, -25, 25);
		
		
		for (std::vector<double>::iterator tdiff=tdiff_vec.begin(); tdiff != tdiff_vec.end(); tdiff++) hist_tdistr->Fill(*tdiff);
		double avg = hist_tdistr->GetMean();
		double rms = hist_tdistr->GetRMS();
		double eta = Eta[id];
		double phi = Phi[id];
		hist_tdiffs->SetBinContent(eta, phi, avg);
		hist_trms->SetBinContent(eta, phi, rms);
	}

	*/		
	
}

void draw_qualitytimedistr(const char * run, const char * outputfile="/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/summary_hists.root") {

	// setting up calo quality maps
	TH2TT* hist_qualEM = new TH2TT("hist_qualEM", "avg calo quality (EM);#eta;#phi");
	TH2TT* hist_qrmsEM = new TH2TT("hist_qrmsEM", "rms of avg calo quality (EM);#eta;#phi");
	TH2TT* hist_qualHAD = new TH2TT("hist_qualHAD", "avg calo quality (HAD);#eta;#phi");
	TH2TT* hist_qrmsHAD = new TH2TT("hist_qrmsHAD", "rms of avg calo quality (HAD);#eta;#phi");
        hist_qualEM->Fillall(-1000);
        hist_qrmsEM->Fillall(-1000);
        hist_qualEM->SetMinimum(0);
        hist_qrmsEM->SetMinimum(0);
        hist_qualEM->SetStats(0);
        hist_qrmsEM->SetStats(0);
        hist_qualHAD->Fillall(-1000);
        hist_qrmsHAD->Fillall(-1000);
        hist_qualHAD->SetMinimum(0);
        hist_qrmsHAD->SetMinimum(0);
        hist_qualHAD->SetStats(0);
        hist_qrmsHAD->SetStats(0);
  	hist_qualEM->SetOption("colz");
  	hist_qrmsEM->SetOption("colz");
  	hist_qualHAD->SetOption("colz");
  	hist_qrmsHAD->SetOption("colz");

	// property maps 
	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;
	std::map<unsigned int,double> Qmean, Qrms;
	std::map<unsigned int,double> QmeanHAD, QrmsHAD;
	std::map<unsigned int,double> NFits;

	TTxml m_props;
	m_props.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");

	Eta = m_props.Get("eta");
	Phi = m_props.Get("phi");
	Part = m_props.Get("part");

	if (!(Eta.size()==7168)){
		std::cout<<"size of TT parameter maps of wrong size. Should be 7168 but is "<<Eta.size()<<"."<<std::endl;
	}

	set_plot_style();


	char input_name[100];
	if (run=="combined") sprintf(input_name,"/afs/cern.ch/work/c/cantel/private/collisions/IDPropagation_%s.root", run);
	else sprintf(input_name,"/afs/cern.ch/work/c/cantel/private/collisions/%s/IDPropagation_combined%s.root", run, run);

	TFile* ID_hists = new TFile(input_name);
	std::cout<<"opened ID_hists file"<<std::endl;

	ID_hists->cd("EM");
	TDirectory *QvsTEM = gDirectory->GetDirectory("QvsT");
	QvsTEM->cd(); 
	TIter nextEM2( gDirectory->GetListOfKeys() );

	TKey* key;

	unsigned int cnt=0;

	while( (key=(TKey*) nextEM2() )){
		std::cout <<"("<<cnt<< ") key is... " << key->GetName() << std::endl;
		TString histName; 
		histName= key->GetName();
		TObject *o = (TH1D*) key->ReadObj()->Clone(histName);
		if(!o->InheritsFrom("TH2D")) continue; //only look at histos
		TH2D *hist_q = (TH2D*)o->Clone();
		o->Delete();
		TString idstring = histName(20, histName.Length()-20);
		unsigned int id=  idstring.Atoi(); 
		//std::cout << "histname is... " << histName << std::endl;
		//std::cout << "corresponding id is... " << id << std::endl;
		double qual = hist_q->GetMean(2);
		double qrms = hist_q->GetRMS(2);
		Qmean[id] = qual;	
		Qrms[id] = qrms;
		histName.Clear();
		idstring.Clear();
		hist_q->Delete();
		//if (cnt%100 == 0)  gObjectTable->Print();
		cnt++;
	}

	cnt=0;

	std::map<unsigned int,double>::iterator it;
  	for ( it = Qmean.begin(); it != Qmean.end(); it++ ){
		unsigned int id = it->first;
		double eta = Eta[id];
		double phi = Phi[id];
		double qual = Qmean[id];
		double qrms = Qrms[id];
		int etabin = hist_qualEM->GetXaxis()->FindBin(eta);
		int phibin = hist_qualEM->GetYaxis()->FindBin(phi);
		int part = (int) Part[id];
		if (part<=3){
			//std::cout <<"("<<cnt<< ") qual mean for ID "<<id<<" in EM is ... " << qual << std::endl;
			hist_qualEM->SetBinContent(etabin, phibin, qual);
			hist_qrmsEM->SetBinContent(etabin, phibin, qrms);
			//cnt++;
		}
		else {
			std::cout<<"WARNING: part for ID 0x"<< id<<" returned not in EM layer. Histos in wrong file."<<std::endl;
		}
	}



	//Qmean.clear();
	//Qrms.clear();

	ID_hists->cd("HAD");
	TDirectory *QvsTHAD = gDirectory->GetDirectory("QvsT");
	QvsTHAD->cd();
	TIter nextHAD2( gDirectory->GetListOfKeys() );
	std::cout<<"about to read in quality in HAD layer... "<<std::endl;

	std::string histName;

	while( (key=(TKey*) nextHAD2() )){
		std::cout <<"("<<cnt<< ") key is... " << key->GetName() << std::endl;
		TString histName; 
		histName= key->GetName();
		TObject *o = (TH1D*) key->ReadObj()->Clone(histName);
		if(!o->InheritsFrom("TH2D")) continue; //only look at histos
		TH2D *hist_q = (TH2D*)o->Clone();
		o->Delete();
		TString idstring = histName(20, histName.Length()-20);
		unsigned int id=  idstring.Atoi(); 
		//std::cout << "histname is... " << histName << std::endl;
		//std::cout << "corresponding id is... " << id << std::endl;
		double qual = hist_q->GetMean(2);
		double qrms = hist_q->GetRMS(2);
		Qmean[id] = qual;
		Qrms[id] = qrms;
		histName.Clear();
		idstring.Clear();
		hist_q->Delete();
		//if (cnt%100 == 0)  gObjectTable->Print();
		cnt++;
	}

	cnt=0;

	std::map<unsigned int,double>::iterator itHAD;
  	for ( itHAD = QmeanHAD.begin(); itHAD != QmeanHAD.end(); itHAD++ ){
		unsigned int id = it->first;
		double eta = Eta[id];
		double phi = Phi[id];
		double qual = QmeanHAD[id];
		double qrms = QrmsHAD[id];
		int etabin = hist_qualHAD->GetXaxis()->FindBin(eta);
		int phibin = hist_qualHAD->GetYaxis()->FindBin(phi);
		int part = (int) Part[id];
		if (part<=3){
			//std::cout <<"("<<cnt<< ") qual mean for ID "<<id<<" in HAD is ... " << qual << std::endl;
			hist_qualHAD->SetBinContent(etabin, phibin, qual);
			hist_qrmsHAD->SetBinContent(etabin, phibin, qrms);
			//cnt++;
		}
		else {
			std::cout<<"WARNING: part for ID 0x"<< id<<" returned not in HAD layer. Histos in wrong file."<<std::endl;
		}
	}

	Qmean.clear();
	Qrms.clear();
	QmeanHAD.clear();
	QrmsHAD.clear();

	TCanvas* c2 = new TCanvas("canvas2", "c2", 1280, 1024);
	c2->Divide(2,2);
	c2->cd(1);
	hist_qualEM->Draw();
	c2->cd(2);
	hist_qrmsEM->Draw();
	c2->cd(3);
	hist_qualHAD->Draw();
	c2->cd(4);
	hist_qrmsHAD->Draw();
	c2->Update();
	c2->Print("caloquality_maps.png");
	//c2->WaitPrimitive();

	TFile* out = new TFile(outputfile, "RECREATE");
	hist_qualEM->Write();
	hist_qrmsEM->Write();
	hist_qualHAD->Write();
	hist_qrmsHAD->Write();		
	out->Close();
	ID_hists->Close();


}

void draw_qualitydistr(const char * run){//, const char * outputfile="/afs/cern.ch/work/c/cantel/private/collisions/Timing80MHz2016/summary_hists.root") {

	// property maps 
	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;

	TTxml m_props;
	m_props.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");

	Eta = m_props.Get("eta");
	Phi = m_props.Get("phi");
	Part = m_props.Get("part");

	set_plot_style();

	char input_name[100];
	if (run=="combined") sprintf(input_name,"/afs/cern.ch/work/c/cantel/private/collisions/TimingFitHistos_%s.root", run);
	else sprintf(input_name,"/afs/cern.ch/work/c/cantel/private/collisions/%s/TimingFitHistos_combined%s.root", run, run);

	TFile* summary_hists = new TFile(input_name);
	std::cout<<"opened fithistos file"<<std::endl;

	summary_hists->cd("HistsFit");
	//gDirectory->GetListOfKeys()->Print();
	TKey* key;

	key = gDirectory->GetKey("EMB_qualitytimecorr");
	TObject *o = (TH2D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2D *EMB = (TH2D*)o;//new TH2TT("NFitsEM1", "emN1");

	key = gDirectory->GetKey("OVERLAP_qualitytimecorr");
	o = (TH2D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2D *OVERLAP = (TH2D*)o;//new TH2TT("NFitsEM1", "emN1");

	key = gDirectory->GetKey("EMEC_qualitytimecorr");
	o = (TH2D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2D *EMEC = (TH2D*)o;//new TH2TT("NFitsEM1", "emN1");

	key = gDirectory->GetKey("FCALEM_qualitytimecorr");
	o = (TH2D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2D *FCALEM = (TH2D*)o;//new TH2TT("NFitsEM1", "emN1");

       // ---------------------------------------------------------------------

	key = gDirectory->GetKey("FCALHAD_qualitytimecorr");
	o = (TH2D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2D *FCALHAD = (TH2D*)o;//new TH2TT("NFitsEM1", "emN1");

	key = gDirectory->GetKey("HEC_qualitytimecorr");
	o = (TH2D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2D *HEC = (TH2D*)o;//new TH2TT("NFitsEM1", "emN1");

	key = gDirectory->GetKey("TileEB_qualitytimecorr");
	o = (TH2D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2D *TileEB = (TH2D*)o;//new TH2TT("NFitsEM1", "emN1");
		
	key = gDirectory->GetKey("TileLB_qualitytimecorr");
	o = (TH2D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
	TH2D *TileLB = (TH2D*)o;//new TH2TT("NFitsEM1", "emN1");
	
	int rebin = 40; 
/*	double qualcut = 20000;
	double qualcut_hec = 5000;*/

	char PNG1[40];
	sprintf(PNG1, "caloquality_EM_run%s.png", run);
	char PNG2[40];
	sprintf(PNG2, "caloquality_HAD_run%s.png", run);

	TCanvas* c = new TCanvas("canvasEM", "cEM", 1680, 1050);
	c->Divide(2,2);
	c->cd(1);
	//gPad->SetLogy();
	EMB->RebinY(rebin);
	EMB->Draw();
	//draw_line(qualcut, EMB->GetMaximum());
	c->cd(2);
	//gPad->SetLogy();
	OVERLAP->RebinY(rebin);
	OVERLAP->Draw();
	//draw_line(qualcut, OVERLAP->GetMaximum());
	c->cd(3);
	//gPad->SetLogy();
	EMEC->RebinY(rebin);
	EMEC->Draw();
	//draw_line(qualcut, EMEC->GetMaximum());
	c->cd(4);
	//gPad->SetLogy();
	FCALEM->RebinY(rebin);
	FCALEM->Draw();	
	//draw_line(qualcut, FCAL1->GetMaximum());

	c->Update();
	//sleep(2000);
	c->Print(PNG1);
	//c->WaitPrimitive();


	TCanvas* c2 = new TCanvas("canvasHAD", "cHAD", 1680, 1050);
	c2->Divide(2,2);
	c2->cd(1);
	//gPad->SetLogy();
	TileLB->RebinY(rebin);
	TileLB->Draw();
	//draw_line(qualcut, TileLB->GetMaximum());
	c2->cd(2);
	//gPad->SetLogy();
	TileEB->RebinY(rebin);
	TileEB->Draw();
	//draw_line(qualcut, TileEB->GetMaximum());
	c2->cd(3);
	//gPad->SetLogy();
	HEC->RebinY(rebin);
	HEC->Draw();
	//draw_line(qualcut_hec, HEC->GetMaximum());
	c2->cd(4);
	//gPad->SetLogy();
	//FCAL2->Rebin(rebin);
	FCALHAD->RebinY(rebin);
	//FCALHAD->Add(FCAL3);
	FCALHAD->SetTitle("caloquality distribution in fcal had layer");
	FCALHAD->Draw();
	//draw_line(qualcut, FCAL2->GetMaximum());
	/*c2->cd(4);
	gPad->SetLogy();
	FCAL3->Rebin(rebin);
	FCAL3->Draw();	
	draw_line(qualcut, FCAL3->GetMaximum()); */

	c2->Update();
	//sleep(2000);
	c2->Print(PNG2);
	//c2->WaitPrimitive();

}

void draw_tdiffmaps_old(const char * outputfile="/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/tdiff_maps.root") {
	
	// property maps 
	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;
	std::map<unsigned int,double> TDiff, Trms;
	std::map<unsigned int,double> NFits;

	TTxml m_props;
	m_props.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");

	Eta = m_props.Get("eta");
	Phi = m_props.Get("phi");
	Part = m_props.Get("part");

	if (!(Eta.size()==7168)){
		std::cout<<"size of TT parameter maps of wrong size. Should be 7168 but is "<<Eta.size()<<"."<<std::endl;
	}

	set_plot_style();

	TFile* timing_hists = new TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingPropagation_combined.root");
	std::cout<<"opened timing_hists file"<<std::endl;

	int minval = -10;
	int maxval = 10;

	// setting up timing maps
	TH2TT* hist_tdiffsEM = new TH2TT("hist_tdiffsEM", "avg timing offsets (EM);#eta;#phi");
	TH2TT* hist_trmsEM = new TH2TT("hist_trmsEM", "rms of avg timing offsets (EM);#eta;#phi");
	TH2TT* hist_tdiffsEM_EM = new TH2TT("hist_tdiffsEM_EM", "avg timing offsets EM triggers (EM);#eta;#phi");
	TH2TT* hist_trmsEM_EM = new TH2TT("hist_trmsEM_EM", "rms of avg timing offsets EM triggers (EM);#eta;#phi");
	TH2TT* hist_tdiffsEM_JET = new TH2TT("hist_tdiffsEM_JET", "avg timing offsets JET triggers (EM);#eta;#phi");
	TH2TT* hist_trmsEM_JET = new TH2TT("hist_trmsEM_JET", "rms of avg timing offsets JET triggers (EM);#eta;#phi");
	TH2TT* hist_tdiffsHAD = new TH2TT("hist_tdiffsHAD", "avg timing offsets (HAD);#eta;#phi");
	TH2TT* hist_trmsHAD = new TH2TT("hist_trmsHAD", "rms of avg timing offsets (HAD);#eta;#phi");
	TH2TT* hist_tdiffsHAD_EM = new TH2TT("hist_tdiffsHAD_EM", "avg timing offsets EM triggers (HAD);#eta;#phi");
	TH2TT* hist_trmsHAD_EM = new TH2TT("hist_trmsHAD_EM", "rms of avg timing offsets EM triggers (HAD);#eta;#phi");
	TH2TT* hist_tdiffsHAD_JET = new TH2TT("hist_tdiffsHAD_JET", "avg timing offsets JET triggers (HAD);#eta;#phi");
	TH2TT* hist_trmsHAD_JET = new TH2TT("hist_trmsHAD_JET", "rms of avg timing offsets JET triggers (HAD);#eta;#phi");
	TH2TT* hist_goodfitsHAD = new TH2TT("hist_goodfitsfromEntriesHAD", "no. of good fits (HAD);#eta;#phi");
	TH2TT* hist_goodfitsEM = new TH2TT("hist_goodfitsfromEntriesEM", "no. of good fits (EM);#eta;#phi");
        hist_tdiffsEM->Fillall(-1000);
        hist_trmsEM->Fillall(-1000);
        hist_tdiffsEM_EM->Fillall(-1000);
        hist_trmsEM_EM->Fillall(-1000);
        hist_tdiffsEM_JET->Fillall(-1000);
        hist_trmsEM_JET->Fillall(-1000);
  	hist_goodfitsHAD->Fillall(-1000);
  	hist_goodfitsEM->Fillall(-1000);
        hist_tdiffsEM_EM->SetMinimum(minval);
        hist_trmsEM_EM->SetMinimum(0);
        hist_tdiffsEM_JET->SetMinimum(minval);
        hist_trmsEM_JET->SetMinimum(0);
        hist_tdiffsEM->SetMinimum(minval);
        hist_trmsEM->SetMinimum(0);
        hist_tdiffsEM->SetStats(0);
        hist_trmsEM->SetStats(0);
        hist_tdiffsEM_EM->SetMinimum(minval);
        hist_trmsEM_EM->SetMinimum(0);
        hist_tdiffsEM_EM->SetStats(0);
        hist_trmsEM_EM->SetStats(0);
	hist_tdiffsEM_EM->SetOption("colz");
  	hist_trmsEM_EM->SetOption("colz");

        hist_tdiffsEM_JET->SetMinimum(minval);
        hist_trmsEM_JET->SetMinimum(0);
        hist_tdiffsEM_JET->SetStats(0);
        hist_trmsEM_JET->SetStats(0);
	hist_tdiffsEM_JET->SetOption("colz");
  	hist_trmsEM_JET->SetOption("colz");

        hist_tdiffsHAD->Fillall(-1000);
        hist_trmsHAD->Fillall(-1000);
        hist_tdiffsHAD->SetMinimum(minval);
        hist_trmsHAD->SetMinimum(0);
	hist_tdiffsHAD->SetMaximum(maxval);
        hist_trmsHAD->SetMaximum(3);
        hist_tdiffsHAD->SetStats(0);
        hist_trmsHAD->SetStats(0);

        hist_tdiffsHAD_EM->Fillall(-1000);
        hist_trmsHAD_EM->Fillall(-1000);
        hist_tdiffsHAD_EM->SetMinimum(minval);
        hist_trmsHAD_EM->SetMinimum(0);
	hist_tdiffsHAD_EM->SetMaximum(maxval);
        hist_trmsHAD_EM->SetMaximum(3);
        hist_tdiffsHAD_EM->SetStats(0);
        hist_trmsHAD_EM->SetStats(0);
 	hist_tdiffsHAD_EM->SetOption("colz");
  	hist_trmsHAD_EM->SetOption("colz");

        hist_tdiffsHAD_JET->Fillall(-1000);
        hist_trmsHAD_JET->Fillall(-1000);
        hist_tdiffsHAD_JET->SetMinimum(minval);
        hist_trmsHAD_JET->SetMinimum(0);
	hist_tdiffsHAD_JET->SetMaximum(maxval);
        hist_trmsHAD_JET->SetMaximum(3);
        hist_tdiffsHAD_JET->SetStats(0);
        hist_trmsHAD_JET->SetStats(0);
 	hist_tdiffsHAD_JET->SetOption("colz");
  	hist_trmsHAD_JET->SetOption("colz");

  	hist_goodfitsHAD->SetStats(0);
  	hist_goodfitsEM->SetStats(0);
  	hist_tdiffsEM->SetOption("colz");
  	hist_trmsEM->SetOption("colz");
  	hist_tdiffsHAD->SetOption("colz");
  	hist_trmsHAD->SetOption("colz");
  	hist_goodfitsHAD->SetOption("colz");
  	hist_goodfitsEM->SetOption("colz");

	//setting up 1D profiles

	TH1D* hist_EMB = new TH1D("hist_EMB", "timing distribution in EMB; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_OVERLAP = new TH1D("hist_OVERLAP", "timing distribution in OVERLAP; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_EMEC = new TH1D("hist_EMEC", "timing distribution in EMEC; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_FCAL1 = new TH1D("hist_FCAL1", "timing distribution in FCAL1; time offset [ns]; events", 122,-30.5,30.5);

	TH1D* hist_TileLB = new TH1D("hist_TileLB", "timing distribution in TileLB; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_TileEB = new TH1D("hist_TileEB", "timing distribution in TileEB; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_HEC = new TH1D("hist_HEC", "timing distribution in HEC; time offset [ns]; events", 122,-30.5,30.5);
	TH1D* hist_FCALHAD = new TH1D("hist_FCALHAD", "timing distribution in FCALHAD; time offset [ns]; events", 122,-30.5,30.5);

	timing_hists->cd("timing");
	TIter next( gDirectory->GetListOfKeys() );

	std::string histName;
	TKey* key;
	while( (key=(TKey*) next() )){
		//std::cout << "key is... " << key->GetName() << std::endl;
		histName = key->GetName();
		TObject *o = (TH1D*) key->ReadObj()->Clone(TString::Format("%s", histName.c_str()));
		if(!o->InheritsFrom("TH1")) continue; //only look at histos
		Long64_t foundtime = histName.find("hist_tdistr_id");
		//std::cout<<"found time is "<<foundtime<<std::endl;
		if ( foundtime<histName.length()){
			TH1 *hist_t = (TH1*)o;
			//printf("key: %s points to an object of class: %s at %dn",key->GetName(),key->GetClassName(),key->GetSeekKey());
			unsigned int id=  atoi(histName.substr(14, histName.length()-14).c_str()); 
			//std::cout << "histname is... " << histName << std::endl;
			//std::cout << "corresponding id is... " << id << std::endl;
			double eta = Eta[id];
			double phi = Phi[id];
			int etabin = hist_tdiffsEM->GetXaxis()->FindBin(eta);
			int phibin = hist_tdiffsEM->GetYaxis()->FindBin(phi);
			unsigned int maxbin = hist_t->GetMaximumBin();
			double startrange = hist_t->GetXaxis()->GetBinCenter(maxbin)-10.;
			double endrange = hist_t->GetXaxis()->GetBinCenter(maxbin)+10.;			
			hist_t->Fit("gaus", "", "", startrange, endrange);
			unsigned int fits = hist_t->GetEntries();
			double time = hist_t->GetFunction("gaus")->GetParameter(1);
			double rms = hist_t->GetFunction("gaus")->GetParameter(2);
// 			double time = hist_t->GetMean();
// 			double rms = hist_t->GetRMS();
			int part = (int) Part[id];
			TDiff[id]=time;
			Trms[id]=rms;
			if (part<=3){
				if (time>10 || time<-10) std::cout << "WARNING, large time offset for ID: "<< id<<" part: "<<part<<" eta: "<<eta<<" phi: "<<phi<<" time: "<<time<<std::endl;
				hist_tdiffsEM->SetBinContent(etabin, phibin, time);
				hist_trmsEM->SetBinContent(etabin, phibin, rms);
				hist_goodfitsEM->SetBinContent(etabin, phibin, fits);
				if (part==0) hist_FCAL1->Fill(time);
				if (part==1) hist_EMEC->Fill(time);
				if (part==2) hist_OVERLAP->Fill(time);
				if (part==3) hist_EMB->Fill(time);
			}
			else if (part>3 && part<=8){
					if (time>10 || time<-10) std::cout << "WARNING, large time offset for ID: "<< id<<" part: "<<part<<" eta: "<<eta<<" phi: "<<phi<<" time: "<<time<<std::endl;
					TDiff[id]=time;
					Trms[id]=rms;
					hist_tdiffsHAD->SetBinContent(etabin, phibin, time);
					hist_trmsHAD->SetBinContent(etabin, phibin, rms);
					hist_goodfitsHAD->SetBinContent(etabin, phibin, fits);
					if (part==4 || part==5) hist_FCALHAD->Fill(time);
					if (part==6) hist_HEC->Fill(time);
					if (part==7) hist_TileEB->Fill(time);
					if (part==8) hist_TileLB->Fill(time);
			}
			else {
				std::cout<<"WARNING: part for ID 0x"<< id<<" returned not valid."<<std::endl;
			}
		}
	}


	std::map<unsigned int,double>::iterator it;
	for ( it = Eta.begin(); it != Eta.end(); it++ ){
			unsigned int id = it->first;
			double eta = Eta[id];
			double phi = Phi[id];
			int etabin = hist_goodfitsEM->GetXaxis()->FindBin(eta);
			int phibin = hist_goodfitsEM->GetYaxis()->FindBin(phi);
			int part = (int) Part[id];
			if (part<=3){
				NFits[id]=hist_goodfitsEM->GetBinContent(etabin, phibin);
			}
			if (part>3 && part<=8){
				NFits[id]=hist_goodfitsHAD->GetBinContent(etabin, phibin);
			}
	}		

	TTxml ftime_values;
	ftime_values.Add("Eta", Eta);
	ftime_values.Add("Phi", Phi);
	ftime_values.Add("Part", Part);
	ftime_values.Add("TDiff", TDiff);
	ftime_values.Add("Trms", Trms);
	ftime_values.Add("NFits", NFits);
	ftime_values.Write("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/TimeValuesJuly2015.xml");

	std::cout<<"size of histograms..."<<hist_tdiffsEM->GetEntries()<<" and "<<hist_tdiffsEM->GetEntries()<<std::endl;

	double ymin = 0;
	double ymax = 6.3;
	TLine l;
	l.SetLineWidth(2);
	unsigned int bins[6]={5, 63, 19, 48, 20, 49};
	
	/*
	double ls=0.06;
	double os=0.5;

	hist_tdiffsEM->GetXaxis()->SetLabelSize(ls);
	hist_tdiffsEM->GetYaxis()->SetLabelSize(ls);
	hist_tdiffsEM->GetXaxis()->SetLabelOffset(os);
	hist_tdiffsEM->GetYaxis()->SetLabelOffset(os);

	hist_trmsEM->GetXaxis()->SetLabelSize(ls);
	hist_trmsEM->GetYaxis()->SetLabelSize(ls);
	hist_trmsEM->GetXaxis()->SetLabelOffset(os);
	hist_trmsEM->GetYaxis()->SetLabelOffset(os);

	hist_tdiffsHAD->GetXaxis()->SetLabelSize(ls);
	hist_tdiffsHAD->GetYaxis()->SetLabelSize(ls);
	hist_tdiffsHAD->GetXaxis()->SetLabelOffset(os);
	hist_tdiffsHAD->GetYaxis()->SetLabelOffset(os);

	hist_trmsHAD->GetXaxis()->SetLabelSize(ls);
	hist_trmsHAD->GetYaxis()->SetLabelSize(ls);
	hist_trmsHAD->GetXaxis()->SetLabelOffset(os);
	hist_trmsHAD->GetYaxis()->SetLabelOffset(os);*/

	TCanvas* c = new TCanvas("canvas1", "c1", 1024, 980);
	c->Divide(1,2);
	//c->cd();
	c->cd(1);
	hist_tdiffsEM->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_tdiffsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymin,hist_tdiffsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymax);
	c->cd(2);
	hist_trmsEM->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(bins[bin]),ymax);
	c->Update();
	//sleep(2000);
	c->Print("tdiff_mapEM_old.png");
	//c->WaitPrimitive();

	unsigned int binsHAD[4]={5, 63, 19, 49};
	TCanvas* ch = new TCanvas("canvash", "ch", 1024, 980);
	ch->Divide(1,2);
	ch->cd(1);
	hist_tdiffsHAD->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymax);
	ch->cd(2);
	hist_trmsHAD->Draw();
	for (unsigned int bin=0; bin<6; bin++)
		l.DrawLine(hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymin,hist_trmsEM->GetXaxis()->GetBinLowEdge(binsHAD[bin]),ymax);
	ch->Update();
	//sleep(2000);
	ch->Print("tdiff_mapsHAD_old.png");
	//ch->WaitPrimitive();



	TCanvas* c3 = new TCanvas("canvas3EM", "c3EM", 1280, 1024);
	c3->Divide(2,2);
	c3->cd(1);
	hist_EMB->Draw();
	//draw_box(hist_EMB, "EMB");
	c3->cd(2);
	hist_OVERLAP->Draw();
	c3->cd(3);
	hist_EMEC->Draw();
	c3->cd(4);
	hist_FCAL1->Draw();
	c3->Update();
	//sleep(2000);
	c3->Print("tdiff_profiles_EM_old.png");
	//c3->WaitPrimitive();

	TCanvas* c4 = new TCanvas("canvas4HAD", "c4HAD", 1280, 1024);
	c4->Divide(2,2);
	c4->cd(1);
	hist_TileLB->Draw();
	c4->cd(2);
	hist_TileEB->Draw();
	c4->cd(3);
	hist_HEC->Draw();
	c4->cd(4);
	hist_FCALHAD->Draw();
	c4->Update();
	//sleep(2000);
	c4->Print("tdiff_profiles_HAD_old.png");
	//c4->WaitPrimitive();

	TFile* hitmap_out = new TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/hit_maps.root", "UPDATE");
	hist_goodfitsEM->Write();
	hist_goodfitsHAD->Write();
	hitmap_out->Close();

	TFile* out = new TFile(outputfile, "RECREATE");
	hist_tdiffsEM->Write();
	hist_trmsEM->Write();
	hist_tdiffsHAD->Write();
	hist_trmsHAD->Write();
	hist_EMB->Write();	
	hist_OVERLAP->Write();		
	hist_EMEC->Write();		
	hist_FCAL1->Write();		
	hist_TileLB->Write();		
	hist_TileEB->Write();
	hist_HEC->Write();	
	hist_FCALHAD->Write();									
	out->Close();
	timing_hists->Close();
		
}

void draw_parametermaps(const char * name, const char * file) {
  
      std::cout<<"drawing parameter maps now......"<<std::endl;

  
       TFile* outputGausfits = new TFile("Gaussfitcheck.root", "RECREATE");
       outputGausfits->mkdir("sigLeft");
       outputGausfits->mkdir("sigRight");
    
	
	// property maps 
	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;
	std::map<unsigned int,double> SigL, SigR, SigLrms, SigRrms;
	std::map<unsigned int,double> NFits;
	std::map<unsigned int,double> Chi2NDF_sigL;
	std::map<unsigned int,double> Chi2NDF_sigR;

	
	std::cout<<"reading in TT properties..."<<std::endl;

	TTxml m_props;
	m_props.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");

	Eta = m_props.Get("eta");
	Phi = m_props.Get("phi");
	Part = m_props.Get("part");

	if (!(Eta.size()==7168)){
		std::cout<<"size of TT parameter maps of wrong size. Should be 7168 but is "<<Eta.size()<<"."<<std::endl;
	}

	//set_plot_style();
	
	std::cout<<"about to open parameter file."<<std::endl;

	char input_name[150];
	sprintf(input_name,"/afs/cern.ch/work/c/cantel/private/collisions/%s", file);


	TFile* pars_hists = new TFile(input_name);
	std::cout<<"opened parameter hists file"<<std::endl;

	int minval = 0;
	int maxval = 40;
	int maxvalrms = 4;

	// setting up parameter maps
	TH2TT* hist_siglEM = new TH2TT("hist_siglEM", "avg sigma left (EM);#eta;#phi");
	TH2TT* hist_siglrmsEM = new TH2TT("hist_siglrmsEM", "rms of avg sigma left (EM);#eta;#phi");
	TH2TT* hist_sigrEM = new TH2TT("hist_sigrEM", "avg sigma right (EM);#eta;#phi");
	TH2TT* hist_sigrrmsEM = new TH2TT("hist_sigrrmsEM", "rms of avg sigma right (EM);#eta;#phi");
	//TH2TT* hist_uvsaEM = new TH2TT("hist_uvsaEM", "avg uvsa (EM);#eta;#phi");
	//TH2TT* hist_uvsarmsEM = new TH2TT("hist_uvsarmsEM", "rms of avg uvsa (EM);#eta;#phi");
	
	TH2TT* hist_siglHAD = new TH2TT("hist_siglHAD", "avg sigma left (HAD);#eta;#phi");
	TH2TT* hist_siglrmsHAD = new TH2TT("hist_siglrmsHAD", "rms of avg sigma left (HAD);#eta;#phi");
	TH2TT* hist_sigrHAD = new TH2TT("hist_sigrHAD", "avg sigma right (HAD);#eta;#phi");
	TH2TT* hist_sigrrmsHAD = new TH2TT("hist_sigrrmsHAD", "rms of avg sigma right (HAD);#eta;#phi");
	//TH2TT* hist_uvsaHAD = new TH2TT("hist_uvsaHAD", "avg uvsa (HAD);#eta;#phi");
	//TH2TT* hist_uvsarmsHAD = new TH2TT("hist_uvsarmsHAD", "rms of avg uvsa (HAD);#eta;#phi");
		
	TH2TT* hist_goodfitsHAD = new TH2TT("hist_goodfitsfromEntriesHAD", "no. of good fits (HAD);#eta;#phi");
	TH2TT* hist_goodfitsEM = new TH2TT("hist_goodfitsfromEntriesEM", "no. of good fits (EM);#eta;#phi");
	
	hist_siglEM->Fillall(-1000);
	hist_sigrEM->Fillall(-1000);
	hist_siglrmsEM->Fillall(-1000);
	hist_sigrrmsEM->Fillall(-1000);
	hist_siglHAD->Fillall(-1000);
	hist_siglrmsHAD->Fillall(-1000);
	hist_sigrHAD->Fillall(-1000);
	hist_sigrrmsHAD->Fillall(-1000);
	
  	hist_goodfitsHAD->Fillall(-1000);
  	hist_goodfitsEM->Fillall(-1000);

	hist_siglEM->SetMinimum(minval);
	hist_sigrEM->SetMinimum(minval);
	hist_siglrmsEM->SetMinimum(minval);
	hist_sigrrmsEM->SetMinimum(minval);
	hist_siglHAD->SetMinimum(minval);
	hist_siglrmsHAD->SetMinimum(minval);
	hist_sigrHAD->SetMinimum(minval);
	hist_sigrrmsHAD->SetMinimum(minval);
	
	hist_siglEM->SetMaximum(maxval);
	hist_sigrEM->SetMaximum(maxval);
	hist_siglrmsEM->SetMaximum(maxvalrms);
	hist_sigrrmsEM->SetMaximum(maxvalrms);
	hist_siglHAD->SetMaximum(maxval);
	hist_siglrmsHAD->SetMaximum(maxvalrms);
	hist_sigrHAD->SetMaximum(maxval);
	hist_sigrrmsHAD->SetMaximum(maxvalrms);
	
	hist_siglEM->SetStats(0);
	hist_sigrEM->SetStats(0);
	hist_siglrmsEM->SetStats(0);
	hist_sigrrmsEM->SetStats(0);
	hist_siglHAD->SetStats(0);
	hist_siglrmsHAD->SetStats(0);
	hist_sigrHAD->SetStats(0);
	hist_sigrrmsHAD->SetStats(0);
	
	hist_siglEM->SetOption("colz");
	hist_sigrEM->SetOption("colz");
	hist_siglrmsEM->SetOption("colz");
	hist_sigrrmsEM->SetOption("colz");
	hist_siglHAD->SetOption("colz");
	hist_siglrmsHAD->SetOption("colz");
	hist_sigrHAD->SetOption("colz");
	hist_sigrrmsHAD->SetOption("colz");
	
  	hist_goodfitsHAD->SetStats(0);
  	hist_goodfitsEM->SetStats(0);
  	hist_goodfitsHAD->SetOption("colz");
  	hist_goodfitsEM->SetOption("colz");

	pars_hists->cd("sigLeft/");
	TIter next( gDirectory->GetListOfKeys() );
	
	std::cout<<"about to run through sigmal keys."<<std::endl;

	std::string histName;
	TKey* key;
	while( (key=(TKey*) next() )){
		//std::cout << "key is... " << key->GetName() << std::endl;
		histName = key->GetName();
		TObject *o = (TH1D*) key->ReadObj()->Clone(TString::Format("%s", histName.c_str()));
		if(!o->InheritsFrom("TH1")) continue; //only look at histos
		Long64_t foundtime = histName.find("hist_sigmal_id");
		//std::cout<<"found time is "<<foundtime<<std::endl;
		if ( foundtime<histName.length()){
			//THStack *hist_stack = (THStack*)o;
			TH1 *hist_t = (TH1*)o;
			//std::cout<<"Line colour of histogram is "<<hist_t->GetLineColor()<<std::endl;
			//printf("key: %s points to an object of class: %s at %dn",key->GetName(),key->GetClassName(),key->GetSeekKey());
			unsigned int id=  atoi(histName.substr(14, histName.length()-14).c_str()); 
			double eta = Eta[id];
			double phi = Phi[id];
			int etabin = hist_siglEM->GetXaxis()->FindBin(eta);
			int phibin = hist_siglEM->GetYaxis()->FindBin(phi);
			
			// perform Gaussian fit to obtain mean
			if ((hist_t->GetEntries()<10) || ((hist_t->Integral(0, hist_t->GetXaxis()->FindBin(50.)))==0)) continue;
			unsigned int maxbin = hist_t->GetMaximumBin();
			double startrange = hist_t->GetXaxis()->GetBinCenter(maxbin)-5.;
			double endrange = hist_t->GetXaxis()->GetBinCenter(maxbin)+5.;			
			hist_t->Fit("gaus", "", "", startrange, endrange);
			double chi2ndf = (hist_t->GetFunction("gaus")->GetChisquare())/(hist_t->GetFunction("gaus")->GetNDF());
			if (std::isnormal(chi2ndf)) Chi2NDF_sigL[id] = chi2ndf;
			else Chi2NDF_sigL[id] = -1;
			
			char titletext[45];
			sprintf (titletext, "Chi2/NDF = %.2f, maxvalue = %.1f", chi2ndf, hist_t->GetXaxis()->GetBinCenter(maxbin) );
			std::cout<<titletext<<std::endl;
			hist_t->SetTitle(titletext);
			outputGausfits->cd("sigLeft");
			hist_t->Write();
			double parameter = hist_t->GetFunction("gaus")->GetParameter(1);
			double rms = hist_t->GetFunction("gaus")->GetParameter(2);
			unsigned int fits = hist_t->GetEntries();
			int part = (int) Part[id];
			std::cout << "corresponding part, etabin, phibin, mean (gauss), rms (gauss)... " << part  <<"\t"<< etabin << "\t"<< phibin <<"\t"<< parameter << "\t"<<rms<<std::endl;
			SigL[id]=parameter;
			SigLrms[id]=rms;
			NFits[id] = fits;
			if (part>= 0 && part<= 3){
			  hist_siglEM->SetBinContent(etabin, phibin, parameter);
			  hist_siglrmsEM->SetBinContent(etabin, phibin, rms);
			  hist_goodfitsEM->SetBinContent(etabin, phibin, fits);
			}
			else if (part>3 && part<=8){
			  hist_siglHAD->SetBinContent(etabin, phibin, parameter);
			  hist_siglrmsHAD->SetBinContent(etabin, phibin, rms);
			  hist_goodfitsHAD->SetBinContent(etabin, phibin, fits);				  
			}
			else {
				std::cout<<"WARNING: part for ID 0x"<< id<<" returned INVALID."<<std::endl;
			}
		}
		pars_hists->cd("sigLeft/");
	}
	
	pars_hists->cd("sigRight/");
	TIter nextHAD( gDirectory->GetListOfKeys() );

	while( (key=(TKey*) nextHAD() )){
		//std::cout << "key is... " << key->GetName() << std::endl;
		histName = key->GetName();
		TObject *o = (TH1D*) key->ReadObj()->Clone(TString::Format("%s", histName.c_str()));
		if(!o->InheritsFrom("TH1")) continue; //only look at histos
		Long64_t foundtime = histName.find("hist_sigmar_id");
		//std::cout<<"found time is "<<foundtime<<std::endl;
		if ( foundtime<histName.length()){
			//THStack *hist_stack = (THStack*)o;
			TH1 *hist_t = (TH1*)o;
			//std::cout<<"Line colour of histogram is "<<hist_t->GetLineColor()<<std::endl;
			//printf("key: %s points to an object of class: %s at %dn",key->GetName(),key->GetClassName(),key->GetSeekKey());
			unsigned int id=  atoi(histName.substr(14, histName.length()-14).c_str()); 
			double eta = Eta[id];
			double phi = Phi[id];
			int etabin = hist_sigrEM->GetXaxis()->FindBin(eta);
			int phibin = hist_sigrEM->GetYaxis()->FindBin(phi);
			
			// perform Gaussian fit to obtain mean
			if ((hist_t->GetEntries()<10) || ((hist_t->Integral(0, hist_t->GetXaxis()->FindBin(50.)))==0)) continue;
			std::cout<<"fitting gaussian to TT id.."<<id<<std::endl;
			unsigned int maxbin = hist_t->GetMaximumBin();
			double startrange = hist_t->GetXaxis()->GetBinCenter(maxbin)-5.;
			double endrange = hist_t->GetXaxis()->GetBinCenter(maxbin)+5.;			
			hist_t->Fit("gaus", "", "", startrange, endrange);
			double chi2ndf = (hist_t->GetFunction("gaus")->GetChisquare())/(hist_t->GetFunction("gaus")->GetNDF());
			if (std::isnormal(chi2ndf)) Chi2NDF_sigR[id] = chi2ndf;
			else Chi2NDF_sigR[id] = -1;
			
			char titletext[45];
			sprintf (titletext, "Chi2/NDF = %.2f, maxvalue = %.1f", chi2ndf, hist_t->GetXaxis()->GetBinCenter(maxbin) );
			std::cout<<titletext<<std::endl;
			hist_t->SetTitle(titletext);
			outputGausfits->cd("sigRight");
			hist_t->Write();
			double parameter = hist_t->GetFunction("gaus")->GetParameter(1);
			double rms = hist_t->GetFunction("gaus")->GetParameter(2);
			
			int part = (int) Part[id];
			SigR[id]=parameter;
			SigRrms[id]=rms;
			std::cout << "corresponding part, etabin, phibin, mean, rms... " << part  <<"\t"<< etabin << "\t"<< phibin <<"\t"<< parameter << "\t"<<rms<<std::endl;
			if (part<= 3){
			  std::cout << "filling for em"<<std::endl;
			  hist_sigrEM->SetBinContent(etabin, phibin, parameter);
			  hist_sigrrmsEM->SetBinContent(etabin, phibin, rms);
			}
			else if (part>3){
			  hist_sigrHAD->SetBinContent(etabin, phibin, parameter);
			  hist_sigrrmsHAD->SetBinContent(etabin, phibin, rms);
			}
			else {
				std::cout<<"WARNING: part for ID 0x"<< id<<" returned INVALID."<<std::endl;
			}
		}
		pars_hists->cd("sigRight/");		
	}
	
	outputGausfits->Close();
	
	TFile* fP4 = new TFile("/afs/cern.ch/work/c/cantel/private/output/maps_filledblanks.root");
	fP4->cd();
	
	TH2D* emP4sigl=(TH2D*) gDirectory->Get("emsigmal"); 
	TH2D* emP4sigr=(TH2D*) gDirectory->Get("emsigmar"); 
	TH2D* hadP4sigl=(TH2D*) gDirectory->Get("hadsigmal"); 
	TH2D* hadP4sigr=(TH2D*) gDirectory->Get("hadsigmar");

	int minvalcorr = 0.3;
	int maxvalcorr = 1.6;
	TH2D* hist_siglEM_corr =(TH2D*) hist_siglEM->Clone();
	hist_siglEM_corr->SetMinimum(minvalcorr);	
	hist_siglEM_corr->SetMaximum(maxvalcorr);
	TH2D* hist_sigrEM_corr =(TH2D*) hist_sigrEM->Clone();
	hist_sigrEM_corr->SetMinimum(minvalcorr);	
	hist_sigrEM_corr->SetMaximum(maxvalcorr);
	TH2D* hist_siglHAD_corr =(TH2D*) hist_siglHAD->Clone();	
	hist_siglHAD_corr->SetMinimum(minvalcorr);	
	hist_siglHAD_corr->SetMaximum(maxvalcorr);
	TH2D* hist_sigrHAD_corr =(TH2D*) hist_sigrHAD->Clone();
	hist_sigrHAD_corr->SetMinimum(minvalcorr);	
	hist_sigrHAD_corr->SetMaximum(maxvalcorr);

	hist_siglEM_corr->Divide(emP4sigl);
	hist_sigrEM_corr->Divide(emP4sigr);
	hist_siglHAD_corr->Divide(hadP4sigl);
	hist_sigrHAD_corr->Divide(hadP4sigr);
	
	hist_siglEM_corr->SetName("hist_siglEM_corr");
	hist_sigrEM_corr->SetName("hist_sigrEM_corr");	
	hist_siglHAD_corr->SetName("hist_siglHAD_corr");
	hist_sigrHAD_corr->SetName("hist_sigrHAD_corr");
	
	hist_siglEM_corr->SetMinimum(0.7);
	hist_sigrEM_corr->SetMinimum(0.7);
	hist_siglHAD_corr->SetMinimum(0.7);
	hist_sigrHAD_corr->SetMinimum(0.7);	
	hist_siglEM_corr->SetMaximum(1.6);
	hist_sigrEM_corr->SetMaximum(1.6);
	hist_siglHAD_corr->SetMaximum(1.6);
	hist_sigrHAD_corr->SetMaximum(1.6);	
	
	TH1D* siglEM_corr_px = hist_siglEM_corr->ProjectionX();
	TH1D* sigrEM_corr_px = hist_sigrEM_corr->ProjectionX();
	TH1D* siglHAD_corr_px = hist_siglHAD_corr->ProjectionX();
	TH1D* sigrHAD_corr_px = hist_sigrHAD_corr->ProjectionX();
	int nbinsy = hist_siglEM_corr->GetNbinsY();
	siglEM_corr_px->Scale(1./nbinsy);
	sigrEM_corr_px->Scale(1./nbinsy);
	siglHAD_corr_px->Scale(1./nbinsy);
	sigrHAD_corr_px->Scale(1./nbinsy);
	
	TH1D* SiglEM_px;
	TH1D* SigrEM_px;
	TH1D* SiglHAD_px;
	TH1D* SigrHAD_px;
	
	SiglEM_px = hist_siglEM->ProjectionX();
	SigrEM_px = hist_sigrEM->ProjectionX();
	SiglHAD_px = hist_siglHAD->ProjectionX();
	SigrHAD_px = hist_sigrHAD->ProjectionX();
	nbinsy = hist_siglEM->GetNbinsY();
	SiglEM_px->Scale(1./nbinsy);
	SigrEM_px->Scale(1./nbinsy);
	SiglHAD_px->Scale(1./nbinsy);
	SigrHAD_px->Scale(1./nbinsy);

	char * outputname =  (char *) malloc(snprintf(NULL, 0, "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/fixedSigmaMaps_%s.root", name)+1);
	sprintf(outputname, "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/fixedSigmaMaps_%s.root", name);
	TFile* out = new TFile(outputname, "RECREATE");
	out->cd();
	
	hist_goodfitsEM->Write();
	hist_goodfitsHAD->Write();
	hist_siglEM->Write();
	hist_sigrEM->Write();
	hist_siglrmsEM->Write();
	hist_sigrrmsEM->Write();
	hist_siglHAD->Write();
	hist_siglrmsHAD->Write();
	hist_sigrHAD->Write();
	hist_sigrrmsHAD->Write();
	SiglEM_px->Write();
	SigrEM_px->Write();
	SiglHAD_px->Write();
	SigrHAD_px->Write();
	hist_siglEM_corr->Write();
	hist_sigrEM_corr->Write();
	hist_siglHAD_corr->Write();
	hist_sigrHAD_corr->Write();
	siglEM_corr_px->Write();
	sigrEM_corr_px->Write();
	siglHAD_corr_px->Write();	
	sigrHAD_corr_px->Write();
	
	out->Close();
	pars_hists->Close();
	
	TTxml ftime_values;
	ftime_values.Add("Eta", Eta);
	ftime_values.Add("Phi", Phi);
	ftime_values.Add("Part", Part);
	ftime_values.Add("SigL", SigL);
	ftime_values.Add("SigLrms", SigLrms);
	ftime_values.Add("SigR", SigR);
	ftime_values.Add("SigRrms", SigRrms);	
	ftime_values.Add("NFits", NFits);
	ftime_values.Add("Chi2NDF_sigL", Chi2NDF_sigL);
	ftime_values.Add("Chi2NDF_sigR", Chi2NDF_sigR);
	
	char * outputXML =  (char *) malloc(snprintf(NULL, 0, "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/FittedParameters_%s.xml", name)+1);
	sprintf(outputXML, "/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/FittedParameters_%s.xml", name);
	ftime_values.Write(outputXML);
	
} 
