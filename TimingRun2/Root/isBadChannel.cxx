
// determine if bad channel:
// returns 0 if good channel, 1 if bad RMS, 2 if bad timing, -1 if indeterminate (no neighbours with valid timing)
// a bad channel is bad if 
//	RMS > 3.2ns (returns 1)
//	TIMING from neighbours > 5ns (returns 2)

// #include "TAxis.h"
#include "TimingRun2/isBadChannel.h"


int isBadChannel(double eta, double phi, char const * layer, double RMS, TH2TT *timingHist){

	int iRg = isRMSgood(RMS);
	if ( iRg == 0 ) // bad RMS
		return 1;    // bad channel
		
	int iTG = 0;
	iTG = isTimingGood(eta,phi,layer,timingHist); //checks timing in neighbours to determine significant deviation of timing.
	
	if ( iTG == 0 ) // bad timing
		return 2;    // bad channel
	else if ( iTG == -1 )
		return -1;
	
	return 0;      // good channel
}


int isRMSgood(double RMS){

	if ( RMS <= 3. )//acceptable --> good RMS
		return 1;
		
	else return 0;   //bad RMS


}


int isTimingGood(double eta, double phi, const char * layer, TH2TT *timingHist){
	
	int nbinsx = timingHist->GetNbinsX();
	int nbinsy = timingHist->GetNbinsY();
// 	std::cout<<"nbinsx: "<< nbinsx << " nbinsy: " << nbinsy<<std::endl;
	
	int binx = 0;
	int biny = 0;
	int binz = 0;
	int globalbin = timingHist->FindBin(eta,phi);
	timingHist->GetBinXYZ(globalbin,binx,biny,binz);
	double timing = timingHist->GetBinContent(binx,biny);
	
	double meantiming = 0.;
	int N = 0;
	
	
	int phibegin;
	int phiend;
	if ( fabs(eta) < 2.5 ){ //barrel region, trigger towers: 0.1 in phi direction
		phibegin = -2;
		phiend = 2;
	}
	else if ( fabs(eta) > 2.5 && fabs(eta) < 3.2 ){ //endcap, trigger towers: 0.2 in phi direction
		phibegin = -4;
		phiend = 5;
	}
	else { //fcal, trigger towers: 0.4 in phi direction
		phibegin = -8;
		phiend = 11;
	}
	for ( int i = -2; i <= 2; i++ ){
		for ( int j = phibegin; j <= phiend; j++ ){
			//don't look at original tower
			if ( i == 0 && j == 0 ) continue;
			
			//reached the borders of the histogram
			if ( binx+i < 1 || binx+i > nbinsx ) continue;
			if ( biny+j < 1 || biny+j > nbinsy ) continue;
		
			
			//distinguish different trigger tower sizes and CaloDivisions
			if ( strcmp(layer,"em")==0 ){ //emlayer
				//the following part is different for em & had
				if ( fabs(eta) < 1.4 ){ //0.1x0.1 trigger tower size & emb
					double eta_nb = eta+i*0.1;
					if ( fabs(eta_nb) > 1.4 ) continue;
				}
				else if ( fabs(eta) > 1.4 && fabs(eta) < 1.5 ){ //0.1x0.1 trigger tower size & overlap
					double eta_nb = eta+i*0.1;
					if ( fabs(eta_nb) < 1.4 || fabs(eta_nb) > 1.5 ) continue;
				}
				else if ( fabs(eta) > 1.5 && fabs(eta) < 2.5 ){ //0.1x0.1 trigger tower size & emec
					double eta_nb = eta+i*0.1;
					if ( fabs(eta_nb) < 1.5 || fabs(eta_nb) > 2.5 ) continue;
				}
				//from here on it's the same for em & had
				else if ( fabs(eta) > 2.5 && fabs(eta) < 3.1 ) { //0.2x0.2 trigger tower size & emec
					if ( i == 0 && j == 1 ) continue;
					double eta_nb = eta+i*0.2;
					if ( fabs(eta_nb) < 2.5 || fabs(eta_nb) > 3.1 ) continue;
				}
				else if ( fabs(eta) > 3.1 && fabs(eta) < 3.2 ) { //single column of 0.1x0.2 trigger tower size & emec
					if ( i == 0 && j == 1 ) continue;
					double eta_nb = eta+i*0.1;
					if ( fabs(eta_nb) < 3.1 || fabs(eta_nb) > 3.2 ) continue;
				}
				else if ( fabs(eta) > 3.2 ) { //0.4x0.4 trigger tower size = fcal
					if ( i == 0 && (j == 1 || j == 2 || j == 3 ) ) continue;
					double eta_nb = eta+i*0.4;
					if ( fabs(eta_nb) < 3.2 ) continue;
				}
			}
			else if ( strcmp(layer,"had")==0 ){ //hadlayer
				//the following part is different for em & had
				if ( fabs(eta) < 1.5 ){ //0.1x0.1 trigger tower size & tile
					double eta_nb = eta+i*0.1;
					if ( fabs(eta_nb) > 1.5 ) continue;
				}
				else if ( fabs(eta) > 1.5 && fabs(eta) < 2.5 ){ //0.1x0.1 trigger tower size & hec
					double eta_nb = eta+i*0.1;
					if ( fabs(eta_nb) < 1.5 || fabs(eta_nb) > 2.5 ) continue;
				}
				//from here on it's the same for em & had
				else if ( fabs(eta) > 2.5 && fabs(eta) < 3.1 ) { //0.2x0.2 trigger tower size & hec
					if ( i == 0 && j == 1 ) continue;
					double eta_nb = eta+i*0.2;
					if ( fabs(eta_nb) < 2.5 || fabs(eta_nb) > 3.1 ) continue;
				}
				else if ( fabs(eta) > 3.1 && fabs(eta) < 3.2 ) { //single column of 0.1x0.2 trigger tower size & hec
					if ( i == 0 && j == 1 ) continue;
					double eta_nb = eta+i*0.1;
					if ( fabs(eta_nb) < 3.1 || fabs(eta_nb) > 3.2 ) continue;
				}
				else if ( fabs(eta) > 3.2 ) { //0.4x0.4 trigger tower size = fcal
					if ( i == 0 && (j == 1 || j == 2 || j == 3 ) ) continue;
					double eta_nb = eta+i*0.4;
					if ( fabs(eta_nb) < 3.2 ) continue;
				}
			}
			
			double content = timingHist->GetBinContent(binx+i,biny+j);
			if ( content < -100. ) continue;
// 			if (content < -20.)
// 				std::cout << "content binx+i,biny+j ( " << binx<<"+"<<i<<" , "<<biny<<"+"<<j<<" ) = "<<content<<std::endl;
// 			
			meantiming += content;
			N++;
		}
	}
	if ( N > 0 ){
		meantiming = meantiming/N;
		if ( fabs(timing-meantiming) > 3. ) {//strong difference to timing mean of neighbours
 			//std::cout<<"timing: "<<timing<< " , meantiming: "<<meantiming<<std::endl;
 			//std::cout<<"timing-meantiming: " <<fabs(timing-meantiming) << "for eta: " <<eta<<" and phi: "<<phi<<std::endl;
// 			if ( fabs(timing-meantiming) > 100. )
// 				std::cout<<"timing: "<<timing<< " , meantiming: "<<meantiming<<std::endl;
			return 0;
		}
		else                                //good timing
			return 1;
	}
	else
		return -1;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
//compare if channel is considered as interesting for data saving
/////////////////////////////////////////////////////////////////////////////////////////////////////

int isChannelInteresting(unsigned int id, double eta, double phi, int caldiv){
	char channelname[15];
	sprintf(channelname,"0x%08x",id);
	
	if ( strcmp(channelname,"0x031e0603") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x021b0b01") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x021c0000") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x061b0303") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x061b0c02") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x04130700") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x041a0103") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x041e0e01") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x041b0d00") == 0 ){
		return 1;
	}
	if ( strcmp(channelname,"0x051e0f02") == 0 ){
		return 1;
	}
	
	// in em layer
	if ( caldiv >= 0 && caldiv <= 7 ){
		if ( fabs(eta) > 2.0 && fabs(eta) < 2.1 )
			return 1;
		if ( fabs(eta) > 2.3 && fabs(eta) < 2.5 )
			return 1;
		if ( caldiv == 2 || caldiv == 5 ) // Overlap region
			return 1;
		if ( caldiv == 0 || caldiv == 7 ) // FCal1
			return 1;
	}
	// in hadronic layer
	else if ( caldiv >= 8 && caldiv <= 15 ){
		if ( fabs(eta) > 0.9 && fabs(eta) < 1.0 )
			return 1;
		if ( fabs(eta) > 1.3 && fabs(eta) < 1.4 )
			return 1;
		if ( fabs(eta) > 2.0 && fabs(eta) < 2.1 )
			return 1;
		if ( caldiv == 8 || caldiv == 15 ) // FCal2&3
			return 1;
	}
	
	
	
	return 0;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
//add eta - phi maps bin by bin
/////////////////////////////////////////////////////////////////////////////////////////////////////

void addBinbyBin(TH2TT *timingHist, TH2TT *goodTimingHist, TH2TT *calcTimingHist){

	int nbinsx = goodTimingHist->GetNbinsX();
	int nbinsy = goodTimingHist->GetNbinsY();
	
	for ( int i = 1; i <= nbinsx; i++ ){
		for ( int j = 1; j <= nbinsy; j++ ){
		
			double content1 = goodTimingHist->GetBinContent(i,j);
			double content2 = calcTimingHist->GetBinContent(i,j);
			
			if ( (content1 > -100.) && (content2 < -100.) )
				timingHist->SetBinContent(i,j,content1);
			else if ( (content1 < -100.) && (content2 > -100.) )
				timingHist->SetBinContent(i,j,content2);
			else if ( (content1 < -100.) && (content2 < -100.) )
				continue;
			else if ( (content1 > -100.) && (content2 > -100.) )
				std::cout<<"ERROR in addBinbyBin() function --> both histograms contain valid information for the same trigger tower!"<<std::endl;
		
		}
	}
}



