#ifndef DERIVECORRECTIONFACTORS_h
#define DERIVECORRECTIONFACTORS_h

#include <TH1.h>
#include "TH2TT.h"
#include "TTxml.h"
#include <sstream>
#include <iostream>
#include "TFile.h"
#include <algorithm>
#include <iostream>

void FillMandRMS(std::vector<double> vec , std::map<unsigned int,double> &Mmap, std::map<unsigned int,double> &RMSmap, unsigned int bin );
TH1D* makeHist(std::map<unsigned int,double> MEANmap, std::map<unsigned int,double> RMSmap, std::string name);
std::pair<double, double> computeMandRMS(std::vector<double> vec);
void runSigValues(std::string xmlName, std::string rootName);
//void runSigValuesFromCombHists(std::string rootInName, std::string rootOutName);
void runSigCorrections(std::string xmlName, std::string rootName);
#endif