#ifndef IntTTxml_h
#define IntTTxml_h

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>

class IntTTxml {
private:
	std::set <unsigned int> m_id;
   std::vector <std::string> m_attribute;
	std::vector <std::map <unsigned int, int> > m_data;
	std::vector <std::string> m_type;
	FILE * m_file;

	void WriteHeader();
	void WriteFooter();
	void WriteData();

	int ReadHeader();
	int ReadFooter();
	int ReadData();
	
public :
   IntTTxml();
   ~IntTTxml();
// 	void Read(std::string filename);
// 	void Write(std::string filename);
	bool Read(std::string filename);   //new
	bool Write(std::string filename);  //new
	void Add(std::string attribute, std::map<unsigned int, int> data);
	void Add(std::string attribute, unsigned int id, int data);
	std::map<unsigned int, int> Get(std::string attribute);
};

#endif
