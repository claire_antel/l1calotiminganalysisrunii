#ifndef TTxml_h
#define TTxml_h

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>

class TTxml {
private:
	std::set <unsigned int> m_id;
   std::vector <std::string> m_attribute;
	std::vector <std::map <unsigned int, double> > m_data;
	std::vector <std::string> m_type;
	FILE * m_file;

	void WriteHeader();
	void WriteFooter();
	void WriteData();

	int ReadHeader();
	int ReadFooter();
	int ReadData();
	
public :
   TTxml();
   ~TTxml();
// 	void Read(std::string filename);
// 	void Write(std::string filename);
	bool Read(std::string filename);   //new
	bool Write(std::string filename);  //new
	void Add(std::string attribute, std::map<unsigned int, double> data);
	void Add(std::string attribute, unsigned int id, double data);
	std::map<unsigned int, double> Get(std::string attribute);
};

#endif
