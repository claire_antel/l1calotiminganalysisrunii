#ifndef TIMINGRUN2_TIMEFITTING_H
#define TIMINGRUN2_TIMEFITTING_H 1

#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include <TString.h>
#include "TrigDecisionTool/TrigDecisionTool.h"
//#include "GoodRunsLists/IGoodRunsListSelectionTool.h"
#include "TProfile.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"

#include "TrigAnalysisInterfaces/IBunchCrossingTool.h"


class TimeFitting: public ::AthHistogramAlgorithm { 
 public: 
  TimeFitting( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~TimeFitting(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();


 private: 
   
  ToolHandle<Trig::TrigDecisionTool> m_tdTool;  //this is a data member
  ToolHandle<Trig::IBunchCrossingTool> m_bc_tool;

  //ToolHandle<IGoodRunsListSelectionTool> m_grlTool;

 //property declarations
 std::string m_TTpropertyfile;
 std::string m_sigmacffile;
 bool m_80MHz;
 bool m_fixed;
 

 std::string m_VxPrimContainerName="PrimaryVertices";

 // file names
 std::ofstream m_log;

 // hist names
 char m_histname2[60];
 char pulse15HistName[50];
 char pulse7HistName[50];
 char pulse5HistName[50];

 unsigned int m_runNo;
 unsigned int m_lumibNo;
 unsigned int m_eventNo;
 bool peakBinHistoRegist;

 //cut variables
 double m_noiseThres;
 double m_lowThres;
 double m_upThres;
 float m_qualityCut;
 float m_qualityCut_loose;
 
  int m_distancefromfrontCut;

  bool m_EMtrig_passed = false;
  bool m_Jtrig_passed = false;
  bool m_TAUtrig_passed = false;
  bool m_XEtrig_passed = false;
  bool m_TEtrig_passed = false;	
  bool m_One_Of_Triggers_Passed = false;
  
  bool m_isBG;
  bool m_isLArBurst;
  
 //counting variables
 int Nevents;
 int Nevents_TRAINBULK;
 int Nevents_TRIG;
 int Nevents_BG;
 int Nevents_GRL;
 int Nevents_PVtx;
 int nfits;
 int ngoodfits;
 int nfailedfits;
 int nerrorfits;
 int m_evtcnt;
 int m_noverticesCut;
 int nVtx; 
 //counted maps
 std::map<unsigned int,int> NFits;
 std::map<unsigned int,int> NGoodFits;
 std::map<unsigned int,int> NFailedFits;
 std::map<unsigned int,int> NFailedFits1, NFailedFits2, NFailedFits3;
 std::map<unsigned int,int> NErrorFits;
 std::map<unsigned int,int> FittedTPeaks;

 // property maps 
 std::map<unsigned int,double> Part;
 std::map<unsigned int,double> Pedestal;
 std::map<unsigned int,double> Eta, Phi;
 std::map<unsigned int,double> SigmaLeft, SigmaRight, UndershootFix, UndVsAmpl;
 std::map<unsigned int,double> cfSigmal, cfSigmar; 

 // propagating maps
 std::map<unsigned int,std::vector<double>> TimingPropagationEM;
 std::map<unsigned int,std::vector<double>> TimingPropagationJET;
 std::map<unsigned int,std::vector<double>> TimingPropagationTAU;
 std::map<unsigned int,std::vector<double>> TimingPropagationXE;
 std::map<unsigned int,std::vector<double>> TimingPropagation;
 std::map<unsigned int,std::vector<double>> BGflaggedTimingPropagation;
 std::map<unsigned int,std::vector<double>> LArBURSTflaggedTimingPropagation;
 std::map<unsigned int,std::vector<double>> CaloQualityPropagation;
 std::map<unsigned int,std::vector<double>> ETPropagation;
 std::map<unsigned int,std::vector<double>> SigmaLeft_Fit, SigmaRight_Fit, UndVsAmpl_Fit;

 std::map<unsigned int,TProfile*> h_energyNormPulses;
 std::map<unsigned int, TH2D*> h_energyNormPulses_colour;
 std::map<unsigned int, TH2D*> h_energyNormPulses_colour_bgflagged;
 std::map<unsigned int, TH2D*> h_energyNormPulses_colour_larburstflagged;
 
 //TProfile* hist_energyNormPulses;
 //TH2D* hist_energyNormPulses_colour;
 // stream declarations
 std::string m_InfoHistsStream;
 std::string m_PulseHistsStream_BGtwoside;
 std::string m_PulseHistsStream_LArBurst;
 std::string m_PulseHistsStream_current; 
 
 
 TH1D* pulsehist5;
 TH1D* pulsehist7;
 TH1D* pulsehist15;

 StatusCode fitTTsignal(unsigned int nslices, unsigned int id, int layer, int part, double eta, double phi, int maxbin, int pedestal, double quality, double caloEnergy, double TTlut, std::vector<uint_least16_t> adc_vec);
 float getADCenergy(float m_ADCvalue, float pedestal); 
 StatusCode setupHistograms();
 double caloCellET(const xAOD::TriggerTower& tt);
 std::pair<double, double> computeMandRMS(std::vector<double> vec);
 bool matchTTtoTopocluster(double TTeta, double TTphi, int TTpart, double& topotime, const DataVector<xAOD::CaloCluster>* clusters);
 StatusCode fillNormPulse( unsigned int id, std::map<unsigned int, TH2D*>& histcolourpulses, std::string histstream, std::vector<float> yvec, int nslices, std::string layerdir);

}; 

#endif //> !TIMINGRUN2_TIMEFITTING_H
