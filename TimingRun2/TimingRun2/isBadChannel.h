#ifndef ISBADCHANNEL_h
#define ISBADCHANNEL_h

#include <TH1.h>
#include <cmath>
#include "TH2TT.h"
#include <sstream>
#include <iostream>

int isBadChannel(double,double, char const *,double,TH2TT *);
int isRMSgood(double);
int isTimingGood(double,double,const char *,TH2TT *);

int isChannelInteresting(unsigned int,double,double,int);

void addBinbyBin(TH2TT *,TH2TT *,TH2TT *);

#endif
