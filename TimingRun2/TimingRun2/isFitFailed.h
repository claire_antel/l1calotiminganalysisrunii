#ifndef ISFITFAILED_h
#define ISFITFAILED_h

#include <cstring>
#include <algorithm>
#include "TMath.h"
#include "TStyle.h"
#include <TH1.h>
#include <vector>
#include <TVector.h>
#include <sstream>
#include <TF1.h>
#include "TFile.h"
#include "FitFunctions.h"

int isFitFailed(int,TH1*,int,int,TF1*,double,double,int);
int isFitFailed_80MHz(int,TH1*,int,int,TF1*,double,double,int);
int do_gof(std::vector<double>,double,std::vector<double>,double,double,int); 

//double step_to_ns = 25./24.;

#endif

