#include "TimingRun2/TTxml.h"

// TTxml::TTxml() : m_file(0) {
TTxml::TTxml() { //new
// 	;
}

TTxml::~TTxml() {
// 	if (m_file) {
// 		std::cout << "TTxml::~TTxml(): File was not closed correctly. Closing it now." <<  std::endl;
// 		fclose(m_file);
// 	}
}

// void TTxml::Write(std::string filename) {
bool TTxml::Write(std::string filename) {  //new
	m_file = fopen (filename.c_str(), "w+");
	if (m_file != NULL) {
   	std::cout << "File '" << filename << "' opened for writing." << std::endl;
		WriteHeader();
		WriteData();
		WriteFooter();
	} else {
	   std::cout << "Error opening file." <<  std::endl;
// 		exit( 1 ); //new
		return false;  //new
	}
	fclose(m_file);
	return true; //new
}

// void TTxml::Read(std::string filename) {
bool TTxml::Read(std::string filename) { //new
        std::cout << "filename = " << filename << std::endl;
	m_file = fopen (filename.c_str(), "r");
	if (m_file != NULL) {
   	std::cout << "File '" << filename << "' opened for reading." << std::endl;
		ReadHeader();
		ReadData();
		ReadFooter();
	} else {
	   std::cout << "Error opening file." <<  std::endl;
// 		exit( 1 );  //new
		return false;  //new
	}
	fclose(m_file);
	return true; //new
}

int TTxml::ReadHeader() {
	std::string xmldef1 = "<?xml version='1.0' encoding='utf-8'?>\n";
	std::string xmldef2 = "<L1CaloMapData keytype=\"CoolChannel\" divopt=\"PpmMcmChan\" version=\"1\">\n";
	std::string attdef = " <AttributeDef name=\"%s\" type=\"Double\"/>\n";

	int result = 0;
   char line[256];
	std::map<unsigned int, double> map;
	std::string str;
   if (fgets (line, sizeof(line), m_file) != NULL )
		if (std::string(line).find(xmldef1) == std::string::npos)
			return -1;
   if (fgets (line, sizeof(line), m_file) != NULL )
		if (std::string(line).find(xmldef2) == std::string::npos)
			return -1;
	while (fgets (line, sizeof(line), m_file) != NULL ) {
		if (std::string(line).find(attdef.substr(0, 14)) == std::string::npos)
			break;
		char c[256];
		sscanf(line, attdef.c_str(), c);
		std::string str(c);
		str.erase(str.length()-1, 1); // remove '"'
		m_attribute.push_back(str);
		m_data.push_back(map);
		result++;
	}
	return result;
}

int TTxml::ReadData() {
	std::string chbegin = " <Channel id=\"0x%08X\">\n";
	std::string entry =   "  <%s value=\"%lf\"/>\n";
	std::string chend =   " </Channel>\n";
	
	int result = 0;
   char line[256];
	std::string str;
	rewind(m_file);
	// loop over channels
	while ( fgets (line, sizeof(line), m_file) != NULL ) {
	   if (std::string(line).find(chbegin.substr(0, 12)) == std::string::npos) continue;
		unsigned int id;
		sscanf(line, chbegin.c_str(), &id);
		while (( fgets (line, sizeof(line), m_file) != NULL )	&& ( (std::string(line).find(chend) == std::string::npos) )) {
			char c[256];
			double d;
			sscanf(line, entry.c_str(), c, &d);
			Add(std::string (c), id, d);
			result++;
		}
	}
	return result;
}

int TTxml::ReadFooter() {
	std::string foodef = "</L1CaloMapData>\n";
	
	char line[256];
   if (fgets (line, sizeof(line), m_file) != NULL ) {
		if (std::string(line).find(foodef) == std::string::npos)
			return -1;
	}
	return 1;
}

void TTxml::WriteData() {
	std::vector <std::string>::iterator att_it;
	std::vector <std::map <unsigned int, double> >::iterator dat_it;
	std::map <unsigned int, double>::iterator it;
	for ( std::set <unsigned int>::iterator id_it = m_id.begin(); id_it != m_id.end(); id_it++) {
		unsigned int id = *id_it;
      fprintf(m_file, " <Channel id=\"0x%08X\">\n", id);
		att_it = m_attribute.begin();
		dat_it = m_data.begin();
		for ( ; att_it != m_attribute.end(); att_it++, dat_it++) {
			it = dat_it->find(id);
			if ( it != dat_it->end() ) {
				std::string attribute = *att_it;
				double value = it->second;
				fprintf(m_file, "  <%s value=\"%.2f\"/>\n", attribute.c_str(), value);
			}
		}
      fprintf(m_file, " </Channel>\n");
   }
}

void TTxml::WriteFooter() {
	fprintf(m_file, "</L1CaloMapData>\n");
	return;
}

void TTxml::WriteHeader() {
	fprintf(m_file, "<?xml version='1.0' encoding='utf-8'?>\n<L1CaloMapData keytype=\"CoolChannel\" divopt=\"PpmMcmChan\" version=\"1\">\n");
	for (std::vector <std::string>::iterator att_it = m_attribute.begin(); att_it != m_attribute.end(); att_it++)
		fprintf(m_file, " <AttributeDef name=\"%s\" type=\"Double\"/>\n", att_it->c_str());
	return;
}

void TTxml::Add(std::string attribute, std::map<unsigned int, double> data) {
   m_data.push_back(data);
	m_attribute.push_back(attribute);
	for (std::map<unsigned int, double>::iterator it=data.begin(); it != data.end(); it++)
		m_id.insert(it->first);
	return;
}

void TTxml::Add(std::string attribute, unsigned int id, double data) {
	std::vector <std::string>::iterator att_it = m_attribute.begin();
	std::vector <std::map <unsigned int, double> >::iterator dat_it = m_data.begin();
	while (att_it != m_attribute.end()) {
		if (att_it->compare(attribute) == 0){
		    break;
                }
		att_it++;
		dat_it++;
	}
	if (att_it == m_attribute.end()) {
	   std::cout << "Attribute " << attribute << " was not defined." << std::endl;
		std::map <unsigned int, double> m;
		m[id] = data;
		m_attribute.push_back(attribute);
		m_data.push_back(m);
		m_id.insert(id);
// 		exit(1);
	} else {
		(*dat_it)[id] = data;
		m_id.insert(id);
	}
	return;	
}

std::map<unsigned int, double> TTxml::Get(std::string attribute) {
   std::vector <std::string>::iterator att_it = m_attribute.begin();
	std::vector <std::map <unsigned int, double> >::iterator dat_it = m_data.begin();
	while (att_it != m_attribute.end()) {
		if (att_it->compare(attribute) == 0)
		    return *dat_it;
		att_it++;
		dat_it++;
	}
	std::map<unsigned int, double> result;
   return result;
}

