// TimingRun2 includes
#define _USE_MATH_DEFINES
#include <math.h>

#include "TimingRun2/TTxml.h"
#include <map>
#include "TimingRun2/TH2TT.h"
#include "TimingRun2/TimeFitting.h"
#include "TimingRun2/isFitFailed.h"
#include "TimingRun2/FitFunctions.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
#include "xAODTrigL1Calo/TriggerTowerAuxContainer.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "xAODTrigger/TrigDecision.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"

#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TF1.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TObjArray.h>

//using namespace std;

namespace {
  xAOD::TriggerTower::ConstAccessor <float> ttCellQuality("CaloCellQuality");
} // anonymous namespace



TimeFitting::TimeFitting( const std::string& name, ISvcLocator* pSvcLocator ) : AthHistogramAlgorithm( name, pSvcLocator ){

  declareProperty( "TTpropertyfile", m_TTpropertyfile );
  declareProperty( "sigmacffile", m_sigmacffile );  
  //declareProperty( "useRunIcf", m_useRunIcf); 
  //declareProperty("GRLTool",  m_grlTool, "The private GoodRunsListSelectionTool" );

}


TimeFitting::~TimeFitting() {}


StatusCode TimeFitting::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  m_log.open("log.txt");
  m_log<<"Running in 40MHz mode. ";
  m_log<<"Using fixed parameters? "<< std::boolalpha << m_fixed<<std::endl;
//  m_log<<"Using run I parameters? "<< std::boolalpha << m_useRunIcf<<std::endl;
  
  m_InfoHistsStream="/TowerInfoHistoStream/";
  m_PulseHistsStream_BGtwoside="/TowerPulsetwosideHistoStream/";
  m_PulseHistsStream_LArBurst="/TowerPulselarburstHistoStream/";
  
  ServiceHandle<ITHistSvc> histSvc("THistSvc",name());
  StatusCode scHist = histSvc.retrieve();
  if (scHist.isFailure()) {
	ATH_MSG_ERROR("Hist Service Tool not retrievable.");
	m_log<<"Hist Service Tool not retrievable.\n";
	return StatusCode::FAILURE;
  }
  else ATH_MSG_INFO("Hist Service Tool succesfully retrieved");

  // retrieving trigger tool
  StatusCode sc =m_tdTool.retrieve(); // *MUST* retrieve the tool in initialize otherwise it gets into a bad state... this is a bug in tdt that should be fixed
  if (sc.isFailure()) {
	ATH_MSG_ERROR("Trigger Decision Tool not retrievable.");
	m_log<<"Trigger Decision Tool not retrievable.\n";
	return StatusCode::FAILURE;
  }
  else ATH_MSG_INFO("Trigger Decision Tool succesfully retrieved");
  
   if(m_bc_tool.retrieve().isFailure()){
   ATH_MSG_WARNING ( "Failed to retrieve tool " << m_bc_tool );
   }
   else{
   ATH_MSG_INFO ( "Retrieved tool " << m_bc_tool );
   }

 // I never made use of the GRL. It takes a while for it to be available.
 /*sc =m_grlTool.retrieve();
  if (sc.isFailure()) {
	ATH_MSG_ERROR("GRL Tool not retrievable.");
	m_log<<"GRL Tool not retrievable.\n";
	return StatusCode::FAILURE;
  }
  else ATH_MSG_INFO("GRL Tool succesfully retrieved");
  */

  //initialize cut variables
  m_noiseThres = 40;
  m_lowThres = 90; // raised threshold to cut out pile-up pulses. (Jan 2016)
  m_upThres = 1020; // to be safe - some TTs saturate sooner.
  m_qualityCut = 4000.;
  m_qualityCut_loose = 4000.;
  m_noverticesCut = 5;
  peakBinHistoRegist = false;
  m_distancefromfrontCut = 375; 
  
  //initialize counting variables
  //for events
  Nevents = 0;
  Nevents_TRAINBULK = 0;
  Nevents_TRIG = 0;
  Nevents_BG = 0;
  Nevents_GRL = 0;
  Nevents_PVtx = 0;
  //for fits
  m_evtcnt = 0;
  nfits = 0;
  ngoodfits = 0;
  nfailedfits = 0;
  nerrorfits = 0;
  
  std::map<unsigned int,int>::iterator it;
  for ( it = NFits.begin(); it != NFits.end(); it++ ) {
     it->second = 0;
  }
  for ( it = NGoodFits.begin(); it != NGoodFits.end(); it++ ) {
     it->second = 0;
  }
  for ( it = NFailedFits.begin(); it != NFailedFits.end(); it++ ) {
     it->second = 0;
	  NFailedFits1[it->first] = 0;
	  NFailedFits2[it->first] = 0;
	  NFailedFits3[it->first] = 0;
  }
  for ( it = NErrorFits.begin(); it != NErrorFits.end(); it++ ) {
     it->second = 0;
  }
  for ( it = FittedTPeaks.begin(); it != FittedTPeaks.end(); it++ ) {
     it->second = 0;
  }

  TTxml m_props;

  m_props.Read(m_TTpropertyfile);

  Eta = m_props.Get("eta");
  Phi = m_props.Get("phi");
  SigmaLeft = m_props.Get("sigmal");
  SigmaRight = m_props.Get("sigmar");
  UndVsAmpl = m_props.Get("undVsAmpl");
  Pedestal = m_props.Get("pedestal");
  Part = m_props.Get("part");

  TTxml m_fixedsig;

  m_fixedsig.Read(m_sigmacffile);

  cfSigmal = m_fixedsig.Get("cf_sigmal");
  cfSigmar = m_fixedsig.Get("cf_sigmar");

  CHECK(setupHistograms());

 


  return StatusCode::SUCCESS;
}

StatusCode TimeFitting::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  ATH_MSG_INFO("-----------------------------------------------");
  ATH_MSG_INFO("Number of events after different cuts");
  ATH_MSG_INFO("-----------------------------------------------");
  ATH_MSG_INFO("Total number of events\t\t" << Nevents);
  ATH_MSG_INFO("#Events after train bulk selection\t\t" << Nevents_TRAINBULK);
  ATH_MSG_INFO("#Events after NBG selection\t\t" << Nevents_BG);
  ATH_MSG_INFO("#Events after Trigger selection\t\t" << Nevents_TRIG);
  ATH_MSG_INFO("#Events after GRL selection\t\t" << Nevents_GRL);
  ATH_MSG_INFO("#Events after prim vertex selection\t" << Nevents_PVtx);
  ATH_MSG_INFO( "-----------------------------------------------");
  ATH_MSG_INFO( "Fit statistics:");
  ATH_MSG_INFO( "-----------------------------------------------");
  ATH_MSG_INFO("number of\tnumber of\tproportion  of\tnumber of\tproportion  of\tnumber of\tproportion of");
  ATH_MSG_INFO( "fits\t\tgood fits\tgood fits\tfailed fits\tfailed fits\terror fits\terror fits");
  ATH_MSG_INFO( nfits << "\t" << ngoodfits << "\t\t" << double(ngoodfits)/double(nfits)*100. << "\t\t" << nfailedfits << "\t\t" << double(nfailedfits)/double(nfits)*100. << "\t\t" << nerrorfits << "\t\t" << double(nerrorfits)/double(nfits)*100.);
  ATH_MSG_INFO( "-----------------------------------------------" );
  
  m_log<<"-----------------------------------------------\n";
  m_log<<"Number of events after different cuts\n";
  m_log<<"-----------------------------------------------\n";
  m_log<<"Total number of events\t\t" << Nevents<<"\n";
  m_log<<"#Events after train bulk selection\t\t" << Nevents_TRAINBULK<<"\n";
  m_log<<"#Events after NBG selection\t\t" << Nevents_BG<<"\n";
  m_log<<"#Events after Trigger selection\t\t" << Nevents_TRIG<<"\n";
  m_log<<"#Events after GRL selection\t\t" << Nevents_GRL<<"\n";
  m_log<<"#Events after prim vertex selection\t" << Nevents_PVtx<<"\n";
  m_log<< "-----------------------------------------------\n";
  m_log<< "Fit statistics:\n";
  m_log<< "-----------------------------------------------\n";
  m_log<<"number of\tnumber of\tproportion  of\tnumber of\tproportion  of\tnumber of\tproportion of\n";
  m_log<<"fits\t\tgood fits\tgood fits\tfailed fits\tfailed fits\terror fits\terror fits\n";
  m_log<<nfits << "\t" << ngoodfits << "\t\t" << double(ngoodfits)/double(nfits)*100. << "\t\t" << nfailedfits << "\t\t" << double(nfailedfits)/double(nfits)*100. << "\t\t" << nerrorfits << "\t\t" << double(nerrorfits)/double(nfits)*100.<<"\n";
  m_log<< "-----------------------------------------------\n" ;

  m_log.close();			


  std::map<unsigned int,int>::iterator it_nfits;
  for ( it_nfits = NFits.begin(); it_nfits != NFits.end(); it_nfits++ ){
     unsigned int id = it_nfits->first;
	  double eta = Eta[id];
	  double phi = Phi[id];
          int etabin=hist("hadNFits")->GetXaxis()->FindBin(eta);
          int phibin=hist("hadNFits")->GetYaxis()->FindBin(phi);
	  int part = (int) Part[id];
	  if ( part >=0 && part <= 3 )
	     hist("emNFits")->SetBinContent(etabin,phibin,double(it_nfits->second));
	  else if ( part > 3 && part <= 8 )
	     hist("hadNFits")->SetBinContent(etabin,phibin, double(it_nfits->second));
  }
  std::map<unsigned int,int>::iterator it_ngoodfits;
  for ( it_ngoodfits = NGoodFits.begin(); it_ngoodfits != NGoodFits.end(); it_ngoodfits++ ){
     unsigned int id = it_ngoodfits->first;
	  double eta = Eta[id];
	  double phi = Phi[id];
          int etabin=hist("hadNGoodFits")->GetXaxis()->FindBin(eta);
          int phibin=hist("hadNGoodFits")->GetYaxis()->FindBin(phi);
	  int part = (int) Part[id];
	  if ( part >=0 && part <= 3 )
	     hist("emNGoodFits")->SetBinContent(etabin,phibin,double(it_ngoodfits->second));
	  else if ( part > 3 && part <= 8 )
	     hist("hadNGoodFits")->SetBinContent(etabin,phibin,double(it_ngoodfits->second));
  }
  std::map<unsigned int,int>::iterator it_nfailedfits;
  for ( it_nfailedfits = NFailedFits.begin(); it_nfailedfits != NFailedFits.end(); it_nfailedfits++ ){
     unsigned int id = it_nfailedfits->first;
	  double eta = Eta[id];
	  double phi = Phi[id];
	  int part = (int) Part[id];
          int etabin=hist("hadNFailedFits")->GetXaxis()->FindBin(eta);
          int phibin=hist("hadNFailedFits")->GetYaxis()->FindBin(phi);
	  if ( part >=0 && part <= 3 ){
	     hist("emNFailedFits")->SetBinContent(etabin,phibin,double(it_nfailedfits->second));
	     hist("emNFailedFits_crit1")->SetBinContent(etabin,phibin,double(NFailedFits1[id]));
	     hist("emNFailedFits_crit2")->SetBinContent(etabin,phibin,double(NFailedFits2[id]));
	     hist("emNFailedFits_crit3")->SetBinContent(etabin,phibin,double(NFailedFits3[id]));
	  }
	  else if ( part > 3 && part <= 8 ){
	     hist("hadNFailedFits")->SetBinContent(etabin,phibin,double(it_nfailedfits->second));
	     hist("hadNFailedFits_crit1")->SetBinContent(etabin,phibin,double(NFailedFits1[id]));
	     hist("hadNFailedFits_crit2")->SetBinContent(etabin,phibin,double(NFailedFits2[id]));
	     hist("hadNFailedFits_crit3")->SetBinContent(etabin,phibin,double(NFailedFits3[id]));
	  }
  }
  std::map<unsigned int,int>::iterator it_nerrorfits;
  for ( it_nerrorfits = NErrorFits.begin(); it_nerrorfits != NErrorFits.end(); it_nerrorfits++ ){
     unsigned int id = it_nerrorfits->first;
	  double eta = Eta[id];
	  double phi = Phi[id];
          int etabin=hist("emNErrorFits")->GetXaxis()->FindBin(eta);
          int phibin=hist("emNErrorFits")->GetYaxis()->FindBin(phi);
	  int part = (int) Part[id];
	  if ( part >=0 && part <= 3 ){
	     hist("emNErrorFits")->SetBinContent(etabin,phibin,double(it_nerrorfits->second));
	  }
	  else if ( part > 3 && part <= 8 ){
	     hist("hadNErrorFits")->SetBinContent(etabin,phibin,double(it_nerrorfits->second));
	  }
  }


  std::map<unsigned int,int>::iterator it_tpeaks;
  for ( it_tpeaks = FittedTPeaks.begin(); it_tpeaks != FittedTPeaks.end(); it_tpeaks++ ){
	  unsigned int id = it_tpeaks->first;
	  double eta = Eta[id];
	  double phi = Phi[id];
          int etabin=hist("hadTPeak")->GetXaxis()->FindBin(eta);
          int phibin=hist("hadTPeak")->GetYaxis()->FindBin(phi);
	  int part = (int) Part[id];
	  if ( part >=0 && part <= 3 ){
	     int nentriesEM = hist("emNGoodFits")->GetBinContent(etabin, phibin);
	     double tavg = (it_tpeaks->second)/nentriesEM;
	     hist("emTPeak")->SetBinContent(etabin,phibin,tavg);
	     if (part==0) hist("FCAL1tpeakdistribution")->Fill(tavg);
	     if (part==1) hist("EMECtpeakdistribution")->Fill(tavg);
	     if (part==2) hist("OVERLAPtpeakdistribution")->Fill(tavg);
	     if (part==3) hist("EMBtpeakdistribution")->Fill(tavg);
	  }
	  else if ( part > 3 && part <= 8 ){
	     int nentriesHAD = hist("hadNGoodFits")->GetBinContent(etabin, phibin);
	     double tavg = (it_tpeaks->second)/nentriesHAD;
	     hist("hadTPeak")->SetBinContent(etabin,phibin,tavg);
	     if (part==4) hist("FCAL2tpeakdistribution")->Fill(tavg);
	     if (part==5) hist("FCAL3tpeakdistribution")->Fill(tavg);
	     if (part==6) hist("HECtpeakdistribution")->Fill(tavg);
	     if (part==7) hist("TileEBtpeakdistribution")->Fill(tavg);
	     if (part==8) hist("TileLBtpeakdistribution")->Fill(tavg);
	  }
  }

  //THStack *time_stack;
  TH1D* hist_tdistr;
  //TH1D* hist_tdistrEM;
  //TH1D* hist_tdistrJET;
  TH2D* hist_caloquality;
  TH2D* hist_ETvsTime;
  
  std::string TimingHistsStream("/TowerTimingHistoStream/");
  std::string layerdir;
  
  ATH_MSG_INFO ("Filling TT hists... ");

  // filling histograms for each TT 
  std::map<unsigned int,std::vector<double>>::iterator it;
  for ( it = TimingPropagation.begin(); it != TimingPropagation.end(); it++ ){
		unsigned int id = it->first;

	  	int part = (int) Part[id];
	 	if ( part >=0 && part <= 3 )layerdir = "EM/";
	 	else layerdir = "HAD/";

		std::vector<double> tdiff_vec = it->second;
		std::vector<double> tdiffEM_vec = TimingPropagationEM[id];
		std::vector<double> tdiffJET_vec = TimingPropagationJET[id];
		std::vector<double> calo_vec = CaloQualityPropagation[id];
		std::vector<double> ET_vec = ETPropagation[id];
		char * thist_name = (char *) malloc(snprintf(NULL, 0, "hist_tdistr_id%u", id) + 1);
		sprintf(thist_name, "hist_tdistr_id%u", id);  
 		//time_stack = new THStack(thist_name,"");
		hist_tdistr = new TH1D(thist_name, "timing distribution; time offset [ns]; events", 201, -50.5, 50.5);
		CHECK(histSvc()->regHist(TimingHistsStream+layerdir+thist_name,hist_tdistr));
		/*hist_tdistrEM = new TH1D("t_distributionEM", "timing distribution; time offset [ns]; events", 200, -50., 50.);
		hist_tdistrEM->SetMarkerColor(46);
		hist_tdistrEM->SetLineColor(46);
		hist_tdistrJET = new TH1D("t_distributionJET", "timing distribution; time offset [ns]; events", 200, -50., 50.);
		hist_tdistrJET->SetMarkerColor(85);
		hist_tdistrJET->SetLineColor(85);*/
		char * qualityhistname = (char *) malloc(snprintf(NULL, 0, "hist_qualitydistr_id%u", id) + 1);
		sprintf(qualityhistname, "hist_qualitydistr_id%u", id);   
		hist_caloquality = new TH2D(qualityhistname, "timing offset vs calo quality distribution; timing offset; caloquality", 100, -50., 50., 500, 0., 50000.);
		CHECK(histSvc()->regHist(m_InfoHistsStream+layerdir+"QvsT/"+qualityhistname,hist_caloquality));
		char * ethistname = (char *) malloc(snprintf(NULL, 0, "hist_ETvsTdistr_id%u", id) + 1);
		sprintf(ethistname, "hist_ETvsTdistr_id%u", id);  
		hist_ETvsTime = new TH2D(ethistname, "timing offset vs TT energy; timing offset; E_T", 100, -50., 50., 52, 0., 260.);
		CHECK(histSvc()->regHist(m_InfoHistsStream+layerdir+"EvsT/"+ethistname,hist_ETvsTime));

		unsigned int iter_counter = 0;

		for (std::vector<double>::iterator tdiff=tdiff_vec.begin(); tdiff != tdiff_vec.end(); tdiff++){
			hist_tdistr->Fill(*tdiff);
			hist_caloquality->Fill(*tdiff, calo_vec[iter_counter]);
			hist_ETvsTime->Fill(*tdiff, ET_vec[iter_counter]);
			iter_counter++;
		}

		free(thist_name);
		free(qualityhistname);
		free(ethistname);
  }
  
  // filling timing histogram for bg flagged TT 
  std::string BGflaggedTimingHistsStream("/BGflaggedTowerTimingHistoStream/");
  
  for ( it = BGflaggedTimingPropagation.begin(); it != BGflaggedTimingPropagation.end(); it++ ){
		unsigned int id = it->first;

	  	int part = (int) Part[id];
	 	if ( part >=0 && part <= 3 ) layerdir = "EM/";
	 	else layerdir = "HAD/";

		std::vector<double> tdiff_vec = it->second;

		char * thist_name = (char *) malloc(snprintf(NULL, 0, "hist_tdistr_id%u", id) + 1);
		sprintf(thist_name, "hist_tdistr_id%u", id);  

		hist_tdistr = new TH1D(thist_name, "timing distribution (BGflagged); time offset [ns]; events", 201, -50.5, 50.5);
		CHECK(histSvc()->regHist(BGflaggedTimingHistsStream+layerdir+thist_name,hist_tdistr));

		for (std::vector<double>::iterator tdiff=tdiff_vec.begin(); tdiff != tdiff_vec.end(); tdiff++){
			hist_tdistr->Fill(*tdiff);
		}
  }
  
  // filling timing histogram for LAr burst flagged TT 
 /*  std::string LArBurstflaggedTimingHistsStream("/LArBurstflaggedTowerTimingHistoStream/");
  
  for ( it = LArBURSTflaggedTimingPropagation.begin(); it != LArBURSTflaggedTimingPropagation.end(); it++ ){
		unsigned int id = it->first;

	  	int part = (int) Part[id];
	 	if ( part >=0 && part <= 3 ) layerdir = "EM/";
	 	else layerdir = "HAD/";

		std::vector<double> tdiff_vec = it->second;

		char * thist_name = (char *) malloc(snprintf(NULL, 0, "hist_tdistr_id%u", id) + 1);
		sprintf(thist_name, "hist_tdistr_id%u", id);  

		hist_tdistr = new TH1D(thist_name, "timing distribution (BGflagged); time offset [ns]; events", 201, -50.5, 50.5);
		CHECK(histSvc()->regHist(LArBurstflaggedTimingHistsStream+layerdir+thist_name,hist_tdistr));

		for (std::vector<double>::iterator tdiff=tdiff_vec.begin(); tdiff != tdiff_vec.end(); tdiff++){
			hist_tdistr->Fill(*tdiff);
		}
  } */
  return StatusCode::SUCCESS;
}

StatusCode TimeFitting::execute() {  
  ATH_MSG_DEBUG("Executing " << name() << "...");

  const xAOD::EventInfo* eventInfo = 0;

  CHECK( evtStore()->retrieve( eventInfo) );
  
  // retrieving basic event info
  m_eventNo = eventInfo->eventNumber();
  m_runNo = eventInfo->runNumber();
  m_lumibNo = eventInfo-> lumiBlock();
  
  auto bcidNo = eventInfo->bcid();
  
  Nevents++;

// interesting investigations I briefly did into timing dependencies on bunch train position ( I was very excited about having discovered the bunch crossing tool! ) -- results were sadly pretty unremarkable. 
/*  int distancefromfront(-1);  
  distancefromfront = m_bc_tool->distanceFromFront( bcidNo); // default bunch spacing unit is nanoseconds
  ATH_MSG_DEBUG("distance from front of train for BCID "<<bcidNo<<" is.. "<<distancefromfront<<" ns");
  
  //if (distancefromfront<m_distancefromfrontCut) return StatusCode::SUCCESS;
  
  Nevents_TRAINBULK++;*/
  
/*  int distancefromtail(-1);  
  distancefromtail = m_bc_tool->distanceFromTail( bcidNo); // default bunch spacing unit is nanoseconds
  ATH_MSG_INFO("distance from tail of train for BCID "<<bcidNo<<" is.. "<<distancefromtail<<" ns");*/
  
/*  std::vector< bool > bunchesinfront; 
  bunchesinfront = m_bc_tool->bunchesInFront( bcidNo, 35 );
  ATH_MSG_INFO("bunches - filled unfilled before present bunch..");
  for(std::vector<bool>::iterator it = bunchesinfront.begin(); it != bunchesinfront.end(); ++it) {
    ATH_MSG_INFO(*it);
   }
  ATH_MSG_INFO("end of vector.");*/
  
 
  // ooOOooOOooOOoo BIB flag cuts ooOOooOOooOOoo
  m_isBG = false;
  m_isLArBurst = false;
  
  m_isBG = eventInfo->isEventFlagBitSet(eventInfo->Background,eventInfo->HaloMuonTwoSided);
  //m_isLArBurst = eventInfo->isEventFlagBitSet(eventInfo->Background,eventInfo->LArECTimeDiffHalo); I guess this cut was no good since it was commented? I would try with next line (stole from my sat80 bcid algo studies)
  // m_isLArBurst = eventInfo->isEventFlagBitSet(eventInfo->LAr,eventInfo->LArECTimeDiffHalo); // this works nicely!

  if (m_isBG) hist("backgroundflags_hist")->Fill(1);
  //if (m_isLArBurst) hist("backgroundflags_hist")->Fill(2);
  
  if (m_isBG) return StatusCode::SUCCESS;
  
  hist("backgroundflags_rm_hist")->Fill(m_isBG);

  Nevents_BG++;
  
  if (m_isBG) hist("BGevtsVsLB")->Fill(m_lumibNo);
  
  //if (!m_isLArBurst) ATH_MSG_DEBUG("LAr halo not set. Is Col set? "<<eventInfo->isEventFlagBitSet(eventInfo->Background,eventInfo->LArECTimeDiffCol));
  //if (m_isLArBurst) ATH_MSG_DEBUG("LAr halo set. Is Col set? "<<eventInfo->isEventFlagBitSet(eventInfo->Background,eventInfo->LArECTimeDiffCol));
  
  // ooOOooOOooOOoo TRIGGER cuts ooOOooOOooOOoo
  
  auto allChains = m_tdTool->getChainGroup("L1_.*");
  ATH_MSG_DEBUG( "Triggers that passed : ");

  //resetting triggers

  m_EMtrig_passed = false;
  m_Jtrig_passed = false;
  m_TAUtrig_passed = false;
  m_XEtrig_passed = false;
  m_TEtrig_passed = false;	
  m_One_Of_Triggers_Passed = false;

  for(auto& trig : allChains->getListOfTriggers()){
	
	//the Timing analysis runs on events triggered by these L1's (i.e. only calo-based triggers)
	if (!((trig.find("L1_EM")==0) || (trig.find("L1_J")==0) || (trig.find("L1_TAU")==0) || (trig.find("L1_XE")==0) || (trig.find("L1_TE")==0))) continue;
	if(m_tdTool->getChainGroup(trig)->isPassed()){
	 	ATH_MSG_DEBUG(trig<<", ");
		m_One_Of_Triggers_Passed = true;
  		//m_log<<trig<<", ";
		if (trig.find("L1_EM")==0) m_EMtrig_passed = true;
		if (trig.find("L1_J")==0) m_Jtrig_passed = true;
		if (trig.find("L1_TAU")==0) m_TAUtrig_passed = true;
		if (trig.find("L1_XE")==0) m_XEtrig_passed = true;
		if (trig.find("L1_TE")==0) m_TEtrig_passed = true;
	}
  }

  if (!(m_One_Of_Triggers_Passed)) return StatusCode::SUCCESS;

  Nevents_TRIG++;

	// Check whether runNo and m_lumibNo are in GRL

	bool is_good_event=false;
	is_good_event=true;//m_grlTool->passRunLB(*eventInfo);
	
	ATH_MSG_INFO("GRL selection is disabled!");
	//m_log<<"GRL selection is disabled!\n";

	if(!is_good_event) {
		 ATH_MSG_INFO("Event from LB"<<m_lumibNo<<" did NOT pass GRL check.");
		 m_log<<"Event from LB"<<m_lumibNo<<" did NOT pass GRL check.\n";
		 return StatusCode::SUCCESS; //checks the GRL and skips to next event if not passing
	}
	Nevents_GRL++;
	//	h_larTimeDiff_GRL->Fill(m_larTimeDiff);

 // ooOOooOOooOOoo primary vertex  checks and cuts ooOOooOOooOOoo

  ATH_MSG_DEBUG( "attempting to retrieve vertex container..."); 

  StatusCode sc;

  const xAOD::VertexContainer* m_vertices(0);
      

  if ( evtStore()->contains<xAOD::VertexContainer>(m_VxPrimContainerName)) {
	sc = evtStore()->retrieve(m_vertices,m_VxPrimContainerName);
  }

  if (sc.isFailure()) {
	ATH_MSG_ERROR("No collection with name " << m_VxPrimContainerName << " found in evtStore()");
	m_log<<"No collection with name " << m_VxPrimContainerName << " found in evtStore()\n";
	return StatusCode::FAILURE;
  }

        bool primary_vertex_found= false;

	nVtx = 0;
	int numTracksPerVertex=0;

	xAOD::VertexContainer::const_iterator vxItr = m_vertices->begin();
	xAOD::VertexContainer::const_iterator vxItrE = m_vertices->end();

	for (; vxItr != vxItrE; ++vxItr) {
	    if ( !((*vxItr)->vertexType()>0 && fabs((*vxItr)->z()) <100)) continue; 
	    //ATH_MSG_INFO("Vertex z impact parameter < 100mm.");
	    nVtx++;
	    //if (! ((*vxItr)->vxTrackAtVertexAvailable())) continue; 
	    if (!((*vxItr)->vertexType() == 1)) continue;
	    numTracksPerVertex = (*vxItr)->nTrackParticles();
	    //ATH_MSG_DEBUG("Checking number of tracks: "<<numTracksPerVertex);
	    hist("ntracks")->Fill(numTracksPerVertex);
	    if (! (numTracksPerVertex>=m_noverticesCut)) continue; 
	    //ATH_MSG_DEBUG("More than "<<m_noverticesCut<<" tracks at primary vertex exist.");
	    //ATH_MSG_INFO("number of tracks at primary vertex... "<<numTracksPerVertex);
	    primary_vertex_found=true;
	    //if (numTracksPerVertex>=(m_noverticesCut)) {
	    	//vertexCut = true;
	 	//}
	}
    
	hist("nVtx_total")->Fill(nVtx);

	if (!(primary_vertex_found)) return StatusCode::SUCCESS;
	ATH_MSG_DEBUG("primary vertex found.");	

	hist("nPVtx")->Fill(nVtx);

	Nevents_PVtx++;
	

	//-------------------------------------------------
	//looping over all trigger towers
	//-------------------------------------------------
	const xAOD::TriggerTowerContainer* TTs = 0;

	sc =  evtStore()->retrieve( TTs, "xAODTriggerTowers") ;
	if (sc.isFailure()){
	ATH_MSG_ERROR("Not retrievable.");
	m_log<<"xAODTriggerTower container not retrievable.\n";
	return StatusCode::FAILURE;
	}
	else {
	ATH_MSG_INFO( "Successfully retrieved xAODTriggerTowers.");
	}
	int goodhits =0;


  	for ( const auto* TT : *TTs ) {

		double caloquality = ttCellQuality(*TT);
		double caloEnergy= caloCellET(*TT); // now new function as caloCellET no longer readout - function sums over layers.
		if (caloEnergy <0) ATH_MSG_ERROR("Invalid calo energy!");
		unsigned int Peak = TT->peak();
		double TTlut = TT->lut_jep()[Peak];

		//get basic tower information: eta, phi, id, caldiv
		unsigned int id = TT->coolId();

		double eta =TT->eta();
		double phi = TT->phi();
		int part = (int) Part[id];
		int layer = TT->layer();
		std::vector<uint_least16_t> adc_vec = TT->adc();
		
		unsigned int nslices = adc_vec.size();

		//check whether the tower has some content at all

		if (nslices == 0) {
			ATH_MSG_WARNING("Trigger Tower with zero number of readout slices discarded!");
			m_log<<"Trigger Tower with zero number of readout slices discarded!\n";
			continue; //continue with next TT
		}

		//check ADC height -> throw away pulses that contain only noise
		//but were saved because the corresponding em or had towers showed signal
		int ADCheight = 0;
		ADCheight = (int) *max_element(adc_vec.begin(),adc_vec.end());
		if ( ADCheight < m_noiseThres ) continue;

		//start filling hit maps
		if (layer == 0) hist("emHits_total")->Fill(eta,phi);
		else hist("hadHits_total")->Fill(eta,phi);

		// cut on caloquality
		if (part == 0) hist("FCAL1qualitydistribution")->Fill(caloquality);
		if (part == 1) hist("EMECqualitydistribution")->Fill(caloquality);
		if (part == 2) hist("OVERLAPqualitydistribution")->Fill(caloquality);
		if (part == 3) hist("EMBqualitydistribution")->Fill(caloquality);
		if (part == 4) hist("FCAL2qualitydistribution")->Fill(caloquality);
		if (part == 5) hist("FCAL3qualitydistribution")->Fill(caloquality);
		if (part == 6) hist("HECqualitydistribution")->Fill(caloquality);
		if (part == 7) hist("TileEBqualitydistribution")->Fill(caloquality);
		if (part == 8) hist("TileLBqualitydistribution")->Fill(caloquality);
		if (!(part==7 || part==8)){
			if (caloquality<0 || caloquality >= m_qualityCut_loose) continue;
		}
		if (part==6 && caloquality >= m_qualityCut) continue;


		//hit maps
		if (layer==0) hist("emHits_quality")->Fill(eta,phi);
		else hist("hadHits_quality")->Fill(eta,phi);

		//define histograms for pulses
		

		if ( nslices == 15 ) {
			sprintf(pulse15HistName,"pulse15_%i_%i_%i_%u",part,m_lumibNo,m_eventNo,id);
			pulsehist15=new TH1D(pulse15HistName,"pulse15;Time [ns];Height [ADC counts]", 15, 0.,375.);
			pulsehist15->Sumw2();
		}
		else if (nslices == 7 ) {
			sprintf(pulse7HistName,"pulse7_%i_%i_%i_%u",part,m_lumibNo,m_eventNo,id);
			pulsehist7=new TH1D(pulse7HistName,"pulse7;Time [ns];Height [ADC counts]",7,100.,275.);
			pulsehist7->Sumw2();
		}
		else if (nslices == 5 ) {
			sprintf(pulse5HistName,"pulse5_%i_%i_%i_%u",part,m_lumibNo,m_eventNo,id);
			pulsehist5=new TH1D(pulse5HistName,"pulse5;Time [ns];Height [ADC counts]",5,125.,250.);
			pulsehist5->Sumw2();
		}
		else {
			ATH_MSG_ERROR("Unknown number of slices for emADC and hadADC values:\t" << nslices << "\t --> in lumiblock: " << m_lumibNo << " and TT: " << std::hex << id << std::dec);
			continue;
		}

		//read in histogram values from data file and set bin error
		for (unsigned int k = 0; k < nslices; k++){
			if ( nslices == 15 ) {
				pulsehist15->SetBinContent(k+1,adc_vec[k]);				
				pulsehist15->SetBinError(k+1,1.5); // validation of 1.5 error?
			}
			else if ( nslices == 7 ){
				pulsehist7->SetBinContent(k+1,adc_vec[k]);
				pulsehist7->SetBinError(k+1,1.5);
			}
			else {
				pulsehist5->SetBinContent(k+1,adc_vec[k]);
				pulsehist5->SetBinError(k+1,1.5);
			}
		}		
	
		//define pedestal and get basic pulse information: maximum bin and pulse height
		int pedestal;
		if (Pedestal.count(id)) pedestal= Pedestal[id];
		else pedestal=32.;
		if (pedestal<20. || pedestal > 45.) pedestal = 32.;

		int maxbin = 0;
		double maxval =  0.;
		if ( nslices == 15 ) {
			maxbin =  pulsehist15->GetMaximumBin();
			maxval =  pulsehist15->GetBinContent(maxbin);
		}
		else if ( nslices == 7 ){
			maxbin = pulsehist7->GetMaximumBin();
			maxval = pulsehist7->GetBinContent(maxbin);
		}
		else {
			maxbin =  pulsehist5->GetMaximumBin();
			maxval =  pulsehist5->GetBinContent(maxbin);
		}

		//exclude low energy pulses
		if ( maxval < m_lowThres) continue;// check pulse height
			//goto loopend;

		//exclude saturated pulses
		if ( maxval >= m_upThres) { //saturated
			if ( layer == 0 ) {
				hist("emSaturatedHits")->Fill(eta,phi);
			}
			else {
				hist("hadSaturatedHits")->Fill(eta,phi);
			}
			continue;
			//goto loopend;
	
		}

		goodhits++;
		//-------------------------------------------
		//from here on work only with good pulses
		//-------------------------------------------
		//fill hit maps
		if (layer == 0 ) hist("emHits")->Fill(eta,phi);
		else hist("hadHits")->Fill(eta,phi);
		//fill hit maps for high energy pulses
		if ( maxval >= 100. ) { // raised thesholds (Jan 2016)
			if (layer == 0) hist("emhighEHits")->Fill(eta,phi);
			else hist("hadhighEHits")->Fill(eta,phi);
		}
		if ( maxval >= 120. ) { // raised thesholds (Jan 2016)
			if (layer == 0) hist("emhugeEHits")->Fill(eta,phi);
			else hist("hadhugeEHits")->Fill(eta,phi);
		}
		//plotting the hit numbers with time
		hist("hitsVsLB")->Fill(m_lumibNo);
		//for the different CaloDivisions individually
		if ( part == 3 ) //EMB
			hist("hitsVsLB_EMB")->Fill(m_lumibNo);
		else if ( part == 1 ) //EMEC
			hist("hitsVsLB_EMEC")->Fill(m_lumibNo);
		else if ( part == 2 ) //emOverlap
			hist("hitsVsLB_EmOverlap")->Fill(m_lumibNo);
		else if ( part == 0 ) //FCal1
			hist("hitsVsLB_FCal1")->Fill(m_lumibNo);
		else if ( part ==7 || part == 8 ) //Tile
			hist("hitsVsLB_Tile")->Fill(m_lumibNo);
		else if ( part == 6 ) // HEC
			hist("hitsVsLB_HEC")->Fill(m_lumibNo);
		else if ( part == 4 || part == 5 ) //FCal23
			hist("hitsVsLB_FCal23")->Fill(m_lumibNo);

		//register peak bin histos and plot maximum bin in corresponding histo
		if ( nslices == 15 ) {
			if (!peakBinHistoRegist) {
				peakBinHistoRegist = true;
				CHECK(book(new TH1D("peakBinDistribution15slicesEMB","Distribution of peak position of pulses in EMB;Bin;Number of signals",15,0.5,15.5)));
				CHECK(book(new TH1D("peakBinDistribution15slicesEMEC","Distribution of peak position of pulses in EMEC;Bin;Number of signals",15,0.5,15.5)));
				CHECK(book(new TH1D("peakBinDistribution15slicesFCAL1","Distribution of peak position of pulses in FCAL1;Bin;Number of signals",15,0.5,15.5)));
				CHECK(book(new TH1D("peakBinDistribution15slicesTILE","Distribution of peak position of pulses in TILE;Bin;Number of signals",15,0.5,15.5)));
				CHECK(book(new TH1D("peakBinDistribution15slicesHEC","Distribution of peak position of pulses in HEC;Bin;Number of signals",15,0.5,15.5)));
				CHECK(book(new TH1D("peakBinDistribution15slicesFCAL2","Distribution of peak position of pulses in FCAL2;Bin;Number of signals",15,0.5,15.5)));
				CHECK(book(new TH1D("peakBinDistribution15slicesFCAL3","Distribution of peak position of pulses in FCAL3;Bin;Number of signals",15,0.5,15.5)));
			}
			if ( part == 3 ) //EMB
				hist("peakBinDistribution15slicesEMB")->Fill(maxbin);
			else if ( part == 1 ) //EMEC
				hist("peakBinDistribution15slicesEMEC")->Fill(maxbin);
			else if ( part == 0 ) //FCal1
				hist("peakBinDistribution15slicesFCAL1")->Fill(maxbin);
			else if ( part ==7 || part == 8 ) //Tile
				hist("peakBinDistribution15slicesTILE")->Fill(maxbin);
			else if ( part == 6 ) // HEC
				hist("peakBinDistribution15slicesHEC")->Fill(maxbin);
			else if ( part == 4 ) //FCal2
				hist("peakBinDistribution15slicesFCAL2")->Fill(maxbin);
			else if ( part == 5 ) //FCal3
				hist("peakBinDistribution15slicesFCAL3")->Fill(maxbin);
		}
		else if ( nslices == 7 ){
			if (!peakBinHistoRegist) {
				peakBinHistoRegist = true;
				CHECK(book(new TH1D("peakBinDistribution7slicesEMB","Distribution of peak position of pulses in EMB;Bin;Number of signals",7,0.5,7.5)));
				CHECK(book(new TH1D("peakBinDistribution7slicesEMEC","Distribution of peak position of pulses in EMEC;Bin;Number of signals",7,0.5,7.5)));
				CHECK(book(new TH1D("peakBinDistribution7slicesFCAL1","Distribution of peak position of pulses in FCAL1;Bin;Number of signals",7,0.5,7.5)));
				CHECK(book(new TH1D("peakBinDistribution7slicesTILE","Distribution of peak position of pulses in TILE;Bin;Number of signals",7,0.5,7.5)));
				CHECK(book(new TH1D("peakBinDistribution7slicesHEC","Distribution of peak position of pulses in HEC;Bin;Number of signals",7,0.5,7.5)));
				CHECK(book(new TH1D("peakBinDistribution7slicesFCAL2","Distribution of peak position of pulses in FCAL2;Bin;Number of signals",7,0.5,7.5)));
				CHECK(book(new TH1D("peakBinDistribution7slicesFCAL3","Distribution of peak position of pulses in FCAL3;Bin;Number of signals",7,0.5,7.5)));
			}
			if ( part == 3 ) //EMB
				hist("peakBinDistribution7slicesEMB")->Fill(maxbin);
			else if ( part == 1 ) //EMEC
				hist("peakBinDistribution7slicesEMEC")->Fill(maxbin);
			else if ( part == 0 ) //FCal1
				hist("peakBinDistribution7slicesFCAL1")->Fill(maxbin);
			else if ( part ==7 || part == 8 ) //Tile
				hist("peakBinDistribution7slicesTILE")->Fill(maxbin);
			else if ( part == 6 ) // HEC
				hist("peakBinDistribution7slicesHEC")->Fill(maxbin);
			else if ( part == 4 ) //FCal2
				hist("peakBinDistribution7slicesFCAL2")->Fill(maxbin);
			else if ( part == 5 ) //FCal3
				hist("peakBinDistribution7slicesFCAL3")->Fill(maxbin);
		}
		else if ( nslices == 5 ){
			if (!peakBinHistoRegist) {
				peakBinHistoRegist = true;
				CHECK(book(new TH1D("peakBinDistribution5slicesEMB","Distribution of peak position of pulses in EMB;Bin;Number of signals",5,0.5,5.5)));
				CHECK(book(new TH1D("peakBinDistribution5slicesEMEC","Distribution of peak position of pulses in EMEC;Bin;Number of signals",5,0.5,5.5)));
				CHECK(book(new TH1D("peakBinDistribution5slicesFCAL1","Distribution of peak position of pulses in FCAL1;Bin;Number of signals",5,0.5,5.5)));
				CHECK(book(new TH1D("peakBinDistribution5slicesTILE","Distribution of peak position of pulses in TILE;Bin;Number of signals",5,0.5,5.5)));
				CHECK(book(new TH1D("peakBinDistribution5slicesHEC","Distribution of peak position of pulses HEC;Bin;Number of signals",5,0.5,5.5)));
				CHECK(book(new TH1D("peakBinDistribution5slicesFCAL2","Distribution of peak position of pulses in FCAL2;Bin;Number of signals",5,0.5,5.5)));
				CHECK(book(new TH1D("peakBinDistribution5slicesFCAL3","Distribution of peak position of pulses in FCAL3;Bin;Number of signals",5,0.5,5.5)));
			}
			if ( part == 3 ) //EMB
				hist("peakBinDistribution5slicesEMB")->Fill(maxbin);
			else if ( part == 1 ) //EMEC
				hist("peakBinDistribution5slicesEMEC")->Fill(maxbin);
			else if ( part == 0 ) //FCal1
				hist("peakBinDistribution5slicesFCAL1")->Fill(maxbin);
			else if ( part ==7 || part == 8 ) //Tile
				hist("peakBinDistribution5slicesTILE")->Fill(maxbin);
			else if ( part == 6 ) // HEC
				hist("peakBinDistribution5slicesHEC")->Fill(maxbin);
			else if ( part == 4 ) //FCal2
				hist("peakBinDistribution5slicesFCAL2")->Fill(maxbin);
			else if ( part == 5 ) //FCal3
				hist("peakBinDistribution5slicesFCAL3")->Fill(maxbin);
		}

		//---------------------------------------------------
		//Start with the fitting procedure
		//---------------------------------------------------
		sc = fitTTsignal(nslices, id, layer, part , eta, phi, maxbin, pedestal, caloquality, caloEnergy, TTlut, adc_vec);
		if (sc.isFailure()){
			m_log<<"Fitting failed for TT"<<id<<" due to missing information.\n";
			ATH_MSG_ERROR("Fitting failed for TT"<<id<<" due to missing information.");
		}
	
		m_evtcnt++;

		}

  return StatusCode::SUCCESS;
}


// fits the digitized TT signal and stores the specified plots
StatusCode TimeFitting::fitTTsignal(unsigned int nslices, unsigned int id, int layer, int part, double eta, double phi, int maxbin, int pedestal, double caloquality, double caloEnergy, double TTlut, std::vector<uint_least16_t> adc_vec)
{
	ATH_MSG_DEBUG("fitTTsignal()");
	
	double sigmaLeft = 0.;
	double sigmaRight = 0.;
	double undVsAmpl = 0.;
	double cfsigl = 0;
	double cfsigr = 0;
	
	//define fit range
	int minRange = 0;
	int maxRange = 0;
	double minRangeValue = 0.;
	double maxRangeValue = 0.;
	double idtime = 0.;
	float maxvalue = 0.; // for energy calculation later


	TF1 *fit;

	if (part == 1 || part == 2 || part == 3 || part == 6 || part == 7 || part == 8){
		minRange = -25; 	
		maxRange = 50;
		if ( nslices == 15 ){
			minRangeValue = pulsehist15->GetBinCenter(maxbin)+minRange;
			maxRangeValue = pulsehist15->GetBinCenter(maxbin)+maxRange;
			idtime = pulsehist15->GetBinCenter(8);
			maxvalue = pulsehist15->GetBinContent(maxbin);
		}
		else if ( nslices == 7 ){
			minRangeValue = pulsehist7->GetBinCenter(maxbin)+minRange;
			maxRangeValue = pulsehist7->GetBinCenter(maxbin)+maxRange;
			idtime = pulsehist7->GetBinCenter(4);
			maxvalue = pulsehist7->GetBinContent(maxbin);
		}
		else {
			minRangeValue = pulsehist5->GetBinCenter(maxbin)+minRange;
			maxRangeValue = pulsehist5->GetBinCenter(maxbin)+maxRange;
			idtime = pulsehist5->GetBinCenter(3);
			maxvalue = pulsehist5->GetBinContent(maxbin);
		}
		if (part == 7 || part == 8) fit = new TF1("fit",Fit_GLuF,minRangeValue,maxRangeValue,6);
		else if ( part == 3 ) { fit = new TF1("fit",Fit_GLu,minRangeValue,maxRangeValue,6); ATH_MSG_WARNING("Using Glu function for EMB.");}
		else fit = new TF1("fit",Fit_LLuF,minRangeValue,maxRangeValue,6);
	}
	else if (part == 0 || part == 4 || part == 5){
		minRange = -25; 	
		maxRange = 25; 
		if ( nslices == 15 ){
			minRangeValue = pulsehist15->GetBinCenter(maxbin)+minRange;
			maxRangeValue = pulsehist15->GetBinCenter(maxbin)+maxRange;
			idtime = pulsehist15->GetBinCenter(8);
			maxvalue = pulsehist15->GetBinContent(maxbin);
		}
		else if ( nslices == 7 ){
			minRangeValue = pulsehist7->GetBinCenter(maxbin)+minRange;
			maxRangeValue = pulsehist7->GetBinCenter(maxbin)+maxRange;
			idtime = pulsehist7->GetBinCenter(4);
			maxvalue = pulsehist7->GetBinContent(maxbin);
		}
		else {
			minRangeValue = pulsehist5->GetBinCenter(maxbin)+minRange;
			maxRangeValue = pulsehist5->GetBinCenter(maxbin)+maxRange;
			idtime = pulsehist5->GetBinCenter(3);
			maxvalue = pulsehist5->GetBinContent(maxbin);
		}
		fit = new TF1("fit",Fit_LLuF,minRangeValue,maxRangeValue,6);	
	}
	else{
		ATH_MSG_ERROR("Invalid part returned. Stopping.");
		m_log<<"Invalid part returned for TT"<<id<<".\n";
		return StatusCode::FAILURE;
	}

			
	//set fit parameters
	fit->SetParNames("Maximumposition","Amplitude","sigmaLeft","sigmaRight", "undershoot", "pedestal");
	fit->SetParameter(0,minRangeValue+5.);
	fit->SetParLimits(0,minRangeValue,maxRangeValue);
	fit->SetParameter(1,80.);
	fit->SetParLimits(1, 20.,1900.);
	
	if (SigmaLeft.count(id) && SigmaRight.count(id) && UndVsAmpl.count(id)) {
		sigmaLeft = (double) SigmaLeft[id];
		sigmaRight = (double) SigmaRight[id];
		undVsAmpl = (double) UndVsAmpl[id];
	}			
	else {
		ATH_MSG_ERROR("No fixed parameters found for TT"<<id);
		m_log<<"No fixed parameters found for TT"<<id<<".\n";
		return StatusCode::FAILURE;
	}

	if (cfSigmal.count(id) && cfSigmar.count(id) ) {
		cfsigl = (double) cfSigmal[id]; 
		cfsigr = (double) cfSigmar[id]; 
	}
	else {
		ATH_MSG_ERROR("No fixed parameter correction factors found for TT"<<id);
		m_log<<"No fixed parameter correction factors found for TT"<<id<<".\n";
		return StatusCode::FAILURE;
	}

	fit->FixParameter(2,sigmaLeft*cfsigl);
	fit->FixParameter(3,sigmaRight*cfsigr);	
	
	//undershoot parameter
	//********* SOME INFO ON UNDERSHOOT (US) *****
	//run 1 config was US UNFIXED for EMB
	//run 2 found dependency US <-> sigma_right so also looked at fixing US parameter 
	//for fitting to 80 MHz pulses (for derivation of sigma correction factor), certainly kept US *fixed* as sigma_right values looked extreme.
	//current set-up here is run 1 set-up.
	//********************************************
	
	if ( part == 3 ) {
	  fit->SetParameter(4,1.);
	  fit->SetParLimits(4,0,350);
	}
	else { 
	  undVsAmpl = (double) UndVsAmpl[id];
	  fit->FixParameter(4,undVsAmpl);
	}
	ATH_MSG_WARNING("NOT fixing UvsA for EMB!");
	
/*	undVsAmpl = (double) UndVsAmpl[id];
	fit->FixParameter(4,undVsAmpl);  // fxing UvsA for all regions !!!
	ATH_MSG_WARNING("Fixing UvsA for all regions (incl. EMB)!");*/
	
	//pedestal
	fit->FixParameter(5,(double) pedestal);
	fit->SetLineColor(4);
	
	//apply the fit
	int refit = 1;
	if ( nslices == 15 )
		refit = (int) pulsehist15->Fit("fit","RQ");
	else if ( nslices == 7 )
		refit = (int) pulsehist7->Fit("fit","RQ");
	else
		refit = (int) pulsehist5->Fit("fit","RQ");
	//fit statistics
	nfits++;
	NFits[id] = NFits[id]+1;			
			
	//get fit parameters
	double par[2];
	par[0] = fit->GetParameter(0);
	par[1] = fit->GetParameter(1);
	double err = fit->GetParError(0);
	
	//check whether fit is failed or not
	int iFF = 1;
	if ( nslices == 15 )
		iFF = isFitFailed(refit,pulsehist15,part,maxbin,fit,par[0],par[1],pedestal);
	else if ( nslices == 7 )
		iFF = isFitFailed(refit,pulsehist7,part,maxbin,fit,par[0],par[1],pedestal);
	else
		iFF = isFitFailed(refit,pulsehist5,part,maxbin,fit,par[0],par[1],pedestal);

	if ( part >= 0 ){
		//define energies
		//float caloEnergy = 0.;
		double bincenter = 0.;
		std::vector<float> tmp;
		bool pulseOK = true;
		float ADCenergy= 0;
		//determine energies
		//if ( getCaloEnergy(id,n,nttEM) == -1 )
		//	LOG_WARNING << "Error in getCaloEnergy() function for TT " << std::hex<<id<<std::dec << endreq;
		//else
		//	caloEnergy = getCaloEnergy(id,n,nttEM);
		for (unsigned int k = 0; k < nslices; k++){
			tmp.push_back(0.);
			ADCenergy = getADCenergy(float(adc_vec[k]), pedestal);
			//caloEnergy = getADCenergy(float(adc_vec[2]),float(pedestal));
			//fill histogram
			if ( caloEnergy > 0. ) {
				//tmp.at(k) = float(adc_vec[k])/float(maxvalue);
				tmp.at(k) = ADCenergy/caloEnergy;
			}
			else {
				pulseOK = false; //only use pulses for the energy normalization where all bins contain valid energy
				ATH_MSG_WARNING("ADC energy not valid! Energy is "<<ADCenergy<<".");
				break;
			}
		}
		if ( pulseOK ) {  
		        ATH_MSG_INFO ("pulse ok... ");
			
			std::string hdir;
			if (part>=0 && part<=3) hdir = "EM/normalised/";
			else hdir = "HAD/normalised/";
			/*
			char histname[40];// = (char *) malloc(snprintf(NULL, 0, "energyNormPulse0x%08x",id) + 1);
			sprintf(histname, "energyNormPulse0x%08x",id);
			if (!h_energyNormPulses.count(id) ) {
				ATH_MSG_INFO("about to create normalised pulse..");
				char histtitle[100]; //= (char *) malloc(snprintf(NULL, 0, "%s, eta=%.2f, phi=%.2f;Time [ns];E_{ADC}/E_{calo}",histname,eta,phi) + 1);
				sprintf(histtitle, "%s, eta=%.2f, phi=%.2f;Time [ns];E_{ADC}/E_{calo}",histname,eta,phi);
				if ( nslices == 15 ) h_energyNormPulses[id] = new TProfile(histname,histtitle,15, 93.75,281.25);
				if ( nslices == 5 )  h_energyNormPulses[id] = new TProfile(histname,histtitle,5,125.,250.);
				h_energyNormPulses[id]->SetTitle(histtitle);
				ATH_MSG_INFO("about to register normalised pulse..");
				CHECK(histSvc()->regHist(m_InfoHistsStream+hdir+histname,h_energyNormPulses[id]));
			}
			char hname_colour[50];// = (char *) malloc(snprintf(NULL, 0, "energyNormPulse0x%08x_col",id) + 1);
			sprintf(hname_colour, "energyNormPulse0x%08x_col",id);
			if (!h_energyNormPulses_colour.count(id) ) {
				char histtitle_color[100];// = (char *) malloc(snprintf(NULL, 0, "%s, eta=%.2f, phi=%.2f;Time [ns];E_{ADC}/E_{calo}",histname_color,eta,phi) + 1);
				sprintf(histtitle_color, "%s;Time [ns];E_{ADC}/E_{calo}",hname_colour);
				//if ( nslices == 15 ) CHECK(book(new TH2D(histname_color,"normalised pulse",15, 93.75,281.25,226,-0.51,1.5)));
				//if ( nslices == 5 ) CHECK(book(new TH2D(histname_color,"normalised pulse",5,125.,250.,148,-0.51,1.5)));
				if ( nslices == 5 ) h_energyNormPulses_colour[id] = new TH2D(hname_colour,histtitle_color,5,125.,250.,126,-0.51,2.01);
				if ( nslices == 15 ) h_energyNormPulses_colour[id] = new TH2D(hname_colour,histtitle_color,15, 93.75,281.25,176,-0.51,3.01);
				h_energyNormPulses_colour[id]->SetOption("colz");
				h_energyNormPulses_colour[id]->SetTitle(histtitle_color);
				CHECK(histSvc()->regHist(m_InfoHistsStream+hdir+hname_colour,h_energyNormPulses_colour[id]));
			}
			for (unsigned int k = 0; k < nslices; k++){
				bincenter = h_energyNormPulses[id]->GetBinCenter(k+1);
				h_energyNormPulses[id]->Fill(bincenter,tmp.at(k));
				h_energyNormPulses_colour[id]->Fill(bincenter,tmp.at(k));
			 }*/
			 
			 //if (m_isBG) CHECK(fillNormPulse( id, h_energyNormPulses_colour_bgflagged, m_PulseHistsStream_BGtwoside, tmp, nslices, hdir));
			 //if (m_isLArBurst) CHECK(fillNormPulse( id, h_energyNormPulses_colour_larburstflagged, m_PulseHistsStream_LArBurst, tmp, nslices, hdir));
			 CHECK(fillNormPulse( id, h_energyNormPulses_colour, m_InfoHistsStream, tmp, nslices, hdir));	 
	        }
	}

		
	if ( iFF == 0 ){ //Fit worked
		ATH_MSG_INFO("fit worked");
		double pardiff = par[0]-idtime;
		float TTenergy = getADCenergy(maxvalue,float(pedestal));
		/*if (m_isBG || m_isLArBurst) {
		  if (m_isBG) BGflaggedTimingPropagation[id].push_back(pardiff); 
		  if (m_isLArBurst) LArBURSTflaggedTimingPropagation[id].push_back(pardiff); 
		  ATH_MSG_DEBUG("flagged as BIB, discontinuing..");
		  return StatusCode::SUCCESS;  
		}*/
		TimingPropagation[id].push_back(pardiff);
		CaloQualityPropagation[id].push_back(caloquality);
		ETPropagation[id].push_back(TTenergy);
		SigmaLeft_Fit[id].push_back(fit->GetParameter(2));
		SigmaRight_Fit[id].push_back(fit->GetParameter(3));

		double undvsamp;
		if ( part == 3 ) undvsamp = fit->GetParameter(4);//par[1];
		else undvsamp = fit->GetParameter(4);
		UndVsAmpl_Fit[id].push_back(undvsamp);
		
		if (!(m_EMtrig_passed && m_Jtrig_passed)) {//(!((m_EMtrig_passed? 1:0) + (m_Jtrig_passed? 1:0) > 1)) {
			if (m_EMtrig_passed) TimingPropagationEM[id].push_back(pardiff);
			else if (m_Jtrig_passed) TimingPropagationJET[id].push_back(pardiff);
		}
		
		//fit success statistics
		ngoodfits++;	
		NGoodFits[id] = NGoodFits[id]+1;
		//fill trigger tower timing histograms
		//h_towerTiming[id]->Fill(pardiff);
		FittedTPeaks[id]=FittedTPeaks[id]+par[0];
		double chi2 = fit->GetChisquare ();	
		ATH_MSG_DEBUG("Chi2 is ..."<<chi2);		
		//write out a certain number of pulse histograms for every TT
		if ( id == 1966336 || id == 18415619 || id == 1445120 || id == 18547201 || id == 1445376 || id == 1770499  || id == 18091264 || id == 52038146  || id == 52036353 || id == 51709952 || id == 35327488 || id == 51644160 || id == 84935170 || id == 84937987 || id == 84936448 || id == 119210242 || id == 102434561 || id == 68750851 || id == 69009923 || id == 1507585 || id == 18091778 ){ 
			ATH_MSG_DEBUG("printing out success..");
			char * cloneROOTname = (char *) malloc(snprintf(NULL, 0, "pulsefit_run%i_event%i_id0x%08x_part%i",m_runNo,m_eventNo,id, part) + 1);
			sprintf(cloneROOTname, "pulsefit_run%i_event%i_id0x%08x_part%i",m_runNo,m_eventNo,id, part);
			char * cloneTitle = (char *) malloc(snprintf(NULL, 0, "pulsefit_run%i_event%i_id0x%08x_Chi2=%f",m_runNo,m_eventNo,id, chi2) + 1);
			sprintf(cloneTitle, "pulsefit_run%i_event%i_id0x%08x_Chi2=%f",m_runNo,m_eventNo,id, chi2);
			if ( nslices == 15 )
				CHECK(book((TH1*)pulsehist15->Clone(cloneROOTname)));
			else if ( nslices == 7 )
				CHECK(book((TH1*)pulsehist7->Clone(cloneROOTname)));
			else
				CHECK(book((TH1*)pulsehist5->Clone(cloneROOTname)));
			hist(cloneROOTname)->SetTitle(cloneTitle);
			free(cloneROOTname);
			free(cloneTitle);
		}

		//check energy distribution of signals and energy dependence of timing
		if ( layer == 0 ){ // em layer
			hist("emADCdistribution")->Fill(maxvalue);
			hist("emEtdistribution")->Fill(TTenergy);
			hist("emtpeakdistribution")->Fill(par[0]);
			if (part==0) {hist("FCALEM_qualitytimecorr")->Fill(pardiff, caloquality);}
			if (part==1) {hist("EMEC_qualitytimecorr")->Fill(pardiff, caloquality);}
			if (part==2) {hist("OVERLAP_qualitytimecorr")->Fill(pardiff, caloquality);}
			if (part==3) {hist("EMB_qualitytimecorr")->Fill(pardiff, caloquality);}
			if (caloEnergy>0.) {
			double dev_l1caloE = (caloEnergy-TTenergy)/caloEnergy;
			double dev_lutE = (caloEnergy-TTlut)/caloEnergy;
			if (part == 3)
			  { hist("caloEvsL1CaloE_EMB")->Fill((caloEnergy-TTenergy)/caloEnergy); hist("caloEvsLUT_EMB")->Fill((caloEnergy-TTlut)/caloEnergy); }
			if (part == 2)
			  { hist("caloEvsL1CaloE_Overlap")->Fill((caloEnergy-TTenergy)/caloEnergy); hist("caloEvsLUT_Overlap")->Fill((caloEnergy-TTlut)/caloEnergy); }
			if (part == 1)
			  { hist("caloEvsL1CaloE_EMEC")->Fill((caloEnergy-TTenergy)/caloEnergy); hist("caloEvsLUT_EMEC")->Fill((caloEnergy-TTlut)/caloEnergy); }
			if (part == 0)
			  { hist("caloEvsL1CaloE_FCAL1")->Fill((caloEnergy-TTenergy)/caloEnergy); hist("caloEvsLUT_FCAL1")->Fill((caloEnergy-TTlut)/caloEnergy); }
			
			unsigned int etabin = hist("caloEvsL1CaloEaccumulated_EMmap")->GetXaxis()->FindBin(eta);
			unsigned int phibin = hist("caloEvsL1CaloEaccumulated_EMmap")->GetYaxis()->FindBin(phi);			
			double current_sum_l1caloE = hist("caloEvsL1CaloEaccumulated_EMmap")->GetBinContent(etabin, phibin);
			double new_sum_l1caloE = current_sum_l1caloE+dev_l1caloE; //((current_avg_l1caloE*(NGoodFits[id]-1))+dev_l1caloE)/NGoodFits[id];
			hist("caloEvsL1CaloEaccumulated_EMmap")->SetBinContent(etabin, phibin, new_sum_l1caloE);
			double current_sum_lutE = hist("caloEvsLUTaccumulated_EMmap")->GetBinContent(etabin, phibin);
			double new_sum_lutE = current_sum_lutE + dev_lutE; //((current_avg_lutE*(NGoodFits[id]-1))+dev_lutE)/NGoodFits[id];			
		        hist("caloEvsLUTaccumulated_EMmap")->SetBinContent(etabin, phibin, new_sum_lutE);
			}
		}
		else if ( layer == 1 ) { // had layer
			hist("hadADCdistribution")->Fill(maxvalue);
			hist("hadEtdistribution")->Fill(TTenergy);
			hist("hadtpeakdistribution")->Fill(par[0]);
			if (part==4 || part==5) {hist("FCALHAD_qualitytimecorr")->Fill(pardiff, caloquality);}
			if (part==6) {hist("HEC_qualitytimecorr")->Fill(pardiff, caloquality);}
			if (part==7 || part ==8 ) {hist("TILE_qualitytimecorr")->Fill(pardiff, caloquality);}
			if (caloEnergy>0.) {
			double dev_l1caloE = (caloEnergy-TTenergy)/caloEnergy;
			double dev_lutE = (caloEnergy-TTlut)/caloEnergy;
			if (part == 7 || part == 8)
			  { hist("caloEvsL1CaloE_Tile")->Fill((caloEnergy-TTenergy)/caloEnergy); hist("caloEvsLUT_Tile")->Fill((caloEnergy-TTlut)/caloEnergy); }
			if (part == 6)
			  { hist("caloEvsL1CaloE_HEC")->Fill((caloEnergy-TTenergy)/caloEnergy); hist("caloEvsLUT_HEC")->Fill((caloEnergy-TTlut)/caloEnergy); }
			if (part == 5)
			  { hist("caloEvsL1CaloE_FCAL3")->Fill((caloEnergy-TTenergy)/caloEnergy); hist("caloEvsLUT_FCAL3")->Fill((caloEnergy-TTlut)/caloEnergy); }
			if (part == 4)
			  { hist("caloEvsL1CaloE_FCAL2")->Fill((caloEnergy-TTenergy)/caloEnergy); hist("caloEvsLUT_FCAL2")->Fill((caloEnergy-TTlut)/caloEnergy); }
			unsigned int etabin = hist("caloEvsL1CaloEaccumulated_HADmap")->GetXaxis()->FindBin(eta);
			unsigned int phibin = hist("caloEvsL1CaloEaccumulated_HADmap")->GetYaxis()->FindBin(phi);			
			double current_sum_l1caloE = hist("caloEvsL1CaloEaccumulated_HADmap")->GetBinContent(etabin, phibin);
			double new_sum_l1caloE = current_sum_l1caloE + dev_l1caloE; //((current_avg_l1caloE*(NGoodFits[id]-1))+dev_l1caloE)/NGoodFits[id];
			hist("caloEvsL1CaloEaccumulated_HADmap")->SetBinContent(etabin, phibin, new_sum_l1caloE);
			double current_sum_lutE = hist("caloEvsLUTaccumulated_HADmap")->GetBinContent(etabin, phibin);
			double new_sum_lutE = current_sum_lutE + dev_lutE; // ((current_sum_lutE*(NGoodFits[id]-1))+dev_lutE)/NGoodFits[id];			
		        hist("caloEvsLUTaccumulated_HADmap")->SetBinContent(etabin, phibin, new_sum_lutE);
			}
		}
	}
	else if ( iFF == 1 ) { //Fit failed
		ATH_MSG_INFO("fit failed");
		//fit failed statistics
		nfailedfits++;
		NFailedFits[id] = NFailedFits[id]+1;
		//write out a certain number of failed fits
		if ( nfailedfits%10000==0 ){
			ATH_MSG_INFO("printing out failed fit..");
			//write out failed fits
			char * cloneROOTname = (char *) malloc(snprintf(NULL, 0, "pulsefit_run%i_event%i_trigTow0x%08x_part%i_failed",m_runNo,m_eventNo,id, part) + 1);
			sprintf(cloneROOTname, "pulsefit_run%i_event%i_trigTow0x%08x_part%i_failed",m_runNo,m_eventNo,id, part);
			char * cloneTitle = (char *) malloc(snprintf(NULL, 0, "pulsefit_run%i_event%i_trigTow0x%08x",m_runNo,m_eventNo,id) + 1);
			sprintf(cloneTitle, "pulsefit_run%i_event%i_trigTow0x%08x",m_runNo,m_eventNo,id);
			if ( nslices == 15 )
				CHECK(book((TH1*)pulsehist15->Clone(cloneROOTname)));
			else if ( nslices == 7 )
				CHECK(book((TH1*)pulsehist7->Clone(cloneROOTname)));
			else
				CHECK(book((TH1*)pulsehist5->Clone(cloneROOTname)));
			hist(cloneROOTname)->SetTitle(cloneTitle);
		}
		//distinguish different fit criteria
		int histocolor = 0;
		if ( nslices == 15 )
			histocolor = pulsehist15->GetLineColor();
		else if ( nslices == 7 )
			histocolor = pulsehist7->GetLineColor();
		else
			histocolor = pulsehist5->GetLineColor();
		if ( histocolor == 2 ) // red, i.e. failed at criterion 1
			NFailedFits1[id] = NFailedFits1[id]+1;
		else if ( histocolor == 6 ) // pink, i.e. failed at criterion 2
			NFailedFits2[id] = NFailedFits2[id]+1;
		else if ( histocolor == kOrange-3 ) // orange, i.e. failed at criterion 3
			NFailedFits3[id] = NFailedFits3[id]+1;
		else {
			ATH_MSG_WARNING("Fit in TT " << std::hex << id << std::dec << " failed for unknown reason!");
			m_log<<"Fit in TT " << std::hex << id << std::dec << " failed for unknown reason!\n";
		}

	}
	else { //Parabola fit in isFitFailed method failed
		ATH_MSG_WARNING("error in isFitFailed determination for channel: " << std::hex << id << std::dec);
		m_log<<"error in isFitFailed determination for channel: " << std::hex << id << std::dec<<"\n";
		//fit error statistics
		nerrorfits++;
		NErrorFits[id] = NErrorFits[id]+1;
	}
	delete fit;

	return StatusCode::SUCCESS;
}


float TimeFitting::getADCenergy(float m_ADCvalue, float pedestal)
{
	//ATH_MSG_DEBUG("getting ADCenergy");
	
	//convert ADC counts into energy
	float ADCenergy = (m_ADCvalue-pedestal)/4.;
	
	return ADCenergy;
}



// function to setup all histograms
StatusCode TimeFitting::setupHistograms()
{
  ATH_MSG_DEBUG("setting up Histograms()");

 // vertex histograms

 CHECK(book(new TH1D("nVtx_total","Distribution of number of all primary vertices;Number of primary vertices;Events",21,-0.5,20.5)));
 CHECK(book(new TH1D("nPVtx","Distribution of number of primary vertices (after vertex cut);Number of primary vertices;Events",21,-0.5,20.5)));
 CHECK(book(new TH1D("ntracks","Distribution of number of tracks at primary vertex (cut at 5);Number of tracks;Events",101,-0.5,100.5)));

 // quality histograms

  CHECK(book(new TH2TT("emHits_quality","Total Number of good quality hits in em layer;#eta;#phi")));
  hist("emHits_quality")->SetOption("colz");
  CHECK(book(new TH2TT("hadHits_quality","Total Number of good quality hits in had layer;#eta;#phi")));
  hist("hadHits_quality")->SetOption("colz");

  CHECK(book(new TH2D("EMB_qualitytimecorr","corr. calo quality-time offset (EMB);timing offset [ns];quality", 60, -30, 30, 2000, 0., 20000.)));
  hist("EMB_qualitytimecorr")->SetOption("colz");
  CHECK(book(new TH2D("OVERLAP_qualitytimecorr","corr. calo quality-time offset (OVERLAP);timing offset [ns];quality", 60, -30, 30, 2000, 0., 20000.)));
  hist("OVERLAP_qualitytimecorr")->SetOption("colz");
  CHECK(book(new TH2D("EMEC_qualitytimecorr","corr. calo quality-time offset (EMEC);timing offset [ns];quality", 60, -30, 30, 2000, 0., 20000.)));
  hist("EMEC_qualitytimecorr")->SetOption("colz");
  CHECK(book(new TH2D("FCALEM_qualitytimecorr","corr. calo quality-time offset (FCALEM)timing offset [ns];quality", 60, -30, 30, 2000, 0., 20000.)));
  hist("FCALEM_qualitytimecorr")->SetOption("colz");


  CHECK(book(new TH2D("TILE_qualitytimecorr","corr. calo quality-time offset (TileLB);timing offset [ns];quality", 60, -30, 30, 2000, 0., 20000.)));
  hist("TILE_qualitytimecorr")->SetOption("colz");
  CHECK(book(new TH2D("HEC_qualitytimecorr","corr. calo quality-time offset (HEC);timing offset [ns];quality", 60, -30, 30, 2000, 0., 20000.)));
  hist("HEC_qualitytimecorr")->SetOption("colz");
  CHECK(book(new TH2D("FCALHAD_qualitytimecorr","corr. calo quality-time offset (FCALHAD);timing offset [ns];quality", 60, -30, 30, 2000, 0., 20000.)));
  hist("FCALHAD_qualitytimecorr")->SetOption("colz");
  
  // energy calibration check histograms
  int devE_lorange = -30;
  int devE_hirange = 30;
  unsigned int devEbins = (devE_hirange-devE_lorange)*15;
  CHECK(book(new TH1D("caloEvsL1CaloE_EMB","deviation of L1CaloEnergy from caloEnergy (EMB);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsLUT_EMB","deviation of L1Calo jep LUT from caloEnergy (EMB);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsL1CaloE_EMEC","deviation of L1CaloEnergy from caloEnergy (EMEC);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsLUT_EMEC","deviation of L1Calo jep LUT from caloEnergy (EMEC);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsL1CaloE_FCAL1","deviation of L1CaloEnergy from caloEnergy (FCal1);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsLUT_FCAL1","deviation of L1Calo jep LUT from caloEnergy (FCal1);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsL1CaloE_Overlap","deviation of L1CaloEnergy from caloEnergy (Overlap);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsLUT_Overlap","deviation of L1Calo jep LUT from caloEnergy (Overlap);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsL1CaloE_Tile","deviation of L1CaloEnergy from caloEnergy (Tile);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsLUT_Tile","deviation of L1Calo jep LUT from caloEnergy (Tile);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsL1CaloE_HEC","deviation of L1CaloEnergy from caloEnergy (HEC);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsLUT_HEC","deviation of L1Calo jep LUT from caloEnergy (HEC);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsL1CaloE_FCAL2","deviation of L1CaloEnergy from caloEnergy (FCal2);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsLUT_FCAL2","deviation of L1Calo jep LUT from caloEnergy (FCal2);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsL1CaloE_FCAL3","deviation of L1CaloEnergy from caloEnergy (FCal3);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  CHECK(book(new TH1D("caloEvsLUT_FCAL3","deviation of L1Calo jep LUT from caloEnergy (FCal3);(caloE-L1CaloE)/caloE;Events",devEbins,devE_lorange,devE_hirange)));
  
  CHECK(book(new TH2TT("caloEvsL1CaloEaccumulated_EMmap", "L1Calo energy deviation from caloEnergy (EM); #eta;#phi")));
  CHECK(book(new TH2TT("caloEvsLUTaccumulated_EMmap", "LUT energy deviation from caloEnergy (EM); #eta;#phi")));
  CHECK(book(new TH2TT("caloEvsL1CaloEaccumulated_HADmap", "L1Calo energy deviation from caloEnergy (HAD); #eta;#phi")));
  CHECK(book(new TH2TT("caloEvsLUTaccumulated_HADmap", "LUT energy deviation from caloEnergy (HAD); #eta;#phi")));
  hist("caloEvsL1CaloEaccumulated_EMmap")->SetOption("colz");
  hist("caloEvsLUTaccumulated_EMmap")->SetOption("colz");
  hist("caloEvsL1CaloEaccumulated_HADmap")->SetOption("colz");
  hist("caloEvsLUTaccumulated_HADmap")->SetOption("colz");

  //hit maps
  CHECK(book(new TH2TT("emHits_total","Total Number of Hits in em layer (only disabled TTs excluded);#eta;#phi")));
  hist("emHits_total")->SetOption("colz");
  CHECK(book(new TH2TT("hadHits_total","Total Number of Hits in had layer (only disabled TTs excluded);#eta;#phi")));
  hist("hadHits_total")->SetOption("colz");
//  CHECK(book( new TH2TT("emHits_quality","Number of Hits in em layer with good caloQuality;#eta;#phi")));
//  CHECK(book( new TH2TT("hadHits_quality","Number of Hits in had layer with good caloQuality;#eta;#phi")));
  CHECK(book( new TH2TT("emHits","Number of good Hits in em layer;#eta;#phi")));
  hist("emHits")->SetOption("colz");
  CHECK(book( new TH2TT("hadHits","Number of good Hits in had layer;#eta;#phi")));
  hist("hadHits")->SetOption("colz");
  CHECK(book( new TH2TT("emhighEHits","Number of good Hits with ADC_max>=80 in em layer;#eta;#phi")));
  hist("emhighEHits")->SetOption("colz");
  CHECK(book( new TH2TT("hadhighEHits","Number of good Hits with ADC_max>=80 in had layer;#eta;#phi")));
  hist("hadhighEHits")->SetOption("colz");
  CHECK(book( new TH2TT("emhugeEHits","Number of good Hits with ADC_max>=100 in em layer;#eta;#phi")));
  hist("emhugeEHits")->SetOption("colz");
  CHECK(book( new TH2TT("hadhugeEHits","Number of good Hits with ADC_max>=100 in had layer;#eta;#phi")));
  hist("hadhugeEHits")->SetOption("colz");
  CHECK(book( new TH1I("hitsVsLB","hitsVsLB",6000,0,6000)));
  CHECK(book( new TH1I("hitsVsLB_EMB","hitsVsLB_EMB",6000,0,6000)));
  CHECK(book( new TH1I("hitsVsLB_EMEC","hitsVsLB_EMEC",6000,0,6000)));
  CHECK(book( new TH1I("hitsVsLB_EmOverlap","hitsVsLB_EmOverlap",6000,0,6000)));
  CHECK(book( new TH1I("hitsVsLB_FCal1","hitsVsLB_FCal1",6000,0,6000)));
  CHECK(book( new TH1I("hitsVsLB_Tile","hitsVsLB_Tile",6000,0,6000)));
  CHECK(book( new TH1I("hitsVsLB_HEC","hitsVsLB_HEC",6000,0,6000)));
  CHECK(book( new TH1I("hitsVsLB_FCal23","hitsVsLB_FCal23",6000,0,6000)));

  //bg checks
  CHECK(book( new TH1I("backgroundflags_rm_hist","background flags (bit-20 events removed)",5,-0.5,4.5)));
  CHECK(book( new TH1I("backgroundflags_hist","background flags",5,-0.5,4.5)));
  CHECK(book( new TH1I("BGevtsVsLB","BGevtsVsLB",6000,0,6000))); 
  
 
  // for saturation studies

  CHECK(book( new TH2TT("emSaturatedHits","Number of saturated Hits in em layer;#eta;#phi")));
  hist("emSaturatedHits")->SetOption("colz");
  CHECK(book(new TH2TT("hadSaturatedHits","Number of saturated Hits in had layer;#eta;#phi")));
  hist("hadSaturatedHits")->SetOption("colz");


  //fit statistics
  CHECK(book(new TH2TT("emNFits","Number of applied Fits in em layer;#eta;#phi")));
  hist("emNFits")->SetOption("colz");
  CHECK(book(new TH2TT("hadNFits","Number of applied Fits in had layer;#eta;#phi")));
  hist("hadNFits")->SetOption("colz");
  CHECK(book(new TH2TT("emNGoodFits","Number of good fits in em layer;#eta;#phi")));
  hist("emNGoodFits")->SetOption("colz");
  CHECK(book(new TH2TT("hadNGoodFits","Number of good fits in had layer;#eta;#phi")));
  hist("hadNGoodFits")->SetOption("colz");
  CHECK(book(new TH2TT("emNFailedFits","Number of failed fits in em layer;#eta;#phi")));
  hist("emNFailedFits")->SetOption("colz");
  CHECK(book(new TH2TT("hadNFailedFits","Number of failed fits in had layer;#eta;#phi")));
  hist("hadNFailedFits")->SetOption("colz");
  CHECK(book(new TH2TT("emNFailedFits_crit1","Number of failed fits at criterion 1 in em layer;#eta;#phi")));
  hist("emNFailedFits_crit1")->SetOption("colz");
  CHECK(book(new TH2TT("emNFailedFits_crit2","Number of failed fits at criterion 2 in em layer;#eta;#phi")));
  hist("emNFailedFits_crit2")->SetOption("colz");
  CHECK(book(new TH2TT("emNFailedFits_crit3","Number of failed fits at criterion 3 in em layer;#eta;#phi")));
  hist("emNFailedFits_crit3")->SetOption("colz");
  CHECK(book(new TH2TT("hadNFailedFits_crit1","Number of failed fits at criterion 1 in had layer;#eta;#phi")));
  hist("hadNFailedFits_crit1")->SetOption("colz");
  CHECK(book(new TH2TT("hadNFailedFits_crit2","Number of failed fits at criterion 2 in had layer;#eta;#phi")));
  hist("hadNFailedFits_crit2")->SetOption("colz");
  CHECK(book(new TH2TT("hadNFailedFits_crit3","Number of failed fits at criterion 3 in had layer;#eta;#phi")));
  hist("hadNFailedFits_crit3")->SetOption("colz");
  CHECK(book(new TH2TT("emNErrorFits","Number of error fits in em layer;#eta;#phi")));
  hist("emNErrorFits")->SetOption("colz");
  CHECK(book(new TH2TT("hadNErrorFits","Number of error fits in had layer;#eta;#phi")));
  hist("hadNErrorFits")->SetOption("colz");

  CHECK(book(new TH2TT("emTPeak","fitted Tpeaks in em layer;#eta;#phi")));
  hist("emTPeak")->SetOption("colz");
  CHECK(book(new TH2TT("hadTPeak","fitted Tpeaks in had layer;#eta;#phi")));
  hist("hadTPeak")->SetOption("colz");
  
  //energy dependence of hitnumbers and timing corrections
  CHECK(book(new TH1D("emADCdistribution","ADC count distribution in em layer;Energy [ADC counts];Events/4ADC counts",256,0.,1023.)));
  CHECK(book(new TH1D("hadADCdistribution","ADC count distribution in had layer;Energy [ADC counts];Events/4ADC counts",256,0.,1023.)));
  CHECK(book( new TH1D("emEtdistribution","Et distribution in em layer; E_{T} [GeV];Events/1GeV",256,0.,256.)));  
  CHECK(book( new TH1D("hadEtdistribution","Et distribution in had layer; E_{T} [GeV];Events/1GeV",256,0.,256.)));  

  CHECK(book(new TH1D("emtpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("hadtpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));

  CHECK(book(new TH1D("EMBtpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("OVERLAPtpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("EMECtpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("FCAL1tpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("FCAL2tpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("FCAL3tpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("TileLBtpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("TileEBtpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));
  CHECK(book(new TH1D("HECtpeakdistribution","t peak distribution in em layer;timing [ns];counts",125,125.,250.)));

  CHECK(book(new TH1D("EMBqualitydistribution","caloquality distribution in emb layer;timing [ns];counts",10000,0,50000.)));
  CHECK(book(new TH1D("OVERLAPqualitydistribution","caloquality peak distribution in overlap layer;timing [ns];counts",10000,0,50000.)));
  CHECK(book(new TH1D("EMECqualitydistribution","caloquality peak distribution in emec layer;timing [ns];counts",10000,0,50000.)));
  CHECK(book(new TH1D("FCAL1qualitydistribution","caloquality peak distribution in fcal1 layer;timing [ns];counts",10000,0,50000.)));
  CHECK(book(new TH1D("FCAL2qualitydistribution","caloquality peak distribution in fcal2 layer;timing [ns];counts",10000,0,50000.)));
  CHECK(book(new TH1D("FCAL3qualitydistribution","caloquality peak distribution in fcal3 layer;timing [ns];counts",10000,0,50000.)));
  CHECK(book(new TH1D("TileLBqualitydistribution","caloquality peak distribution in tilelb layer;timing [ns];counts",10000,0,50000.)));
  CHECK(book(new TH1D("TileEBqualitydistribution","caloquality peak distribution in tileeb layer;timing [ns];counts",10000,0,50000.)));
  CHECK(book(new TH1D("HECqualitydistribution","caloquality peak distribution in hec layer;timing [ns];counts",10000,0,50000.)));

 return StatusCode::SUCCESS;

}

double TimeFitting::caloCellET(const xAOD::TriggerTower& tt) {
/*	static xAOD::TriggerTower::ConstAccessor<std::vector<float>> 
	caloCellEtByLayer("CaloCellETByLayer");
	if(!caloCellEtByLayer.isAvailable(tt)) return -1000.;
	const std::vector<float>& v = caloCellEtByLayer(tt);

	return std::accumulate(std::begin(v), std::end(v), 0.);*/

      // Get Calo  ET
      double caloE(0.);
      double eta = tt.eta();
      double absEta = fabs(eta);
      std::vector<float> CaloEnergyLayers = tt.auxdataConst< std::vector<float> > ("CaloCellEnergyByLayer");
      std::vector<float> CaloETLayers = tt.auxdataConst< std::vector<float> > ("CaloCellETByLayer");
     
      if (absEta < 3.2) {
        if ( tt.isAvailable< std::vector<float> > ("CaloCellEnergyByLayer") ) {
          for (std::vector<float>::iterator it = CaloEnergyLayers.begin(); it != CaloEnergyLayers.end(); it++) { 
            caloE += *it;
          }
          caloE = caloE / cosh(eta);
        }
      } else {  // FCal: need to use different method due to large variation in cell eta
        if ( tt.isAvailable< std::vector<float> > ("CaloCellETByLayer") ) {
          for (std::vector<float>::iterator it = CaloETLayers.begin(); it != CaloETLayers.end(); it++) {
            caloE += *it;
          }
        }
      }
      if (caloE < 0) caloE = 0; //convert -ve energies to 0
      caloE = int(caloE+0.5); //round calo energy to nearest GeV for comparison with L1 energies
//       if (em_caloE > 255) em_caloE = 255; //set calo tt energies to saturate
      return caloE;
}


std::pair<double, double> TimeFitting::computeMandRMS(std::vector<double> vec) {
	  // compute rms
	  double sum = std::accumulate(std::begin(vec), std::end(vec), 0.0);
	  double m =  sum / vec.size();
	  double accum = 0.0;
	  std::for_each (std::begin(vec), std::end(vec), [&](const double d) {
	      accum += (d - m) * (d - m);
	  });
	  double stdev = sqrt(accum / (vec.size()-1));
	  if (isinf(stdev)) {
		std::pair<double, double> mRMS_pair = std::make_pair(-1000, -1000);
		return mRMS_pair;
		
	  } 
	  std::pair<double, double> mRMS_pair = std::make_pair(m, stdev);
	  return mRMS_pair;
}

bool TimeFitting::matchTTtoTopocluster(double TTeta, double TTphi, int TTpart, double& topotime, const DataVector<xAOD::CaloCluster>* clusters) {
	// topo cluster timing
	double mindR = 1000;
	double topoeta;
	double topophi;
	double thistopotime;
	for (auto it = clusters->begin(); it != clusters->end(); ++it){
	    thistopotime = (*it)->time();
	    topoeta = (*it)->calEta();
	    topophi = (*it)->calPhi() + M_PI;
	    //double topoE = (*it)->rawE();
	    double dR = sqrt((topoeta-TTeta)*(topoeta-TTeta)+(topophi-TTphi)*(topophi-TTphi));
	    if (dR < mindR) {mindR = dR; topotime = thistopotime;}
	}
	if (mindR<0.5) return true;
	else return false;
}

StatusCode TimeFitting::fillNormPulse( unsigned int id, std::map<unsigned int, TH2D*>& histcolourpulses, std::string histstream, std::vector<float> yvec, int nslices, std::string layerdir) {
  
      double bincenter;
      if (!histcolourpulses.count(id) ) {
	      char hname[50];// = (char *) malloc(snprintf(NULL, 0, "energyNormPulse0x%08x_col",id) + 1);
	      //if (histstream == m_PulseHistsStream_BGtwoside) sprintf(hname, "BGflaggedNormPulse0x%08x", id);
	      //else 
	      sprintf(hname, "NormPulse0x%08x", id);
	      ATH_MSG_INFO("about to create normalised colour pulse for stream "<<histstream);
	      char histtitle_color[100];// = (char *) malloc(snprintf(NULL, 0, "%s, eta=%.2f, phi=%.2f;Time [ns];E_{ADC}/E_{calo}",histname_color,eta,phi) + 1);
	      sprintf(histtitle_color, "%s;Time [ns];E_{ADC}/E_{calo}",hname);
	      //if ( nslices == 15 ) CHECK(book(new TH2D(histname_color,"normalised pulse",15, 93.75,281.25,226,-0.51,1.5)));
	      //if ( nslices == 5 ) CHECK(book(new TH2D(histname_color,"normalised pulse",5,125.,250.,148,-0.51,1.5)));
	      if ( nslices == 5 ) histcolourpulses[id] = new TH2D(hname,histtitle_color,5,125.,250.,126,-0.51,2.01);
	      if ( nslices == 7 ) histcolourpulses[id] = new TH2D(hname,histtitle_color,7,100.,275.,176,-0.51,3.01);
	      if ( nslices == 15 ) histcolourpulses[id] = new TH2D(hname,histtitle_color,15, 93.75,281.25,176,-0.51,3.01);
	      histcolourpulses[id]->SetOption("colz");
	      histcolourpulses[id]->SetTitle(histtitle_color);
	      ATH_MSG_INFO("about to register normalised colour pulse for stream "<<histstream);
	      CHECK(histSvc()->regHist(histstream+layerdir+hname,histcolourpulses[id]));
      }
      ATH_MSG_INFO("Filling normalised pulse..");
      for (unsigned int k = 0; k < nslices; k++){
	      bincenter = histcolourpulses[id]->GetXaxis()->GetBinCenter(k+1);
	      histcolourpulses[id]->Fill(bincenter,yvec.at(k));
	      histcolourpulses[id]->Fill(bincenter,yvec.at(k));
      }
      return StatusCode::SUCCESS;
}
