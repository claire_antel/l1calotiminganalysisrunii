
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "TimingRun2/TimeFitting.h"

DECLARE_ALGORITHM_FACTORY( TimeFitting )


#include "TimingRun2/TimeFitting80MHz.h"
DECLARE_ALGORITHM_FACTORY( TimeFitting80MHz )

DECLARE_FACTORY_ENTRIES( TimingRun2 ) 
{
  DECLARE_ALGORITHM( TimeFitting80MHz );
  DECLARE_ALGORITHM( TimeFitting );
}
