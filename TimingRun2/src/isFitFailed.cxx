
//This function checks whether a 4 or 3 bin fit the a digitised PPM sample worked or not.
//possible return values are the following:
// 0 means that the fit worked, i.e. isFitFailed = no
// 1 means that the fit failed, i.e. isFitFailed = yes
//-1 means that some error occured, i.e. isFitFailed = not determinable
//date: 20.05.2010

#include "TimingRun2/isFitFailed.h"


int isFitFailed(int fitreturn, TH1 * hist, int part, int maxbin, TF1 *fit, double fitmaxpos, double fitampl, int pedestal){

	//check whether fit is failed in 3 steps
	//1.step --> check whether return value of TH1D::Fit() is okay
	if (fitreturn != 0){
		hist->SetLineColor(2); //red
		hist->SetLineWidth(2);
		return 1;
	}
	else{
// 		cout<<"fit return value okay"<<endl;
		//2.step --> check roughly whether the fit looks like a pulse (do_gof function)
		//define and fill vector for fitted histogram values
		std::vector<double> yvalues;
		int length = 0;
		if ( part == 0 || part == 4 || part == 5  ) // FCal
			length = 3;
		else length = 4;
		for ( int i = 0; i < length; i++ ){
			double content;
			content = hist->GetBinContent(maxbin+i-1);
			yvalues.push_back(content);
		}
		
		double maxbincenter = hist->GetBinCenter(maxbin);
		double fitheight = fitampl*TMath::Exp(-0.5)+pedestal;
		
		//define and fill vectors for fitfunction
		std::vector<double> tfit;
		std::vector<double> yfit;
		
		tfit.push_back(maxbincenter-25.);
		tfit.push_back(maxbincenter);
		tfit.push_back(maxbincenter+25.);
		if (length > 3) tfit.push_back(maxbincenter+50.);
		
		for (int i = 0; i < length; i++){
			yfit.push_back( fit->Eval(tfit.at(i)) );
		}
		//investigate goodness of fit
		//works only for 3 or 4 bin fit
		int gof = do_gof( yvalues, maxbincenter, yfit, fitmaxpos, fitheight, pedestal);
	
		if (gof != 3333) {
			hist->SetLineColor(6); //pink
			hist->SetLineWidth(2);
			return 1;
		}
		else {
			double step_to_ns = 25./24.;
// 			cout<<"goodness of fit okay"<<endl;
			//3.step --> compare the fit with a parabola fit
			double maxvalue = hist->GetBinContent(maxbin);
			
			double minRangeP =  maxbincenter-25.;
			double maxRangeP = maxbincenter+25.;
			
			TF1 *fitP = new TF1("fitP",Fit_Pa,minRangeP,maxRangeP,3);
			fitP->SetParNames("maxposX","maxposY","width");
			//fitP->SetParameter(0,minRangeP+5);
			//fitP->SetParameter(1,maxvalue*1.01);
			//fitP->SetParameter(2,0.0001);
			//fitP->SetParLimits(0,minRangeP,maxRangeP);
			//fitP->SetParLimits(1,45.,1022.);
			//fitP->SetParLimits(2,0.,10.);
			fitP->SetLineColor(8);

			int y1 = hist->GetBinContent(maxbin-1);
			int y2 = maxvalue;
			int y3 = hist->GetBinContent(maxbin+1);
			double x1 = hist->GetXaxis()->GetBinCenter(maxbin-1);
			double x2 = hist->GetXaxis()->GetBinCenter(maxbin);
			double x3 = hist->GetXaxis()->GetBinCenter(maxbin+1);

			int dy23= y2-y3;
			int dy31 = y3-y1;
			int dy12 = y1-y2;
			double dx21 = x2-x1;
			double dx31 = x3-x1;

			double x0=0.5*(x1*x1*dy23+x2*x2*dy31+x3*x3*dy12)/(dy31*dx21+dy12*dx31);
			double width = dy31/((x1-x0)*(x1-x0)-(x3-x0)*(x3-x0));
			double y0=y1+width*(x1-x0)*(x1-x0);

			fitP->FixParameter(0,x0);
			fitP->FixParameter(1,y0);
			fitP->FixParameter(2,width);

			//int refitP = 0; 
			hist->GetListOfFunctions()->Add(fitP);

			
			double *parP = new double[3];
			double *pardiff = new double[2];
			fitP->GetParameters(parP);
			
			//if (refitP==0){
 				//std::cout<<"***fitmaxpos = "<<fitmaxpos<<", maxpos par = "<<parP[0]<<std::endl;
 				//std::cout<<"***fitheight = "<<fitheight<<", maxheight par = "<<parP[1]<<std::endl;
			pardiff[0] = fabs(fitmaxpos-parP[0]);
			pardiff[1] = (fitheight-parP[1])/parP[1];
			
			if (pardiff[0] > 7.5 || (pardiff[1] < -0.035 || pardiff[1] > 0.075 )) {
				hist->SetLineColor(kOrange-3); //orange
				hist->SetLineWidth(2);
				//std::cout<<"***comparision with parabola NOT okay --> pardiff[0] = "<<pardiff[0]<<", pardiff[1] = "<<pardiff[1]<<std::endl;
				return 1;
			}
			else {
				//std::cout<<"comparision with parabola okay --> pardiff[0] = "<<pardiff[0]<<", pardiff[1] = "<<pardiff[1]<<std::endl;
				return 0;
			}
			/*}
			else {
				return -1;
			}*/
		}
		
	}
}



int isFitFailed_80MHz(int fitreturn, TH1 * hist, int part, int maxbin, TF1 *fit, double fitmaxpos, double fitampl, int pedestal){
	//check whether fit is failed in 3 steps
	//1.step --> check whether return value of TH1D::Fit() is okay
	if (fitreturn != 0){
		hist->SetLineColor(2); //red
		hist->SetLineWidth(2);
		return 1;
	}
	else{
// 		cout<<"fit return value okay"<<endl;
		//2.step --> check roughly whether the fit looks like a pulse (do_gof function)
		//define and fill vector for fitted histogram values
		std::vector<double> yvalues;
		int length = 0;
		if ( part == 0 || part == 4 || part == 5  ) // FCal
			length = 3;
		else length = 4;
		for ( int i = 0; i < length; i++ ){
			double content;
			content = hist->GetBinContent(maxbin+i*2-1*2);
			yvalues.push_back(content);
		}
		
		double maxbincenter = hist->GetBinCenter(maxbin);
		double fitheight = fitampl*TMath::Exp(-0.5)+pedestal;
		
		//define and fill vectors for fitfunction
		std::vector<double> tfit;
		std::vector<double> yfit;
		
		tfit.push_back(maxbincenter-25.);
		tfit.push_back(maxbincenter);
		tfit.push_back(maxbincenter+25.);
		if (length > 3) tfit.push_back(maxbincenter+50.);
		
		for (int i = 0; i < length; i++){
			yfit.push_back( fit->Eval(tfit.at(i)) );
		}
		//investigate goodness of fit
		//works only for 3 or 4 bin fit
		int gof = do_gof( yvalues, maxbincenter, yfit, fitmaxpos, fitheight, pedestal);
	
		if (gof != 3333) {
			hist->SetLineColor(6); //pink
			hist->SetLineWidth(2);
			return 1;
		}
		else {
			double step_to_ns = 25./24.;
// 			cout<<"goodness of fit okay"<<endl;
			//3.step --> compare the fit with a parabola fit
			double maxvalue = hist->GetBinContent(maxbin);
			
			double minRangeP =  maxbincenter-25.;
			double maxRangeP = maxbincenter+25.;
			
			TF1 *fitP = new TF1("fitP",Fit_Pa,minRangeP,maxRangeP,3);
			fitP->SetParNames("maxposX","maxposY","width");
			//fitP->SetParameter(0,minRangeP+5);
			//fitP->SetParameter(1,maxvalue*1.01);
			//fitP->SetParameter(2,0.0001);
			//fitP->SetParLimits(0,minRangeP,maxRangeP);
			//fitP->SetParLimits(1,45.,1022.);
			//fitP->SetParLimits(2,0.,10.);
			fitP->SetLineColor(8);

			int y1 = hist->GetBinContent(maxbin-1);
			int y2 = maxvalue;
			int y3 = hist->GetBinContent(maxbin+1);
			double x1 = hist->GetXaxis()->GetBinCenter(maxbin-1);
			double x2 = hist->GetXaxis()->GetBinCenter(maxbin);
			double x3 = hist->GetXaxis()->GetBinCenter(maxbin+1);

			int dy23= y2-y3;
			int dy31 = y3-y1;
			int dy12 = y1-y2;
			double dx21 = x2-x1;
			double dx31 = x3-x1;

			double x0=0.5*(x1*x1*dy23+x2*x2*dy31+x3*x3*dy12)/(dy31*dx21+dy12*dx31);
			double width = dy31/((x1-x0)*(x1-x0)-(x3-x0)*(x3-x0));
			double y0=y1+width*(x1-x0)*(x1-x0);

			fitP->FixParameter(0,x0);
			fitP->FixParameter(1,y0);
			fitP->FixParameter(2,width);

			//int refitP = 0; 
			hist->GetListOfFunctions()->Add(fitP);

			
			double *parP = new double[3];
			double *pardiff = new double[2];
			fitP->GetParameters(parP);
			
			//if (refitP==0){
 				//std::cout<<"***fitmaxpos = "<<fitmaxpos<<", maxpos par = "<<parP[0]<<std::endl;
 				//std::cout<<"***fitheight = "<<fitheight<<", maxheight par = "<<parP[1]<<std::endl;
			pardiff[0] = fabs(fitmaxpos-parP[0]);
			pardiff[1] = (fitheight-parP[1])/parP[1];
			
			if (pardiff[0] > 7.5 || (pardiff[1] < -0.035 || pardiff[1] > 0.075 )) {
				hist->SetLineColor(kOrange-3); //orange
				hist->SetLineWidth(2);
				//std::cout<<"***comparision with parabola NOT okay --> pardiff[0] = "<<pardiff[0]<<", pardiff[1] = "<<pardiff[1]<<std::endl;
				return 1;
			}
			else {
				//std::cout<<"comparision with parabola okay --> pardiff[0] = "<<pardiff[0]<<", pardiff[1] = "<<pardiff[1]<<std::endl;
				return 0;
			}
			/*}
			else {
				return -1;
			}*/
		}
		
	}
}



int do_gof( std::vector<double> yvalues, double mbc, std::vector<double> yfit , double fmp, double fheight, int pedestal )
{
	int gof;
	//mbc = maxbincenter
	//fmp = fitmaxpos
	
	//check on position of fit maximum
	std::vector<double>::iterator it_max;
	it_max = std::max_element( yvalues.begin(), yvalues.end() );

	if( it_max[-1] > it_max[1] )
	{
		if( fmp >= mbc-25. && fmp <= mbc+12.5 )  gof = 3000;
		else if( fmp >= mbc-37.5 && fmp <= mbc+25. ) 
		{
			gof = 2000;
		}
		else
		{
			gof= 1000; 
		}
	}
	else
	{

		if( fmp >= mbc-12.5 && fmp <= mbc+25. )  gof = 3000;
		else if( fmp >= mbc-25. && fmp <= mbc+37.5 ) 
		{
			gof = 2000;
		}
		else
		{
			gof= 1000; 
		}
	}

	//check on size of fit amplitude
	if( 0.85* *it_max <= fheight && 1.15* *it_max >= fheight ) gof += 300;
	else if(( 0.65* *it_max <= fheight && 1.35* *it_max >= fheight ))
	{	
		gof += 200;
	}	
	else
	{
		gof+= 100; 
	}
	
	 
	double ymean_begin = ( yvalues.at(0) + yfit.at(0) )/2.;
	double ydiff_begin = fabs( yvalues.at(0) - yfit.at(0) );
	double ymean_end = ( yvalues.back() + yfit.back() )/2.;
	double ydiff_end = fabs( yvalues.back() - yfit.back() );
	
	//check on deviation at the left tail of the fit
	if( ydiff_begin <= 0.6*ymean_begin ) gof += 30;
	else if( ydiff_begin <= 1.*ymean_begin )
	{
		gof += 20;
	}
	else
	{
		gof+= 10; 
	}

	//check on deviation at the right tail of the fit
	if( ydiff_end <= 0.25*ymean_end ) gof += 3;
	else if( ydiff_end <= 0.4*ymean_end )
	{
		gof += 2;
	}
	else
	{
		gof += 1; 
	}
	
	return gof;
}
