#include "TTxml.h"
#include <map>
#include "TH2TT.h"
#include <TStyle.h>
#include <TFile.h>
#include <TKey.h>
#include <TF1.h>
#include <TH1D.h>
#include "FitFunctions.h"
#include <vector>
#include <algorithm>

TH1*fitTTsignal(TH1* pulsehist5, unsigned int id, int part, double eta, double phi, int maxbin, 	double sigmaLeft, double sigmaRight, double undVsAmpl, int pedestal)
{
		
	//double undershootFix = (double) UndershootFix[id];

	
	//define fit range
	int minRange = 0;
	int maxRange = 0;
	double minRangeValue = 0.;
	double maxRangeValue = 0.;
	double idtime = 0.;
	float maxvalue = 0.; // for energy calculation later

	TF1 *fit;

	if (part == 1 || part == 2 || part == 3 || part == 6 || part == 7 || part == 8){
		minRange = -25; 	
		maxRange = 50;
		minRangeValue = pulsehist5->GetBinCenter(maxbin)+minRange;
		maxRangeValue = pulsehist5->GetBinCenter(maxbin)+maxRange;
		idtime = pulsehist5->GetBinCenter(3);
		maxvalue = pulsehist5->GetBinContent(maxbin);

		if (part == 3 || part == 7 || part == 8) fit = new TF1("fit",Fit_GLu,minRangeValue,maxRangeValue,6);
		else fit = new TF1("fit",Fit_LLu,minRangeValue,maxRangeValue,6);
	}
	else if (part == 0 || part == 4 || part == 5){
		minRange = -25; 	
		maxRange = 25; 
		minRangeValue = pulsehist5->GetBinCenter(maxbin)+minRange;
		maxRangeValue = pulsehist5->GetBinCenter(maxbin)+maxRange;
		idtime = pulsehist5->GetBinCenter(3);
		maxvalue = pulsehist5->GetBinContent(maxbin);

		fit = new TF1("fit",Fit_LLu,minRangeValue,maxRangeValue,6);	
	}
	else{
		std::cout<<"Invalid part returned for TT"<<id<<".\n";
		return pulsehist5;
	}

	std::cout<<"maxbin... "<<maxbin<<std::endl;
	std::cout<<"maxvalue... "<<maxvalue<<std::endl;
			
	//set fit parameters
	fit->SetParNames("Maximumposition","Amplitude","sigmaLeft","sigmaRight", "undershoot", "pedestal");
	fit->SetParameter(0,minRangeValue+5.);
	std::cout<<"initial maxpos... "<<minRangeValue+5.<<std::endl;	
	fit->SetParLimits(0,minRangeValue,maxRangeValue);
	fit->SetParameter(1,1.);
	fit->SetParLimits(1, 0.3,3.);
	//-->take scaling factors from calibration to collision pulses for sigmas into account
	if ( part == 1 ) { //EMEC
		if ( fabs(eta)>1.5 && fabs(eta)<2.2 ){ //finer granularity
			fit->FixParameter(2,sigmaLeft*0.95);
			fit->FixParameter(3,sigmaRight*0.95);
		}
		else if ( fabs(eta)>2.2 && fabs(eta)<2.4 ){ //finer granularity
			fit->FixParameter(2,sigmaLeft*0.9);
			fit->FixParameter(3,sigmaRight*0.9);
		}
		else if ( fabs(eta)>2.4 && fabs(eta)<2.5 ){ //finer granularity
			fit->FixParameter(2,sigmaLeft*0.85);
			fit->FixParameter(3,sigmaRight*0.85);
		}
		else { //coarser granularity
			fit->FixParameter(2,sigmaLeft);
			fit->FixParameter(3,sigmaRight);
		}
	}
	else if ( part == 2 ) { //Overlap
		fit->FixParameter(2,sigmaLeft*0.85);
		fit->FixParameter(3,sigmaRight*0.85);
	}
	else if ( part == 0 ) { //FCal1
		fit->FixParameter(2,sigmaLeft*1.05);
		fit->FixParameter(3,sigmaRight*1.05);
	}
	else if ( part == 4 || part == 5 ) { //FCal2&3
		if ( (eta<-4.05 && eta>-4.475) || (eta<3.2 && eta>-3.625) || (eta>3.625 && eta<4.05) || (eta>4.475) ){ //FCal3
			fit->FixParameter(2,sigmaLeft*1.3);
			fit->FixParameter(3,sigmaRight*1.3);
		}
		else { //FCal2
			fit->FixParameter(2,sigmaLeft*1.2);
			fit->FixParameter(3,sigmaRight*1.2);
		}
	}
	else { //Other CaloDivisions
		fit->FixParameter(2,sigmaLeft);
		fit->FixParameter(3,sigmaRight);
	}
	//undershoot parameter
	if ( part == 3 ) {
		fit->SetParameter(4,1.);
		fit->SetParLimits(4,0,350);
	}
	else fit->FixParameter(4,undVsAmpl);

	//pedestal
	fit->FixParameter(5,(double) pedestal);
	fit->SetLineColor(4);
	
	//apply the fit
	int refit = 1;

	refit = (int) pulsehist5->Fit("fit","RQ");
	
			
	//get fit parameters
	double par[2];
	par[0] = fit->GetParameter(0);
	par[1] = fit->GetParameter(1);
	double err = fit->GetParError(0);

	return pulsehist5;
}

int FitAccumulativePulse(const char * run="all"){

	std::map<unsigned int,double> Eta,Phi,Part;
 	std::map<unsigned int,double> SigmaLeft, SigmaRight, UndVsAmpl;

	TTxml TTprops;
	if (!(TTprops.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml"))){
		puts("ERROR: TT properties xml file not found.");
		return 0;
	};

	Eta = TTprops.Get("eta");
	Phi = TTprops.Get("phi");
	SigmaLeft = TTprops.Get("sigmal");
	SigmaRight = TTprops.Get("sigmar");
	UndVsAmpl = TTprops.Get("undVsAmpl");
	Part = TTprops.Get("part");

	double sigmaLeft = 0.;
	double sigmaRight = 0.;
	double undVsAmpl = 0.;
	int pedestal = 0;

	static const int arr[]={102108931};//17826563, 68159233, 68159489, 68159745, 68222978, 68881664, 84934656, 84934912, 84936448, 84936704, 85458945, 85462785, 85855745, 85856002, 102631171, 118754306, 118817280, 119342593};
	std::vector<unsigned int> IDlist (arr, arr + sizeof(arr) / sizeof(arr[0]) );

	TFile* rootfile = new TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/FittedPulses.root", "UPDATE");

	//if (sizeof(run)<4) {
	TFile* timing_hists = new TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingPropagation_combined.root");
	/*}
	else {
		char filename[130];
		sprintf(filename,"/afs/cern.ch/work/c/cantel/private/collisions/%s/TimingPropagation_combined%s.root",run, run);
		TFile* timing_hists = new TFile(filename);
	}*/
	std::cout<<"opened timing_hists file"<<std::endl;

	timing_hists->cd("normalised");
	TIter next( gDirectory->GetListOfKeys() );

	std::string chbegin = "energyNormPulse0x%08x";

	std::string histName;
	TKey* key;
	while( (key=(TKey*) next() )){
		//std::cout << "key is... " << key << std::endl;
		histName = key->GetName();
		TObject *o = (TH1D*) key->ReadObj()->Clone(TString::Format("%s", histName.c_str()));
		if(!o->InheritsFrom("TH1")) continue; //only look at histos
		if (sizeof(histName.c_str())>26) continue;

	  	if (std::string(histName).find(chbegin.substr(0, 15)) == std::string::npos) continue;
	 	unsigned int id;
		sscanf(histName.c_str(), chbegin.c_str(), &id);
		//std::cout<<"ID ..."<<id<<std::endl;
		if (std::find(IDlist.begin(), IDlist.end(), id)==IDlist.end()) continue;
		std::cout<<"doing fitting for ID ..."<<id<<std::endl;
		TH1D *hist_t = (TH1D*)o;
		double eta = Eta[id];
		double phi = Phi[id];
		int part = (int) Part[id];
		int maxbin = hist_t->GetMaximumBin();
		if (SigmaLeft.count(id)) {
			sigmaLeft = (double) SigmaLeft[id];
			sigmaRight = (double) SigmaRight[id];
			undVsAmpl = (double) UndVsAmpl[id];
		}			
		else {
			std::cout<<"No fixed parameters found for TT"<<id<<".\n";
			return -1;
		}
		
		TH1 * fitted_hist=fitTTsignal(hist_t, id, part, eta, phi, maxbin, sigmaLeft, sigmaRight, undVsAmpl, pedestal);

		//fitted_hist->SetOptStat(0);
		//fitted_hist->SetOptFit(111);

		rootfile->cd();
		fitted_hist->Write();
	}
	rootfile->Close();

}

