# -*- coding: iso-8859-1 -*-
from ROOT import *

gROOT.LoadMacro("plot_style.C")

set_plot_style() 

gStyle.SetTextFont(50)
gStyle.SetTitleXOffset(1.4)
gStyle.SetTitleYOffset(1.4)
from time import sleep

#f0 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingAugust2015/tdiff_maps_runfixedUvsA.root") 
#f1 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingAugust2015/tdiff_maps_run278880widesigmalimits.root")
#f2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingAugust2015/tdiff_maps_runbulkonly.root")

f0 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingAugust2015/tdiff_maps_runwidesigmalimits.root") 
f1 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingAugust2015/tdiff_maps_runfixedUvsAinEMB.root")
f2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingAugust2015/tdiff_maps_runnewcf.root")

hist_list=[]
f0.cd()
print "available histograms are: ", gDirectory.GetListOfKeys().Print()
histEMB0 = gDirectory.Get("hist_TileLB") 
hist_list.append(histEMB0)
histEMEC0 = gDirectory.Get("hist_HEC") 
hist_list.append(histEMEC0)
histFCAL10 = gDirectory.Get("hist_FCAL2") 
hist_list.append(histFCAL10)
histFCAL3 = gDirectory.Get("hist_FCAL3") 
hist_list.append(histFCAL3)

labels = ["Tile", "HEC", "FCal2", "FCal3"]
leg = TLegend(0.6, 0.95, 0.9, 0.6)
for h in range(len(hist_list)):

	  hist_list[h].SetStats(0)
	  #hist_list[h].SetMaximum(1.05*max(maxvals))
	  if h>0:
	 	 hist_list[h].Draw("SAME")
	  else:
	  	hist_list[h].Draw()
	  leg.AddEntry(hist_list[h], labels[h], "l")
leg.Draw()
raw_input("press enter..")


#title1 = "80 MHz fits"
#title2 = "40 MHz fits - old CF"
#title3 = "40 MHz fits - new CF"
title1 = "80 MHz fits - free UvsA"
title2 = "80 MHz fits - fixed UvsA"
title3 = ""

spreadEM1title = "freeparsEM"
spreadEM2title = "fixednewEM"
spreadEM3title = "fixedoldEM"
spreadHAD1title = "freeparsHAD"
spreadHAD2title = "fixednewHAD"
spreadHAD3title = "fixedoldHAD"

low = -10
high = 20

hist_list = []
hist_list1 = []
hist_list2 = []

f0.cd()
print "available histograms are: ", gDirectory.GetListOfKeys().Print()
histEMB0 = gDirectory.Get("hist_EMB") 
hist_list.append(histEMB0)
histEMEC0 = gDirectory.Get("hist_EMEC") 
hist_list.append(histEMEC0)
histFCAL10 = gDirectory.Get("hist_FCAL1") 
hist_list.append(histFCAL10)

f1.cd()
print "available histograms are: ", gDirectory.GetListOfKeys().Print()
histEMB1 = gDirectory.Get("hist_EMB") 
hist_list1.append(histEMB1)
histEMEC1 = gDirectory.Get("hist_EMEC") 
hist_list1.append(histEMEC1)
histFCAL11 = gDirectory.Get("hist_FCAL1") 
hist_list1.append(histFCAL11)

f2.cd()
print "available histograms are: ", gDirectory.GetListOfKeys().Print()
histEMB2 = gDirectory.Get("hist_EMB") 
hist_list2.append(histEMB2)
histEMEC2 = gDirectory.Get("hist_EMEC") 
hist_list2.append(histEMEC2)
histFCAL12 = gDirectory.Get("hist_FCAL1") 
hist_list2.append(histFCAL12)
#histTileLBold = gDirectory.Get("hist_TileLB")
#hist_list.append(histTileLBold) 
#histTileEBold = gDirectory.Get("hist_TileEB") 
#hist_list.append(histTileEBold)
#histHECold = gDirectory.Get("hist_HEC") 
#hist_list.append(histHECold)
#histFCAL2old = gDirectory.Get("hist_FCAL2")
#hist_list.append(histFCAL2old) 
#histFCAL3old = gDirectory.Get("hist_FCAL3")
#hist_list.append(histFCAL3old) 

#SpreadEM = histEMBold
#SpreadEM.SetName(spreadEM1title)
#SpreadEM.SetTitle("EM timing spread (unfixed)")
#SpreadEM25 = histEMB25
#SpreadEM25.SetName(spreadEM2title)
#SpreadEM25.SetTitle("EM timing spread (new CF)")
#SpreadEM50 = histEMB50
#SpreadEM50.SetName(spreadEM3title)
#SpreadEM50.SetTitle("EM timing spread (old CF)")
#SpreadHAD = histTileLBold
#SpreadHAD.SetName(spreadHAD1title)
#SpreadHAD.SetTitle("HAD timing spread (unfixed)")
#SpreadHAD25 = histTileLB25
#SpreadHAD25.SetName(spreadHAD2title)
#SpreadHAD25.SetTitle("HAD timing spread (new cf)")
#SpreadHAD50 = histTileLB50
#SpreadHAD50.SetName(spreadHAD3title)
#SpreadHAD50.SetTitle("HAD timing spread (old cf)")
drawSame= False
maxvals=[]

for h in range(len(hist_list)):
	hist_list[h].SetStats(0)
	hist_list[h].SetLineColor(kAzure+2)
	hist_list[h].SetLineWidth(2)
	hist_list[h].SetFillStyle(3001)
	hist_list[h].SetFillColor(kAzure+2)
	hist_list[h].GetXaxis().SetRangeUser(low, high)
	maxvals.append(hist_list[h].GetMaximum())

for h in range(len(hist_list1)):
	hist_list1[h].SetStats(0)
	hist_list1[h].SetLineColor(kGreen+2)
	hist_list1[h].SetLineWidth(2)
	hist_list1[h].SetFillStyle(3001)
	hist_list1[h].SetFillColor(kGreen+2)
	hist_list1[h].GetXaxis().SetRangeUser(low, high)
	maxvals.append(hist_list1[h].GetMaximum())

for h in range(len(hist_list2)):
	hist_list2[h].SetStats(0)
	hist_list2[h].SetLineColor(46)
	hist_list2[h].SetLineWidth(2)
	hist_list2[h].SetFillStyle(3001)
	hist_list2[h].SetFillColor(46)
	hist_list2[h].GetXaxis().SetRangeUser(low, high)
	maxvals.append(hist_list2[h].GetMaximum())
#	if (h>=0 and h<=2):
#		print "adding ", hist_list[h].GetName(), "to EM spread."
#		SpreadEM.Add(hist_list[h])
#		SpreadEM25.Add(hist_list25[h])
#		SpreadEM50.Add(hist_list50[h])
#	elif (h>=3):
#		print "adding ", hist_list[h].GetName(), "to HAD spread."
#		SpreadHAD.Add(hist_list[h])
#		SpreadHAD25.Add(hist_list25[h])
#		SpreadHAD50.Add(hist_list50[h])


for h in range(len(hist_list)):
	  leg = TLegend(0.6, 0.95, 0.9, 0.6)

	  hist_list[h].SetMaximum(1.05*max(maxvals))

	  hist_list[h].Draw()
	  hist_list1[h].Draw("SAME")
	  hist_list2[h].Draw("SAME")
	  leg.AddEntry(hist_list[h], title1, "l")
	  leg.AddEntry(hist_list1[h], title2, "l")
	  leg.AddEntry(hist_list2[h], title3, "l")
	  leg.Draw()
	  raw_input("press enter..")

	#c1.Print(hist_list[h].GetName()+"_comparisons.png")

	 










