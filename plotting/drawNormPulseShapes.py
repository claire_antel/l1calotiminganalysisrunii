from ROOT import *

gROOT.LoadMacro("plot_style.C")

set_plot_style() 

# bin 0: 05180x00, bin 1: 05180x02, bin 2: 05180x03, bin 3: 05180x01, bin 62: 04180x00, bin 63: 04180x02, bin 64: 04180x03, bin 65: 04180x01 

outFile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/286711/FCal23PulseShapes.root", "RECREATE")

inFile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/286711/TimingFitHistos_HardProbepulses.root")
inFile.cd("HistsFit")
keys = gDirectory.GetListOfKeys()


for bin in [0, 1, 2, 3, 62, 63, 64, 65]:

	cnt = 0	

    	outFile.mkdir("bin"+str(bin))

	if bin == 0:
		index = "05180"
		ch = "00"

	if bin == 1:
		index = "05180"
		ch = "02"

	if bin == 2:
		index = "05180"
		ch = "03"

	if bin == 3:
		index = "05180"
		ch = "01"

	if bin == 62:
		index = "04180"
		ch = "00"

	if bin == 63:
		index = "04180"
		ch = "02"

	if bin == 64:
		index = "04180"
		ch = "03"

	if bin == 65:
		index = "04180"
		ch = "01"


	for i in ['0', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd', 'e', 'f']:
		ID = index+i+ch
		hname="energyNormPulse0x"+ID
		hname2="energyNormPulse0x"+ID+"_col"

		if hname in keys:
			print hname
			#c= TCanvas("c", "c", 800, 600)
			inFile.cd("HistsFit")
			hist = gDirectory.Get(hname)
			hist2 = gDirectory.Get(hname2)
			if cnt == 0:
				histbyeta = hist2.Clone()
				histbyeta.SetName("normpulse_etabin"+str(bin))
				histbyeta.SetTitle("normalised overlayed pulses etabin "+str(bin))
			else:
				histbyeta.Add(hist2)
			hist.SetTitle("normalised pulse profile TTID"+ID+", etabin "+str(bin))
			hist2.SetTitle("normalised overlayed pulses TTID"+ID+", etabin "+str(bin))
			hist2.GetYaxis().SetRangeUser(0, 1.4)
			#hist2.Draw()
			#raw_input("hit enter...")
			#c.Print("normPulseID"+ID+"_EtaBin"+bin+".pdf")
			outFile.cd("bin"+str(bin))
			hist.Write()
			hist2.Write()
			cnt+=1
	if not cnt == 0:
		outFile.cd()
		histbyeta.Write()
			
	
outFile.Close()
inFile.Close()
