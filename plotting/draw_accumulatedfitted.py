from ROOT import *
import re
import os

gROOT.LoadMacro("plot_style.C")

set_plot_style() 

gStyle.SetTextFont(50)
gStyle.SetTitleXOffset(1.4)
gStyle.SetTitleYOffset(1.4)

inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/FittedPulses.root")

IDlist=['06160f03']#'05100800', '05100700', '05100100', '05100000', '04100901', '04100801', '04100701', '05180f01', '05180001', '041b0d00', '051e0e01', '051e0f02', '071d0601', '061e0703', '07140c02', '07150200', '04110002']

keys=gDirectory.GetListOfKeys()

for ID in IDlist:
	hname="energyNormPulse0x"+ID
	if hname in keys:
		pulse=gDirectory.Get(hname)
		gStyle.SetOptStat(0)
		gStyle.SetOptFit(111)
		Fit=pulse.GetFunction('fit')
		time=Fit.GetParameter("Maximumposition")
		offset=round(time-187.5,1)
		c=TCanvas('c1', 'c1', 700, 600)
		pulse.Draw()
		l=TLatex()
		l.SetNDC()
		l.SetTextFont(42)
		l.SetTextColor(36)
		l.DrawLatex(0.5,0.2,"t offset = "+str(offset)+" ns");
		c.Print("fiteted_accumulatedpulse0x"+ID+".png")
