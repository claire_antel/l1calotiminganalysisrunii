#include "TTxml.h"
#include <iostream>
#include <fstream> 

void output_IDtable()
{
	TTxml m_props;
	m_props.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");

	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;

	Eta = m_props.Get("eta");
	Phi = m_props.Get("phi");
	Part = m_props.Get("part");

	ofstream output("TTproperties.txt");
	output<<"ID \t eta \t phi \t part"<<std::endl;

	std::map<unsigned int,double>::iterator it;
	for ( it = Eta.begin(); it != Eta.end(); it++ ){
			unsigned int id = it->first;
			double eta = Eta[id];
			double phi = Phi[id];
			int part = (int) Part[id];
			
			output<<id<<"\t"<<eta<<"\t"<<phi<<"\t"<<part<<std::endl;
	}
	output.close();
}
