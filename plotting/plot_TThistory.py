from ROOT import *


timevecmap={}
lbvec=[]
qualityvecmap={}

dirname="/afs/cern.ch/work/c/cantel/private/collisions/";
ext=".root"

dir = TSystemDirectory(dirname, dirname)
files = dir.GetListOfFiles()

if files:
	for file in files:
		fname= file.GetName()
		if ((not file.IsDirectory()) and fname.endswith(ext) and fname.startswith("TimingPropagation_")):
			print fname[18:21]
			LB = int(fname[18:21])
			print "LB: ", LB
			#if LB>250: continue
			lbvec.append(LB)
			roofile=TFile(dirname+fname)
			for key in gDirectory.GetListOfKeys():
				histname = key.GetName() 
				if histname.startswith("hist_tdistr_id"):
					print "hist name is ....", histname
					ID = int(histname[14:len(histname)])
					print "id is ...", ID
					hist=key.ReadObj().Clone(histname)
					timevecmap.setdefault(ID,[]).append([LB, hist.GetMean(), hist.GetRMS()])
				if histname.startswith("hist_qualitydistr_id"):
					print "hist name is ....", histname
					ID = int(histname[20:len(histname)])
					print "id is ...", ID
					hist=key.ReadObj().Clone(histname)
					qualityvecmap.setdefault(ID,[]).append([LB, hist.GetMean(), hist.GetRMS()])

lumimax=max(lbvec)
lumimin=min(lbvec)


continuing = 'Y'

while continuing == 'Y':
	TT = int(raw_input('look at Trigger Tower:'))
	hist_t = TGraphErrors() #TH1D("time_history", "", lumimax-lumimin, lumimin, lumimax) 
	hist_qual = TGraphErrors() #TH1D("quality_history", "", lumimax-lumimin, lumimin, lumimax) 
	hist_t.SetMarkerStyle(21)
	hist_qual.SetMarkerStyle(21)
	elements = timevecmap[TT]
	for element in elements:
			hist_t.SetPoint(element[0]-lumimin, element[0], element[1] )#SetBinContent(element[0]-lumimin, element[1])
			hist_t.SetPointError(element[0]-lumimin, 0, element[2])#SetBinError(element[0]-lumimin, element[2])
	elements2 = qualityvecmap[TT]
	for element2 in elements2:
			hist_qual.SetPoint(element2[0]-lumimin, element2[0], element2[1] )#SetBinContent(element[0]-lumimin, element[1])
			hist_qual.SetPointError(element2[0]-lumimin, 0, element2[2])#SetBinError(element[0]-lumimin, element2[2])

	
	ctitle = "history plots for Trigger Tower %i"%TT
	c=TCanvas("historyplots", ctitle, 1680, 1050)
	c.Divide(1,2)
	c.cd(1) 
	hist_t.Draw()
	c.cd(2)
	hist_qual.Draw()
	continuing = raw_input('Continue?(Y/N)')
