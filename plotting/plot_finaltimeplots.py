from ROOT import *

from time import sleep

gROOT.LoadMacro("plot_style.C")

set_plot_style() 

gStyle.SetTextFont(50)
gStyle.SetTitleXOffset(1.4)
gStyle.SetTitleYOffset(1.4)

inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/finaltiming_July2015_rounded.root")
inputf.cd()

emTiming=gDirectory.Get("emTiming")
hadTiming=gDirectory.Get("hadTiming")
emTimingRMS=gDirectory.Get("emTimingRMS")
hadTimingRMS=gDirectory.Get("hadTimingRMS")
emTiming_trunc=gDirectory.Get("emTiming_trunc")
hadTiming_trunc=gDirectory.Get("hadTiming_trunc")
emTiming_spread=gDirectory.Get("emTiming_spread")
hadTiming_spread=gDirectory.Get("hadTiming_spread")
emTiming_trunc_spread=gDirectory.Get("emTiming_trunc_spread")
hadTiming_trunc_spread=gDirectory.Get("hadTiming_trunc_spread")
emTiming_P4correction_spread=gDirectory.Get("emTiming_P4correction_spread")
hadTiming_P4correction_spread=gDirectory.Get("hadTiming_P4correction_spread")
emFullDelay_trunc=gDirectory.Get("emFullDelayNew_trunc")
hadFullDelay_trunc=gDirectory.Get("hadFullDelayNew_trunc")
emFullDelayOld_trunc=gDirectory.Get("emFullDelayOld_trunc")
hadFullDelayOld_trunc=gDirectory.Get("hadFullDelayOld_trunc")
emFullDelayDiff_trunc=gDirectory.Get("emFullDelayDiff_trunc")
hadFullDelayDiff_trunc=gDirectory.Get("hadFullDelayDiff_trunc")
emErrorCode=gDirectory.Get("emErrorCode")
hadErrorCode=gDirectory.Get("hadErrorCode")

emTiming.SetMinimum(-10);
hadTiming.SetMinimum(-10);
emTiming.SetMaximum(10);
hadTiming.SetMaximum(10);
emTiming_trunc.SetMinimum(-10);
hadTiming_trunc.SetMinimum(-10);
emTiming_trunc.SetMaximum(10);
hadTiming_trunc.SetMaximum(10);
emTimingRMS.SetMaximum(6.5);
hadTimingRMS.SetMaximum(6.5);
emErrorCode.SetMaximum(6);
hadErrorCode.SetMaximum(6);
emTiming_trunc_spread.GetXaxis().SetRangeUser(-25, 25);
hadTiming_trunc_spread.GetXaxis().SetRangeUser(-25, 25);
emTiming_P4correction_spread.GetXaxis().SetRangeUser(-25, 25);
hadTiming_P4correction_spread.GetXaxis().SetRangeUser(-25, 25);
emFullDelay_trunc.SetMinimum(35);
hadFullDelay_trunc.SetMinimum(35);
emFullDelayOld_trunc.SetMinimum(35);
hadFullDelayOld_trunc.SetMinimum(35);
emFullDelay_trunc.SetMaximum(260);
hadFullDelay_trunc.SetMaximum(260);
emFullDelayOld_trunc.SetMaximum(260);
hadFullDelayOld_trunc.SetMaximum(260);
emFullDelayDiff_trunc.SetMinimum(-11);
hadFullDelayDiff_trunc.SetMinimum(-11);
emFullDelayDiff_trunc.SetMaximum(7);
hadFullDelayDiff_trunc.SetMaximum(7);

ymin=0
ymax=6.3
bins=[5, 63, 19, 48, 20, 49]
binsHAD=[5, 63, 19, 49]


c0 =  TCanvas("c0", "c0", 1800, 600)
c0.Divide(2,1)
c0.cd(1)
emTiming.Draw()
for bin in bins:
	line=TLine()
  	line.SetLineWidth(2)
	line.DrawLine(emTiming.GetXaxis().GetBinLowEdge(bin),ymin,emTiming.GetXaxis().GetBinLowEdge(bin),ymax)
 	#line->SetLineColor(kRed);
c0.cd(2)
hadTiming.Draw()
for bin in binsHAD:
  	line.SetLineWidth(2)
	line.DrawLine(hadTiming.GetXaxis().GetBinLowEdge(bin),ymin,hadTiming.GetXaxis().GetBinLowEdge(bin),ymax)
 	#line->SetLineColor(kRed);
sleep(1.0)		
c0.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/Timing.png")	

c2 =  TCanvas("c2", "c2", 1800, 600)
c2.Divide(2,1)
c2.cd(1)
emErrorCode.Draw()
for bin in bins:
	line=TLine()
  	line.SetLineWidth(2)
	line.DrawLine(emErrorCode.GetXaxis().GetBinLowEdge(bin),ymin,emErrorCode.GetXaxis().GetBinLowEdge(bin),ymax)
 	#line->SetLineColor(kRed);
c2.cd(2)
hadErrorCode.Draw()
for bin in binsHAD:
	line.DrawLine(hadErrorCode.GetXaxis().GetBinLowEdge(bin),ymin,hadErrorCode.GetXaxis().GetBinLowEdge(bin),ymax)
 	#line->SetLineColor(kRed);
  	line.SetLineWidth(2)
sleep(1.0)				
c2.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/ErrorCode.png")	

c3 =  TCanvas("c3", "c3", 1800, 600)
c3.Divide(2,1)
c3.cd(1)
emTiming_trunc.SetTitle("emTiming rounded")
emTiming_trunc.Draw()
for bin in bins:
	line=TLine()
  	line.SetLineWidth(2)
	line.DrawLine(emTiming_trunc.GetXaxis().GetBinLowEdge(bin),ymin,emTiming_trunc.GetXaxis().GetBinLowEdge(bin),ymax)
 	#line->SetLineColor(kRed);
c3.cd(2)
hadTiming_trunc.Draw()
hadTiming_trunc.SetTitle("hadTiming rounded")
for bin in binsHAD:
	line.DrawLine(hadTiming_trunc.GetXaxis().GetBinLowEdge(bin),ymin,hadTiming_trunc.GetXaxis().GetBinLowEdge(bin),ymax)
 	#line->SetLineColor(kRed);
  	line.SetLineWidth(2)
sleep(1.0)		
c3.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/TimingTrunc.png")

c4 =  TCanvas("c4", "c4", 1800, 600)
c4.Divide(2,1)
c4.cd(1)
emTimingRMS.Draw()
for bin in bins:
	line=TLine()
  	line.SetLineWidth(2)
	line.DrawLine(emTimingRMS.GetXaxis().GetBinLowEdge(bin),ymin,emTimingRMS.GetXaxis().GetBinLowEdge(bin),ymax)
 	#line->SetLineColor(kRed);
c4.cd(2)
hadTimingRMS.Draw()
for bin in binsHAD:
	line.DrawLine(hadTimingRMS.GetXaxis().GetBinLowEdge(bin),ymin,hadTimingRMS.GetXaxis().GetBinLowEdge(bin),ymax)
 	#line->SetLineColor(kRed);
  	line.SetLineWidth(2)
sleep(1.0)			
c4.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/TimingRMS.png")

c5 =  TCanvas("c5", "c5", 1800, 600)
c5.Divide(2,1)
c5.cd(1)
emFullDelay_trunc.Draw()
c5.cd(2)
hadFullDelay_trunc.Draw()
sleep(1.0)				
c5.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/FullDelay.png")

c6 =  TCanvas("c6", "c6", 1800, 600)
c6.Divide(2,1)
c6.cd(1)
emFullDelayDiff_trunc.Draw()
c6.cd(2)
hadFullDelayDiff_trunc.Draw()
sleep(1.0)				
c6.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/FullDelayDiff.png")	

gStyle.SetOptFit(111)

c7 =  TCanvas("c7", "c7", 1800, 600)
c7.Divide(2,1)
c7.cd(1)
emTiming_spread.Draw()
c7.cd(2)
hadTiming_spread.Draw()	
sleep(1.0)			
c7.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/timingspread.png")

c8 =  TCanvas("c8", "c8", 1800, 600)
c8.Divide(2,1)
c8.cd(1)
emTiming_trunc_spread.SetTitle("timing corrections rounded (EM)")
emTiming_trunc_spread.Draw()
c8.cd(2)
hadTiming_trunc_spread.SetTitle("timing corrections rounded (HAD)")
hadTiming_trunc_spread.Draw()	
sleep(1.0)			
c8.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/timingspread_rounded.png")

c9 =  TCanvas("c9", "c9", 1800, 600)
c9.Divide(2,1)
c9.cd(1)
emFullDelayOld_trunc.Draw()
c9.cd(2)
hadFullDelayOld_trunc.Draw()
sleep(1.0)				
c9.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/FullDelayOld.png")

c10 =  TCanvas("c10", "c10", 1800, 600)
c10.Divide(2,1)
c10.cd(1)
emTiming_P4correction_spread.Draw()
c10.cd(2)
hadTiming_P4correction_spread.Draw()
sleep(1.0)				
c10.Print("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/timingspread_P4correction.png")
