# -*- coding: utf-8 -*-
from ROOT import *
import re
import csv

gROOT.LoadMacro("plot_style.C")

set_plot_style() 

def LLu(x, par):
      xmax = par[0]
      ampl = par[1]
      sigma_l1 = par[2]
      sigma_l2 = par[3]
      undershoot = par[4];
      pedestal = par[5];
     
      if ( x[0] <= par[0]):
        #Landau function 1
        return ampl*exp(-0.5*((x[0]-xmax)/sigma_l1 + exp(-(x[0]-xmax)/sigma_l1))) +pedestal
      if (x[0] > par[0]):
        #Landau function 2
        return (ampl+undershoot*exp(0.5))*exp(-0.5*((x[0]-xmax)/sigma_l2 + exp(-(x[0]-xmax)/sigma_l2)))+pedestal-undershoot;
      else:
	return -1


draw = raw_input('Draw... (shapes, maps, mapsDESD, profiles, per region, stats, normpulses, entries)')

if draw == "entries":

  mapfile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")
  mapfile.cd("SatHists")
  mapTH2 = gDirectory.Get("emHits_total").Clone()
  mapTH2.Reset("ICES")

  layer = raw_input("Enter layer (EM/HAD):")
  outFile2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/BGflaggedTowerTimingHistos.root")
  outFile2.cd(layer)
  keys = gDirectory.GetListOfKeys()

  for key in keys:
	  print "looking at hist ", key.GetName()
	  IDint = re.findall(r'\d+', key.GetName()) 
	  histid = int(IDint[0])
	  #IDhex = key.GetName()[14:20]
	  #histid = int(IDhex)

	  Eta = -100
	  with open('TTproperties.txt','r') as f:
	      next(f) # skip headings
	      reader=csv.reader(f,delimiter='\t')
	      for row in reader:
		  #print "ID in loop is... ", row[0]
		  if int(row[0]) == histid:
			  Eta= float(row[1])
			  Phi = float(row[2])
			  part = int(row[3])
			  break				
	  if Eta == -100:
		  print "hist id "+str(histid)+" not found"
		  continue
	  else:
		  print "Eta is ..", Eta

	  hist = gDirectory.Get(key.GetName())

	  etabin = mapTH2.GetXaxis().FindBin(Eta)
	  phibin = mapTH2.GetYaxis().FindBin(Phi)
	  mapTH2.SetBinContent(etabin, phibin, hist.GetEntries())
  
  mapTH2.SetTitle("Pulse concentration in BIB tagged events ("+layer+")")
  mapTH2.SetStats(0)
  mapTH2.Draw()
  raw_input("Press enter..")

if draw == "mean":

  mapfile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")
  mapfile.cd("SatHists")
  mapTH2 = gDirectory.Get("emHits_total").Clone()
  mapTH2.Reset("ICES")

  layer = raw_input("Enter layer (EM/HAD):")
  outFile2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/TowerPulsetopoHistos.root")
  outFile2.cd(layer+"/normalised")
  keys = gDirectory.GetListOfKeys()

  for key in keys:
	  print "looking at hist ", key.GetName()
	  IDhex = key.GetName()[23:33]
	  histid = int(IDhex,0)

	  Eta = -100
	  with open('TTproperties.txt','r') as f:
	      next(f) # skip headings
	      reader=csv.reader(f,delimiter='\t')
	      for row in reader:
		  #print "ID in loop is... ", row[0]
		  if int(row[0]) == histid:
			  Eta= float(row[1])
			  Phi = float(row[2])
			  part = int(row[3])
			  break				
	  if Eta == -100:
		  print "hist id "+str(histid)+" not found"
		  continue
	  else:
		  print "Eta is ..", Eta

	  hist = gDirectory.Get(key.GetName())

	  etabin = mapTH2.GetXaxis().FindBin(Eta)
	  phibin = mapTH2.GetYaxis().FindBin(Phi)
	  mapTH2.SetBinContent(etabin, phibin, hist.GetMean())
  
  mapTH2.SetTitle("Pulse mean in BG tagged events ("+layer+")")
  mapTH2.SetStats(0)
  mapTH2.Draw()
  raw_input("Press enter..")
#desd = str(raw_input("DESD type (CALJET, PHOJET, SGLEL).: "))
if draw == "normpulses":
  
  layer = raw_input("Enter layer (EM/HAD):")
  TTid1 = raw_input("Enter TT id 1 (0x..): ")
  TTid2 = raw_input("Enter TT id 2 (0x..): ")

  outFile0 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/TowerInfoHistos_widesigmalimits.root")
  outFile0.cd(layer+"/normalised")
  pulse1 = gDirectory.Get("NormPulse"+TTid1)

  outFile1 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/TowerInfoHistos_widesigmalimits.root")
  outFile1.cd(layer+"/normalised")
  pulse2 = gDirectory.Get("NormPulse"+TTid2)

  pulse1.Print()
  pulse2.Print()

#  outFile2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/TowerPulsetwosideHistos.root")
#  outFile2.cd(layer+"/normalised")
#  bgflagged = gDirectory.Get("BGflaggedNormPulse"+TTid)

  c = TCanvas('c', 'c', 1024, 800)
  c.Divide(2,1)
  c.cd(1)
  #pulse1.SetTitle("energy normalised pulse - TT 1")
  pulse1.Draw()
  c.cd(2)
  #pulse2.SetTitle("energy normalised pulse - TT 2")
  pulse2.Draw()
#  c.cd(3)
#  bgflagged.SetTitle("energy normalised pulse - BIB flagged")
#  bgflagged.Draw()

  raw_input("Press Enter..")

  outFile0.Close()
  outFile1.Close()

if draw == "normpulse":
  
  layer = raw_input("Enter layer (EM/HAD):")
  TTid = raw_input("Enter TT id 0x: ")
  TTid = "0x"+TTid
  outFile0 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/TowerInfoHistos_widesigmalimits.root")
  outFile0.cd(layer+"/normalised")
  pulse0 = gDirectory.Get("NormPulse"+TTid)

  c = TCanvas('c', 'c', 1024, 800)

  c.cd()
  pulse0.SetTitle("energy normalised pulse - all pulses")
  pulse0.Draw()

  raw_input("Press Enter..")

  outFile0.Close()


if draw=="shapes":

	outFile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/FCal23PulseShapes.root", "RECREATE")
	# bin 0: 05180x00, bin 1: 05180x02, bin 2: 05180x03, bin 3: 05180x01, bin 62: 04180x00, bin 63: 04180x02, bin 64: 04180x03, bin 65: 04180x01 
	for etabin in [0, 1, 2, 3, 62, 63, 64, 65]:
		if etabin == 0:
			ID = "05180000" 
			sigl = 25. 
			sigr = 24.5
		if etabin == 1:
			ID = "05180002" 
			sigl = 25.
			sigr = 26.
		if etabin == 2:
			ID = "05180003" 
			sigl = 22.
			sigr = 21.5
		if etabin == 3:
			ID = "05180001" 
			sigl = 25.
			sigr = 25.4
		if etabin == 62:
			ID = "04180000" 
			sigl = 22.
			sigr = 21.3
		if etabin == 63:
			ID = "04180002" 
			sigl = 24.
			sigr = 25.3
		if etabin == 64:
			ID = "04180003" 
			sigl = 24.
			sigr = 24.
		if etabin == 65:
			ID = "04180001" 
			sigl = 25.
			sigr = 25.3

		func = TF1("curveID"+ID+"_etabin"+str(etabin), LLu, 0, 400, 6)
		func.SetParameters(150, 1.*exp(0.5), sigl, sigr, 0.4, 0)
		func.SetTitle("expected curve for eta bin "+str(etabin)+", ID"+ID)
		func.Draw()
		raw_input("press enter to continue...")
		outFile.cd()
		func.Write()

if draw=="pulsecompare":

  layer = raw_input("Enter layer (EM/HAD):")
  idlist =["0x01150c03", "0x01150d03", "0x01140d00", "0x01160f03", "0x01170a01"] #["0x011c0e00", "0x011f0e02"]#["0x04180000", "0x04180003", "0x04180002", "0x04180001", "0x04100003"] #"0x04100f00", "0x05100f01", 
 # labels = ["FCal2A_OR","FCal2A_IR", "FCal3A_OR","FCal3A_IR", "FCal1A"] 
  outFile0 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/TowerInfoHistos.root")
  outFile0.cd(layer+"/normalised")

  colourvec = [64, 3, 95, 6, 36, 36]

  leg = TLegend(0.6, 0.95, 0.95, 0.75)

  i=0
  for TTid in idlist:
    pulse = gDirectory.Get("NormPulse"+TTid)
    pulse.Print()
    pulse = pulse.ProfileX()
    pulse.SetLineColor(colourvec[i])
    pulse.SetStats(0)
    pulse.SetLineWidth(4)

    if i>0:
      pulse.Draw('ESAME')
    else:
      pulse.SetTitle("")
      pulse.GetYaxis().SetRangeUser(-0.4, 1.05)
      pulse.Draw('E')
    leg.AddEntry(pulse,idlist[i], "l")
    i+=1

  leg.Draw()
  raw_input("Press enter...")


# drawing maps of sigma_l, sigma_r and UvsA from perTT histograms  
if draw=="maps":

	mapfile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")
	mapfile.cd("SatHists")
	mapSigLEM = gDirectory.Get("emHits_total").Clone()
	mapSigLEM.SetName("mapSigLEM")
	mapSigLEM.Reset("ICES")
	mapSigLEM.SetTitle("Mean - Sigma left");
	mapSigREM = mapSigLEM.Clone()
	mapSigREM.SetName("mapSigREM")
	mapSigREM.Reset("ICES")
	mapSigREM.SetTitle("Mean - Sigma right");
	mapUvsAEM = mapSigLEM.Clone()
	mapUvsAEM.SetName("mapUvsAEM")
	mapUvsAEM.Reset("ICES")
	mapUvsAEM.SetTitle("Mean - UvsA ");

	mapSigLHAD =  mapSigLEM.Clone()
	mapSigLHAD.SetName("mapSigLHAD")
	mapSigLHAD.Reset("ICES")
	mapSigLHAD.SetTitle("Mean - Sigma left");
	mapSigRHAD = mapSigLEM.Clone()
	mapSigRHAD.SetName("mapSigRHAD")
	mapSigRHAD.Reset("ICES")
	mapSigRHAD.SetTitle("Mean - Sigma right");
	mapUvsAHAD = mapSigLEM.Clone()
	mapUvsAHAD.SetName("mapUvsAHAD")
	mapUvsAHAD.Reset("ICES")
	mapUvsAHAD.SetTitle("Mean - UvsA ");

	mapSigLrmsEM = mapSigLEM.Clone()
	mapSigLrmsEM.SetName("mapSigLrmsEM")
	mapSigLrmsEM.Reset("ICES")
	mapSigLrmsEM.SetTitle("RMS - Sigma left")
	mapSigRrmsEM = mapSigLEM.Clone()
	mapSigRrmsEM.SetName("mapSigRrmsEM")
	mapSigRrmsEM.Reset("ICES")
	mapSigRrmsEM.SetTitle("RMS - Sigma right")
	mapUvsArmsEM = mapSigLEM.Clone()
	mapUvsArmsEM.SetName("mapUvsArmsEM")
	mapUvsArmsEM.Reset("ICES")
	mapUvsArmsEM.SetTitle("RMS - UvsA ");

	mapSigLrmsHAD = mapSigLEM.Clone()
	mapSigLrmsHAD.SetName("mapSigLrmsHAD")
	mapSigLrmsHAD.Reset("ICES")
	mapSigLrmsHAD.SetTitle("RMS - Sigma left")
	mapSigRrmsHAD = mapSigLEM.Clone()
	mapSigRrmsHAD.SetName("mapSigRrmsHAD")
	mapSigRrmsHAD.Reset("ICES")
	mapSigRrmsHAD.SetTitle("RMS - Sigma right")
	mapUvsArmsHAD = mapSigLEM.Clone()
	mapUvsArmsHAD.SetName("mapUvsArmsHAD")
	mapUvsArmsHAD.Reset("ICES")
	mapUvsArmsHAD.SetTitle("RMS - UvsA ");

	mapSigLEM.SetStats(0)
	mapSigREM.SetStats(0)
	mapUvsAEM.SetStats(0)
	mapSigLrmsEM.SetStats(0)
	mapSigRrmsEM.SetStats(0)
	mapUvsArmsEM.SetStats(0)
	mapSigLHAD.SetStats(0)
	mapSigRHAD.SetStats(0)
	mapUvsAHAD.SetStats(0)
	mapSigLrmsHAD.SetStats(0)
	mapSigRrmsHAD.SetStats(0)
	mapUvsArmsHAD.SetStats(0)

	minval = 10
	maxval = 35
	minvalrms = 0
	maxvalrms = 5

	mapSigLEM.SetMinimum(minval)
	mapSigREM.SetMinimum(minval)
	mapSigLEM.SetMaximum(maxval)
	mapSigREM.SetMaximum(maxval)

	mapSigLrmsEM.SetMinimum(minvalrms)
	mapSigRrmsEM.SetMinimum(minvalrms)
	mapSigLrmsEM.SetMaximum(maxvalrms)
	mapSigRrmsEM.SetMaximum(maxvalrms)

	mapSigLHAD.SetMinimum(minval)
	mapSigRHAD.SetMinimum(minval)
	mapSigLHAD.SetMaximum(maxval)
	mapSigRHAD.SetMaximum(maxval)

	mapSigLrmsHAD.SetMinimum(minvalrms)
	mapSigRrmsHAD.SetMinimum(minvalrms)
	mapSigLrmsHAD.SetMaximum(maxvalrms)
	mapSigRrmsHAD.SetMaximum(maxvalrms)

	infile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/FixedParametersHistos_fixedUvsAinEMB.root")

	parameters = ["sigLeft","sigRight", "UvsA"];

	print "Creating maps.."

	for par in parameters:

	  infile.cd(par)

	  keys = gDirectory.GetListOfKeys()

	  for key in keys:
		  print "looking at hist ", key.GetName()
		  IDint = re.findall(r'\d+', key.GetName()) 
		  histid = int(IDint[0])
		  #print "ID is.. ", histid

		  Eta = -100
		  with open('TTproperties.txt','r') as f:
		      next(f) # skip headings
		      reader=csv.reader(f,delimiter='\t')
		      for row in reader:
			  #print "ID in loop is... ", row[0]
			  if int(row[0]) == histid:
				  Eta= float(row[1])
				  Phi = float(row[2])
				  part = int(row[3])
				  break				
		  if Eta == -100:
			  print "hist id "+str(histid)+" not found"
			  continue

		  hist = gDirectory.Get(key.GetName())

		  etabin = mapSigLEM.GetXaxis().FindBin(Eta)
		  phibin = mapSigLEM.GetYaxis().FindBin(Phi)

		  if (part>=0 and part<=3) :
		    if par == "sigLeft":
		      mapSigLEM.SetBinContent(etabin, phibin, hist.GetMean())
		      mapSigLrmsEM.SetBinContent(etabin, phibin, hist.GetRMS())
		    if par == "sigRight":
		      mapSigREM.SetBinContent(etabin, phibin, hist.GetMean())
		      mapSigRrmsEM.SetBinContent(etabin, phibin, hist.GetRMS())
		    if par == "UvsA":
		      mapUvsAEM.SetBinContent(etabin, phibin, hist.GetMean())
		      mapUvsArmsEM.SetBinContent(etabin, phibin, hist.GetRMS())
		  elif (part>3 and part<=8) :
		    if par == "sigLeft":
		      mapSigLHAD.SetBinContent(etabin, phibin, hist.GetMean())
		      mapSigLrmsHAD.SetBinContent(etabin, phibin, hist.GetRMS())
		    if par == "sigRight":
		      mapSigRHAD.SetBinContent(etabin, phibin, hist.GetMean())
		      mapSigRrmsHAD.SetBinContent(etabin, phibin, hist.GetRMS())
		    if par == "UvsA":
		      mapUvsAHAD.SetBinContent(etabin, phibin, hist.GetMean())
		      mapUvsArmsHAD.SetBinContent(etabin, phibin, hist.GetRMS())
		  else:
		    print "Invalid part for TTid ", histid

	  c = TCanvas("c", "c", 1200, 800)
	  c.Divide(2,1)

	  if par == "sigLeft":
	      c.cd(1)
	      mapSigLEM.Draw()
	      c.cd(2)
	      mapSigLrmsEM.Draw()
	  if par == "sigRight":
	      c.cd(1)
	      mapSigREM.Draw()
	      c.cd(2)
	      mapSigRrmsEM.Draw()
	  if par == "UvsA":
	      c.cd(1)
	      mapUvsAEM.Draw()
	      c.cd(2)
	      mapUvsArmsEM.Draw()
	  c.Print("parmaps80MHzEM_"+par+"_fixedUvsA.png")
	  raw_input("Press Enter to continue...")

	  c = TCanvas("c", "c", 1200, 800)
	  c.Divide(2,1)

	  if par == "sigLeft":
	      c.cd(1)
	      mapSigLHAD.Draw()
	      c.cd(2)
	      mapSigLrmsHAD.Draw()
	  if par == "sigRight":
	      c.cd(1)
	      mapSigRHAD.Draw()
	      c.cd(2)
	      mapSigRrmsHAD.Draw()
	  if par == "UvsA":
	      c.cd(1)
	      mapUvsAHAD.Draw()
	      c.cd(2)
	      mapUvsArmsHAD.Draw()
	  c.Print("parmaps80MHzHAD_"+par+"_fixedUvsA.png")
	  raw_input("Press Enter to continue...")

	out = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/parameterMaps_fixedUvsA.root", "RECREATE")
	out.cd()
	mapSigLEM.Write()
	mapSigLrmsEM.Write()
	mapSigREM.Write()
	mapSigRrmsEM.Write()
	mapUvsAEM.Write()
	mapUvsArmsEM.Write()
	mapSigLHAD.Write()
	mapSigLrmsHAD.Write()
	mapSigRHAD.Write()
	mapSigRrmsHAD.Write()
	mapUvsAHAD.Write()
	mapUvsArmsHAD.Write()
	out.Close()


# drawing maps of sigma_l, sigma_r and UvsA from given DESD.  
if draw=="corrections":

	rootname = str(raw_input("root file name: .root"))
	run = str(raw_input("run no: "))

	file_list = []

	combinedFile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/"+rootname+".root")
	combinedFile.cd()
	EMhist_no=gDirectory.Get("hist_goodfitsfromEntriesEM")
	HADhist_no=gDirectory.Get("hist_goodfitsfromEntriesHAD")

	EMhist_Sigl = gDirectory.Get("hist_siglEM")
	EMhist_Sigr = gDirectory.Get("hist_sigrEM")
	#EMhist_UvsA = gDirectory.Get("emUvsA_eightyMHzFit")

	HADhist_Sigl = gDirectory.Get("hist_siglHAD")
	HADhist_Sigr = gDirectory.Get("hist_sigrHAD")
	#HADhist_UvsA = gDirectory.Get("hadUvsA_eightyMHzFit")


	EMhist_Sigl.SetMinimum(10)
	EMhist_Sigl.SetMaximum(40)
	EMhist_Sigr.SetMinimum(10)
	EMhist_Sigr.SetMaximum(40)
	#EMhist_UvsA.SetMinimum(10)
	#EMhist_UvsA.SetMaximum(40)
	HADhist_Sigl.SetMinimum(10)
	HADhist_Sigl.SetMaximum(40)
	HADhist_Sigr.SetMinimum(10)
	HADhist_Sigr.SetMaximum(40)
	
	HADhist_Sigl.SetStats(0)
	HADhist_Sigr.SetStats(0)
#	HADhist_UvsA.SetStats(0)
	EMhist_Sigl.SetStats(0)
	EMhist_Sigr.SetStats(0)
#	EMhist_UvsA.SetStats(0)
	

	EMhist_Sigl.Draw()
	#raw_input("Press Enter to continue...")
	#EMhist_Sigl.ProjectionX().Draw()
	raw_input("Press Enter to continue...")
	EMhist_Sigr.Draw()
	#raw_input("Press Enter to continue...")
	#EMhist_Sigr.ProjectionX().Draw()
	raw_input("Press Enter to continue...")

	HADhist_Sigl.Draw()
	#raw_input("Press Enter to continue...")
	#HADhist_Sigl.ProjectionX().Draw()
	raw_input("Press Enter to continue...")
	HADhist_Sigr.Draw()
	#raw_input("Press Enter to continue...")
	#HADhist_Sigr.ProjectionX().Draw()
	raw_input("Press Enter to continue...")

	parhistlist = [EMhist_Sigl, EMhist_Sigr, HADhist_Sigl, HADhist_Sigr]

	#comparing to P4 values

	for binx in range(EMhist_Sigl.GetXaxis().GetNbins()):
	  for biny in range(EMhist_Sigl.GetYaxis().GetNbins()):
	      for parhist in parhistlist:
		if parhist.GetBinContent(binx, biny) < 0.:
		    print "value below 0! Value is.. ",parhist.GetBinContent(binx, biny)
		    if not parhist.GetBinContent(binx, biny+1) < 0.:
			  val1 = parhist.GetBinContent(binx, biny+1)
		    else:
			  val1 = parhist.GetBinContent(binx, biny+2)
		    if not parhist.GetBinContent(binx, biny-1) < 0.:
			  val2 = parhist.GetBinContent(binx, biny-1)
		    else:
			  val2 = parhist.GetBinContent(binx, biny-2)
		    average = (val1+val2)/2.
		    parhist.SetBinContent(binx, biny, average)
		    print "Value is now... ", parhist.GetBinContent(binx, biny) 

	out = TFile("/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/"+rootname+"_filledblanks.root", "RECREATE")
	out.cd()
	for parhist in parhistlist:
	  parhist.Write()
	EMhist_no.Write()
	HADhist_no.Write()	



	fP4 = TFile("/afs/cern.ch/work/c/cantel/private/output/maps_filledblanks.root")
	fP4.cd()
	
	emP4sigl = gDirectory.Get("emsigmal") 
	emP4sigr = gDirectory.Get("emsigmar") 
	hadP4sigl = gDirectory.Get("hadsigmal") 
	hadP4sigr = gDirectory.Get("hadsigmar") 

	EMhist_Sigl_corr = EMhist_Sigl.Clone()
	EMhist_Sigr_corr = EMhist_Sigr.Clone()
	HADhist_Sigl_corr = HADhist_Sigl.Clone()
	HADhist_Sigr_corr = HADhist_Sigr.Clone()

	EMhist_Sigl_corr.SetName("hist_siglEM_corr")
	EMhist_Sigr_corr.SetName("hist_sigrEM_corr")
	HADhist_Sigl_corr.SetName("hist_siglHAD_corr")
	HADhist_Sigr_corr.SetName("hist_sigrHAD_corr")
	

	EMhist_Sigl_corr.Divide(emP4sigl)
	EMhist_Sigr_corr.Divide(emP4sigr)
	
	HADhist_Sigl_corr.Divide(hadP4sigl)
	HADhist_Sigr_corr.Divide(hadP4sigr)

	maxsig = 1.8
	minsig = 0.7

	EMhist_Sigl_corr.SetMinimum(minsig)
	EMhist_Sigl_corr.SetMaximum(maxsig)
	EMhist_Sigr_corr.SetMinimum(minsig)
	EMhist_Sigr_corr.SetMaximum(maxsig)
	HADhist_Sigl_corr.SetMinimum(minsig)
	HADhist_Sigl_corr.SetMaximum(maxsig)
	HADhist_Sigr_corr.SetMinimum(minsig)
	HADhist_Sigr_corr.SetMaximum(maxsig)

	HADhist_Sigl_corr.SetTitle("sigma_left (HAD)")
	HADhist_Sigr_corr.SetTitle("sigma_right (HAD)")
	EMhist_Sigl_corr.SetTitle("sigma_left (EM)")
	EMhist_Sigr_corr.SetTitle("sigma_right (EM)")

	out.cd()
	EMhist_Sigl_corr.Write()
	EMhist_Sigr_corr.Write()
	HADhist_Sigl_corr.Write()
	HADhist_Sigr_corr.Write()

	nbinsy = EMhist_Sigl_corr.GetNbinsY()

	EMsigl_Px=EMhist_Sigl_corr.ProjectionX()
	EMsigl_Px.Scale(1./nbinsy)
	EMsigr_Px=EMhist_Sigr_corr.ProjectionX()
	EMsigr_Px.Scale(1./nbinsy)

	HADsigl_Px=HADhist_Sigl_corr.ProjectionX()
	HADsigl_Px.Scale(1./nbinsy)
	HADsigr_Px=HADhist_Sigr_corr.ProjectionX()
	HADsigr_Px.Scale(1./nbinsy)
		  

	EMhist_Sigl.Draw("lego2 Z")
	#raw_input("Press Enter to continue...")
	#EMhist_Sigl.ProjectionX().Draw()
	raw_input("Press Enter to continue...")
	EMhist_Sigr.Draw("lego2 Z")
	#raw_input("Press Enter to continue...")
	#EMhist_Sigr.ProjectionX().Draw()
	raw_input("Press Enter to continue...")
	
	HADhist_Sigl.Draw("lego2 Z")
	#raw_input("Press Enter to continue...")
	#HADhist_Sigl.ProjectionX().Draw()
	raw_input("Press Enter to continue...")
	HADhist_Sigr.Draw("lego2 Z")
	#raw_input("Press Enter to continue...")
	#HADhist_Sigr.ProjectionX().Draw()
	raw_input("Press Enter to continue...")

	line = TLine()
	line.SetLineColor(kRed)
	line.SetLineWidth(3)

	EMsigl_Px.SetStats(0)
	EMsigr_Px.SetStats(0)
	HADsigl_Px.SetStats(0)
	HADsigr_Px.SetStats(0)

	mincf = 0.6
	maxcf = 1.8

	EMsigl_Px.SetMinimum(mincf)
	EMsigr_Px.SetMinimum(mincf)
	HADsigl_Px.SetMinimum(mincf)
	HADsigr_Px.SetMinimum(mincf)

	EMsigl_Px.SetMaximum(maxcf)
	EMsigr_Px.SetMaximum(maxcf)
	HADsigl_Px.SetMaximum(maxcf)
	HADsigr_Px.SetMaximum(maxcf)

	EMsigl_Px.Draw()
	line.DrawLine(-4.9, 1.05, -3.2, 1.05)
	line.DrawLine(3.2, 1.05, 4.9, 1.05)
	line.DrawLine(-2.2, 0.95, -1.5, 0.95)
	line.DrawLine(2.2, 0.95, 1.5, 0.95)
	line.DrawLine(-2.2, 0.90, -2.4, 0.90)
	line.DrawLine(2.2, 0.90, 2.4, 0.90)
	line.DrawLine(-2.4, 0.85, -2.5, 0.85)
	line.DrawLine(2.4, 0.85, 2.5, 0.85)
	line.DrawLine(-1.4, 0.85, -1.5, 0.85)
	line.DrawLine(1.4, 0.85, 1.5, 0.85)
	line.DrawLine(-1.4, 1, 1.4, 1)
	raw_input("Press Enter to continue...")
	EMsigr_Px.Draw()
	line.DrawLine(-4.9, 1.05, -3.2, 1.05)
	line.DrawLine(3.2, 1.05, 4.9, 1.05)
	line.DrawLine(-2.2, 0.95, -1.5, 0.95)
	line.DrawLine(2.2, 0.95, 1.5, 0.95)
	line.DrawLine(-2.2, 0.90, -2.4, 0.90)
	line.DrawLine(2.2, 0.90, 2.4, 0.90)
	line.DrawLine(-2.4, 0.85, -2.5, 0.85)
	line.DrawLine(2.4, 0.85, 2.5, 0.85)
	line.DrawLine(-1.4, 0.85, -1.5, 0.85)
	line.DrawLine(1.4, 0.85, 1.5, 0.85)
	line.DrawLine(-1.4, 1, 1.4, 1)
	raw_input("Press Enter to continue...")
	HADsigl_Px.Draw()
	line.DrawLine(-4.9, 1.20, -4.46, 1.20)
	line.DrawLine(-4.46, 1.30, -4.05, 1.30)
	line.DrawLine(-3.63, 1.20, -4.05, 1.20)
	line.DrawLine(-3.63, 1.30, -3.2, 1.30)
	line.DrawLine(4.9, 1.30, 4.46, 1.30)
	line.DrawLine(4.46, 1.20, 4.05, 1.20)
	line.DrawLine(3.63, 1.30, 4.05, 1.30)
	line.DrawLine(3.63, 1.20, 3.2, 1.20)
	line.DrawLine(-3.2, 1, 3.2, 1)
	raw_input("Press Enter to continue...")
	HADsigr_Px.Draw()
	line.DrawLine(-4.9, 1.20, -4.46, 1.20)
	line.DrawLine(-4.46, 1.30, -4.05, 1.30)
	line.DrawLine(-3.63, 1.20, -4.05, 1.20)
	line.DrawLine(-3.63, 1.30, -3.2, 1.30)
	line.DrawLine(4.9, 1.30, 4.46, 1.30)
	line.DrawLine(4.46, 1.20, 4.05, 1.20)
	line.DrawLine(3.63, 1.30, 4.05, 1.30)
	line.DrawLine(3.63, 1.20, 3.2, 1.20)
	line.DrawLine(-3.2, 1, 3.2, 1)
	raw_input("Press Enter to continue...")

	out.cd()	
	EMsigl_Px.Write()
	EMsigr_Px.Write()
	HADsigl_Px.Write()
	HADsigr_Px.Write()
	out.Close()	

# drawing maps of sigma_l, sigma_r and UvsA from given DESD.  
if draw=="parcompare":

	run = str(raw_input("run no: "))

	file_list = []

	infile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/parameterMaps.root")

	EMhist_Sigl = gDirectory.Get("mapSigLEM")
	EMhist_Sigr = gDirectory.Get("mapSigREM")
	EMhist_UvsA = gDirectory.Get("mapUvsAEM")
	HADhist_Sigl = gDirectory.Get("mapSigLHAD")
	HADhist_Sigr = gDirectory.Get("mapSigLHAD")
	HADhist_UvsA = gDirectory.Get("mapUvsAHAD")

	HADhist_Sigl.SetStats(0)
	HADhist_Sigr.SetStats(0)
	HADhist_UvsA.SetStats(0)
	EMhist_Sigl.SetStats(0)
	EMhist_Sigr.SetStats(0)
	EMhist_UvsA.SetStats(0)

	#comparing to P4 values

	fP4 = TFile("/afs/cern.ch/work/c/cantel/private/output/maps_filledblanks.root")
	fP4.cd()
	
	emP4sigl = gDirectory.Get("emsigmal") 
	emP4sigr = gDirectory.Get("emsigmar") 
	hadP4sigl = gDirectory.Get("hadsigmal") 
	hadP4sigr = gDirectory.Get("hadsigmar") 

	emP4uvsa = gDirectory.Get("emDA") 
	hadP4uvsa = gDirectory.Get("hadDA") 

	EMhist_Sigl.Divide(emP4sigl)
	EMhist_Sigr.Divide(emP4sigr)
	
	HADhist_Sigl.Divide(hadP4sigl)
	HADhist_Sigr.Divide(hadP4sigr)

	EMhist_UvsA.Divide(emP4uvsa)
	HADhist_UvsA.Divide(hadP4uvsa)

	maxsig = 4.

	EMhist_Sigl.SetMinimum(0)
	EMhist_Sigl.SetMaximum(maxsig)
	EMhist_Sigr.SetMinimum(0)
	EMhist_Sigr.SetMaximum(maxsig)
	HADhist_Sigl.SetMinimum(0)
	HADhist_Sigl.SetMaximum(maxsig)
	HADhist_Sigr.SetMinimum(0)
	HADhist_Sigr.SetMaximum(maxsig)
	EMhist_UvsA.SetMinimum(0)
	EMhist_UvsA.SetMaximum(maxsig)
	HADhist_UvsA.SetMinimum(0)
	HADhist_UvsA.SetMaximum(maxsig)

	HADhist_Sigl.SetTitle("sigma_left (HAD)")
	HADhist_Sigr.SetTitle("sigma_right (HAD)")
	EMhist_Sigl.SetTitle("sigma_left (EM)")
	EMhist_Sigr.SetTitle("sigma_right (EM)")
	EMhist_UvsA.SetTitle("UVSA (EM)")
	HADhist_UvsA.SetTitle("UVSA (HAD)")

	nbinsy = EMhist_Sigl.GetNbinsY()

	EMsigl_Px=EMhist_Sigl.ProjectionX()
	EMsigl_Px.Scale(1./nbinsy)
	EMsigr_Px=EMhist_Sigr.ProjectionX()
	EMsigr_Px.Scale(1./nbinsy)

	HADsigl_Px=HADhist_Sigl.ProjectionX()
	HADsigl_Px.Scale(1./nbinsy)
	HADsigr_Px=HADhist_Sigr.ProjectionX()
	HADsigr_Px.Scale(1./nbinsy)

	EMuvsa_Px=EMhist_UvsA.ProjectionX()
	EMuvsa_Px.Scale(1./nbinsy)
	HADuvsa_Px=HADhist_UvsA.ProjectionX()
	HADuvsa_Px.Scale(1./nbinsy)

	out = TFile("/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/fixedSigmaMaps_"+run+".root", "RECREATE")
	out.cd()
	EMhist_Sigl.Write()
	EMhist_Sigr.Write()
	HADhist_Sigl.Write()
	HADhist_Sigr.Write()
	EMsigl_Px.Write()
	EMsigr_Px.Write()
	HADsigl_Px.Write()
	HADsigr_Px.Write()
	EMhist_UvsA.Write()
	HADhist_UvsA.Write()
	EMuvsa_Px.Write()
	HADuvsa_Px.Write()
	out.Close()		
		  

	EMhist_Sigl.Draw("lego2 Z")
	#raw_input("Press Enter to continue...")
	#EMhist_Sigl.ProjectionX().Draw()
	#raw_input("Press Enter to continue...")
	EMhist_Sigr.Draw("lego2 Z")
	#raw_input("Press Enter to continue...")
	#EMhist_Sigr.ProjectionX().Draw()
	#raw_input("Press Enter to continue...")
	
	HADhist_Sigl.Draw("lego2 Z")
	#raw_input("Press Enter to continue...")
	#HADhist_Sigl.ProjectionX().Draw()
	#raw_input("Press Enter to continue...")
	HADhist_Sigr.Draw("lego2 Z")
	#raw_input("Press Enter to continue...")
	#HADhist_Sigr.ProjectionX().Draw()
	#raw_input("Press Enter to continue...")

	line = TLine()
	line.SetLineColor(kRed)
	line.SetLineWidth(3)

	EMsigl_Px.SetStats(0)
	EMsigr_Px.SetStats(0)
	HADsigl_Px.SetStats(0)
	HADsigr_Px.SetStats(0)

	EMsigl_Px.Draw()
	line.DrawLine(-4.9, 1.05, -3.2, 1.05)
	line.DrawLine(3.2, 1.05, 4.9, 1.05)
	line.DrawLine(-2.2, 0.95, -1.5, 0.95)
	line.DrawLine(2.2, 0.95, 1.5, 0.95)
	line.DrawLine(-2.2, 0.90, -2.4, 0.90)
	line.DrawLine(2.2, 0.90, 2.4, 0.90)
	line.DrawLine(-2.4, 0.85, -2.5, 0.85)
	line.DrawLine(2.4, 0.85, 2.5, 0.85)
	line.DrawLine(-1.4, 0.85, -1.5, 0.85)
	line.DrawLine(1.4, 0.85, 1.5, 0.85)
	line.DrawLine(-1.4, 1, 1.4, 1)
	raw_input("Press Enter to continue...")
	EMsigr_Px.Draw()
	line.DrawLine(-4.9, 1.05, -3.2, 1.05)
	line.DrawLine(3.2, 1.05, 4.9, 1.05)
	line.DrawLine(-2.2, 0.95, -1.5, 0.95)
	line.DrawLine(2.2, 0.95, 1.5, 0.95)
	line.DrawLine(-2.2, 0.90, -2.4, 0.90)
	line.DrawLine(2.2, 0.90, 2.4, 0.90)
	line.DrawLine(-2.4, 0.85, -2.5, 0.85)
	line.DrawLine(2.4, 0.85, 2.5, 0.85)
	line.DrawLine(-1.4, 0.85, -1.5, 0.85)
	line.DrawLine(1.4, 0.85, 1.5, 0.85)
	line.DrawLine(-1.4, 1, 1.4, 1)
	raw_input("Press Enter to continue...")
	HADsigl_Px.Draw()
	line.DrawLine(-4.9, 1.20, -4.46, 1.20)
	line.DrawLine(-4.46, 1.30, -4.05, 1.30)
	line.DrawLine(-3.63, 1.20, -4.05, 1.20)
	line.DrawLine(-3.63, 1.30, -3.2, 1.30)
	line.DrawLine(4.9, 1.30, 4.46, 1.30)
	line.DrawLine(4.46, 1.20, 4.05, 1.20)
	line.DrawLine(3.63, 1.30, 4.05, 1.30)
	line.DrawLine(3.63, 1.20, 3.2, 1.20)
	line.DrawLine(-3.2, 1, 3.2, 1)
	raw_input("Press Enter to continue...")
	HADsigr_Px.Draw()
	line.DrawLine(-4.9, 1.20, -4.46, 1.20)
	line.DrawLine(-4.46, 1.30, -4.05, 1.30)
	line.DrawLine(-3.63, 1.20, -4.05, 1.20)
	line.DrawLine(-3.63, 1.30, -3.2, 1.30)
	line.DrawLine(4.9, 1.30, 4.46, 1.30)
	line.DrawLine(4.46, 1.20, 4.05, 1.20)
	line.DrawLine(3.63, 1.30, 4.05, 1.30)
	line.DrawLine(3.63, 1.20, 3.2, 1.20)
	line.DrawLine(-3.2, 1, 3.2, 1)
	raw_input("Press Enter to continue...")	
	EMuvsa_Px.Draw()
	raw_input("Press Enter to continue...")
	HADuvsa_Px.Draw()
	raw_input("Press Enter to continue...")

	infile.Close()
	out.Close()


if draw=="stats":

	file_list = []

	desd_list = ["CALJET", "SGLEL", "PHOJET"]

	for desd in desd_list:
		f = TFile("/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/TimingFitHistos_combined80MHz_"+desd+".root")
		file_list.append(f)


	c1=TCanvas("c1", "hits", 1024, 800)
	c1.Divide(2,3)
	#c2=TCanvas("c2", "HADhits", 1024, 800)
	#c1.Divide(2,2)

	for i in range(0, len(file_list)):
		file_list[i].cd("HistsFit")
		emNo=gDirectory.Get("emNFits")
		hadNo=gDirectory.Get("hadNFits")
		
		emNo.SetTitle("no. of good hits for DESD "+desd_list[i]+" (EM)")		
		hadNo.SetTitle("no. of good hits for DESD "+desd_list[i]+" (HAD)")
		emNo.SetStats(0)
		hadNo.SetStats(0)

		c1.cd((i+1)+i)
		emNo.Draw()
		c1.cd(2*(i+1))
		hadNo.Draw()


	raw_input("Press Enter to continue...")


if draw == "fits":

  fname = raw_input("directory/file: ")
  TTid = raw_input("TT id (0x...): ")
  part = raw_input("part: ")

  f = TFile(fname)
  f.cd("HistsFit")

  keys = gDirectory.GetListOfKeys()


  for key in keys:

    #print key.GetName()
    #if not key.startswith("pulsefit"): continue

    if not key.GetName().endswith(TTid+"_part"+part): continue

    hist = gDirectory.Get(key.GetName())

    gStyle.SetOptStat(0)
    gStyle.SetOptFit(111)

    hist.Draw()

    outputpng = raw_input("Print to file?(y/n)")

    if outputpng == "y":
	c1.Print(hist.GetName()+".png")