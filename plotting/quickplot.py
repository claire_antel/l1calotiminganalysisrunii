# -*- coding: utf-8 -*-
from ROOT import *
import re
import os

gROOT.LoadMacro("plot_style.C")

set_plot_style() 

gStyle.SetTextFont(50)
gStyle.SetTitleXOffset(1.4)
gStyle.SetTitleYOffset(1.4)

draw = raw_input('Draw... (fitted, hits, quality, pulsehists (rows), timehists (rows), qualhists (rows), corrhists, quick, fixedpars, quicklook(l))')

if draw == "TH2":
  fstring = str(raw_input("input dir/file: "))
  subf = str(raw_input("sub directory: "))
  histname = str(raw_input("name of hist: "))

  inputf = TFile(fstring)
  inputf.cd(subf)
  hist = gDirectory.Get(histname)

  hist.SetOption("colz")
  hist.Draw()

  raw_input("Press enter to continue.. ")


run = str(raw_input("run no.: "))

inputf = TFile("/afs/cern.ch/user/c/cantel/20.1.5.1_2/run/TimingFitHistos_run"+run+".root")

if draw=="fitted":
		ID = str(raw_input("TT id0x"))
		inputf.cd("HistsFit")
		keys = gDirectory.GetListOfKeys()
		cnt=1
		cntTot=0
		directory='inspectedTTs_Feb16/TT0x'+ID
		if not os.path.exists(directory):
    			os.makedirs(directory)

		for key in keys:
			match = re.search(r'_id0x'+ID+'_part', key.GetName())
			if not match: continue
			match2 = re.search(r'failed$', key.GetName())
			#match2 = 0
			if not match2:
				cntTot+=1
		if cntTot>300:
			cntTot=300
		for key in keys:
			match = re.search(r'_id0x'+ID+'_part', key.GetName())
			if not match: continue
			match2 = re.search(r'failed$', key.GetName())
			#match2 = 0
			if not match2:
				print "match found."
				c=TCanvas('c1', 'c1', 700, 600)
				hist=gDirectory.Get(key.GetName())
				FitPar=hist.GetFunction('fitP')
				FitPar.Delete()
				Fit=hist.GetFunction('fit')
				time=Fit.GetParameter("Maximumposition")
				gStyle.SetOptStat(0)
				gStyle.SetOptFit(111)
				hist.GetYaxis().SetRangeUser(0.95*hist.GetMinimum(), 1.05*hist.GetMaximum())
				#gStyle.SetOptFit(111)
				hist.Draw()
				offset=round(time-187.5,1)
				l=TLatex()
				l.SetNDC()
				l.SetTextFont(42)
				l.SetTextColor(46)
				l.DrawLatex(0.5,0.2,"t offset = "+str(offset)+" ns");
				#l2=TLatex()
				#l2.SetNDC()
				#l2.SetTextFont(42)
				#l2.DrawLatex(0.75,0.84,str(cnt)+"/"+str(cntTot));
				c.Print(directory+'/TT0x'+ID+'_fitted'+str(cnt)+'run'+'.png')
				if cnt == 10:
					break
				cnt=cnt+1
				raw_input("Press Enter to continue...")

#inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/271048/TimingFitHistos_combined_271048.root")

if draw=="hits":

		inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/eightyMHzruns/TimingFitHistos_combined80MHz_CALJET.root")

		inputf.cd("HistsFit")

		emgoodhits=gDirectory.Get("emHits")
		hadgoodhits=gDirectory.Get("hadHits")
		emgoodfits=gDirectory.Get("emNGoodFits")
		hadgoodfits=gDirectory.Get("hadNGoodFits")
		emfailedfits=gDirectory.Get("emNFailedFits")
		hadfailedfits=gDirectory.Get("hadNFailedFits")

		emgoodfitsX = emgoodfits.ProjectionX()
		hadgoodfitsX = hadgoodfits.ProjectionX()	

		emgoodhits.SetStats(0)
		hadgoodhits.SetStats(0)
		emgoodfits.SetStats(0)
		hadgoodfits.SetStats(0)
		emfailedfits.SetStats(0)
		hadfailedfits.SetStats(0)
		#maxbin=4000
		#emgoodhits.SetMaximum(maxbin)
		#hadgoodhits.SetMaximum(maxbin)
		#emgoodfits.SetMaximum(maxbin)
		#hadgoodfits.SetMaximum(maxbin)
		#emfailedfits.SetMaximum(maxbin)
		#hadfailedfits.SetMaximum(maxbin)

		output_root = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingAugust2015/hit_maps.root", "RECREATE")
		output_root.cd()
		emgoodhits.Write()
		hadgoodhits.Write()
		emgoodfits.Write()
		hadgoodfits.Write()
		emfailedfits.Write()
		hadfailedfits.Write()

		output_root.Close()		

		cHFEM= TCanvas("HitsFits", "Hits and Fits in EM", 1600, 1200)
		cHFEM.Divide(2,2)
		cHFEM.cd(1)
		gPad.SetLogz()
		emgoodhits.Draw()
		cHFEM.cd(2)
		gPad.SetLogz()
		hadgoodhits.Draw()
		cHFEM.cd(3)
		gPad.SetLogz()
		emgoodfits.Draw()
		cHFEM.cd(4)
		gPad.SetLogz()
		hadgoodfits.Draw()
	#	cHFEM.cd(5)
	#	emfailedfits.Draw()
	#	cHFEM.cd(6)
	#	hadfailedfits.Draw()
		raw_input("Press enter... ")
		#cHFEM.Print("hitsMap_run"+run+".png")
		

		l=TLatex()
		l.SetNDC()
		l.SetTextFont(42)
		l.SetTextColor(46)
		emgoodfitsX.SetLineWidth(6)
		hadgoodfitsX.SetLineWidth(6)
		emgoodfitsX.SetStats(0)
		hadgoodfitsX.SetStats(0)
		emgoodfitsX.SetTitle("")
		hadgoodfitsX.SetTitle("")

		cProf= TCanvas("HitsFitsProf", "Hits and Fits Profiles", 1500, 1200)
		cProf.cd()
		emgoodfitsX.Draw()
		l.DrawLatex(0.4,0.7,"number of good fits");
		l.DrawLatex(0.5,0.64,"EM");
		raw_input("Press enter... ")
		cProf.Print("EM_goodfitProfile_run"+run+".png")
		cProf.cd()
		hadgoodfitsX.Draw()
		l.DrawLatex(0.4,0.7,"number of good fits");
		l.DrawLatex(0.5,0.64,"HAD");
		raw_input("Press enter... ")
		#cProf.Print("HAD_goodfitProfile_run"+run+".png")
		
		

if draw=="quality":

		inputf.cd("HistsFit")
		emec_corr=gDirectory.Get("EMEC_qualitytimecorr")

		c= TCanvas("c", "c", 1080, 1024)
		c.cd(1)
		emb_corr.Draw()
		c.WaitPrimitive()


#inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/"+run+"/IDPropagation_combined"+run+".root")
inputf2 = TFile("/afs/cern.ch/user/c/cantel/20.1.5.1_2/run/TimingFitHistos_run_energynorm_279685.root")
if draw=="pulsehists":

		layer=raw_input("Layer (EM/HAD): ")

		c= TCanvas("c", "c", 800,600 )
		#c.Divide(5,1)

		#inputf2.cd(layer+"/normalised")
		inputf2.cd("HistsFit")


		keys=gDirectory.GetListOfKeys()

		prefixA="0x"
		IDpart = str(raw_input('TT id 0x-----'))
		trailingA = str(raw_input('channel (00-03):'))
		k=0
		#for i in ['0', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd', 'e', 'f']:
		#for i in ['7']:
		ID=prefixA+IDpart
		hname="energyNormPulse"+ID+"_col"
		#if not hname in keys: continue
		print hname
		k+=1
		c.cd(k)
		#if not gDirectory.GetObject(hname, pulse): continue
		pulse=gDirectory.Get(hname)
		pulse.GetXaxis().SetLabelSize(0.06)
		pulse.GetYaxis().SetLabelSize(0.06)
		pulse.GetXaxis().SetTitleSize(0.06)
		pulse.GetYaxis().SetTitleSize(0.06)
		pulse.SetStats(0)
		pulse.Draw()


		raw_input("press Enter...")
		c.Print("pulse_rows_"+prefixA+IDpart+"X"+trailingA+"_"+run+".png")


			#continuing = raw_input('Continue?(Y/N)')
inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/TowerTimingHistos_bulkonly.root")
inputf3 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/suspiciousphotons/IDPropagation_run"+run+".root")
if draw=="quicklookl":
		layer=raw_input("Layer (EM/HAD): ")
		#run=str(raw_input('run no (or all):'))
		#if run =='all':
		#	inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingPropagation_combined.root")
		#else:
		#	inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/"+run+"/TimingPropagation_combined"+run+".root")
		prefix="0x"
		IDpart = str(raw_input('TT id 0x-----'))
		trailingA = str(raw_input('channel (00-03):'))
		for i in ['0', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd', 'e', 'f']:

			prefixA="0x"+IDpart+i #04180"
			ID=prefixA+trailingA
			IDint=int(ID, 0)

			print "looking at ID",ID 

			c= TCanvas("c", "c", 1024,800 )
			c.Divide(2,2)

			inputf2.cd(layer+"/timing")
			keys=gDirectory.GetListOfKeys()

			hname="hist_tdistr_id"+str(IDint)
			if hname in keys:
				c.cd(1)
				pulse=gDirectory.Get(hname)
				pulse.SetTitle("timing offset")
				#ulse.GetXaxis().SetRangeUser(-60,60)
				pulse.Draw("nostack")
			else:
				print "time hist not found."

			inputf3.cd(layer+"/EvsT")

			keys=gDirectory.GetListOfKeys()

			hname="hist_ETvsTdistr_id"+str(IDint)		
			if hname in keys:
				c.cd(2)
				pulse=gDirectory.Get(hname)
				pulse.SetMarkerStyle(kStar)
				pulse.SetMarkerColor(46)
				pulse.SetOption("colz")
				#pulse.RebinY(40)
				pulse.Draw()
			else:
				print "EvsT hist not found."


			inputf3.cd(layer+"/normalised")

			keys=gDirectory.GetListOfKeys()
		
			hname="energyNormPulse"+ID+"_col"
			if hname in keys:
				c.cd(3)
				pulse=gDirectory.Get(hname)
				pulse.SetStats(0)
				pulse.Draw()
			else:
				print "coloured energy normal. pulse hist not found."

			inputf3.cd(layer+"/QvsT")

			keys=gDirectory.GetListOfKeys()

			hname="hist_qualitydistr_id"+str(IDint)
			if hname in keys:
				c.cd(4)
				pulse=gDirectory.Get(hname)
				pulse.SetOption("colz")
				pulse.Draw()
			else:
				print "QvsT hist not found."

			raw_input("press ENTER to continue... ")
			c.Print("inspectedTTs/TT"+ID+"_highDT_run"+run+".png")
		#c.WaitPrimitive()

if draw=="quicklook":
		layer=raw_input("Layer (EM/HAD): ")
		IDin=str(raw_input("Tower ID: 0x"))

		prefix="0x"
		ID=prefix+IDin
		IDint=int(ID, 0)

		c= TCanvas("c", "c", 1024,800 )
		c.Divide(2,2)

		inputf2.cd(layer+"/timing")
		keys=gDirectory.GetListOfKeys()

		hname="hist_tdistr_id"+str(IDint)
		if hname in keys:
			c.cd(1)
			pulse=gDirectory.Get(hname)
			histl = pulse.GetHists()
			print histl[0].GetName()
			mean = round(histl[0].GetMean(),1)
			rms = round(histl[0].GetRMS(),1)	
			pulse.SetTitle("timing offset")
			l=TLatex()
			l.SetNDC()
			l.SetTextFont(42)
			l.SetTextColor(4)
			pulse.Draw("nostack")
			l.DrawLatex(0.64,0.8,"t offset = "+str(mean)+" ns")
			l.DrawLatex(0.64,0.74,"t rms = "+str(rms)+" ns")
			l.DrawLatex(0.2,0.6,"TT"+ID)
			#ulse.GetXaxis().SetRangeUser(-60,60)
		else:
			print "time hist not found."

		inputf3.cd(layer+"/EvsT")

		keys=gDirectory.GetListOfKeys()

		hname="hist_ETvsTdistr_id"+str(IDint)		
		if hname in keys:
			c.cd(2)
			pulse=gDirectory.Get(hname)
			pulse.SetMarkerStyle(kStar)
			pulse.SetMarkerColor(46)
			pulse.SetOption("colz")

			pulse.Draw()
		else:
			print "EvsT hist not found."


		inputf3.cd(layer+"/normalised")

		keys=gDirectory.GetListOfKeys()
	
		hname="energyNormPulse"+ID+"_col"
		if hname in keys:
			c.cd(3)
			pulse=gDirectory.Get(hname)
			pulse.SetStats(0)
			pulse.Draw()
		else:
			print "coloured energy normal. pulse hist not found."

		inputf3.cd(layer+"/QvsT")

		keys=gDirectory.GetListOfKeys()

		hname="hist_qualitydistr_id"+str(IDint)
		if hname in keys:
			c.cd(4)
			pulse=gDirectory.Get(hname)
			pulse.GetYaxis().SetRangeUser(0,4000)
			pulse.SetOption("colz")
			pulse.Draw()
		else:
			print "QvsT hist not found."

		raw_input("press ENTER to continue... ")
		c.Print("overviewTT"+ID+"_run"+run+""+run+".png")
		#c.WaitPrimitive()

if draw=="timehist":
		layer=raw_input("Layer (EM/HAD): ")
		inputf2.cd(layer)

		IDhex = str(raw_input('TT id 0x'))
		IDint = int(IDhex, 0)
		hname="hist_tdistr_id"+str(IDint)
		print hname
		pulse=gDirectory.Get(hname)
		pulse.Draw()
		raw_input("press enter..")


if draw=="timehists":

		layer=raw_input("Layer (EM/HAD): ")
		inputf2.cd(layer+"/timing")
		
		#continuing = 'Y'
		#while continuing == 'Y':
		c= TCanvas("c", "c", 1500,1200 )
		c.Divide(4,4)
		
		keys=gDirectory.GetListOfKeys()
		
		
		prefix="0x"
		IDpart = str(raw_input('TT id 0x-----'))
		trailingA = str(raw_input('channel (00-03):'))
		k=0
		for i in ['0', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd', 'e', 'f']:
		#for i in ['b', 'c', 'd', 'e', 'f']:
			prefixA="0x"+IDpart #04180"
			ID=prefixA+i+trailingA
			IDint=int(ID, 0)
			hname="hist_tdistr_id"+str(IDint)
			if not hname in keys: continue
			print "plotting ", hname
			k+=1
			c.cd(k)
			#if not gDirectory.GetObject(hname, pulse): continue
			pulse=gDirectory.Get(hname)
			#pulse.GetXaxis().SetRangeUser(-25, 15)
			#pulse.GetXaxis().SetLabelSize(0.04)
			#pulse.GetYaxis().SetLabelSize(0.06)
			#pulse.GetXaxis().SetTitleSize(0.06)
			#pulse.GetYaxis().SetTitleSize(0.06)
			#pulse.SetLineWidth(3)
			pulse.SetTitle("timing TT"+ID)
			pulse.Draw("nostack")
		raw_input("press Enter...")
		c.Print("timehists_rows_"+prefixA+"X"+trailingA+"_"+run+".png")		


if draw=="qualhists":

		layer=raw_input("Layer (EM/HAD): ")
		inputf3.cd(layer+"/QvsT")
		
		#continuing = 'Y'
		#while continuing == 'Y':
		c= TCanvas("c", "c", 1500,1200)
		c.Divide(4,4)
		
		keys=gDirectory.GetListOfKeys()
		
		

		prefix="0x"
		IDpart = str(raw_input('TT id 0x-----'))
		trailingA = str(raw_input('channel (00-03):'))
		k=0
		for i in ['0', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd', 'e', 'f']:			
		#for i in ['b', 'c', 'd', 'e', 'f']:
			prefixA="0x"+IDpart #04180"
			print i
			ID=prefixA+i+trailingA
			IDint=int(ID, 0)
			hname="hist_qualitydistr_id"+str(IDint)
			if not hname in keys: continue
			print hname
			k+=1
			c.cd(k)
			#if not gDirectory.GetObject(hname, pulse): continue
			pulse=gDirectory.Get(hname)
			pulse.GetXaxis().SetLabelSize(0.04)
			#pulse.GetYaxis().SetLabelSize(0.06)
			pulse.GetXaxis().SetTitleSize(0.06)
			pulse.GetYaxis().SetTitleSize(0.06)
			pulse.GetXaxis().SetRangeUser(-15, 15)
			pulse.GetYaxis().SetRangeUser(0, 4000)
			#pulse.RebinY(80)
			#pulse.RebinX(2)
			pulse.SetOption("colz")
			#pulse.SetLineWidth(3)
			pulse.SetTitle("time vs caloquality TT"+ID)
			pulse.Draw()
		raw_input("press Enter...")		
		c.Print("qualhists_rows_"+prefixA+"X"+trailingA+"_"+run+".png")		

if draw=="EvsThists":
		layer=raw_input("Layer (EM/HAD): ")

		inputf3.cd(layer+"/EvsT")

	#continuing = 'Y'
	#while continuing == 'Y':
		c= TCanvas("c", "c", 1500,1200 )
		c.Divide(4,4)

		keys=gDirectory.GetListOfKeys()

		prefixA="0x"
		IDpart = str(raw_input('TT id 0x-----'))
		trailingA = str(raw_input('channel (00-03):'))
		k=0
		for i in ['0', '2', '3', '4', '5', '6', '7', '8', '9','a', 'b', 'c', 'd', 'e', 'f']:
		#for i in ['b', 'c', 'd', 'e', 'f']:
			ID=prefixA+IDpart+i+trailingA
			IDint=int(ID, 0)
			hname="hist_ETvsTdistr_id"+str(IDint)		
			if not hname in keys: continue
			print hname
			k+=1
			c.cd(k)
			#if not gDirectory.GetObject(hname, pulse): continue
			pulse=gDirectory.Get(hname)
			pulse.SetTitle("EvsT ID"+ID)
			pulse.SetOption("colz")
			pulse.GetXaxis().SetLabelSize(0.06)
			pulse.GetYaxis().SetLabelSize(0.06)
			pulse.GetXaxis().SetTitleSize(0.06)
			pulse.GetYaxis().SetTitleSize(0.06)
			pulse.SetStats(0)
			pulse.Draw()


		raw_input("Press enter")
		c.Print("EvsT_rows_"+prefixA+IDpart+"X"+trailingA+"_"+run+".png")
#continuing = raw_input('Continue?(Y/N)')

if draw=="corrhists":
		inputf.cd("HistsFit")

		pulse=gDirectory.Get("EMEC_tower1")
		pulse.GetXaxis().SetRangeUser(-30, 20)
		pulse.GetXaxis().SetLabelSize(0.05)
		pulse.GetYaxis().SetLabelSize(0.05)
		#pulse.GetYaxis().SetLabelOffset(1.5)
		#pulse.GetYaxis().Rebin(10)
		pulse.GetXaxis().SetTitleSize(0.05)
		pulse.GetYaxis().SetTitleSize(0.05)
		pulse.SetStats(0)

		c= TCanvas("c", "c", 1024,800 )
		pulse.Draw()
		c.WaitPrimitive()
		c.Print("emec_tower_corr"+run+".png")
 
if draw=="quick":

		layer=raw_input("Layer (EM/HAD): ")
		inputf3.cd(layer+"/normalised")
		
		#continuing = 'Y'
		#while continuing == 'Y':	
		keys=gDirectory.GetListOfKeys()
		
		
		hname="energyNormPulse0x02170f02_col"
		pulse=gDirectory.Get(hname)

		c= TCanvas("c", "c", 2024,1000 )
		c.cd()	
		pulse.Draw()
		c.WaitPrimitive()



inputf_fixed = TFile("/afs/cern.ch/work/c/cantel/private/output/maps_filledblanks.root")
#inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/271048/TimingFitHistos_combined_271048.root")

if draw=="fixedpars":
	inputf_fixed.cd()
	
	emsigmal=gDirectory.Get("emsigmal")
	emsigmar=gDirectory.Get("emsigmar")
	hadsigmal=gDirectory.Get("hadsigmal")
	hadsigmar=gDirectory.Get("hadsigmar")


	cEM= TCanvas("cEM", "cEM", 980,900 )
	cEM.Divide(1,2)
	cEM.cd(1)	
	emsigmal.Draw()
	cEM.cd(2)	
	emsigmar.Draw()
	cEM.WaitPrimitive()

	cHAD= TCanvas("cHAD", "cHAD", 980,900 )
	cHAD.Divide(1,2)
	cHAD.cd(1)	
	hadsigmal.Draw()
	cHAD.cd(2)	
	hadsigmar.Draw()
	cHAD.WaitPrimitive()

if draw=="spread":

	inputf_spread1 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingJuly2015/finaltiming_July2015_truncated.root")
	inputf_spread2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingAugust2015/finaltiming_August2015_truncated.root")

	inputf_spread1.cd()
	spreadEM1 = gDirectory.Get("emTiming_trunc_spread")
	spreadHAD1 = gDirectory.Get("hadTiming_trunc_spread")
	inputf_spread2.cd()
	spreadEM2 = gDirectory.Get("emTiming_trunc_spread")
	spreadHAD2 = gDirectory.Get("hadTiming_trunc_spread")
	
	spreadEM2.GetXaxis().SetRangeUser(-10, 10)
	spreadHAD2.GetXaxis().SetRangeUser(-10, 10)
	spreadEM1.SetFillStyle(3001)
	spreadEM1.SetFillColor(kGray+2)
	spreadEM2.SetFillColor(kBlue+2)
	spreadEM1.SetLineColor(kGray+2)
	spreadEM2.SetLineColor(kBlue+2)
	spreadHAD1.SetFillStyle(3001)
	spreadHAD1.SetFillColor(kGray+2)
	spreadHAD2.SetFillColor(kBlue-2)
	spreadHAD1.SetLineColor(kGray+2)
	spreadHAD2.SetLineColor(kBlue-2)



	c= TCanvas("c", "c", 1024,800 )
	c.cd()
	spreadEM2.Draw()
	spreadEM1.Draw("SAME")
	raw_input("Press enter")
	spreadHAD2.Draw()
	spreadHAD1.Draw("SAME")
	raw_input("Press enter")



