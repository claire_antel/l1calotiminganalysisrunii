from ROOT import *


fout= TFile("TimingPropagation_combined.root", "RECREATE")
fout.mkdir("timing")
fout.mkdir("quality")
fin=TFile("TimingPropagation_all.root")

for key in gDirectory.GetListOfKeys():
	histname = key.GetName() 
	if histname.startswith("hist_tdistr_id"):
		hist=key.ReadObj().Clone(histname)
		fout.cd("timing")
		hist.Write()
	if histname.startswith("hist_qualitydistr_id"):
		hist=key.ReadObj().Clone(histname)
		fout.cd("quality")
		hist.Write()

fout.Close()
fin.Close()
