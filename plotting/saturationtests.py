# -*- coding: iso-8859-1 -*-
from ROOT import *
import re
from time import sleep
import csv
from collections import defaultdict

gROOT.LoadMacro("plot_style.C")

set_plot_style() 

# functions

def MergedbyEtaGraph(etabin, etabin_dict):
		
		glist = TList()	

#		temp = TGraph()
#		temp.SetPoint(1, 0, 0)
#		temp.SetPoint(2, 1600, 1050)
#		temp.SetLineColor(0)
#		glist.Add(temp)

		for h in etabin_dict[etabin]:
			#h.SetMarkerStyle(7)
			#h.SetLineColor(0)
			glist.Add(h)

		Merged = TGraph()
 		Merged.SetPoint(1, 700, 1050)
		Merged.Merge(glist)
		Merged.SetMarkerStyle(7)
		Merged.SetLineColor(0)
		return Merged



task = str(raw_input("output plots or stats?: (plots/plotchannel/overview/stats/hits/plotbyeta/plotbyeta2root)"))

if task == "plotchannel":
	IDhex = str(raw_input("enter tower ID in hex: "))
	layer = str(raw_input("layer (EM/HAD): "))
	ID = int(IDhex,0)	
	inputf = TFile("/afs/cern.ch/work/c/cantel/private/SatBCIDoutput/FittedSatSlopes_"+layer+".root")
	#inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_Channel"+str(ID)+".root")

	text=TLatex()
	text.SetNDC()

	keys = gDirectory.GetListOfKeys()
	print "PRINTING KEYS..", keys
	count = 0

	for key in keys:
		print "key is..", key.GetName()
		findid = re.findall(r'\d+', key.GetName())
		histid = int(findid[0])
		#print histid
		if not histid == ID: continue
		plot = gDirectory.Get(key.GetName())
		nslice = int(findid[1])
		#if nslice != 3: continue
		print "slice no is", nslice
		plot.SetMarkerColor(4)
		plot.SetMarkerStyle(7)
		plot.Draw("P")
		frame = c1.DrawFrame(0, 0, 700, 1050)
		#plot.SetStats(0)
		plot.Draw("P")
		text.DrawLatex(0.55, 0.37, "TT id "+IDhex)
		if nslice == 1: 
			text.DrawLatex(0.6, 0.3,"slice n-3")
			#raw_input("Press Enter")
		elif nslice == 2: 
			text.DrawLatex(0.6, 0.3,"slice n-2.5")
		elif nslice == 3: 
			text.DrawLatex(0.6, 0.3,"slice n-2")
			raw_input("Press Enter")
		elif nslice == 4: 
			text.DrawLatex(0.6, 0.3,"slice n-1.5")
		elif nslice == 5: 
			text.DrawLatex(0.6, 0.3,"slice n-1")
		elif nslice == 6: 
			text.DrawLatex(0.6, 0.3,"slice n-0.5")
		elif nslice == 7: 
			text.DrawLatex(0.6, 0.3,"slice n")
		elif nslice == 8: 
			text.DrawLatex(0.6, 0.3,"slice n+0.5")
		elif nslice == 9: 
			text.DrawLatex(0.6, 0.3,"slice n+1")
		elif nslice == 10: 
			text.DrawLatex(0.6, 0.3,"slice n+1.5")
		elif nslice == 11: 
			text.DrawLatex(0.6, 0.3,"slice n+2")
		elif nslice == 12: 
			text.DrawLatex(0.6, 0.3,"slice n+2.5")
		elif nslice == 13: 
			text.DrawLatex(0.6, 0.3,"slice n+3")
		c1.Update()
		count+=1
		#c1.Print("EvsADC_TT0x"+hex(histid)+"_slice"+str(nslice)+".png")
		#if count == 20: break
		raw_input("presssss enter")


if task == "plotbyeta":

	layer = str(raw_input("layer (EM/HAD): "))

	output = TFile("EvsADC_byEtaBin_"+layer+".root", "RECREATE")

	mapfile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")
	mapfile.cd("SatHists")
	mapTH2 = gDirectory.Get("emHits_total").Clone()
	mapTH2.Reset("ICES")

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_"+layer+".root")
	#inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_Channel"+str(ID)+".root")

	text=TLatex()
	text.SetNDC()

	etalice_histnames = []

	etabin_dict_s2 = defaultdict(list)
	etabin_dict_s3 = defaultdict(list)
	etabin_dict_s4 = defaultdict(list)
	etabin_dict_s5 = defaultdict(list)
	etabin_dict_s6 = defaultdict(list)
	etabin_dict_s7 = defaultdict(list)
	etabin_dict_s8 = defaultdict(list)
	etabin_dict_s9 = defaultdict(list)
	etabin_dict_s10 = defaultdict(list)

	keys = gDirectory.GetListOfKeys()

	for key in keys:
		findid = re.findall(r'\d+', key.GetName())
		histid = str(findid[0])
		nslice = int(findid[1])

		Eta = -100
		with open('TTproperties.txt','r') as f:
		    next(f) # skip headings
		    reader=csv.reader(f,delimiter='\t')
		    for row in reader:
			#print "ID in loop is... ", row[0]
			if row[0] == histid:
				Eta= float(row[1])
				Phi = float(row[2])
				part = int(row[3])
				break				
		if Eta == -100:
			print "hist id "+str(histid)+" not found"
			continue

		etabin=mapTH2.GetXaxis().FindBin(Eta)
		
		#print "etabin is ... ", etabin

		if nslice == 2:
			etabin_dict_s2[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 3:
			etabin_dict_s3[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 4:
			etabin_dict_s4[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 5:
			etabin_dict_s5[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 6:
			etabin_dict_s6[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 7:
			etabin_dict_s7[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 8:
			etabin_dict_s8[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 9:
			etabin_dict_s9[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 10:
			etabin_dict_s10[etabin].append(gDirectory.Get(key.GetName()))

	print list(etabin_dict_s2.keys())

	for bin in etabin_dict_s2:
		
		print "plotting slopes for eta bin", bin
		
		output.mkdir("etabin"+str(bin))
		output.cd("etabin"+str(bin))

		if bin ==4: continue
	
		#c.Divide(3,3)

		temp = TGraph()
		temp.SetPoint(1, 0, 0)
		temp.SetPoint(2, 1600, 1050)
		temp.SetLineColor(0)

		for k in range(1,10):
			if k == 1:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s2[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n-2.5")
				bybinGraphs2.Write("slice_nm2p5")
			if k == 2:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s3[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n-2")
				bybinGraphs2.Write("slice_nm2")
			if k == 3:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s4[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n-1.5")
				bybinGraphs2.Write("slice_nm1p5")
			if k == 4:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s5[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n-1")
				bybinGraphs2.Write("slice_nm1")
			if k == 5:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s6[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n-0.5")
				bybinGraphs2.Write("slice_nmp5")
			if k == 6:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s7[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n")
				bybinGraphs2.Write("slice_n")
			if k == 7:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s8[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n+0.5")
				bybinGraphs2.Write("slice_npp5")
			if k == 8:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s9[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n+1")
				bybinGraphs2.Write("slice_np1")
			if k == 9:
#				c.cd(k)
#				frame = c.DrawFrame(0, 0, 1600, 1050)
				bybinGraphs2 = TMultiGraph()
				bybinGraphs2.Add(temp)
				for h in etabin_dict_s10[bin]:
					h.SetMarkerStyle(7)
					h.SetLineColor(0)
					bybinGraphs2.Add(h)
#					h.Draw("PSAME")
#				text.DrawLatex(0.55, 0.37, "etabin "+str(bin))
#				text.DrawLatex(0.6, 0.3,"slice n+1.5")
				bybinGraphs2.Write("slice_np1p5")
		c.Print("ADcvsE_slopes_etabin"+str(bin)+"_"+layer+".png")
		#raw_input("Press enter")

	output.Close()

if task == "plotbyeta2root":

	layer = str(raw_input("layer (EM/HAD): "))

	output = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/EvsADC_byEtaBin_"+layer+".root", "RECREATE")

	mapfile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")
	mapfile.cd("SatHists")
	mapTH2 = gDirectory.Get("emHits_total").Clone()
	mapTH2.Reset("ICES")

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_"+layer+".root")
	#inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_Channel"+str(ID)+".root")

	text=TLatex()
	text.SetNDC()

	etalice_histnames = []

	etabin_dict_s2 = defaultdict(list)
	etabin_dict_s3 = defaultdict(list)
	etabin_dict_s4 = defaultdict(list)
	etabin_dict_s5 = defaultdict(list)
	etabin_dict_s6 = defaultdict(list)
	etabin_dict_s7 = defaultdict(list)
	etabin_dict_s8 = defaultdict(list)
	etabin_dict_s9 = defaultdict(list)
	etabin_dict_s10 = defaultdict(list)

	keys = gDirectory.GetListOfKeys()

	for key in keys:
		findid = re.findall(r'\d+', key.GetName())
		histid = str(findid[0])
		nslice = int(findid[1])

		Eta = -100
		with open('TTproperties.txt','r') as f:
		    next(f) # skip headings
		    reader=csv.reader(f,delimiter='\t')
		    for row in reader:
			#print "ID in loop is... ", row[0]
			if row[0] == histid:
				Eta= float(row[1])
				Phi = float(row[2])
				part = int(row[3])
				break				
		if Eta == -100:
			print "hist id "+str(histid)+" not found"
			continue

		etabin=mapTH2.GetXaxis().FindBin(Eta)
		
		#print "etabin is ... ", etabin

		if nslice == 2:
			etabin_dict_s2[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 3:
			etabin_dict_s3[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 4:
			etabin_dict_s4[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 5:
			etabin_dict_s5[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 6:
			etabin_dict_s6[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 7:
			etabin_dict_s7[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 8:
			etabin_dict_s8[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 9:
			etabin_dict_s9[etabin].append(gDirectory.Get(key.GetName()))
		if nslice == 10:
			etabin_dict_s10[etabin].append(gDirectory.Get(key.GetName()))

	print list(etabin_dict_s2.keys())

	for bin in etabin_dict_s2:

		etaval=mapTH2.GetXaxis().GetBinCenter(bin)
		
		print "plotting slopes for eta bin ", bin, ", at eta ", etaval

		mergeds2 = MergedbyEtaGraph(bin, etabin_dict_s2)
		mergeds2.SetTitle("eta bin "+str(bin)+" at eta "+str(etaval))
		mergeds3 = MergedbyEtaGraph(bin, etabin_dict_s3)
		mergeds4 = MergedbyEtaGraph(bin, etabin_dict_s4)
		mergeds5 = MergedbyEtaGraph(bin, etabin_dict_s5)
		mergeds6 = MergedbyEtaGraph(bin, etabin_dict_s6)
		mergeds7 = MergedbyEtaGraph(bin, etabin_dict_s7)
		mergeds8 = MergedbyEtaGraph(bin, etabin_dict_s8)
		mergeds9 = MergedbyEtaGraph(bin, etabin_dict_s9)
		mergeds10 = MergedbyEtaGraph(bin, etabin_dict_s10)

		output.mkdir("etabin"+str(bin))
	
		output.cd("etabin"+str(bin))

		mergeds2.Write("slice_nm2p5")
		mergeds3.Write("slice_nm2")			
		mergeds4.Write("slice_nm1p5")			
		mergeds5.Write("slice_nm1")			
		mergeds6.Write("slice_nmp5")			
		mergeds7.Write("slice_n")			
		mergeds8.Write("slice_npp5")			
		mergeds9.Write("slice_np1")			
		mergeds10.Write("slice_np1p5")

		c = TCanvas("c", "c", 1200, 800)
		c.Divide(3,3)
		c.cd(1)
		mergeds2.Draw("AP")
		c.cd(2)
		mergeds3.Draw("AP")
		c.cd(3)
		mergeds4.Draw("AP")
		c.cd(4)
		mergeds5.Draw("AP")
		c.cd(5)
		mergeds6.Draw("AP")
		c.cd(6)
		mergeds7.Draw("AP")
		c.cd(7)
		mergeds8.Draw("AP")
		c.cd(8)
		mergeds9.Draw("AP")
		c.cd(9)
		mergeds10.Draw("AP")
		c.WaitPrimitive()
		c.Print("ADcvsE_slopes_etabin"+str(bin)+"_"+layer+".png")

	output.Close()
				



if task == "overview":

	#TTprops=open("TTproperties.txt", "r")

	#inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combinedCALJET.root")

	hist_peakE_EMB = TH1I("peakE_EMB", "peak ADC energy for region EMB; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_EMEC = TH1I("peakE_EMEC", "peak ADC energy for region EMEC; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_FCAL1 = TH1I("peakE_FCAL1", "peak ADC energy for region FCAL1; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_TILE = TH1I("peakE_TILE", "peak ADC energy for region TILE; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_HEC = TH1I("peakE_HEC", "peak ADC energy for region HEC; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_FCAL2 = TH1I("peakE_FCAL2", "peak ADC energy for region FCAL2; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_FCAL3 = TH1I("peakE_FCAL3", "peak ADC energy for region FCAL3; energy (ADC); count", 210, 0, 1050) 

	hist_peakcaloE_EMB = TH1I("peakcaloE_EMB", "peak calo energy for region EMB; calo energy (GeV); count", 200, 0, 2000) 
	hist_peakcaloE_EMEC = TH1I("peakcaloE_EMEC", "peak calo energy for region EMEC; calo energy (GeV); count", 200, 0, 2000) 
	hist_peakcaloE_FCAL1 = TH1I("peakcaloE_FCAL1", "peak calo energy for region FCAL1; calo energy (GeV); count", 200, 0, 2000) 
	hist_peakcaloE_TILE = TH1I("peakcaloE_TILE", "peak calo energy for region TILE; calo energy (GeV); count", 200, 0, 2000) 
	hist_peakcaloE_HEC = TH1I("peakcaloE_HEC", "peak calo energy for region HEC; calo energy (GeV); count", 200, 0, 2000) 
	hist_peakcaloE_FCAL2 = TH1I("peakcaloE_FCAL2", "peak calo energy for region FCAL2; calo energy (GeV); count", 200, 0, 2000) 
	hist_peakcaloE_FCAL3 = TH1I("peakcaloE_FCAL3", "peak calo energy for region FCAL3; calo energy (GeV); count", 200, 0, 2000) 

	mapfile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")
	mapfile.cd("SatHists")
	peakE_map_EM = gDirectory.Get("emHits_total").Clone()
	peakE_map_EM.Reset("ICES")
	peakE_map_EM.SetTitle("peak ADC energy in EM")
	peakE_map_EM.SetName("peakEmap_EM")
	peakE_map_HAD=peakE_map_EM.Clone()	  
	peakE_map_HAD.Reset("ICES") 
	peakE_map_HAD.SetTitle("peak ADC energy in HAD")
	peakE_map_HAD.SetName("peakEmap_HAD")

	peakcaloE_map_EM=peakE_map_EM.Clone()	  
	peakcaloE_map_EM.Reset("ICES") 
	peakcaloE_map_EM.SetTitle("peak calo energy in EM")
	peakcaloE_map_EM.SetName("peakcaloEmap_EM")
	peakcaloE_map_HAD=peakE_map_EM.Clone()	  
	peakcaloE_map_HAD.Reset("ICES") 
	peakcaloE_map_HAD.SetTitle("peak calo energy in HAD")
	peakcaloE_map_HAD.SetName("peakcaloEmap_HAD")  

#	hist_nNvsE_EMB = TH2D("nNvsE_EMB", "proportion of hits above half energy, vs energy for region EMB; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_EMEC = TH2D("nNvsE_EMEC", "proportion of hits above half energy, vs energy for region EMEC; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_FCAL1 = TH2D("nNvsE_FCAL1", "proportion of hits above half energy, vs energy for region FCAL1; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_TILE = TH2D("nNvsE_TILE", "proportion of hits above half energy, vs energy for region TILE; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_HEC = TH2D("nNvsE_HEC", "proportion of hits above half energy, vs energy for region HEC; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_FCAL2 = TH2D("nNvsE_FCAL2", "proportion of hits above half energy, vs energy for region FCAL2; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_FCAL3 = TH2D("nNvsE_FCAL3", "proportion of hits above half energy, vs energy for region FCAL3; energy (ADC)", 210, 0, 1050) 

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_EM.root")
	inputf.cd()
	
	keys = gDirectory.GetListOfKeys()

	for key in keys:
		nslice = int(key.GetName()[-1:])
		if not nslice ==7: continue
		findid = re.findall(r'\d+', key.GetName())
		histid = str(findid[0])

		Eta = -100
		with open('TTproperties.txt','r') as f:
		    next(f) # skip headings
		    reader=csv.reader(f,delimiter='\t')
		    for row in reader:
			#print "ID in loop is... ", row[0]
			if row[0] == histid:
				Eta= float(row[1])
				Phi = float(row[2])
				part = int(row[3])
				break				
		if Eta == -100:
			print "hist id "+str(histid)+" not found"
			continue

		plot = gDirectory.Get(key.GetName())
		
		maxADC = plot.GetHistogram().GetMaximum()
		maxCaloE = TMath.MaxElement(plot.GetN(),plot.GetX())

		etabin=peakE_map_EM.GetXaxis().FindBin(Eta)
		phibin=peakE_map_EM.GetYaxis().FindBin(Phi)

		if maxADC>=1022:
			print "saturated tower 0x%x" %int(histid), " in part ", part, " with energy ", maxCaloE, "GeV"

		if part == 0:
			hist_peakE_FCAL1.Fill(maxADC)
			hist_peakcaloE_FCAL1.Fill(maxCaloE)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_EM.SetBinContent(etabin, phibin, maxCaloE)
		if part == 1:
			hist_peakE_EMEC.Fill(maxADC)
			hist_peakcaloE_EMEC.Fill(maxCaloE)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_EM.SetBinContent(etabin, phibin, maxCaloE)	
		if part == 3:
			hist_peakE_EMB.Fill(maxADC)
			hist_peakcaloE_EMB.Fill(maxCaloE)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_EM.SetBinContent(etabin, phibin, maxCaloE)	
		if part == 4:
			hist_peakE_FCAL2.Fill(maxADC)
			hist_peakcaloE_FCAL2.Fill(maxCaloE)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)	
			peakcaloE_map_HAD.SetBinContent(etabin, phibin, maxCaloE)	
		if part == 5:
			hist_peakE_FCAL3.Fill(maxADC)
			hist_peakcaloE_FCAL3.Fill(maxCaloE)		
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_HAD.SetBinContent(etabin, phibin, maxCaloE)	
		if part == 6:
			hist_peakE_HEC.Fill(maxADC)
			hist_peakcaloE_HEC.Fill(maxCaloE)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_HAD.SetBinContent(etabin, phibin, maxCaloE)		
		if (part == 7 or part == 8):
			hist_peakE_TILE.Fill(maxADC)
			hist_peakcaloE_TILE.Fill(maxCaloE)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_HAD.SetBinContent(etabin, phibin, maxCaloE)		

	inputf.Close()

	inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_HAD.root")
	inputf2.cd()
	
	keys = gDirectory.GetListOfKeys()

	for key in keys:
		nslice = int(key.GetName()[-1:])
		if not nslice ==7: continue
		findid = re.findall(r'\d+', key.GetName())
		histid = str(findid[0])

		Eta = -100
		with open('TTproperties.txt','r') as f:
		    next(f) # skip headings
		    reader=csv.reader(f,delimiter='\t')
		    for row in reader:
			#print "ID in HAD loop is... ", row[0]
			if row[0] == histid:
				Eta= float(row[1])
				Phi = float(row[2])
				part = int(row[3])
				break
						
		if Eta == -100:
			print "hist id "+str(histid)+" not found"
			continue

		plot = gDirectory.Get(key.GetName())
		
		maxADC = plot.GetHistogram().GetMaximum()
		maxCaloE = TMath.MaxElement(plot.GetN(),plot.GetX())

		etabin=peakE_map_EM.GetXaxis().FindBin(Eta)
		phibin=peakE_map_EM.GetYaxis().FindBin(Phi)

		if maxADC>=1022:
			print "saturated tower 0x%x" %int(histid), " in part ", part, " with energy ", maxCaloE, "GeV"


		if part == 0:
			hist_peakE_FCAL1.Fill(maxADC)
			hist_peakcaloE_FCAL1.Fill(maxCaloE)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_EM.SetBinContent(etabin, phibin, maxCaloE)
		if part == 1:
			hist_peakE_EMEC.Fill(maxADC)
			hist_peakcaloE_EMEC.Fill(maxCaloE)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_EM.SetBinContent(etabin, phibin, maxCaloE)	
		if part == 3:
			hist_peakE_EMB.Fill(maxADC)
			hist_peakcaloE_EMB.Fill(maxCaloE)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_EM.SetBinContent(etabin, phibin, maxCaloE)	
		if part == 4:
			hist_peakE_FCAL2.Fill(maxADC)
			hist_peakcaloE_FCAL2.Fill(maxCaloE)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)	
			peakcaloE_map_HAD.SetBinContent(etabin, phibin, maxCaloE)	
		if part == 5:
			hist_peakE_FCAL3.Fill(maxADC)
			hist_peakcaloE_FCAL3.Fill(maxCaloE)		
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_HAD.SetBinContent(etabin, phibin, maxCaloE)	
		if part == 6:
			hist_peakE_HEC.Fill(maxADC)
			hist_peakcaloE_HEC.Fill(maxCaloE)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_HAD.SetBinContent(etabin, phibin, maxCaloE)		
		if (part == 7 or part == 8):
			hist_peakE_TILE.Fill(maxADC)
			hist_peakcaloE_TILE.Fill(maxCaloE)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)
			peakcaloE_map_HAD.SetBinContent(etabin, phibin, maxCaloE)		



	inputf2.Close()

	histlist=[]

	histlistcalo=[]

	histlist.append(hist_peakE_FCAL1)	
	histlist.append(hist_peakE_EMEC)	
	histlist.append(hist_peakE_EMB)	
	histlist.append(hist_peakE_FCAL2)	
	histlist.append(hist_peakE_FCAL3)	
	histlist.append(hist_peakE_HEC)	
	histlist.append(hist_peakE_TILE)

	histlistcalo.append(hist_peakcaloE_FCAL1)	
	histlistcalo.append(hist_peakcaloE_EMEC)	
	histlistcalo.append(hist_peakcaloE_EMB)	
	histlistcalo.append(hist_peakcaloE_FCAL2)	
	histlistcalo.append(hist_peakcaloE_FCAL3)	
	histlistcalo.append(hist_peakcaloE_HEC)	
	histlistcalo.append(hist_peakcaloE_TILE)		

	c = TCanvas('c', 'c', 1024, 800)
	c.Divide(4,2)
	for k in range(1,8):
		histlist[k-1].Rebin(7)
		histlist[k-1].SetLineWidth(3)
		if k>3:
			c.cd(k+1)
		else:
			c.cd(k)
		histlist[k-1].Draw()

	cC = TCanvas('cC', 'cC', 1024, 800)
	cC.Divide(4,2)
	for k in range(1,8):
		histlistcalo[k-1].Rebin(5)
		histlistcalo[k-1].SetLineWidth(3)
		if k>3:
			cC.cd(k+1)
		else:
			cC.cd(k)
		histlistcalo[k-1].Draw()


	raw_input("Press enter")

	c2 = TCanvas('c2', 'c2', 1024, 800)
	c2.Divide(2,1)
	c2.cd(1)
	peakE_map_EM.Draw()	
	c2.cd(2)
	peakE_map_HAD.Draw()
	
	raw_input("Press enter")

	c2C = TCanvas('c2C', 'c2C', 1024, 800)
	c2C.Divide(2,1)
	c2C.cd(1)
	peakcaloE_map_EM.Draw()	
	c2C.cd(2)
	peakcaloE_map_HAD.Draw()
	
	raw_input("Press enter")

	output = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/fittingMaps_EM.root", "UPDATE")
	output.cd()
	peakE_map_EM.Write()	
	peakcaloE_map_EM.Write()
	output.Close()	

	output2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/fittingMaps_HAD.root", "UPDATE")
	output2.cd()
	peakE_map_HAD.Write()
	peakcaloE_map_HAD.Write()
	output2.Close()		
	

	mapfile.Close()

if task == "overviewold":

	TTprops=open("TTproperties.txt", "r")

	#inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combinedCALJET.root")

	hist_peakE_EMB = TH1I("peakE_EMB", "peak energy for region EMB; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_EMEC = TH1I("peakE_EMEC", "peak energy for region EMEC; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_FCAL1 = TH1I("peakE_FCAL1", "peak energy for region FCAL1; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_TILE = TH1I("peakE_TILE", "peak energy for region TILE; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_HEC = TH1I("peakE_HEC", "peak energy for region HEC; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_FCAL2 = TH1I("peakE_FCAL2", "peak energy for region FCAL2; energy (ADC); count", 210, 0, 1050) 
	hist_peakE_FCAL3 = TH1I("peakE_FCAL3", "peak energy for region FCAL3; energy (ADC); count", 210, 0, 1050) 
	mapfile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combinedCALJET.root")
	mapfile.cd("SatHists")
	peakE_map_EM = gDirectory.Get("emHits_total")
	peakE_map_EM.Reset()
	peakE_map_EM.SetTitle("peak energy (ADC) in EM")
	peakE_map_HAD=peakE_map_EM.Clone()	  
	peakE_map_HAD.Reset() 
	peakE_map_HAD.SetTitle("peak energy (ADC) in HAD")  
#	hist_nNvsE_EMB = TH2D("nNvsE_EMB", "proportion of hits above half energy, vs energy for region EMB; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_EMEC = TH2D("nNvsE_EMEC", "proportion of hits above half energy, vs energy for region EMEC; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_FCAL1 = TH2D("nNvsE_FCAL1", "proportion of hits above half energy, vs energy for region FCAL1; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_TILE = TH2D("nNvsE_TILE", "proportion of hits above half energy, vs energy for region TILE; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_HEC = TH2D("nNvsE_HEC", "proportion of hits above half energy, vs energy for region HEC; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_FCAL2 = TH2D("nNvsE_FCAL2", "proportion of hits above half energy, vs energy for region FCAL2; energy (ADC)", 210, 0, 1050) 
#	hist_nNvsE_FCAL3 = TH2D("nNvsE_FCAL3", "proportion of hits above half energy, vs energy for region FCAL3; energy (ADC)", 210, 0, 1050) 

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_combinedEM.root")
	
	keys = gDirectory.GetListOfKeys()

	for key in keys:
		findid = re.findall(r'\d+', key.GetName())
		histid = int(findid[0])
		nslice = int(key.GetName()[-1:])
		if not nslice ==7: continue
		line = 0
		for line in TTprops:
			if line.split('\t')[0] == str(histid):
				props = line
				break
		if line == 0:
			print "hist id "+str(histid)+" not found"
			continue
		part = int(props[3])

		plot = gDirectory.Get(key.GetName())
		#ADClist = plot.GetYaxis.GetCenter()
		nby=plot.GetNbinsY()
		nbx=plot.GetNbinsX()
		maxADC=0
		for binx in range(0, nbx):
			for biny in range(0, nby):
				gbin=plot.GetBin(binx, biny)
				cont=plot.GetBinContent(gbin)
				if cont>0:
					center = plot.GetYaxis().GetBinCenter(biny)
					if center>maxADC:
						maxADC=center
		eta = float(props[1])
		phi = float(props[2])
		etabin=peakE_map_EM.GetXaxis().FindBin(eta)
		phibin=peakE_map_EM.GetYaxis().FindBin(eta)

		print "maxADC: ", maxADC		
		if part == 0:
			hist_peakE_FCAL1.Fill(maxADC)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)
		if part == 1:
			hist_peakE_EMEC.Fill(maxADC)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)	
		if part == 3:
			hist_peakE_EMB.Fill(maxADC)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)	
		if part == 4:
			hist_peakE_FCAL2.Fill(maxADC)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)	
		if part == 5:
			hist_peakE_FCAL3.Fill(maxADC)	
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)
		if part == 6:
			hist_peakE_HEC.Fill(maxADC)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)	
		if (part == 7 or part == 8):
			hist_peakE_TILE.Fill(maxADC)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)	

	inputf.Close()


	inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_combinedHAD.root")
	
	keys = gDirectory.GetListOfKeys()

	for key in keys:
		findid = re.findall(r'\d+', key.GetName())
		histid = int(findid[0])
		nslice = int(key.GetName()[-1:])
		if not nslice ==7: continue
		plot = gDirectory.Get(key.GetName())
		line = 0
		for line in TTprops:
			if line.split('\t')[0] == str(histid):
				props = line
				break
		if line == 0:
			print "hist id "+str(histid)+" not found"
			continue
		part = int(props[3])

		nby=plot.GetNbinsY()
		nbx=plot.GetNbinsX()
		maxADC=0
		for binx in range(0, nbx):
			for biny in range(0, nby):
				gbin=plot.GetBin(binx, biny)
				cont=plot.GetBinContent(gbin)
				if cont>0:
					center = plot.GetYaxis().GetBinCenter(biny)
					if center>maxADC:
						maxADC=center
		eta = float(props[1])
		phi = float(props[2])
		etabin=peakE_map_EM.GetXaxis().FindBin(eta)
		phibin=peakE_map_EM.GetYaxis().FindBin(eta)

		print "maxADC: ", maxADC		
		if part == 0:
			hist_peakE_FCAL1.Fill(maxADC)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)
		if part == 1:
			hist_peakE_EMEC.Fill(maxADC)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)	
		if part == 3:
			hist_peakE_EMB.Fill(maxADC)
			peakE_map_EM.SetBinContent(etabin, phibin, maxADC)	
		if part == 4:
			hist_peakE_FCAL2.Fill(maxADC)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)	
		if part == 5:
			hist_peakE_FCAL3.Fill(maxADC)	
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)
		if part == 6:
			hist_peakE_HEC.Fill(maxADC)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)	
		if (part == 7 or part == 8):
			hist_peakE_TILE.Fill(maxADC)
			peakE_map_HAD.SetBinContent(etabin, phibin, maxADC)	


	inputf2.Close()

	histlist=[]

	histlist.append(hist_peakE_FCAL1)	
	histlist.append(hist_peakE_EMEC)	
	histlist.append(hist_peakE_EMB)	
	histlist.append(hist_peakE_FCAL2)	
	histlist.append(hist_peakE_FCAL3)	
	histlist.append(hist_peakE_HEC)	
	histlist.append(hist_peakE_TILE)	

	c = TCanvas('c', 'c', 1024, 800)
	c.Divide(4,2)
	for k in range(1,7):
		if k>3:
			c.cd(k+1)
		else:
			c.cd(k)
		histlist[k-1].Draw()

	c2 = TCanvas('c2', 'c2', 1024, 800)
	c2.Divide(2,1)
	c2.cd(1)
	peakE_map_EM.Draw()	
	c2.cd(2)
	peakE_map_HAD.Draw()
	
	raw_input("Press enter")


if task == "plots":

	EMparts = ["FCALEM", "EMB", "EMEC"]
	HADparts = ["FCALHAD", "HEC", "TILE"]

	EMdict = {}
	HADdict = {}
	Combdict = {}

	plotlist=[]
 

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/TimingFitHistos_saturationstudies.root")
	inputf2 = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")	

	inputf.cd("HistsFit")
	keys = gDirectory.GetListOfKeys()


	satplotkeys = [key.GetName() for key in keys if key.GetName().startswith("ADC-ETslope")]

	for plotname in satplotkeys:
		inputf.cd("HistsFit")
		plot0 = gDirectory.Get(plotname)
		inputf2.cd("SatHists")
		plot2 = gDirectory.Get(plotname)
		if not (plot0.InheritsFrom('TH1') and plot2.InheritsFrom('TH1')) :
	    		raise Exception('plot does not inherit from TH1.')

		plot0.SetAxisRange(0, 2000, "X");
		plot=plot0.Clone()
		plot.GetXaxis().Set(500, 0, 2000)
		for binx in range (0,plot.GetNbinsX()):
			for biny in range (0,plot.GetNbinsY()):
				content = plot0.GetBinContent(binx, biny)
				plot.SetBinContent(binx, biny, content)
		

		#print "plot 1, x bin length: ",plot.GetXaxis().GetBinWidth(10)
		#print "plot 1, x bins: ",plot.GetNbinsX()
		#print "plot 1, y bins: ",plot.GetNbinsY()
		#print "plot 2, x bin length: ",plot.GetXaxis().GetBinWidth(10)
		#print "plot 2, x bins: ",plot2.GetNbinsX()
		#print "plot 2, y bins: ",plot2.GetNbinsY()


		plot.Add(plot2)

		p=re.compile('_(.*)_')
		s=p.search(plotname)
		nslice = s.group(1)
		#nslice = plotname[12:12+5]
		#if '_' in nslice:
		#	nslice=nslice[:-2]

		p=re.compile('_n.*_(.*?)hist$')
		s=p.search(plotname)
		if s:
			part = s.group(1)
		else:
    			raise Exception('name not found.')

		plot.GetXaxis().SetRangeUser(0, 1000)
		plot.SetStats(0)
		#plot.SetOption("colz")
		plot.SetMarkerColor(93)
		plot.SetMarkerStyle(20)
		plot.SetMarkerSize(0.3)
		plot.GetXaxis().SetTitle("E_T [GeV]")
		plotlist.append(plot)

	c= TCanvas("c", "c", 1280,680 )
	c.Divide(5,3)
	d = 0;

	combined = gDirectory.Get( satplotkeys[0])
	
	part_prev="FCALEM"

	text = TLatex()

	for plot in plotlist:
		plotname = plot.GetName()

		p=re.compile('_(.*)_')
		s=p.search(plotname)
		nslice = s.group(1)
		#nslice = plotname[12:12+5]
		#if '_' in nslice:
		#	nslice=nslice[:-2]

		p=re.compile('_n.*_(.*?)hist$')
		s=p.search(plotname)
		if s:
			part = s.group(1)
		else:
    			raise Exception('name not found.')


		print "current plotname is ", plotname

		print "Looking at part ", part

		if part in EMparts:
			if nslice in EMdict:
				EMdict[nslice].Add(plot)
				#print EMdict[nslice]
			else:
				EMdict[nslice] = plot
		elif part in HADparts:
			if nslice in HADdict:
				HADdict[nslice].Add(plot)
			else:
				HADdict[nslice] = plot
		else:
	    		raise Exception('part not found.')

		if nslice in Combdict:
				Combdict[nslice].Add(plot)
		else:
			Combdict[nslice] = plot		
				

#		if not part == part_prev:
#			print "now printing next slide.."
#			c.Modified()
#			sleep(15)
#			c.Update()
#			c.Print("ADC_ETslopes_part"+part_prev+".jpg")
#			d=0
#			c= TCanvas("c"+part, "c"+part, 1280,680 )
#			c.Divide(5,3)


		#if not plot.GetName() = combined.GetName():
		#	combined.add(plot)

		d+=1

		c= TCanvas("c", "c", 1024,680 )

		#c.cd(d)
		c.cd()

		print "plot # of entries: ", plot.GetEntries()
		
		plot.SetMarkerSize(2)
		plot.SetMarkerSize(1)
		plot.Draw()
		text.DrawLatex(550, 400, part)
		text.DrawLatex(0.6, 0.3,"slice "+nslice)
		#raw_input("press eneter")		
		c.Print("satstudies/"+plotname+".jpg")

		part_prev = part

#	print "now printing next slide.."
#	c.Print("ADC_ETslopes_part"+part_prev+".png")

	raw_input("press ENTER to continue... ")

	
	c= TCanvas("cEM", "cEM", 1280,680 )
	c.Divide(5,3)
	d=0

	for emkeys in EMdict:
		d+=1
		c.cd(d)
		EMdict[emkeys].Draw()
		text.DrawLatex(550, 400, "all EM")
	c.Print("ADC_ETslopes_EM.png")

	c= TCanvas("cHAD", "cHAD", 1280,680 )
	c.Divide(5,3)
	d=0

	for hadkeys in HADdict:
		d+=1
		c.cd(d)
		HADdict[hadkeys].Draw()
		text.DrawLatex(550, 400, "all HAD")
	c.Print("ADC_ETslopes_HAD.png")

	c= TCanvas("cAll", "cAll", 1280,680 )
	c.Divide(5,3)
	d=0

	for allkeys in Combdict:
		d+=1
		c.cd(d)
		Combdict[allkeys].Draw()
		text.DrawLatex(550, 400, "EM + HAD")
	c.Print("ADC_ETslopes_ALL.png")
			


	raw_input("press ENTER to continue... ")
		


if task == "stats":

	run = str(raw_input("run no.: "))

	events_file = open('LBEvents_table_run'+run+'.txt', 'r')

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/"+run+"/TimingFitHistos_combined"+run+".root")

	inputf.cd("HistsFit")

	SatvsLB = gDirectory.Get("SaturatedVsLB")

	totalSatHits = int(SatvsLB.GetEntries())

	HitsvsLB = gDirectory.Get("hitsVsLB")

	emSat = gDirectory.Get("emSaturatedHits")
	hadSat = gDirectory.Get("hadSaturatedHits")

	c1=TCanvas('c1', 'c1', 1280, 1000)
	c1.Divide(2,1)
	c1.cd(1)
	emSat.Draw()
	c1.cd(2)
	hadSat.Draw()
	c1.WaitPrimitive()
	c1.Print("saturationstudies_maps"+run+".png")

	emSliceno = gDirectory.Get("emSaturatednoSlices")
	hadSliceno = gDirectory.Get("hadSaturatednoSlices")

	c1=TCanvas('c1', 'c1', 1280, 680)
	c1.Divide(1,2)
	c1.cd(1)
	emSliceno.Draw()
	c1.cd(2)
	hadSliceno.Draw()
	c1.WaitPrimitive()
	c1.Print("saturationstudies_satslices_distr_run"+run+".png")


	emSatHits = int(emSat.GetEntries())
	hadSatHits = int(hadSat.GetEntries())

	nbins = SatvsLB.GetNbinsX()

	satvslb = []
	hitsvslb = []

	lbevents_dictionary = {}

	for line in events_file:
		lbevents = line.split()
		lbevents_dictionary[int(lbevents[0])] = int(lbevents[1])

	print '*********************************************************************************************************************'
	print '********************************************* Stats per LumiBlock ***************************************************'
	print '*********************************************************************************************************************'
	print 'LB \t\t no. of events \t\t no. of sat hits \t\t total hits \t\t\t % \n'

	LBcount = 0
	totalevents = 0

	for bin in range(nbins):
		 LB = SatvsLB.GetBinLowEdge(bin)
		 sat = SatvsLB.GetBinContent(bin)
		 hits = HitsvsLB.GetBinContent(bin)
		 if hits == 0: continue
		 LBcount+=1
		 events = (2./3.)*lbevents_dictionary[int(LB)]
		 totalevents += events
		 frac = (sat/(sat+hits))*100.
		 print '%i \t\t %i \t\t %i \t\t %i \t\t\t %2.2f%%' %(LB, events, sat, hits+sat, frac)

	print '*********************************************************************************************************************\n'

	print '\n'

	nbins2 = emSliceno.GetNbinsX()

	emTotal = int(emSliceno.GetEntries())
	hadTotal = int(hadSliceno.GetEntries())

	print '*********************************************************************************************************************'
	print '*************************************** Stats for number of saturated slices ****************************************'
	print '*********************************************************************************************************************'

	print 'no. of slices \t\t no of hits in EM \t\t % in EM \t\t no of hits in HAD \t\t % in HAD \n'


	for bin in xrange(1, nbins2+1):
		sliceno = emSliceno.GetBinCenter(bin)
		emY = emSliceno.GetBinContent(bin)
		hadY = hadSliceno.GetBinContent(bin)
		fracEM = 100.*(float(emY)/float(emTotal))
		fracHAD = 100.*(float(hadY)/float(hadTotal))
		print '%i \t\t %i \t\t %2.2f%% \t\t %i \t\t %2.2f%%' %(sliceno, emY, fracEM, hadY, fracHAD)

	print '*********************************************************************************************************************\n'

	HitsvsLB.Add(SatvsLB)

	totalHits = int(HitsvsLB.GetEntries())

	print "----------------- overall stats -----------------"
	print "total number of lumiblocks: "+str(LBcount)
	print "total number of events: "+str(totalevents)
	print "total number of hits: "+str(totalHits)
	print "total number of saturated hits: "+str(totalSatHits)
	print "total number of sat. in EM: "+str(emTotal)
	print "total number of sat. in HAD: "+str(hadTotal)
	print "avg hits per event: %0.4f" %((float(totalHits)/float(totalevents)))
	print "avg sat hits per event: %0.4f" %((float(totalSatHits)/float(totalevents)))
	print "avg sat hits per 1000 events: %0.4f" %((float(totalSatHits)/float(totalevents/1000)))
	print "avg sat hits per 10000 events: %0.4f" %((float(totalSatHits)/float(totalevents/10000)))
	print "----------------- percentages from total number of hits -----------------"
	print "percentage of saturated hits: %0.4f%%" %(100.*(float(totalSatHits)/float(totalHits)))
	print "percentage of saturated hits in EM: %0.4f%%" %(100.*(float(emTotal)/float(totalHits)))
	print "percentage of saturated hits in HAD: %0.4f%%" %(100.*(float(hadTotal)/float(totalHits)))


	#SatvsLB.Divide(HitsvsLB)
	#SatvsLB.Draw()
	#raw_input("Press Enter to continue...")

	events_file.close()

if task=="hits":

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")
	
	inputf.cd("SatHists")

	hitsEM = gDirectory.Get("emHits_total")
	hitsHAD = gDirectory.Get("hadHits_total")

	hitsEM.SetTitle("no. of good hits (EM)")
	hitsHAD.SetTitle("no. of good hits (HAD)")
	hitsEM.SetStats(0)
	hitsHAD.SetStats(0)

	c = TCanvas('c', 'c', 1024, 800)
	c.Divide(2,1)
	c.cd(1)
	hitsEM.Draw()
	c.cd(2)
	hitsHAD.Draw()

	raw_input("press enter")


if task=="fitbyeta":

	layer = str(raw_input("layer (EM/HAD): "))

	SlopeByEta = TH1D("SlopeByEta", "slope by eta bin; eta bin; slope", 66, 0.5, 66.5)
	NDFByEta = TH1I("NDFByEta", "NDF by eta bin; eta bin; ndf", 66, 0.5, 66.5)
	ErrByEta = TH1D("ErrByEta", "error of slope by eta bin; eta bin; chi2", 66, 0.5, 66.5)
	ThreshByEta = TH1D("ThreshByEta", "energy threshold by eta bin; eta bin; calo E [GeV]", 66, 0.5, 66.5)
	Chi2ByEta = TH1D("Chi2ByEta", "Chi2 by eta bin; eta bin; chi2", 66, 0.5, 66.5)

	ThreshByEta.SetMinimum(-10)

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/EvsADC_byEtaBin_"+layer+".root")

	c = TCanvas('c', 'c', 1024, 800)
		
	count=0

	for bin in range(1,67):
		count+=1
		inputf.cd("etabin"+str(bin))
		graph = gDirectory.Get("slice_nm2")

		r = graph.Fit("pol1", "S")
		slope = r.Value(1)
		intercept = r.Value(0)		
		chi2 = r.Chi2()
		error = r.ParError(1)
		ndf= r.Ndf()

		print "y-intercept is..", intercept

		if not slope ==0:
			threshold = (1025.-intercept)/slope
			ThreshByEta.SetBinContent(bin, threshold)

		SlopeByEta.SetBinContent(bin, slope)
		NDFByEta.SetBinContent(bin, ndf)
		ErrByEta.SetBinContent(bin, error)
		Chi2ByEta.SetBinContent(bin, chi2)

		
		if count%2==0:
			c.cd()
			frame = c.DrawFrame(0, 0, 1600, 1050)
			graph.Draw("P")
			c.Update()

	c.cd()
	SlopeByEta.Draw()
	c.Update()
	raw_input("Press Enter.")
	ErrByEta.Draw()	
	c.Update()
	raw_input("Press Enter.")
	NDFByEta.Draw()	
	c.Update()
	raw_input("Press Enter.")
	ThreshByEta.Draw()	
	c.Update()
	raw_input("Press Enter.")
	Chi2ByEta.Draw()	
	c.Update()
	raw_input("Press Enter.")

	output = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/fittingPlots_byetabin_"+layer+".root", "RECREATE")
	output.cd()
	SlopeByEta.Write()
	ErrByEta.Write()
	ThreshByEta.Write()	
	NDFByEta.Write()
	Chi2ByEta.Write()
		
		#raw_input("Press Enter.")
	

if task=="fitbyTT":

	layer = str(raw_input("layer (EM/HAD): "))

	inputf = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatSlopeHists_"+layer+".root")

	mapfile = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/SatStudyPlots_combined.root")
	mapfile.cd("SatHists")
	slopeMap = gDirectory.Get("emHits_total").Clone()
	slopeMap.Reset("ICES")
	slopeMap.SetName("slopeMap_"+layer)
	slopeMap.SetTitle("slopeMap "+layer)

	chi2Map = slopeMap.Clone()
	chi2Map.SetName("chi2Map_"+layer)
	chi2Map.SetTitle("chi2Map "+layer)

	errorMap = slopeMap.Clone()
	errorMap.SetName("errorMap_"+layer)
	errorMap.SetTitle("errorMap "+layer)

	threshMap = slopeMap.Clone()
	threshMap.SetName("EthreshMap_"+layer)
	threshMap.SetTitle("EthreshMap "+layer)

	ndfMap = slopeMap.Clone()
	ndfMap.SetName("ndfMap_"+layer)
	ndfMap.SetTitle("ndfMap "+layer)

	threshMap.SetMinimum(-10)

	c = TCanvas('c', 'c', 1024, 800)

	inputf.cd()

	keys = gDirectory.GetListOfKeys()

	count=0


	for key in keys:
		count+=1
		findid = re.findall(r'\d+', key.GetName())
		histid = str(findid[0])
		nslice = int(findid[1])
	
		if (not nslice == 3): continue
		if (not int(histid) in [int('0x00110302', 0), int('0x00110500', 0),int('0x00110501', 0),int('0x00110603', 0)]): continue

		Eta = -100
		with open('TTproperties.txt','r') as f:
		    next(f) # skip headings
		    reader=csv.reader(f,delimiter='\t')
		    for row in reader:
			#print "ID in loop is... ", row[0]
			if row[0] == histid:
				Eta= float(row[1])
				Phi = float(row[2])
				part = int(row[3])
				break				
		if Eta == -100:
			print "hist id "+str(histid)+" not found"
			continue

		etabin=slopeMap.GetXaxis().FindBin(Eta)
		phibin=slopeMap.GetYaxis().FindBin(Phi)

		graph = gDirectory.Get(key.GetName())

		graph.Fit("pol1", "S+")
		r = graph.Fit("pol2", "S+")

		f1= graph.GetListOfFunctions().FindObject("pol1");
		f1.SetLineColor(kGreen+2)
		f1.SetLineWidth(3)
		f2 = graph.GetListOfFunctions().FindObject("pol2");
		f2.SetLineColor(kRed+2)
		f2.SetLineWidth(3)
		slope = r.Value(1)
		intercept = r.Value(0)		
		chi2 = r.Chi2()
		error = r.ParError(1)
		ndf= r.Ndf()

		text=TLatex()
		text.SetNDC()
		gStyle.SetOptFit(0)
		graph.SetMarkerColor(kAzure-2)
		graph.SetMarkerStyle(8)
		graph.SetMarkerSize(1)
		graph.SetTitle("")
		graph.GetXaxis().SetTitle("E_{T}^{calo} [GeV]")
		graph.GetYaxis().SetTitle("ADC count")
		graph.GetXaxis().SetLabelSize(0.045)
		graph.GetYaxis().SetLabelSize(0.045)
		graph.GetXaxis().SetTitleSize(0.045)
		graph.GetYaxis().SetTitleSize(0.045)


		graph.Draw("AP")
		text.DrawLatex(0.55, 0.37, "TT id 0x00"+hex(int(histid))[2:])
		text.DrawLatex(0.6, 0.3,"slice n-2")
		raw_input("press enter..")
		c.Print("quadfittedslope_TTid0x00"+hex(int(histid))[2:]+"_slice"+str(nslice)+".png")

		slopeMap.SetBinContent(etabin, phibin, slope)
		chi2Map.SetBinContent(etabin, phibin, chi2)
		errorMap.SetBinContent(etabin, phibin, error)
		ndfMap.SetBinContent(etabin, phibin, ndf)

		if not slope ==0:
			threshold = (1025.-intercept)/slope
			threshMap.SetBinContent(etabin, phibin, threshold)
		
		
		if count%50==0:
			c.cd()
			frame = c.DrawFrame(0, 0, 1600, 1050)
			graph.Draw("P")
			c.Update()
	c.cd()
	slopeMap.Draw()
	c.Update()
	raw_input("Press Enter.")
	errorMap.Draw()	
	c.Update()
	raw_input("Press Enter.")
	ndfMap.Draw()	
	c.Update()
	raw_input("Press Enter.")
	threshMap.Draw()	
	c.Update()
	raw_input("Press Enter.")
	chi2Map.Draw()	
	c.Update()
	raw_input("Press Enter.")

#	output = TFile("/afs/cern.ch/work/c/cantel/private/collisions/SaturationInvestigationHists/fittingMaps_"+layer+".root", "RECREATE")
#	output.cd()
#	slopeMap.Write()
#	errorMap.Write()
#	threshMap.Write()
#	ndfMap.Write()
#	chi2Map.Write()



