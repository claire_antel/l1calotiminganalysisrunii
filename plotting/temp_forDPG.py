# -*- coding: iso-8859-1 -*-
from ROOT import *

gROOT.LoadMacro("plot_style.C")

set_plot_style() 

f1= TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/OverviewHistos_newcf.root")
f1.cd("HistsFit")
newcf = gDirectory.Get("pulsefit_run278880_event724240014_id0x05100202_part0")

f2= TFile("/afs/cern.ch/work/c/cantel/private/collisions/BIBcleanup/OverviewHistos_oldcf.root")
f2.cd("HistsFit")
oldcf = gDirectory.Get("pulsefit_run278880_event724240014_id0x05100202_part0")

newfit = newcf.GetFunction("fit")

oldcf.GetXaxis().SetRangeUser(125, 250)

oldcf.SetStats(0)
oldcf.Draw()
newfit.Draw("SAME")

raw_input("pressssssss enter")
x