#include "TTxml.h"
#include <TH1.h>
#include <TH2.h>
#include "TFile.h"
#include <stdio.h>

void writeCorrFactors2xml(const char * inputstring) {

	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;
	std::map<unsigned int,double> NFits;
	std::map<unsigned int,double> cfSigmal, cfSigmar;

	TTxml m_props;
	m_props.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");

	Eta = m_props.Get("eta");
	Phi = m_props.Get("phi");
	Part = m_props.Get("part");

	if (!(Eta.size()==7168)){
		std::cerr<<"size of TT parameter maps of wrong size. Should be 7168 but is "<<Eta.size()<<"."<<std::endl;
	}

/*	TH2* EMhist_Sigl;
	TH2* EMhist_Sigr;
	TH2* HADhist_Sigl;
	TH2* HADhist_Sigr; */

	TH1* EMhist_Sigl;
	TH1* EMhist_Sigr;
	TH1* HADhist_Sigl;
	TH1* HADhist_Sigr;

	std::cout<<"creating input file"<<std::endl;
	
	TFile* inputmaps = new TFile(inputstring);
	inputmaps->cd();

	std::cout<<"opened input file..."<<std::endl;

	inputmaps->GetListOfKeys()->Print();
	EMhist_Sigl=(TH2*)gDirectory->Get("hist_siglEM_corr"); 
	EMhist_Sigr=(TH2*)gDirectory->Get("hist_sigrEM_corr");  
	HADhist_Sigl=(TH2*)gDirectory->Get("hist_siglHAD_corr");  
	HADhist_Sigr=(TH2*)gDirectory->Get("hist_sigrHAD_corr");
	
	std::cout<<"read in all histograms. Now filling sigma maps..."<<std::endl;

	std::map<unsigned int,double>::iterator it;
	for ( it = Eta.begin(); it != Eta.end(); it++ ){
			unsigned int id = it->first;
			double eta = it->second;
			double phi = Phi[id];
			int part = Part[id];
			if (part<=3){
				int etabin = EMhist_Sigl->GetXaxis()->FindBin(eta);
				int phibin = EMhist_Sigl->GetYaxis()->FindBin(phi);				
				cfSigmal[id]=EMhist_Sigl->GetBinContent(etabin, phibin);
				cfSigmar[id]=EMhist_Sigr->GetBinContent(etabin, phibin);
			}
			else if (part>3 && part<=8){
				int etabin = HADhist_Sigl->GetXaxis()->FindBin(eta);
				int phibin = HADhist_Sigl->GetYaxis()->FindBin(phi);				
				cfSigmal[id]=HADhist_Sigl->GetBinContent(etabin, phibin);
				cfSigmar[id]=HADhist_Sigr->GetBinContent(etabin, phibin);
			}
			else std::cerr<<"Part "<<part<<" does not exist!"<<std::endl;
	}

	if (!((cfSigmal.size()==7168) && (cfSigmar.size()==7168))){
		std::cerr<<"size of correction factor maps of wrong size. Should be 7168 but is "<<cfSigmal.size()<<" and "<<cfSigmar.size()<<"."<<std::endl;
	}

	std::cout<<"Retrieval of sigma values completed. Now writing to xml... "<<std::endl;
	
	TTxml sigmavals;
	sigmavals.Add("eta", Eta);
	sigmavals.Add("phi", Phi);
	sigmavals.Add("part", Part);
	sigmavals.Add("cf_sigmal", cfSigmal);
	sigmavals.Add("cf_sigmar", cfSigmar);
	sigmavals.Write("/afs/cern.ch/work/c/cantel/private/output/sigmacorrectionfactors.xml");
}


void writeRunICorrFactors2xml() {

	std::map<unsigned int,double> Part;
	std::map<unsigned int,double> Eta, Phi;
	std::map<unsigned int,double> TDiff, Trms;
	std::map<unsigned int,double> NFits;
	std::map<unsigned int,double> cfSigmal, cfSigmar;

	TTxml m_props;
	m_props.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");

	Eta = m_props.Get("eta");
	Phi = m_props.Get("phi");
	Part = m_props.Get("part");

	if (!(Eta.size()==7168)){
		std::cerr<<"size of TT parameter maps of wrong size. Should be 7168 but is "<<Eta.size()<<"."<<std::endl;
	}


	std::map<unsigned int,double>::iterator it;
	for ( it = Part.begin(); it != Part.end(); it++ ){
			unsigned int id = it->first;
			int part = it->second;
			if (part==1){ //emec
				double eta = Eta[id];
				if ( fabs(eta)>1.5 && fabs(eta)<2.2 ){ //finer granularity
					cfSigmal[id]=0.95;
					cfSigmar[id]=0.95;
				}
				else if ( fabs(eta)>2.2 && fabs(eta)<2.4 ){ //finer granularity
					cfSigmal[id]=0.9;
					cfSigmar[id]=0.9;
				}
				else if ( fabs(eta)>2.4 && fabs(eta)<2.5 ){ //finer granularity
					cfSigmal[id]=0.85;
					cfSigmar[id]=0.85;
				}
				else { //coarser granularity
					cfSigmal[id]==1.0;
					cfSigmar[id]=1.0;
				}
			}
			else if (part == 2){ //overlap
				cfSigmal[id]=0.85;
				cfSigmar[id]=0.85;
			}
			else if (part == 0){ //FCal1
				cfSigmal[id]=1.05;
				cfSigmar[id]=1.05;
			}
			else if (part == 4){ //FCal2
				cfSigmal[id]=1.2;
				cfSigmar[id]=1.2;
			}
			else if (part == 5){ //FCal3
				cfSigmal[id]=1.3;
				cfSigmar[id]=1.3;
			}
			else {
				cfSigmal[id]=1.0;
				cfSigmar[id]=1.0;				
			}
	}

	std::cout<<"Retrieval of sigma values completed. Now writing to xml... "<<std::endl;

	TTxml sigmavals;
	sigmavals.Add("eta", Eta);
	sigmavals.Add("phi", Phi);
	sigmavals.Add("part", Part);
	sigmavals.Add("cf_sigmal", cfSigmal);
	sigmavals.Add("cf_sigmar", cfSigmar);
	sigmavals.Write("/afs/cern.ch/work/c/cantel/private/output/RunIsigmacorrectionfactors.xml");

}

