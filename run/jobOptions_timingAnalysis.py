# -*- coding: utf-8 -*-
# set:
#	number of events
#	run number
# 	running fitting on 40 MHz or 80MHz pulses: is80MHz? (see list of 80 MHz physics runs at https://twiki.cern.ch/twiki/bin/view/Atlas/L1CaloCommissioningChanges)
#	running fitting with fixed parameters: fixedpars? (false if deriving correction factors and/or wish to be independent of parameter choice.)
#	NOTE: fixedpars can only be FALSE if is80MHz is TRUE.
#	outputlevel.


import AthenaPoolCnvSvc.ReadAthenaPool
from TriggerJobOpts.TriggerFlags import TriggerFlags
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

#from RecExConfig.RecFlags  import rec
#rec.readRDO = False #readRDO is the True one by default. Turn it off before turning others on
#rec.readAOD = True #example for reading AOD

from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource = 'data'

m_fixedpars = True  # always true if not 80 MHz analysis.

#*****************************
# USER OPTIONS FOR TIMING ANALYSIS
#*****************************
theApp.EvtMax = 10
run_no = "test"
m_is80MHz = False
if m_is80MHz:
	m_fixedpars = True #user input
m_TTpropertyfile =  "/afs/cern.ch/work/c/cantel/public/l1caloTimingAnalysiSxmls/TTparameters_filledblanks.xml" # make sure this is a valid directory!
m_sigmacffile = "/afs/cern.ch/work/c/cantel/public/l1caloTimingAnalysiSxmls/sigmacorrectionfactors.xml" # make sure this is a valid directory!
m_outputlevel = DEBUG # or INFO
#*****************************

# load files
from glob import glob

import os
if os.getenv('PBS_ATH_INPUT'):
 files = os.getenv('PBS_ATH_INPUT').split(' ')
else:
 files = [
'/afs/cern.ch/work/c/cantel/public/samplefiles/data16_13TeV.00309390.physics_Main.merge.DAOD_L1CALO1.f750_m1693_c1007_m1652._0001.1'
#'root://ccxrdatlas.in2p3.fr:1094//atlas/rucio/data16_13TeV:data16_13TeV.00305380.physics_Main.merge.DESDM_CALJET.f727_m1650._0002.1',
#'root://ccxrdatlas.in2p3.fr:1094//atlas/rucio/data16_13TeV:data16_13TeV.00305380.physics_Main.merge.DESDM_CALJET.f727_m1650._0003.1',
#'root://ccxrdatlas.in2p3.fr:1094//atlas/rucio/data16_13TeV:data16_13TeV.00305380.physics_Main.merge.DESDM_CALJET.f727_m1650._0004.1'
#'root://eosatlas.cern.ch//eos/atlas/atlastier0/rucio/data16_13TeV/physics_Main/00298687/data16_13TeV.00298687.physics_Main.merge.DESDM_PHOJET.f697_m1592/data16_13TeV.00298687.physics_Main.merge.DESDM_PHOJET.f697_m1592._0001.1',       
#'root://eosatlas.cern.ch//eos/atlas/atlastier0/rucio/data16_13TeV/physics_Main/00298687/data16_13TeV.00298687.physics_Main.merge.DESDM_PHOJET.f697_m1592/data16_13TeV.00298687.physics_Main.merge.DESDM_PHOJET.f697_m1592._0002.1'       
#'root://eosatlas.cern.ch//eos/atlas/atlastier0/rucio/data16_13TeV/express_express/00298687/data16_13TeV.00298687.express_express.recon.ESD.x423/data16_13TeV.00298687.express_express.recon.ESD.x423._lb0249._SFO-ALL._0001.1',
#'root://eosatlas.cern.ch//eos/atlas/atlastier0/rucio/data16_13TeV/express_express/00298687/data16_13TeV.00298687.express_express.recon.ESD.x423/data16_13TeV.00298687.express_express.recon.ESD.x423._lb0250._SFO-ALL._0001.1'
#'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00276952.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0114.1',
#'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00276952.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0115.1',
#'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00276952.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0116.1'
]
#'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._1089.1',
#'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._1090.1'
#]

svcMgr.EventSelector.InputCollections = files 

TriggerFlags.configurationSourceList=['ds']
from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
cfg = TriggerConfigGetter()

# load bunchcrossing tool - brief excursion into investigations of timing dependency on bunch position, use of tool now commneted out in code. 
#from TrigBunchCrossingTool.BunchCrossingTool import BunchCrossingTool
#theBCTool = BunchCrossingTool()
#ToolSvc += theBCTool

useCaloCells   = False
rootStreamName1 = "OverviewHistoStream"
rootFileName1   = "OverviewHistos_run"+run_no+".root" 
Rootdirname = "/HistsFit" #"/HistsFit_%d" % GetRunNumber()

rootStreamName2 = "TowerTimingHistoStream"
rootFileName2   = "TowerTimingDistr_run"+run_no+".root" 

rootStreamName3 = "TowerInfoHistoStream"
rootFileName3   = "TowerInfo_run"+run_no+".root" 

#rootStreamName4 = "TowerPulsetwosideHistoStream"
#rootFileName4   = "BGflaggedPulses_run"+run_no+".root"

#rootStreamName8 = "TowerPulselarburstHistoStream"
#rootFileName8   = "LArBurstflaggedPulses_run"+run_no+".root"

#rootStreamName5 = "BGflaggedTowerTimingHistoStream"
#rootFileName5   = "BGflaggedTimingPropagation_run"+run_no+".root" 

#rootStreamName7 = "LArBurstflaggedTowerTimingHistoStream"
#rootFileName7   = "LArBurstflaggedTimingPropagation_run"+run_no+".root"

if m_is80MHz: 
	rootStreamName6 = "FixedParametersHistoStream"
	rootFileName6   = "ParameterFitvalues_run"+run_no+".root" 

#add GRL tool - tool not currently used in code.
#ToolSvc += CfgMgr.GoodRunsListSelectionTool("MyGRLTool",GoodRunsListVec=["data15_13TeV.periodAllYear_DetStatus-v63-pro18-01_DQDefects-00-01-02_PHYS_StandardGRL_All_Good.xml"])


# add the actual algorithms to the topSequence
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
if m_is80MHz:
  topSequence += CfgMgr.TimeFitting80MHz("timefitting", OutputLevel=m_outputlevel, RootStreamName=rootStreamName1, RootDirName=Rootdirname, fixedpars=m_fixedpars, TTpropertyfile = m_TTpropertyfile , sigmacffile = m_sigmacffile)
else:
  topSequence += CfgMgr.TimeFitting("timefitting", OutputLevel=m_outputlevel, RootStreamName=rootStreamName1, RootDirName=Rootdirname, TTpropertyfile = m_TTpropertyfile , sigmacffile = m_sigmacffile)

# output streams for histograms
svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName1, rootFileName1)]
svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName2, rootFileName2)]
svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName3, rootFileName3)]
if m_is80MHz: 
	svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName6, rootFileName6)]
#svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName7, rootFileName7)]
#svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName8, rootFileName8)]

