# -*- coding: iso-8859-1 -*-
# options:
# 	80MHz bit
#	fix parameters


theApp.EvtMax = 1000
run_no = "278748"

import AthenaPoolCnvSvc.ReadAthenaPool
from TriggerJobOpts.TriggerFlags import TriggerFlags
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

#from RecExConfig.RecFlags  import rec
#rec.readRDO = False #readRDO is the True one by default. Turn it off before turning others on
#rec.readAOD = True #example for reading AOD

from AthenaCommon.GlobalFlags import globalflags
globalflags.DataSource = 'data'


# load files
from glob import glob

import os
if os.getenv('PBS_ATH_INPUT'):
 files = os.getenv('PBS_ATH_INPUT').split(' ')
else:
 files = [
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0057.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0058.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0059.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0060.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0061.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0062.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0063.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0064.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0065.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0066.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0067.1',
'root://atlas-xrd-eos-rucio.cern.ch:1094//atlas/rucio/data15_13TeV:data15_13TeV.00278748.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._0068.1'
]

  #files = glob('/afs/cern.ch/work/c/cantel/private/collisions/data15_13TeV.00278880.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516/data15_13TeV.00278880.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._1047.3')
   
svcMgr.EventSelector.InputCollections = files 

#svcMgr.EventSelector.InputCollections = [
#'root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/det-l1calo/Tier0/perm/data15_13TeV/physics_Main/00281143/data15_13TeV.00281143.physics_Main.merge.DAOD_L1CALO1_CALJET.f629_m1508_c906_m1502/data15_13TeV.00281143.physics_Main.merge.DAOD_L1CALO1_CALJET.f629_m1508_c906_m1502._0001.1'
#'root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/det-l1calo/Tier0/perm/data15_13TeV/physics_Main/00284285/data15_13TeV.00284285.physics_Main.merge.DAOD_L1CALO1_CALJET.f643_m1522_c919_m1516/data15_13TeV.00284285.physics_Main.merge.DAOD_L1CALO1_CALJET.f643_m1522_c919_m1516._0001.1'
#'/afs/cern.ch/work/c/cantel/private/collisions/data15_13TeV.00278880.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516/data15_13TeV.00278880.physics_Main.merge.DAOD_L1CALO2.f645_c918_m1516._1047.3'
#]

TriggerFlags.configurationSourceList=['ds']
from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
cfg = TriggerConfigGetter()

# load bunchcrossing tool
from TrigBunchCrossingTool.BunchCrossingTool import BunchCrossingTool
theBCTool = BunchCrossingTool()
ToolSvc += theBCTool

#options for Timing algorithm
m_is80MHz = True
m_fixedpars = False
m_TTpropertyfile =  "TTparameters_filledblanks.xml"
m_sigmacffile = "sigmacorrectionfactors.xml"

useCaloCells   = False
rootStreamName1 = "OverviewHistoStream"
rootFileName1   = "TimingFitHistos_run"+run_no+".root" 
Rootdirname = "/HistsFit" #"/HistsFit_%d" % GetRunNumber()

rootStreamName2 = "TowerTimingHistoStream"
rootFileName2   = "TimingPropagation_run"+run_no+".root" 

rootStreamName3 = "TowerInfoHistoStream"
rootFileName3   = "IDPropagation_run"+run_no+".root" 

#rootStreamName4 = "TowerPulsetwosideHistoStream"
#rootFileName4   = "BGflaggedPulses_run"+run_no+".root"

#rootStreamName8 = "TowerPulselarburstHistoStream"
#rootFileName8   = "LArBurstflaggedPulses_run"+run_no+".root"

#rootStreamName5 = "BGflaggedTowerTimingHistoStream"
#rootFileName5   = "BGflaggedTimingPropagation_run"+run_no+".root" 

#rootStreamName7 = "LArBurstflaggedTowerTimingHistoStream"
#rootFileName7   = "LArBurstflaggedTimingPropagation_run"+run_no+".root"
if m_is80MHz: 
	rootStreamName6 = "FixedParametersHistoStream"
	rootFileName6   = "ParameterFitvalues_run"+run_no+".root" 

#add GRL tool
#ToolSvc += CfgMgr.GoodRunsListSelectionTool("MyGRLTool",GoodRunsListVec=["data15_13TeV.periodAllYear_DetStatus-v63-pro18-01_DQDefects-00-01-02_PHYS_StandardGRL_All_Good.xml"])


# add the actual algorithms to the topSequence
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
if m_is80MHz:
  topSequence += CfgMgr.TimeFitting80MHz("timefitting", OutputLevel=DEBUG, RootStreamName=rootStreamName1, RootDirName=Rootdirname, fixedpars=m_fixedpars, TTpropertyfile = m_TTpropertyfile , sigmacffile = m_sigmacffile)
else:
  topSequence += CfgMgr.TimeFitting("timefitting", OutputLevel=DEBUG, RootStreamName=rootStreamName1, RootDirName=Rootdirname, TTpropertyfile = m_TTpropertyfile , sigmacffile = m_sigmacffile, BCTool = theBCTool)

# output streams for histograms
svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName1, rootFileName1)]
svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName2, rootFileName2)]
svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName3, rootFileName3)]
if m_is80MHz: 
	svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName6, rootFileName6)]
#svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName7, rootFileName7)]
#svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName8, rootFileName8)]

